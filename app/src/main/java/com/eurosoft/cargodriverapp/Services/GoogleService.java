package com.eurosoft.cargodriverapp.Services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.eurosoft.cargodriverapp.CargoApp;
import com.eurosoft.cargodriverapp.GPSTracker;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.SocketEmitResponse;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class GoogleService extends Service implements LocationListener {

    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    double latitude, longitude;
    LocationManager locationManager;
    Location location;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long notify_interval = 2000;
    public static String str_receiver = "servicetutorial.service.receiver";
    Intent intent;
    private Socket mSocket;
    private boolean stop = false;
    int speed;
    private LoginResponse driverObj;
    private String socketId;


    public Socket getmSocket() {
        return mSocket;
    }

    public void setmSocket(Socket mSocket) {
        this.mSocket = mSocket;
    }

    public GoogleService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Stash.init(this);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ,LoginResponse.class);
        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build();

            startForeground(1, notification);
        }

        Stash.init(this);
        mTimer = new Timer();
        mTimer.schedule(new TimerTaskToGetLocation(), 15000, notify_interval);
        intent = new Intent(str_receiver);
        GPSTracker.getInstance().startLocationUpdates(this);
        startServer();
    }


    void startServer() {

        CargoApp app = CargoApp.getInstance(this);
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);

        mSocket.on("ServerMsg", ServerMessageReceived);
        mSocket.on("Ack", onAckRecieved);
//        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.connect();
    }


    private Emitter.Listener onAckRecieved = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
        }
    };


    private Emitter.Listener ServerMessageReceived = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//
            try {
                JSONObject jsonObject = new JSONObject(args[0].toString());
                if (jsonObject.getString("fromname").trim().toLowerCase().contains("android")) {
                    return;
                }
                android.os.Message message = new android.os.Message();
                message.obj = jsonObject.toString();

                if (getNewChatMsg != null) {
                    getNewChatMsg.getMessage(new com.eurosoft.cargodriverapp.Pojo.Message.Builder(com.eurosoft.cargodriverapp.Pojo.Message.TYPE_MESSAGE)
                            .username(jsonObject.getString("fromname")).message(jsonObject.getString("message")).build());
                }

                ///else alert
            } catch (JSONException e) {
                e.printStackTrace();


            }

        }
    };


    GetNewChatMsg getNewChatMsg;

    public interface GetNewChatMsg {
        void getMessage(com.eurosoft.cargodriverapp.Pojo.Message message);

        void getMessageList(ArrayList<com.eurosoft.cargodriverapp.Pojo.Message> messageList);

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        GPSTracker.getInstance().stopLocationUpdates();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getAction().equals(Constants.STARTFOREGROUND_ACTION)) {
            Log.i("TAG", "Received Start Foreground Intent ");
            // your start service code
        } else if (intent.getAction().equals(Constants.STOPFOREGROUND_ACTION)) {
            Log.i("TAG", "Received Stop Foreground Intent");
            //your end servce code
            stopForeground(true);
            stopSelfResult(startId);
            stop = true;
        }
        return START_STICKY;

    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.e("OnConnected", "OnConnected");

        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("onDisconnect", "onDisconnect");

        }
    };



    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {

            Log.e("onConnectError", "onConnectError");

        }
    };

    @Override
    public void onLocationChanged(Location location) {
//        fn_getlocation();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    private void fn_getlocation() {

        LoginResponse driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);


        Stash.put(Constants.SOCKET_ID,mSocket.id());

        SocketEmitResponse socketEmitResponse = new SocketEmitResponse(1, driverObj.getId(), 1, 1, Constants.lat, Constants.lng, null, Constants.speed, null, null, null, null, null, null,   mSocket.id());
        Gson gson = new Gson();
        try {



            JSONObject jsonObject = new JSONObject(gson.toJson(socketEmitResponse));

            if (mSocket != null && mSocket.connected()) {
                mSocket.emit("SendDriverLocation", jsonObject, new Ack() {
                    @Override
                    public void call(Object... args) {

                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {


            if (stop) {
                mTimer.cancel();
                mTimer = null;
                return;
            }

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    GPSTracker.getInstance().startLocationUpdates(GoogleService.this);
                    fn_getlocation();
                }
            });

        }
    }

    private void fn_update(Location location) {
        intent.putExtra("latutide", location.getLatitude() + "");
        intent.putExtra("longitude", location.getLongitude() + "");
        sendBroadcast(intent);
    }

}