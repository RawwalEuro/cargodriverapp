package com.eurosoft.cargodriverapp.Services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.eurosoft.cargodriverapp.ActivityChatDialog;
import com.eurosoft.cargodriverapp.ActivityLogin;
import com.eurosoft.cargodriverapp.CargoApp;
import com.eurosoft.cargodriverapp.GPSTracker;
import com.eurosoft.cargodriverapp.JobDetailsActivity;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.Message;
import com.eurosoft.cargodriverapp.Pojo.RequestLogin;
import com.eurosoft.cargodriverapp.Pojo.RequestLoginBody;
import com.eurosoft.cargodriverapp.Pojo.SocketEmitResponse;
import com.eurosoft.cargodriverapp.R;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.Db.DatabaseClient;
import com.eurosoft.cargodriverapp.Utils.ServiceTools;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
// main service used for sending/recieving socket operations and location
public class SignalRService extends Service implements LocationListener {
    private LoginResponse driverObj;


    private Timer mTimer = null;
    long notify_interval = 2000;
    public static String str_receiver = "servicetutorial.service.receiver";
    Intent intent;
    Intent finalIntent;
    private boolean stop = false;
    private MasterPojo masterPojo;
    private Boolean isFirstTime = false;
    private List<Message> arrayList;
    private int status = 1;


    @Override
    public void onCreate() {

        Stash.init(this);
        isFirstTime = true;
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);

        // need to start foreground server for pervious versions as well
        if (Build.VERSION.SDK_INT >= 26) {
            String CHANNEL_ID = "my_channel_01";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);

            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setSmallIcon(R.drawable.app_icon)
                    .setContentText("").build();
            startForeground(1, notification);
        }

        Stash.init(this);
        mTimer = new Timer();
        mTimer.schedule(new TimerTaskToGetLocation(), 15000, notify_interval);
        intent = new Intent(str_receiver);
        GPSTracker.getInstance().startLocationUpdates(this);
        startServer();
    }


    public SignalRService() {

    }


    public Socket mSocket;

    void startServer() {
        CargoApp app = CargoApp.getInstance(this);

        if (mSocket == null) {
            mSocket = app.getSocket();
            mSocket.on(Socket.EVENT_CONNECT, onConnect);
            mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
            mSocket.on("ReceiveMessage", onLoginRecieved);
            mSocket.on("ServerMsg", ServerMessageReceived);
            mSocket.on("Ack", onAckRecieved);
            mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
//        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
            mSocket.connect();
        }
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {


            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {

                    if (isFirstTime == true) {

                        Toast toast = null;
                        toast = Toast.makeText(SignalRService.this, masterPojo.getServerConnected(), Toast.LENGTH_SHORT);
                        toast.show();
                        isFirstTime = false;
                    }
                }
            });

//            if (getNewChatMsg != null) {
//                getNewChatMsg.getMessage(new Message.Builder(Message.TYPE_MESSAGE)
//                        .username("Connected").message("Server Connected").build());
//            }
            fn_getlocation();
            attemptSend();
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
//
            Log.e("Socket-onDisconnect", "Socket-onDisconnect");

            if (getNewChatMsg != null) {
//                getNewChatMsg.getMessage(new Message.Builder(Message.TYPE_MESSAGE)
//                        .username("Error").message("User Disconnected").build());
            }
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
//                    Toast toast = null;
//                    toast = Toast.makeText(SignalRService.this, R.string.disconnect, Toast.LENGTH_SHORT);
//                    toast.show();
                    // isFirstTime = false;
                }
            });
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Socket-onConnectError", args.toString());

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Toast toast = null;
                    toast = Toast.makeText(SignalRService.this, R.string.error_connect, Toast.LENGTH_SHORT);
                    toast.show();

                }
            });
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);

    }

private int disconnectCounter = 0;
    private void fn_getlocation() {

        LoginResponse driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        try {
            if (Stash.getBoolean(Constants.isLoggenIn) == false) {
                return;
            }

            if (mSocket == null || mSocket.id() == null) {

                if(disconnectCounter>=2){
                    disconnectCounter=0;
                    mSocket=null;
                    startServer();
                }else{
                    disconnectCounter++;
                }
                return;
            }
            Stash.put(Constants.SOCKET_ID, mSocket.id());

            if (Stash.getInt(Constants.CURRENT_STATUS) == 0) {
                status = 1;
            } else {
                status = Stash.getInt(Constants.CURRENT_STATUS);
            }


            SocketEmitResponse socketEmitResponse = new SocketEmitResponse(1, driverObj.getId(), 1, status, Constants.lat, Constants.lng, null, Constants.speed, null, null, null, null, null, null, mSocket.id());
            Gson gson = new Gson();

            JSONObject jsonObject = new JSONObject(gson.toJson(socketEmitResponse));

            // Log.e("JSONObject",jsonObject + "");

            if (mSocket != null && mSocket.connected()) {
                mSocket.emit("SendDriverLocation", jsonObject, new Ack() {
                    @Override
                    public void call(Object... args) {

                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {

    }

    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {


            if (stop) {
                mTimer.cancel();
                mTimer = null;
                return;
            }

            handler__.post(new Runnable() {
                @Override
                public void run() {
                    GPSTracker.getInstance().startLocationUpdates(SignalRService.this);
                    fn_getlocation();
                }
            });

        }
    }


    private Emitter.Listener sendingError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("TAG", "Error msg" + args.toString());

        }
    };
    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public SignalRService getService() {
            // Return this instance of SignalRService so clients can call public methods
            return SignalRService.this;
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        finalIntent = intent;
        try {
            if (intent.getAction().equals(Constants.STARTFOREGROUND_ACTION)) {
                Log.i("TAG", "Received Start Foreground Intent ");
                // your start service code
            } else if (intent.getAction().equals(Constants.STOPFOREGROUND_ACTION)) {
                Log.i("TAG", "Received Stop Foreground Intent");
                //your end servce code
                stopForeground(true);
                stopSelfResult(startId);
                stop = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Stash.init(this);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        //  registerReceiver(broadcastReceiver, new IntentFilter("sendServer"));
        if (mSocket == null) {
            startServer();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startMyOwnForeground();
        } else {
            startForeground(1340, buildForegroundNotification());
        }
        handler.removeCallbacks(m_runnable);
        handler.postDelayed(m_runnable, 4000);
        return START_STICKY;
    }


    Runnable m_runnable = new Runnable() {
        @Override
        public void run() {
        }
    };
    Handler handler = new Handler() {
        @Override
        public void handleMessage(@NonNull android.os.Message msg) {
            super.handleMessage(msg);
            Toast.makeText(SignalRService.this, masterPojo.getServerNotConnected(), Toast.LENGTH_SHORT).show();
        }
    };

    private void attemptSend() {
        String message = "Lat=49.021355=L=69.051465=d=" + driverObj.getId() + "=s=50=j=0=ack";

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("message", message);
            jsonObject.put("user", "Android");
            jsonObject.put("ack", "true");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("Socket Id", "Socket Id" + mSocket.id());

        mSocket.emit("LatLong", jsonObject, new Ack() {
                    @Override
                    public void call(Object... args) {
                        Log.e("TAG", args.toString());
                    }
                }

        );

    }

    @RequiresApi(Build.VERSION_CODES.O)
    private void startMyOwnForeground() {
        final Intent intent = getPackageManager()
                .getLaunchIntentForPackage(getPackageName());
        PendingIntent pIntent = PendingIntent.getActivity(SignalRService.this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        String NOTIFICATION_CHANNEL_ID = getPackageName();
        String channelName = getResources().getString(R.string.app_name);
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setPriority(NotificationManager.IMPORTANCE_MAX)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentIntent(pIntent)
                .build();
        startForeground(2, notification);
    }

    private Notification buildForegroundNotification() {
        NotificationCompat.Builder b = new NotificationCompat.Builder(this);
        final Intent intent = getPackageManager()
                .getLaunchIntentForPackage(getPackageName());
        PendingIntent pIntent = PendingIntent.getActivity(SignalRService.this, 0, intent, PendingIntent.FLAG_IMMUTABLE);
        b.setOngoing(true)
                .setContentTitle("Treasure Mobile")
                .setContentText("Runnning")
                .setContentIntent(pIntent)
                .setSmallIcon(R.mipmap.ic_launcher);

        return (b.build());
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("sendServer")) {
                sendUserMsg(intent.getStringExtra("sendMsg"));
            }
        }
    };

    private void sendUserMsg(String message) {
        try {

            mSocket.emit("ServerMsgsend", new JSONObject(message), new Ack() {
                        @Override
                        public void call(Object... args) {
                            Log.e("TAG", args.toString());
                        }
                    }


            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Emitter.Listener onLoginRecieved = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
        }
    };
    private final Emitter.Listener ServerMessageReceived = new Emitter.Listener() {
        @Override
        public void call(Object... args) {


            try {
                JSONObject jsonObject = new JSONObject(args[0].toString());


                if (jsonObject.getString("Purpose").equalsIgnoreCase("ForceLogout")) {

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Log.e("ServerMessageReceived", jsonObject.toString());
                            callApiLogout(driverObj.getId(), driverObj.getDeviceId());
                            return;
                        }
                    });

                    return;
                }


                if (jsonObject.getString("Purpose").equalsIgnoreCase("ChangeVehicle")) {

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast toast = null;

                            Log.e("ServerMessageReceived", jsonObject.toString());

                            try {
                                toast = Toast.makeText(SignalRService.this, jsonObject.getString("message"), Toast.LENGTH_SHORT);

                                if (jsonObject.getString("message").equalsIgnoreCase("Vehicle Changed Successfully")) {

                                    String vehicleId = jsonObject.getString("VehicleId");
                                    String vehicleNumber = jsonObject.getString("VehicleNumber");

                                    if (vehicleId == null || vehicleNumber == null) {
                                        Log.e("vehicleIdorNumber", "Null");
                                        return;
                                    }

                                    updateHome(vehicleId, vehicleNumber);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            toast.show();

                        }
                    });

                    return;
                }


                if (jsonObject.getString("fromname").trim().toLowerCase().contains("android")) {
                    return;
                }
                Log.e("ServerMessageReceived", jsonObject.toString());
                if (jsonObject.getString("Purpose").trim().toLowerCase().contains("job") || jsonObject.getString("Purpose").equalsIgnoreCase("Plan")) {


                    if (jsonObject.getString("Purpose").trim().toLowerCase().contains("job")) {

                        if (Stash.getBoolean(Constants.JOB_IS_RUNNING)) {

                            return;
                        }
                    }

                    if (jsonObject.getString("Purpose").equalsIgnoreCase("Plan")) {
                        finalIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finalIntent.setComponent(new ComponentName(getBaseContext(), JobDetailsActivity.class.getName()));
                        finalIntent.putExtra(Constants.JOB_TYPE, 2);
                        Stash.put(Constants.PLANNED_ID, jsonObject.getString("PlannedId"));
                        startActivity(finalIntent);
                        return;
                    } else if (jsonObject.getString("JobStatusId").equalsIgnoreCase("4")) {
                        finalIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finalIntent.setComponent(new ComponentName(getBaseContext(), JobDetailsActivity.class.getName()));
                        finalIntent.putExtra(Constants.JOB_TYPE, 0);
                        startActivity(finalIntent);
                        Stash.put(Constants.JOB_ID, jsonObject.getString("JobId"));
                        return;
                    } else if (jsonObject.getString("JobStatusId").equalsIgnoreCase("15")) {
                        finalIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finalIntent.setComponent(new ComponentName(getBaseContext(), JobDetailsActivity.class.getName()));
                        finalIntent.putExtra(Constants.JOB_TYPE, 1);
                        startActivity(finalIntent);
                        Stash.put(Constants.JOB_ID_FOLLOW_ON, jsonObject.getString("JobId"));
                        return;
                    } else {
                        finalIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finalIntent.setComponent(new ComponentName(getBaseContext(), JobDetailsActivity.class.getName()));
                        finalIntent.putExtra(Constants.JOB_TYPE, 0);
                        startActivity(finalIntent);
                        Stash.put(Constants.JOB_ID, jsonObject.getString("JobId"));
                        return;
                    }
                }
                android.os.Message message = new android.os.Message();
                message.obj = jsonObject.toString();
                handler__.sendMessage(message);

                if (messages == null) {
                    messages = new ArrayList<>();
                }
                //if activity fore


                if (getNewChatMsg != null) {
                    if (Stash.getBoolean(Constants.CHATACTIVITY_IS_RUNNING) == false) {
                        Intent it = new Intent("intent.my.action");
                        it.putExtra("IncomingMessageFromServer", jsonObject.getString("message"));
                        it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        it.setComponent(new ComponentName(getBaseContext(), ActivityChatDialog.class.getName()));
                        startActivity(it);

                    }
                    getNewChatMsg.getMessage(new Message.Builder(Message.TYPE_MESSAGE)
                            .username(jsonObject.getString("fromname")).message(jsonObject.getString("message")).build());
                    /*addMessageToList(new Message.Builder(Message.TYPE_MESSAGE)
                            .username(jsonObject.getString("fromname")).message(jsonObject.getString("message")).build());*/
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                    r.play();
                    //TODO
                    return;
                } else {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast toast = null;
                            try {

                                new Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast toast = null;
                                        try {

                                            if (Stash.getBoolean(Constants.CHATACTIVITY_IS_RUNNING) == false) {
                                                Intent it = new Intent("intent.my.action");
                                                it.putExtra("IncomingMessageFromServer", jsonObject.getString("message"));
                                                it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                it.setComponent(new ComponentName(getBaseContext(), ActivityChatDialog.class.getName()));
                                                startActivity(it);


                                                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                                                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                                                r.play();

                                              /*  addMessageToList(new Message.Builder(Message.TYPE_MESSAGE)
                                                        .username(jsonObject.getString("fromname")).message(jsonObject.getString("message")).build());*/

                                            }

                                            toast = Toast.makeText(SignalRService.this, jsonObject.getString("message").trim(), Toast.LENGTH_SHORT);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            Log.e("Socket Error", e.getMessage());
                                        }
                                        // toast.show();
                                    }
                                });


                                toast = Toast.makeText(SignalRService.this, jsonObject.getString("message").trim(), Toast.LENGTH_SHORT);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            //toast.show();
                        }
                    });
                }

                ///else alert
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void updateHome(String vehicleId, String vehicleNumber) {
        intent.putExtra("vehicleId", vehicleId);
        intent.putExtra("vehicleNumber", vehicleNumber);
        sendBroadcast(intent);
    }


    private void addMessageToList(Message message) {
        class AddMessage extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .chatDao()
                        .insert(message);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                Log.e("isSucess", "Added");
                //getChatList();
                //progressVisiblityGone();
                getChatList();
            }
        }

        AddMessage st = new AddMessage();
        st.execute();
    }


    private ArrayList<Message> getChatList() {
        class GetChatList extends AsyncTask<Void, Void, List<Message>> {

            @Override
            protected List<Message> doInBackground(Void... voids) {
                arrayList = new ArrayList();

                arrayList = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .chatDao()
                        .getAll();
                return arrayList;
            }

            @Override
            protected void onPostExecute(List<Message> messages) {
                super.onPostExecute(messages);


                if (messages == null || messages.size() == 0) {
                    Log.e("Being Return", "Being Return");
                    return;
                } else {
                    for (int i = 0; i < messages.size(); i++) {
                        Log.e("Message", messages.get(i).getmMessage());
                    }

                }
            }
        }

        GetChatList gt = new GetChatList();
        gt.execute();

        Log.e("mMessages", messages.size() + "");
        return messages;
    }

    public ArrayList<Message> getMessageList() {
        return messages;
    }

    public void addToMessageList(Message message) {
        messages.add(message);
    }

    ArrayList<Message> messages = new ArrayList<>();
    Handler handler__ = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull android.os.Message msg) {
            super.handleMessage(msg);

        }
    };

    private Emitter.Listener onAckRecieved = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e("Tag", "onAckRecieved ++" + args.toString());
        }
    };

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public void setOnGetNewChatMsg(GetNewChatMsg getNewChatMsg) {
        this.getNewChatMsg = getNewChatMsg;
    }

    GetNewChatMsg getNewChatMsg;

    public interface GetNewChatMsg {
        void getMessage(Message message);

        void getMessageList(ArrayList<Message> messageList);
    }


    private void callApiLogout(int id, String deviceId) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                Log.e("id", id + "");
                Log.e("deviceId", deviceId);

                RequestLoginBody requestLoginBody = new RequestLoginBody(deviceId, id);


                RequestLogin requestLogin = new RequestLogin();
                requestLogin.setRequest(requestLoginBody);

                ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
                Call<WebResponse<LoginResponse>> call = apiService.logout(requestLogin);


                call.enqueue(new Callback<WebResponse<LoginResponse>>() {
                    @Override
                    public void onResponse(Call<WebResponse<LoginResponse>> call, Response<WebResponse<LoginResponse>> response) {

                        if (response.code() != 200 || response.body() == null) {
                            return;
                        }
                        if (response.body().getSuccess()) {
                            return;
                        }

                        if (!response.body().getSuccess() && response.code() == 200) {

                            try {

                                Boolean isServiceRunning = ServiceTools.isServiceRunning(
                                        getApplicationContext(),
                                        SignalRService.class);
                                Log.e("isServiceRunning", isServiceRunning + "");
                                if (isServiceRunning) {
                                    Intent stopIntent = new Intent(getApplicationContext(), SignalRService.class);
                                    stopIntent.setAction(Constants.STOPFOREGROUND_ACTION);
                                    getApplicationContext().startService(stopIntent);
                                } else {
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Stash.clear(Constants.isLoggenIn);
                            Stash.clear(Constants.JOB_IS_RUNNING);
                            Intent i = new Intent(getApplicationContext(), ActivityLogin.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);
                        }
                    }

                    @Override
                    public void onFailure(Call<WebResponse<LoginResponse>> call, Throwable t) {

                    }
                });

            }
        });
    }
}