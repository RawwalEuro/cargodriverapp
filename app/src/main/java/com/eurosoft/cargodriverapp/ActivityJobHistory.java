package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.JobHistoryAdapter;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityJobHistory extends AppCompatActivity {

    Spinner spinner;
    private LoginResponse driverObj;
    LinearLayout Date_View;
    MasterPojo masterPojo;
    String[] FilterList;
    RecyclerView JobHistoryView;
    com.mikhaellopez.circularprogressbar.CircularProgressBar progressBar;
    JobHistoryAdapter jobHistoryAdapter;
    ArrayList<JobDetailData> jobDetailDataArrayList=new ArrayList<JobDetailData>();
    Toolbar toolbar;
    TextView FromDateView,TillDateView;
    LinearLayout FromDateLL,TillDateLL;
    Dialog datePickerDialogfromdate,datePickerDialogtilldate;
   // DatePickerDialog datePickerDialogfromdate,datePickerDialogtilldate;
   DatePicker datePickertilldate;
    Boolean FromDate=false,IsFirstTimeLaunch=false;
    Calendar calendarfromdate,calendertilldate;
    String startdate,enddate;
    TextView DateTitle;
    private int COMPLETEDJOB=7;
    TextView Title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_history);
        initView();
    }
    public void initView(){
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO,MasterPojo.class);
        spinner=(Spinner) findViewById(R.id.spinner);
        Date_View = (LinearLayout) findViewById(R.id.dateview);
        JobHistoryView = (RecyclerView) findViewById(R.id.recyleview);
        progressBar=(com.mikhaellopez.circularprogressbar.CircularProgressBar) findViewById(R.id.progressBar);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        FromDateView=(TextView) findViewById(R.id.fromdate);
        TillDateView=(TextView) findViewById(R.id.tilldate);
        FromDateView.setText(getCurrentDate("dd MMMM"));
        TillDateView.setText(getCurrentDate("dd MMMM"));
        FromDateLL=(LinearLayout) findViewById(R.id.fromdatelayout);
        TillDateLL=(LinearLayout) findViewById(R.id.tilldatelayout);
         DateTitle=(TextView)findViewById(R.id.date);
        Title=(TextView) findViewById(R.id.title);
        Title.setText(masterPojo.getJobHistory());
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        progressbarVisible();
        setuptoolbar();
        setupSpinner();
        setupDateView();
        initDatePickerFromDateDialog();
        initDatePickerTillDateDialog();

    }
    public void setupSpinner(){
        FilterList = new String[]{masterPojo.getToday(),masterPojo.getYesterday(), masterPojo.getDateRange()};
        spinner.getBackground().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.simple_spinner_item, FilterList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setDropDownWidth(300);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        dailywork();
                        break;
                    case 1:
                        yestersdaywork();
                        break;
                    case 2:
                        daterangework();
                        break;

                }

            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void dailywork(){
        IsFirstTimeLaunch=false;
        FromDate=false;
        Date_View.setVisibility(View.GONE);
        DateTitle.setText(""+FilterList[0]+" - "+getCurrentDate("dd/MMM")+"");
        findViewById(R.id.date).setVisibility(View.VISIBLE);
        setupApiWork(driverObj.getId(),COMPLETEDJOB,getCurrentDate("yyyy-MM-dd"),getCurrentDate("yyyy-MM-dd"),FilterList[0]);
    }
    public void yestersdaywork(){
        IsFirstTimeLaunch=false;
        FromDate=false;
        Date_View.setVisibility(View.GONE);
        DateTitle.setText(""+FilterList[1]+" - "+getYestersdayDate("dd/MMM")+"");
        findViewById(R.id.date).setVisibility(View.VISIBLE);
        progressbarVisible();
        setupApiWork(driverObj.getId(),COMPLETEDJOB,getYestersdayDate("yyyy-MM-dd"),getYestersdayDate("yyyy-MM-dd"),FilterList[1]);


    }
    public void daterangework(){
        IsFirstTimeLaunch=true;
        FromDate=true;
        datePickerDialogfromdate.show();
        datePickerDialogfromdate.setCancelable(false);
        findViewById(R.id.date).setVisibility(View.GONE);
        Date_View.setVisibility(View.VISIBLE);

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    private void callApiGetJobHistoryData(int Driverid , int status, String fromDate1, String tillDate, String DateType) {
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseList<JobDetailData>> call = apiService.getjobhistorylist(String.valueOf(Driverid),status,fromDate1,tillDate);
        call.enqueue(new Callback<WebResponseList<JobDetailData>>() {
            @Override
            public void onResponse(Call<WebResponseList<JobDetailData>> call, Response<WebResponseList<JobDetailData>> response) {


                try {
                    if (response.body().getData() == null || response.body().getData().isEmpty()) {
                        progressbargone();
                        Toast.makeText(getApplicationContext(), masterPojo.getNoRecordFound(), Toast.LENGTH_LONG).show();
                        filterlist("null");
                        return;
                    }
                    if (response.code() == 200 && response.body().getData() != null) {
                        progressbargone();
                        jobDetailDataArrayList = response.body().getData();
                        Log.e("Date123","fromDate "+fromDate1);
                        Log.e("Date123","tillDate "+tillDate);
                        Log.e("Date123","size "+jobDetailDataArrayList.size());
                        filterlist(DateType);
                        setupRecyclerView(jobDetailDataArrayList);
                    }
                } catch (Exception e) {
                    progressbargone();
                    Toast.makeText(getApplicationContext(), "" + e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<JobDetailData>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong(), Toast.LENGTH_LONG).show();
                progressbargone();

            }
        });
    }

    public void setupRecyclerView(List<JobDetailData> jobHistory) {
        JobHistoryView.setVisibility(View.VISIBLE);
        jobHistoryAdapter = new JobHistoryAdapter(ActivityJobHistory.this, jobHistory,masterPojo);
        JobHistoryView.setLayoutManager(new LinearLayoutManager(ActivityJobHistory.this));
        JobHistoryView.setAdapter(jobHistoryAdapter);
    }

    private void filterlist(String text) {
        ArrayList<JobDetailData> filteredList = new ArrayList<JobDetailData>();
        if (jobDetailDataArrayList != null) {
            for (JobDetailData item : jobDetailDataArrayList) {
                if (item.getDate().toLowerCase().startsWith(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }
        }
        if (jobHistoryAdapter != null) {
            jobHistoryAdapter.filterList(filteredList);
        }
    }
    public void setupApiWork(int Driverid ,int status, String fromDate1, String tillDate,String DateType){
        if (isNetworkConnected()) {
            callApiGetJobHistoryData(Driverid,status,fromDate1,tillDate,DateType);
        } else {
            progressbargone();
            Toast.makeText(ActivityJobHistory.this, masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_LONG).show();
        }
    }
    private void setuptoolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_24);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setVisibility(View.VISIBLE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
    public String getCurrentDate(String dateformat) {
        Date Date = new Date();
        String TodayDate = String.valueOf(DateFormat.format(dateformat, Date.getTime()));
        return TodayDate;
    }
    private String getYestersdayDate(String dateformat){
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String  YestersDayDate = String.valueOf(DateFormat.format(dateformat, cal.getTime()));
        return YestersDayDate;
    }
    private void setupDateView(){
        getCalendarInstance();
        FromDateLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FromDate=true;
                IsFirstTimeLaunch=false;
                datePickerDialogfromdate.show();
                datePickerDialogfromdate.setCancelable(false);

            }
        });

        TillDateLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FromDate=false;
                IsFirstTimeLaunch=false;
                datePickerDialogtilldate.show();
                datePickerDialogtilldate.setCancelable(false);

            }
        });


    }
    private void getCalendarInstance() {
        calendarfromdate = Calendar.getInstance(TimeZone.getDefault());
          int month=calendarfromdate.get(Calendar.MONTH)+1;
        calendertilldate = Calendar.getInstance(TimeZone.getDefault());
        startdate = "" + calendarfromdate.get(Calendar.YEAR)+ "-" + month + "-" + calendarfromdate.get(Calendar.DAY_OF_MONTH);
        enddate = "" + calendarfromdate.get(Calendar.YEAR) + "-" + month + "-" + calendarfromdate.get(Calendar.DAY_OF_MONTH);
        //   datePickerDialogfromdate = new DatePickerDialog(ActivityJobHistory.this, R.style.MyDatePickerStyle, datePickerListener, calendarfromdate.get(Calendar.YEAR), calendarfromdate.get(Calendar.MONTH), calendarfromdate.get(Calendar.DAY_OF_MONTH));
        //   datePickerDialogfromdate.getDatePicker().setMaxDate(new Date().getTime());

    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        // when dialog box is closed, below method will be called.
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            if(FromDate){
                setFromDate(selectedDay,selectedMonth,selectedYear);
            }
            else {
                setTillDate(selectedDay,selectedMonth,selectedYear);
            }

        }
    };
    public String getMonth(int month) {
        return new DateFormatSymbols().getMonths()[month - 1];
    }
    public void updateDatePickerTillDate(Long mindate){
//        datePickerDialogtilldate = new DatePickerDialog(ActivityJobHistory.this, R.style.MyDatePickerStyle, datePickerListener, calendertilldate.get(Calendar.YEAR), calendertilldate.get(Calendar.MONTH), calendertilldate.get(Calendar.DAY_OF_MONTH));
       // datePickertilldate = datePickerDialogtilldate.findViewById(R.id.simpleDatePicker);
        datePickertilldate.setMaxDate(new Date().getTime());
        datePickertilldate.setMinDate(mindate);

    }

    private void progressbarVisible(){
        progressBar.setVisibility(View.VISIBLE);
        progressBar.setIndeterminateMode(true);
    }

    private void progressbargone(){
        progressBar.setVisibility(View.GONE);
        progressBar.setIndeterminateMode(false);
    }


    private void setFromDate(int selectedDay,int selectedMonth, int selectedYear){
        calendarfromdate.set(selectedYear, selectedMonth, selectedDay);
        String DateFrom = "" + selectedDay + " " + getMonth(selectedMonth+1);
        FromDateView.setText(DateFrom);
       // UpdateDatePickerTillDate(calendarfromdate.getTimeInMillis());
        if(IsFirstTimeLaunch){
            IsFirstTimeLaunch=false;
            FromDate=false;
            datePickerDialogtilldate.show();
        }
        selectedMonth=selectedMonth+1;
        startdate = "" + selectedYear + "-" +selectedMonth + "-" + selectedDay;
        Log.e("dATE1","startdate"+startdate);
        Log.e("dATE1","enddate"+enddate);
        setupApiWork(driverObj.getId(),COMPLETEDJOB,startdate,enddate,FilterList[2]);
      //  DateChangedAutomatically(startdate,enddate,calendarfromdate);
        datePickerDialogfromdate.dismiss();
    }
    private void setTillDate(int selectedDay,int selectedMonth, int selectedYear){
        calendertilldate.set(selectedYear, selectedMonth, selectedDay);
        String  DateTill= "" + selectedDay + " " + getMonth(selectedMonth+1);
        TillDateView.setText(DateTill);
        selectedMonth=selectedMonth+1;
        enddate = "" + selectedYear + "-" +selectedMonth + "-" + selectedDay;
        Log.e("dATE1","startdate"+startdate);
        Log.e("dATE1","enddate"+enddate);
        setupApiWork(driverObj.getId(),COMPLETEDJOB,startdate,enddate,FilterList[2]);
        datePickerDialogtilldate.dismiss();


    }
    public void initDatePickerFromDateDialog() {
        datePickerDialogfromdate = new Dialog(ActivityJobHistory.this);
        datePickerDialogfromdate.setContentView(R.layout.datepicker_fromdate_dialog);
       DatePicker  datePicker = datePickerDialogfromdate.findViewById(R.id.simpleDatePicker);
        datePicker.setMaxDate(new Date().getTime());
        TextView textView1 = datePickerDialogfromdate.findViewById(R.id.today);
        textView1.setText(masterPojo.getFromDate());
        TextView Ok = datePickerDialogfromdate.findViewById(R.id.ok);
        TextView Cancel = datePickerDialogfromdate.findViewById(R.id.cancel);
        Ok.setText(masterPojo.getOk());
        Cancel.setText(masterPojo.getCancel());
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogfromdate.dismiss();
            }
        });

        Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFromDate(datePicker.getDayOfMonth(),datePicker.getMonth(),datePicker.getYear());
                datePickerDialogfromdate.dismiss();

            }
        });

    }

    public void initDatePickerTillDateDialog() {
        datePickerDialogtilldate = new Dialog(ActivityJobHistory.this);
        datePickerDialogtilldate.setContentView(R.layout.datepicker_fromdate_dialog);
        datePickertilldate = datePickerDialogtilldate.findViewById(R.id.simpleDatePicker);
      //  datePickerDialogtilldate = new DatePickerDialog(ActivityJobHistory.this, R.style.MyDatePickerStyle, datePickerListener, calendertilldate.get(Calendar.YEAR), calendertilldate.get(Calendar.MONTH), calendertilldate.get(Calendar.DAY_OF_MONTH));
     //   datePickerDialogtilldate.getDatePicker().setMaxDate(new Date().getTime());
        updateDatePickerTillDate(0l);
        TextView textView1 = datePickerDialogtilldate.findViewById(R.id.today);
        textView1.setText(masterPojo.getTillDate());
        TextView Ok = datePickerDialogtilldate.findViewById(R.id.ok);
        TextView Cancel = datePickerDialogtilldate.findViewById(R.id.cancel);
        Ok.setText(masterPojo.getOk());
        Cancel.setText(masterPojo.getCancel());
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialogtilldate.dismiss();
            }
        });

        Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressbarVisible();
                setTillDate(datePickertilldate.getDayOfMonth(),datePickertilldate.getMonth(),datePickertilldate.getYear());

            }
        });

    }
    public void dateChangedAutomatically(String fromdate, String tilldate, Calendar c) {
        Date date1 = null;
        Date date2 = null;

        try {
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(fromdate);
            date2 = new SimpleDateFormat("yyyy-MM-dd").parse(tilldate);
            if (date1.after(date2)) {
                calendertilldate = Calendar.getInstance(TimeZone.getDefault());
                TillDateView.setText(getCurrentDate("d MMMM"));
                Toast.makeText(ActivityJobHistory.this, "Incorrect Date", Toast.LENGTH_LONG).show();
                return;
            }



        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}