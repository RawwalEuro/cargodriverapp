package com.eurosoft.cargodriverapp.Fragment;

import static com.eurosoft.cargodriverapp.Adapter.AdapterImagePicker.convertBase64ToBitmap;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.ActivityJobStatus;
import com.eurosoft.cargodriverapp.ActivityLogin;
import com.eurosoft.cargodriverapp.ActivtityPlanJobStatus;
import com.eurosoft.cargodriverapp.Adapter.AdapterImagePicker;
import com.eurosoft.cargodriverapp.Pojo.ImageSheetModel;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.sumup.merchant.ui.Activities.LoginActivity;

import java.util.ArrayList;

public class BottomSheetImageLogin extends BottomSheetDialogFragment {


    private ImageView uploadImage;
    private EditText inp_comment;
    private TextView done;
    private Button erase;
    TextView textView, textView1;
    CustomView CusView;
    private Bitmap DrawBitmap;
    private Canvas mCanvas;
    private Paint DrawBitmapPaint;
    Path mPath;
    Bitmap bitmap;
    private Paint mPaint;
    LinearLayout Rl;
    TextView txt1;
    int mDualPane;
    int Width = 5;
    int gone = 0, remove = 0;
    MasterPojo masterPojo;

//    public static BottomSheetImageList newInstance() {
//        return new BottomSheetImageList();
//    }
ImageSheetModel imageSheetModel;
     public BottomSheetImageLogin(ImageSheetModel imageSheetModel)
     {
         this.imageSheetModel = imageSheetModel;
     }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bottom_sheet_images_login, container, false);

        InitView(rootView);

        return rootView;
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet_images_login, null);
        dialog.setContentView(contentView);
        dialog.setCancelable(false);
        CoordinatorLayout.LayoutParams layoutParams = ((CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams());
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(bottomSheetCallback);
            ((BottomSheetBehavior) behavior).setPeekHeight(getResources().getDimensionPixelSize(R.dimen.bottom_sheet_height));
            ((BottomSheetBehavior) behavior).setDraggable(false);
        }

    }

    private BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    private void InitView(View rootView) {
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        uploadImage = (ImageView) rootView.findViewById(R.id.uploadImage);
        inp_comment = (EditText) rootView.findViewById(R.id.inp_details);
        done = (TextView) rootView.findViewById(R.id.done);
        erase = (Button) rootView.findViewById(R.id.eraser);
        String base64 = imageSheetModel.getImageList().get(0);
        ((ImageView) rootView.findViewById(R.id.close)).setImageBitmap(convertBase64ToBitmap(base64));
//        RefreshImageList(imageSheetModel.getImageList());

//        InitSignature(rootView);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
//                    if (AppStatus.getInstance(getActivity()).isOnline()) {
//                        if (!bitmap.sameAs(DrawBitmap)) {
//                            convertBitmapToByteArray();
//
//                        } else {
//                            Toast.makeText(getActivity(), masterPojo.getPleaseDrawYourSignature(), Toast.LENGTH_SHORT).show();
//
//                        }
//                    } else {
//                        Toast.makeText(getActivity(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
//
//                    }
                    if(imageSheetModel.getContext() instanceof ActivityLogin) {
                        ((ActivityLogin) imageSheetModel.getContext()).submitChecklistData(imageSheetModel.getImageList().get(0),inp_comment.getText().toString());
                    }
                } catch (Exception e) {
                    Log.e("E", e.getMessage());
                }
            }
        });

        erase.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                Refresh(rootView);
                dismiss();
            }
        });
        uploadImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               if(imageSheetModel.getContext() instanceof ActivityJobStatus){
                   ((ActivityJobStatus)imageSheetModel.getContext()).gotoGallery();
               }else{
                   ((ActivtityPlanJobStatus)imageSheetModel.getContext()).gotoGallery();
               }
            }
        });

    }






    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    AdapterImagePicker  adapterImagePicker;
    public void RefreshImageList(ArrayList<String> imagesList) {
        imageSheetModel.setImageList(imagesList);
          adapterImagePicker = new AdapterImagePicker(getActivity(), imagesList, new AdapterImagePicker.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                imageSheetModel.getImageList().remove(position);
                adapterImagePicker.notifyDataSetChanged();
            }
        });
//        imageRecycler.setHasFixedSize(true);
//        imageRecycler.setAdapter(adapterImagePicker);
    }
public  ArrayList<String> getImageList(){
        return imageSheetModel.getImageList();
}

    public class CustomView extends View {
        public int reset = 0;

        @SuppressWarnings("deprecation")
        public CustomView(Context c) {
            super(c);
            Display Disp = getActivity().getWindowManager().getDefaultDisplay();
            Log.e("sdkdkdk","Disp.getHeight()"+Disp.getHeight());
            Log.e("sdkdkdk","Disp.getWidth()"+Disp.getWidth());
            DrawBitmap = Bitmap.createBitmap(Disp.getWidth(), Disp.getWidth(), Bitmap.Config.ARGB_4444);
            bitmap = Bitmap.createBitmap(Disp.getWidth(), Disp.getWidth(), Bitmap.Config.ARGB_4444);
            mCanvas = new Canvas(DrawBitmap);
            mPath = new Path();
            DrawBitmapPaint = new Paint(Paint.DITHER_FLAG);
            if (reset == 0) {
                reset = 4;
            }
        }

        @Override
        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
        }

        @Override
        public void onDraw(Canvas canvas) {
            setDrawingCacheEnabled(true);
            canvas.drawBitmap(DrawBitmap, 0, 0, DrawBitmapPaint);
            canvas.drawPath(mPath, mPaint);
            canvas.drawRect(mY, 0, mY, 0, DrawBitmapPaint);
        }

        public float mX, mY;
        public static final float TOUCH_TOLERANCE = 4;

        public void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        public void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;

            }
        }

        public void touch_up() {
            mPath.lineTo(mX, mY);
            mCanvas.drawPath(mPath, mPaint);
            mPath.reset();
        }

        public void clear(int re) {
            if (re == 1) {
                DrawBitmap = null;
                mPath = null;
                DrawBitmap = Bitmap.createBitmap(720, 720, Bitmap.Config.ARGB_8888);
                mPath = new Path();
                invalidate();
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }

    }

    public interface BottomSignatureSucessListener {
        void onMainSignatureSucessClicked(String bytes);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {

        } catch (ClassCastException e) {

        }
    }
}
