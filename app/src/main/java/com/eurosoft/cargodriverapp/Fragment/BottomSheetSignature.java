package com.eurosoft.cargodriverapp.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.ScreenUtils;
import com.fxn.stash.Stash;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.io.ByteArrayOutputStream;

public class BottomSheetSignature extends BottomSheetDialogFragment {


    private ImageView close;
    private TextView done;
    private Button erase;
    TextView textView, textView1;
    CustomView CusView;
    private Bitmap DrawBitmap;
    private Canvas mCanvas;
    private Paint DrawBitmapPaint;
    Path mPath;
    Bitmap bitmap;
    private Paint mPaint;
    LinearLayout Rl;
    TextView txt1;
    int mDualPane;
    int Width = 5;
    int gone = 0, remove = 0;
    private BottomSignatureSucessListener mListener;
    MasterPojo masterPojo;

    public static BottomSheetSignature newInstance() {
        return new BottomSheetSignature();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.bottom_sheet_signature, container, false);

        InitView(rootView);
        return rootView;
    }


    @Override
    public void setupDialog(@NonNull Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.bottom_sheet_signature, null);
        dialog.setContentView(contentView);
        CoordinatorLayout.LayoutParams layoutParams = ((CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams());
        CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(bottomSheetCallback);
            ((BottomSheetBehavior) behavior).setPeekHeight(getResources().getDimensionPixelSize(R.dimen.bottom_sheet_height));
            ((BottomSheetBehavior) behavior).setDraggable(false);
        }

    }

    private BottomSheetBehavior.BottomSheetCallback bottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onStart() {
        super.onStart();
    }

    private void InitView(View rootView) {
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        close = (ImageView) rootView.findViewById(R.id.close);
        done = (TextView) rootView.findViewById(R.id.done);
        erase = (Button) rootView.findViewById(R.id.eraser);
        erase.setText(masterPojo.getClear());
        done.setText(masterPojo.getDone());


        InitSignature(rootView);


        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (AppStatus.getInstance(getActivity()).isOnline()) {
                        if (!bitmap.sameAs(DrawBitmap)) {
                            convertBitmapToByteArray();

                        } else {
                            Toast.makeText(getActivity(), masterPojo.getPleaseDrawYourSignature(), Toast.LENGTH_SHORT).show();

                        }
                    } else {
                        Toast.makeText(getActivity(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();

                    }
                } catch (Exception e) {
                    Log.e("E", e.getMessage());
                }
            }
        });

        erase.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Refresh(rootView);
            }
        });

    }


    public void convertBitmapToByteArray() {
        if (DrawBitmap != null) {
            if (AppStatus.getInstance(getActivity()).isOnline()) {

                mListener.onMainSignatureSucessClicked(encodeTobase64(DrawBitmap));
                dismiss();

                /*if (orderDataModelRoom.getPaymentTypeId() == 1) {
                    //GetSignatureIntoByte(orderDataModelRoom.getId(), prefrenceHanlder.getdriverid(), Constants.Deliver, encodeTobase64(DrawBitmap));
                    //Api
                } else {
                    //prefrenceHanlder.setSignatureCompletebutStillPaymentLeft(true);


                }*/
            } else {
                Toast.makeText(getActivity(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static String encodeTobase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 30, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }


    public void InitSignature(View rootview) {

        textView = new TextView(getActivity());
        textView1 = new TextView(getActivity());
        CusView = new CustomView(getActivity());
        erase = (Button) rootview.findViewById(R.id.eraser);
        Rl = (LinearLayout) rootview.findViewById(R.id.signauture);
        txt1 = (TextView) rootview.findViewById(R.id.enable_text);
        mDualPane = Configuration.ORIENTATION_UNDEFINED;
        LinearLayout lv = (LinearLayout) rootview.findViewById(R.id.Courier_job_layout);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            ViewGroup.LayoutParams params = null;
            params = lv.getLayoutParams();
            lv.setLayoutParams(params);

        } else {
            ViewGroup.LayoutParams params = null;
            params = lv.getLayoutParams();
            lv.setLayoutParams(params);
            ViewGroup.LayoutParams paramspaint = null;
            paramspaint = Rl.getLayoutParams();
            Rl.setLayoutParams(paramspaint);
        }
        Rl.addView(CusView);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(getResources().getColor(android.R.color.holo_red_dark));
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(Width);
        Rl.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub

                if (gone == 0) {
                    txt1.setVisibility(View.GONE);
                    gone = 4;
                }
                if (remove == 10) {
                    Rl.removeAllViews();
                    Rl.refreshDrawableState();
                    Rl.invalidate();
                    Rl.refreshDrawableState();

                    Rl = (LinearLayout) rootview.findViewById(R.id.signauture);

                    Rl.addView(CusView);
                    Rl.addView(txt1);
                    txt1 = (TextView) rootview.findViewById(R.id.enable_text);
                    if (txt1 != null) {
                        txt1.setVisibility(View.VISIBLE);
                    }

                    CusView = new CustomView(getActivity());
                    remove = 0;
                }
                return false;
            }
        });
        erase.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Refresh(rootview);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    public void Refresh(View rootview) {
        txt1.setVisibility(View.VISIBLE);
        Rl.removeAllViews();
        Rl.refreshDrawableState();
        Rl.invalidate();
        Rl.refreshDrawableState();
        Rl = (LinearLayout) rootview.findViewById(R.id.signauture);
        Rl.addView(txt1);
        Rl.addView(CusView);
        txt1 = (TextView) rootview.findViewById(R.id.enable_text);
        if (txt1 != null) {
            txt1.setVisibility(View.VISIBLE);
        }
        CusView = new CustomView(getActivity());
        remove = 10;
    }


    public class CustomView extends View {
        public int reset = 0;

        @SuppressWarnings("deprecation")
        public CustomView(Context c) {
            super(c);
            Display Disp = getActivity().getWindowManager().getDefaultDisplay();
            Log.e("sdkdkdk","Disp.getHeight()"+Disp.getHeight());
            Log.e("sdkdkdk","Disp.getWidth()"+Disp.getWidth());
            DrawBitmap = Bitmap.createBitmap(Disp.getWidth(), Disp.getWidth(), Bitmap.Config.ARGB_4444);
            bitmap = Bitmap.createBitmap(Disp.getWidth(), Disp.getWidth(), Bitmap.Config.ARGB_4444);
            mCanvas = new Canvas(DrawBitmap);
            mPath = new Path();
            DrawBitmapPaint = new Paint(Paint.DITHER_FLAG);
            if (reset == 0) {
                reset = 4;
            }
        }

        @Override
        public void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
        }

        @Override
        public void onDraw(Canvas canvas) {
            setDrawingCacheEnabled(true);
            canvas.drawBitmap(DrawBitmap, 0, 0, DrawBitmapPaint);
            canvas.drawPath(mPath, mPaint);
            canvas.drawRect(mY, 0, mY, 0, DrawBitmapPaint);
        }

        public float mX, mY;
        public static final float TOUCH_TOLERANCE = 4;

        public void touch_start(float x, float y) {
            mPath.reset();
            mPath.moveTo(x, y);
            mX = x;
            mY = y;
        }

        public void touch_move(float x, float y) {
            float dx = Math.abs(x - mX);
            float dy = Math.abs(y - mY);
            if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
                mPath.quadTo(mX, mY, (x + mX) / 2, (y + mY) / 2);
                mX = x;
                mY = y;

            }
        }

        public void touch_up() {
            mPath.lineTo(mX, mY);
            mCanvas.drawPath(mPath, mPaint);
            mPath.reset();
        }

        public void clear(int re) {
            if (re == 1) {
                DrawBitmap = null;
                mPath = null;
                DrawBitmap = Bitmap.createBitmap(720, 720, Bitmap.Config.ARGB_8888);
                mPath = new Path();
                invalidate();
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float x = event.getX();
            float y = event.getY();

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touch_start(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_MOVE:
                    touch_move(x, y);
                    invalidate();
                    break;
                case MotionEvent.ACTION_UP:
                    touch_up();
                    invalidate();
                    break;
            }
            return true;
        }

    }

    public interface BottomSignatureSucessListener {
        void onMainSignatureSucessClicked(String bytes);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (BottomSignatureSucessListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement BottomSheetListener");
        }
    }
}
