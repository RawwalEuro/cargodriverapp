package com.eurosoft.cargodriverapp.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.eurosoft.cargodriverapp.ActivityCompletedPlanJobs;
import com.eurosoft.cargodriverapp.ActivityFutureJobs;
import com.eurosoft.cargodriverapp.ActivityJobStatus;
import com.eurosoft.cargodriverapp.ActivityLogin;
import com.eurosoft.cargodriverapp.ActivityJobHistory;
import com.eurosoft.cargodriverapp.ActivityNextJobs;
import com.eurosoft.cargodriverapp.ActivityPlannedJobs;
import com.eurosoft.cargodriverapp.ActivitySelectLanguage;
import com.eurosoft.cargodriverapp.ActivtityPlanJobStatus;
import com.eurosoft.cargodriverapp.Adapter.AdapterHomeJobs;
import com.eurosoft.cargodriverapp.BuildConfig;
import com.eurosoft.cargodriverapp.MessageActivity;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.RequestLogin;
import com.eurosoft.cargodriverapp.Pojo.RequestLoginBody;
import com.eurosoft.cargodriverapp.R;
import com.eurosoft.cargodriverapp.Services.GoogleService;
import com.eurosoft.cargodriverapp.Services.SignalRService;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.ServiceTools;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;

import org.jetbrains.annotations.NotNull;

import cn.pedant.SweetAlert.SweetAlertDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentLeftMenu extends Fragment implements View.OnClickListener{

    private MasterPojo masterPojo;
    private TextView name,email,logoutTv;
    private RelativeLayout RRLogout;
    private RelativeLayout RRChatMessage,changeLanRv;
    private RelativeLayout RRJobHistory;
    private LoginResponse driverObj;
    private RelativeLayout RRplannedJobs,RRfutureJobs,nextJobsRv;
    private TextView chatMessageTv,orderHistoryTv,changeLanguageTv,appVersion,nextJobsTv;
    private RelativeLayout completedPlanRl;
    private TextView completedPlanTv;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_navigation, container, false);
        Stash.init(getContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO,MasterPojo.class);
        init(view);
        setData();
        return view;
    }

    public static Fragment newInstance() {
        FragmentLeftMenu f = new FragmentLeftMenu();
        return f;
    }

    private void init(View view) {
        name = view.findViewById(R.id.name);
        email = view.findViewById(R.id.email);
        RRLogout = view.findViewById(R.id.RRLogout);
        logoutTv = view.findViewById(R.id.logout);
        RRChatMessage=view.findViewById(R.id.RRMessage);
        RRJobHistory =view.findViewById(R.id.RRHistory);
        RRplannedJobs =view.findViewById(R.id.RRPlannedJobs);
        RRfutureJobs =view.findViewById(R.id.RRfutureJobs);
        changeLanRv =view.findViewById(R.id.changeLanRv);
        nextJobsRv =view.findViewById(R.id.nextJobs);
        completedPlanRl =view.findViewById(R.id.completedPlanRl);
        completedPlanTv =view.findViewById(R.id.completedPlanTv);
        chatMessageTv =view.findViewById(R.id.chat_message);
        orderHistoryTv =view.findViewById(R.id.orderhistory);
        changeLanguageTv =view.findViewById(R.id.changeLanguageTv);
        nextJobsTv =view.findViewById(R.id.nextJobsTv);
        appVersion =view.findViewById(R.id.appVersion);

        chatMessageTv.setText(masterPojo.getMessaging());
        orderHistoryTv.setText(masterPojo.getJobHistoryTv());
        changeLanguageTv.setText(masterPojo.getSelectLanguage());
        logoutTv.setText(masterPojo.getLogout());
        appVersion.setText(masterPojo.getAppVersion()+" " + BuildConfig.VERSION_NAME);

        RRChatMessage.setOnClickListener(this);
        RRLogout.setOnClickListener(this);
        RRJobHistory.setOnClickListener(this);
        RRplannedJobs.setOnClickListener(this);
        RRfutureJobs.setOnClickListener(this);
        changeLanRv.setOnClickListener(this);
        nextJobsRv.setOnClickListener(this);
        completedPlanRl.setOnClickListener(this);
        logoutTv.setOnClickListener(this);
    }

    private void closefragment() {
        getActivity().getFragmentManager().popBackStack();
    }

    private void setData() {

        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        name.setText(driverObj.getFName());
        email.setText(driverObj.getDriverNumber()  + "");

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.logout:
                if(Stash.getBoolean(Constants.JOB_IS_RUNNING) == true){
                    Toast.makeText(getActivity(),"Cant logout while the job is running",Toast.LENGTH_SHORT).show();
                    return;
                }
                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure you want to logout?")
                        .setConfirmText("Yes")

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog
                                        .setTitleText("Logged out")
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                               // callApiLogout(driverObj.getId(), driverObj.getDeviceId());

                                callApiGetPendingJobs(driverObj.getId());
                            }
                        })

                        .setCancelText("No")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
                break;
            case R.id.RRMessage:
                StartActivity(MessageActivity.class);

                break;
            case R.id.RRHistory:
                StartActivity(ActivityJobHistory.class);

                break;


            case R.id.RRPlannedJobs:
                StartActivity(ActivityFutureJobs.class);
                break;


            case R.id.RRfutureJobs:
                StartActivity(ActivityPlannedJobs.class);
                break;


            case R.id.changeLanRv:
                StartActivity(ActivitySelectLanguage.class);
                break;


            case R.id.nextJobs:
                StartActivity(ActivityNextJobs.class);
                break;


            case R.id.completedPlanRl:
                StartActivity(ActivityCompletedPlanJobs.class);
                break;
        }
    }

    private void callApiLogout(int id, String deviceId) {
        Log.e("id", id + "");
        Log.e("deviceId", deviceId);

        RequestLoginBody requestLoginBody = new RequestLoginBody(deviceId, id);


        RequestLogin requestLogin = new RequestLogin();
        requestLogin.setRequest(requestLoginBody);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<LoginResponse>> call = apiService.logout(requestLogin);


        call.enqueue(new Callback<WebResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<WebResponse<LoginResponse>> call, Response<WebResponse<LoginResponse>> response) {

                if (response.code() != 200 || response.body() == null) {
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    try {

                        Boolean isServiceRunning = ServiceTools.isServiceRunning(
                                getActivity(),
                                SignalRService.class);
                        Log.e("isServiceRunning", isServiceRunning + "");
                        if (isServiceRunning) {
                            Intent stopIntent = new Intent(getActivity(), SignalRService.class);
                            stopIntent.setAction(Constants.STOPFOREGROUND_ACTION);
                            getActivity().startService(stopIntent);
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Stash.clear(Constants.isLoggenIn);
                    Intent i = new Intent(getActivity(), ActivityLogin.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);

                }
            }

            @Override
            public void onFailure(Call<WebResponse<LoginResponse>> call, Throwable t) {
                Toast.makeText(getActivity(), masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void callApiGetPendingJobs(Integer id) {

        if (!AppStatus.getInstance(getActivity()).isOnline()) {
            Toast.makeText(getActivity(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();

            return;
        }

      ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseList<JobDetailData>> call = apiService.getPendingJobs(id + "", 15);

        call.enqueue(new Callback<WebResponseList<JobDetailData>>() {
            @Override
            public void onResponse(Call<WebResponseList<JobDetailData>> call, Response<WebResponseList<JobDetailData>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }
                if (response.body().getSuccess()) {
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    if(response.body().getData() == null){
                        callApiLogout(driverObj.getId(), driverObj.getDeviceId());
                    }
                    else if (response.body().getData().size() == 0){
                        callApiLogout(driverObj.getId(), driverObj.getDeviceId());
                    } else {
                        Toast.makeText(getActivity(),"You can't logout untill the next jobs are completed",Toast.LENGTH_SHORT).show();
                        StartActivity(ActivityNextJobs.class);
                    }

                }
            }
            @Override
            public void onFailure(Call<WebResponseList<JobDetailData>> call, Throwable t) {
                callApiLogout(driverObj.getId(), driverObj.getDeviceId());
            }
        });


    }

    public void StartActivity(Class classname){
        Intent intent=new Intent(getActivity(),classname);
        startActivity(intent);
    }
}
