package com.eurosoft.cargodriverapp.NetworkUtils;


import com.eurosoft.cargodriverapp.Adapter.MessageAdapter;
import com.eurosoft.cargodriverapp.Pojo.AddCosigneeInfoMaster;
import com.eurosoft.cargodriverapp.Pojo.AuthPojo;
import com.eurosoft.cargodriverapp.Pojo.ChangeStatus;
import com.eurosoft.cargodriverapp.Pojo.CityCodeResponse;
import com.eurosoft.cargodriverapp.Pojo.ConfimedPlan;
import com.eurosoft.cargodriverapp.Pojo.CosigneeName;
import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.JobDetailDataObject;
import com.eurosoft.cargodriverapp.Pojo.JobNotificationStatus;
import com.eurosoft.cargodriverapp.Pojo.Language;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterChargesCountryNShift;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.MessageHistory;
import com.eurosoft.cargodriverapp.Pojo.PaymentGatewayDetail;
import com.eurosoft.cargodriverapp.Pojo.PaymentNChargesField;
import com.eurosoft.cargodriverapp.Pojo.PlannedAccept;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobs;
import com.eurosoft.cargodriverapp.Pojo.RequestLogin;
import com.eurosoft.cargodriverapp.Pojo.RequestMessage;
import com.eurosoft.cargodriverapp.Pojo.RequestSendMessage;
import com.eurosoft.cargodriverapp.Pojo.ResponseJobNotification;
import com.eurosoft.cargodriverapp.Pojo.SendMUultipleImages;
import com.eurosoft.cargodriverapp.Pojo.SendSignature;
import com.eurosoft.cargodriverapp.Pojo.SmartChecklistPojo;
import com.eurosoft.cargodriverapp.Pojo.SubmitChecklistPojo;
import com.eurosoft.cargodriverapp.Pojo.TemplateMessageList;
import com.eurosoft.cargodriverapp.Pojo.VehicleDetails;
import com.eurosoft.cargodriverapp.Utils.JobData;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.eurosoft.cargodriverapp.Utils.WebResponseLogin;
import com.eurosoft.cargodriverapp.Utils.WebResponseString;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("Driver/DriverLogin")
    Call<WebResponseLogin<LoginResponse>> loginUser(@Body RequestLogin requestLogin);


    @POST("Driver/DriverLogout")
    Call<WebResponse<LoginResponse>> logout(@Body RequestLogin requestLogin);

    @POST("Driver/SendDriverMessage")
    Call<WebResponse<LoginResponse>> sendMessage(@Body RequestSendMessage RequestMessage);

    @POST("Driver/JobNotification")
    Call<WebResponse<ResponseJobNotification>> sendStatus(@Body JobNotificationStatus JobNotificationStatus);


    @POST("Driver/PlanNotification")
    Call<WebResponseString<String>> plannedAccept(@Body PlannedAccept PlannedAccept);

    @GET("Driver/GetCurrentJobs")
    Call<WebResponse<JobData>> getJobData(@Query("JobAddrId") String jobId);


    @GET("Driver/GetCurrentJobDetailModel")
    Call<WebResponseList<JobDetailData>> getPendingJobs(@Query("DriverId") String DriverId,@Query("JobStatusId") int JobStatusId);


    @POST("Home/GetPlanAndJobs")
    Call<WebResponseList<PlannedJobs>> getPlannedJobs(@Body PlannedJobs plannedJobs);


    @POST("Home/GetCompletedPlanAndJobs")
    Call<WebResponseList<PlannedJobs>> getCompletedPlannedJobs(@Body PlannedJobs plannedJobs);

   /* @GET("GetCurrentJobDetailModel_JobAddrId")
    Call<WebResponseList<JobDetailData>> getJobDetailsById(@Query("JobAddrId") String JobAddrId);*/


    @GET("Driver/GetCurrentJobDetailModel_JobAddrId")
    Call<WebResponse<JobDetailDataObject>> getJobDetailsById(@Query("JobAddrId") String JobAddrId);



    @GET("Driver/GetCurrentJobDetailModel_JobAddrIdForAndroid")
    Call<WebResponse<JobDetailDataObject>> getJobDetailsForAndroidId(@Query("JobAddrId") String JobAddrId);

    @POST("Driver/GetPaymentGatwayDetails")
    Call<WebResponse<PaymentGatewayDetail>> getPaymentGatewayDetails(@Query("JobAddrId") String JobAddrId);

    @POST("Driver/UpdateJobAddSingAndImg")
    Call<WebResponse<SendSignature>> sendSignature(@Body SendSignature sendSignature);


    @POST("Driver/UpdateGoodsImg")
    Call<WebResponse<SendSignature>> sendImage(@Body SendSignature sendSignature);

    @POST("Driver/UpdateGoodsImgDummy")
    Call<WebResponse<SendSignature>> sendMultipleImages(@Body SendMUultipleImages sendSignature);

    @POST("Driver/UpdateDriverStatus")
    Call<WebResponse<JobData>> changeDriverStatus(@Body ChangeStatus changeStatus);

    @GET("Driver/GetGeneralSMS")
    Call<WebResponseList<TemplateMessageList>> gettemplatlemessage();


    @GET("Driver/GetCurrentJobDetailModel")
    Call<WebResponseList<JobDetailData>> getjobhistorylist(@Query("DriverId") String DriverId, @Query("JobStatusId") int JobStatusId, @Query("DateFrom") String DateFrom, @Query("DateTo") String DateTo);
   @GET("Driver/GetCheckList")
    Call<WebResponse<List<SmartChecklistPojo>>> getCheckList();

  @GET("VerifyClientAppAccountCargo")
    Call<AuthPojo> getAuthDetails(@Query("accountCode") String accountCode, @Query("deviceInfo") String deviceInfo, @Query("deviceId") String deviceId, @Query("hashKey") String hashKey);


    @GET("Home/GetAllLanguage")
    Call<WebResponseList<Language>> getAllLanguages();


    @POST("Home/DriverVehicles_ByDriver")
    Call<WebResponseList<VehicleDetails>> getAllVehicles(@Body VehicleDetails vehicleDetails);


    @POST("Home/ChangeVehicleByDriverId")
    Call<WebResponseList<VehicleDetails>> updateSelectedVehicles(@Body VehicleDetails vehicleDetails);
    @POST("Driver/AddCheckList")
    Call<WebResponseString<String>> AddCheckList(@Body SubmitChecklistPojo vehicleDetails);


    @POST("Home/GetPageLanguageJsonForAndroid")
    Call<WebResponse<MasterPojo>> getSelectedLanguage(@Body MasterPojo masterPojo);


    @POST("Driver/GetDriverMessage")
    Call<WebResponseList<MessageHistory>> getMessageHistory(@Body MessageHistory messageHistory);


    @POST("Home/GetConfirmedPlanAndJobs")
    Call<WebResponseList<PlannedJobs>> getConfimedPlan(@Body ConfimedPlan confimedPlan);


    @GET("Driver/GetDetailsfor_Consignee")
    Call<WebResponse<MasterChargesCountryNShift>> getDetailsChargesCountryNShift();



    @POST("Driver/FindConsigneeName")
    Call<WebResponseList<CosigneeName>> getFindCosigneeName(@Body CosigneeName cosigneeName);


    @POST("Driver/AddConsigneeDetail")
    Call<WebResponse<AddCosigneeInfoMaster>> sendCosigneeInfoMaster(@Body AddCosigneeInfoMaster addCosigneeInfoMaster);


    @POST("Driver/GetCityCode")
    Call<WebResponseList<CityCodeResponse>> getCityCode(@Body CityCodeResponse cityCodeResponse);
}
