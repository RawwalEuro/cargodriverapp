package com.eurosoft.cargodriverapp.NetworkUtils;


import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;

import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {


    private static APIClient instance;
    public static final String BASE_URL = Constants.BASE_URL;
    public static final String BASE_URL_KEY = "BASE_API_URL";
    public static final String SOCKET_URL_KEY = "BASE_SOCKET_URL";
    public static final String COMPANY_NAME = "KEY_COMPANYClientName";
    public static final String TEST_URL = Constants.TEST_URL;
    private static Retrofit retrofit = null;
    private static Retrofit retrofitAuth = null;
    ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);


    private APIClient() {
    }

    public static APIClient getInstance() {
        if (instance == null) {
            instance = new APIClient();
        }
        return instance;
    }

    final static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .readTimeout(6, TimeUnit.SECONDS)
            .connectTimeout(6, TimeUnit.SECONDS)
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build();

    public static Retrofit getClient() {
       String Url_Base = Stash.getString(BASE_URL_KEY);;
//       Url_Base = "http://88.208.220.41/DTS_API";
       if(!Url_Base.endsWith("/")){
           Url_Base += "/";
       }
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Url_Base)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;
    }
    public static Retrofit getAuthClient() {
//       String Url_Base = Stash.getString(BASE_URL);;
        if (retrofitAuth == null) {
            retrofitAuth = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofitAuth;
    }

}
