package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;
// No API used for getting server url
public class ActivityAuthorization extends AppCompatActivity {

    private Button btnVerify;
    private TextView textHeader;
    private OtpView otp_view;
    private String otpCode;
    private MasterPojo masterPojo;
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        Stash.init(this);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        initViews();
    }

    private void initViews() {

        btnVerify = findViewById(R.id.btnVerify);
        textHeader = findViewById(R.id.textHeader);
        otp_view = findViewById(R.id.otp_view);
        btnVerify.setText(masterPojo.getVerify());
        textHeader.setText(masterPojo.getAuthorizeCode());


        mHandler = new Handler();
        mHandler.post(
                new Runnable() {
                    public void run() {
                        InputMethodManager inputMethodManager =  (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.toggleSoftInputFromWindow(otp_view.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                        otp_view.requestFocus();
                    }
                });

        otp_view.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                otpCode = otp;
            }
        });

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();
            }
        });
    }

    private void validate() {


        if(otp_view.getText().toString() == null || otp_view.getText().toString().isEmpty()){
            Toast.makeText(ActivityAuthorization.this,"Enter code",Toast.LENGTH_SHORT).show();
            return;
        }

        if(otp_view.getText().toString().length() < 6){
            Toast.makeText(ActivityAuthorization.this,"Not a valid code",Toast.LENGTH_SHORT).show();
            return;
        }

        if(otp_view.getText().toString().equalsIgnoreCase("999999")){
            Toast.makeText(ActivityAuthorization.this,"Successfully Authorized",Toast.LENGTH_SHORT).show();
            Stash.put(Constants.IS_AUTHORIZED,true);
            Intent intent = new Intent(ActivityAuthorization.this,ActivityLogin.class);
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(ActivityAuthorization.this,"Not a valid code",Toast.LENGTH_SHORT).show();
        }
        return;
    }
}