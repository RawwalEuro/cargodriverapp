package com.eurosoft.cargodriverapp;

import android.app.Application;
import android.content.Context;

import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.engineio.client.Transport;
import io.socket.engineio.client.transports.Polling;
import io.socket.engineio.client.transports.WebSocket;


public class CargoApp {
    static Context context;
    private static CargoApp instance = new CargoApp();


    public static CargoApp getInstance(Context ctx) {
        context = ctx.getApplicationContext();
        return instance;
    }

    public Socket getSocket() {
        Socket mSocket;
        {
            try {
                IO.Options options = new IO.Options();
                options.reconnection = true;
                options.reconnectionAttempts = 5;
                options.reconnectionDelay = 3000;
//                options.transports = new String[]{WebSocket.NAME,Polling.NAME};
                //ChatApplication.DRIVER_ID  =  PreferenceManager.getDefaultSharedPreferences(this).getString(DRIVER_ID_CONS,"");
                options.query = "name=1234&type=Android,DriverApp";//"SignalRClientsType=3&SignalRUserType=3&SignalRClientDomainId="+ChatApplication.DRIVER_ID;
                //options.transports =new String[]{WebSocket.NAME};
                String socketUrl = Stash.getString(APIClient.SOCKET_URL_KEY);
//                socketUrl = "http://88.208.220.41:9000/";
                mSocket = IO.socket(socketUrl, options);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return mSocket;
    }
}
