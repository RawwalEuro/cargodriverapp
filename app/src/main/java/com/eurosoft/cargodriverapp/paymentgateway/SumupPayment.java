package com.eurosoft.cargodriverapp.paymentgateway;

import android.app.Activity;

import com.eurosoft.cargodriverapp.Pojo.JobDetailDataObject;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;
import com.sumup.merchant.api.SumUpAPI;
import com.sumup.merchant.api.SumUpLogin;
import com.sumup.merchant.api.SumUpPayment;

import java.math.BigDecimal;

public class SumupPayment {
        private static SumupPayment sumupPayment = null;
        public  static SumupPayment getInstance(){
            if(sumupPayment == null){
                sumupPayment = new SumupPayment();
            }
            return  sumupPayment;
        }
    public void sumUpLogin(String affiliateKey, Activity activity) {
//	736ba26a-ef22-476b-96d4-8d17a48b1c0e	demo
//	f88962aa-996a-49ce-b3ef-a899cf334f26   network taxis
        SumUpLogin sumupLogin = SumUpLogin.builder(affiliateKey).build();
        SumUpAPI.openLoginActivity(activity, sumupLogin, 1);
    }

    public void sumUpCheckout(Activity activity, double rawTotal) {
            String job_id = Stash.getString(Constants.JOB_ID);
            SumUpPayment.Currency currency = SumUpPayment.Currency.GBP;

        SumUpPayment payment = SumUpPayment.builder()
                // mandatory parameters
                .total(new BigDecimal(rawTotal)) // minimum 1.00
                .currency(currency)
                // optional: include a tip amount in addition to the total
//				.tip(new BigDecimal(rawTotal))
                // optional: add details
                .title("Taxi Ride")
//				.receiptEmail("customer@mail.com")
//				.receiptSMS("+3531234567890")
                // optional: Add metadata
                .addAdditionalInfo("Booking Ref", job_id)
//				.addAdditionalInfo("From", "Paris")
//				.addAdditionalInfo("To", "Berlin")
                // optional: foreign transaction ID, must be unique!
//				.foreignTransactionId(UUID.randomUUID().toString())  // can not exceed 128 chars
                // optional: skip the success screen
//				.skipSuccessScreen()

                .build();

        SumUpAPI.checkout(activity, payment, 2);
    }
}
