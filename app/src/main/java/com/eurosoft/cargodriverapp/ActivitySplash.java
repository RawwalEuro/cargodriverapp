package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.AdapterSelectLanguage;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.Language;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.LocationEnable;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySplash extends AppCompatActivity {

    private ImageView logo;
    private Animation slideAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Stash.init(this);
        Stash.put(Constants.CHATACTIVITY_IS_RUNNING, false);

        logo = findViewById(R.id.logo);
        slideAnimation = AnimationUtils.loadAnimation(this, R.anim.side_slide);
        logo.startAnimation(slideAnimation);

        //   callApiForLanguage();

        if (Stash.getObject(Constants.MASTER_POJO, MasterPojo.class) == null&&!Stash.getString(APIClient.BASE_URL_KEY).equals("")) {
            try {
                callApiForLanguage();
            } catch (Exception e) {
                e.printStackTrace();
                setNavigation();
            }
        } else {
            setNavigation();
        }


    }

    private void callApiForLanguage() {
        MasterPojo masterPojo = new MasterPojo("en");
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<MasterPojo>> call = apiService.getSelectedLanguage(masterPojo);

        call.enqueue(new Callback<WebResponse<MasterPojo>>() {
            @Override
            public void onResponse(Call<WebResponse<MasterPojo>> call, Response<WebResponse<MasterPojo>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }
                if (!response.body().getSuccess() && response.body().getData() != null) {
                    Stash.put(Constants.MASTER_POJO, response.body().getData());
                    setNavigation();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<MasterPojo>> call, Throwable t) {

            }
        });
    }

    private void setNavigation() {
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms


                Log.e("isAuthorized", Stash.getBoolean(Constants.IS_AUTHORIZED) + "");
                Log.e("isLoggenIn", Stash.getBoolean(Constants.isLoggenIn) + "");
                Log.e("isLocationEnabled", LocationEnable.isLocationEnabled(getApplicationContext()) + "");

                if (Stash.getBoolean(Constants.isLoggenIn) && LocationEnable.isLocationEnabled(getApplicationContext())) {
                    startActivity(new Intent(ActivitySplash.this, HomeActivity.class));
                    finish();
                } else if (Stash.getBoolean(Constants.isLoggenIn) && Stash.getBoolean(Constants.IS_AUTHORIZED) && !LocationEnable.isLocationEnabled(getApplicationContext())) {
                    startActivity(new Intent(ActivitySplash.this, ActivityEnableLocation.class));
                    finish();
                } else if (!Stash.getBoolean(Constants.isLoggenIn) && !Stash.getBoolean(Constants.IS_AUTHORIZED)) {
                    startActivity(new Intent(ActivitySplash.this, Authorize.class));
                    finish();
                } else if (Stash.getBoolean(Constants.IS_AUTHORIZED) && !Stash.getBoolean(Constants.isLoggenIn)) {
                    startActivity(new Intent(ActivitySplash.this, ActivityLogin.class));
                    finish();
                } else {
                    startActivity(new Intent(ActivitySplash.this, Authorize.class));
                    finish();
                }
            }
        }, 3000);
    }
}