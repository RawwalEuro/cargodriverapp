package com.eurosoft.cargodriverapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.eurosoft.cargodriverapp.Utils.Constants;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

public class GPSTracker extends Service {


    Location location; // location

    private static GPSTracker gpsTracker;
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationRequest locationRequest;

    public static GPSTracker getInstance() {
        if (gpsTracker == null) {
            gpsTracker = new GPSTracker();
        }
        return gpsTracker;
    }

    public static boolean isLocationAllowed(Context context) {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return false;
        } else {
            return true;
        }
    }

    public GPSTracker() {
    }

    public boolean startLocationUpdates(Context context) {
        try {
            if (!isLocationAllowed(context)) {
                return false;
            }

            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
//            if (locationRequest == null) {
            locationRequest = LocationRequest.create();
            locationRequest.setInterval(1000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            getLastLocation();
//                fusedLocationProviderClient.requestLocationUpdates(locationRequest, callback, Looper.getMainLooper());
            return true;
//            }
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
//        return false;
    }

    public void getLastLocation() {
        // Get last known recent location using new Google Play Services SDK (v11+)



        fusedLocationProviderClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // GPS location can be null if GPS is switched off
                        if (location != null) {
//                            onLocationChanged(location);
                            Constants.lat = location.getLatitude();
                            Constants.lng = location.getLongitude();
                            Constants.speed = location.getSpeed();
                            Constants.speed *= 2.2369362920544;
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("MapDemoActivity", "Error trying to get last GPS location");
                        e.printStackTrace();
                    }
                });
    }
    LocationCallback callback=new LocationCallback(){
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            try{
                location= locationResult.getLastLocation();
                if(location!=null){
                    Constants.lat = location.getLatitude();
                    Constants.lng = location.getLongitude();
                    Constants.speed = location.getSpeed();
                    Constants.speed *= 2.2369362920544;

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
//        Loc
    };
    @SuppressLint("MissingPermission")
    public  void stopLocationUpdates(){
        try {
            fusedLocationProviderClient.removeLocationUpdates(callback);
        }catch (Exception e){
            e.printStackTrace();
        }
        gpsTracker =null;

    }
    @SuppressLint("MissingPermission")
    void getLocation(){

//		fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
//			@Override
//			public void onSuccess(Location location) {
//				try {
//
//				if(location!=null){
//					CommonObjects.lat = location.getLatitude();
//					CommonObjects.lng = location.getLongitude();
//					CommonObjects.speed = location.getSpeed();
//					CommonObjects.speed *= 2.2369362920544;
//				}else{
//
//
//
//				}
//
//				}catch (Exception e){
//
//				}
//			}
//		}).addOnFailureListener(new OnFailureListener() {
//			@Override
//			public void onFailure(@NonNull Exception e) {
//			e.printStackTrace();
//			}
//		});
    }
    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}
