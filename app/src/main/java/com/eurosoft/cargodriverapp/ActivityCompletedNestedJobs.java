package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.AdapterNestedJobs;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.JobNotificationStatus;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobObject;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobs;
import com.eurosoft.cargodriverapp.Pojo.ResponseJobNotification;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCompletedNestedJobs extends AppCompatActivity {


    private RecyclerView rvNestedJobs;
    private ImageView bckBtn;
    private AdapterNestedJobs adapterNestedJobs;
    private MasterPojo masterPojo;
    private PlannedJobs plannedJob;
    private LoginResponse driverObj;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout mainRl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_nested_jobs);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        initViews();
        getBundle();
    }


    private void getBundle() {
        try {
            plannedJob = (PlannedJobs) getIntent().getExtras().getSerializable(Constants.PLANNED_JOBS);
            if (plannedJob == null) {
                finish();
                return;
            } else {
                setUpAdapter();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();

    }


    private void setUpAdapter() {
        adapterNestedJobs = new AdapterNestedJobs(ActivityCompletedNestedJobs.this, plannedJob.getJobAddrJsons(), new AdapterNestedJobs.interfaceCallApi() {
            @Override
            public void interfaceCall(int jobId) {


            }
        }, new AdapterNestedJobs.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, PlannedJobObject plannedJobObject) {
                switch (view.getId()) {

                    case R.id.start:


                        if (plannedJobObject.getJobStatusId() <= 4) {
                            Log.e("JobStatus", plannedJobObject.getJobStatusId() + "");

                            Toast.makeText(getApplicationContext(), "Please change status to On Route to Start the job", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (plannedJobObject.getJobStatusId() == 7) {
                            Log.e("JobStatus", plannedJobObject.getJobStatusId() + "");

                            Toast.makeText(getApplicationContext(), "You have completed this job", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Intent intent = new Intent(ActivityCompletedNestedJobs.this, ActivtityPlanJobStatus.class);
                        intent.putExtra("object", plannedJobObject);
                        intent.putExtra("showSlider", true);
                        startActivity(intent);

                        break;


                    case R.id.showDetails:

                        Intent intentI = new Intent(ActivityCompletedNestedJobs.this, ActivtityPlanJobStatus.class);
                        intentI.putExtra("object", plannedJobObject);
                        intentI.putExtra("showSlider", false);
                        startActivity(intentI);

                        break;

                }
            }
        }, masterPojo);

        rvNestedJobs.setAdapter(adapterNestedJobs);
        adapterNestedJobs.notifyDataSetChanged();
    }

    private void initViews() {
        rvNestedJobs = findViewById(R.id.rv_nested_job);
        bckBtn = findViewById(R.id.bckPress);

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        rvNestedJobs.setHasFixedSize(true);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        // disableEnableControls(true, mainRl);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
        //   disableEnableControls(false, mainRl);
    }


}