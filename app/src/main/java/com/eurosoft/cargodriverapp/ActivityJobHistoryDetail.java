package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.cargodriverapp.Adapter.AdapterGoodsDetail;
import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.JobDetailDataObject;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;

public class ActivityJobHistoryDetail extends AppCompatActivity {
    private TextView jobIdRef;
    private TextView origin, desitination, totalWeight, totalVolume, totalQty, name, address, textHeader, goodsinfo, totalgoodsinfo, measurementheading, descheading, qtyheading, volumeheading, weightheading, totalWeightheading, totalQtyheading, totalVolumeheading;
    private AdapterGoodsDetail adapterGoodsDetail;
    private RecyclerView rvGoods;
    private LinearLayoutManager layoutManager;
    private RelativeLayout expandRl;
    private MasterPojo masterPojo;
    private String jobType = "";
    private ImageView phone;
    private ImageView bckBtn;
    private Bundle extras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_history_detail);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO,MasterPojo.class);
        initViews();
        getBundle();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initViews() {

        jobIdRef = findViewById(R.id.jobIdRef);
        origin = findViewById(R.id.origin);
        desitination = findViewById(R.id.desitination);
        totalWeight = findViewById(R.id.totalWeight);
        totalVolume = findViewById(R.id.totalVolume);
        totalQty = findViewById(R.id.totalQty);
        rvGoods = findViewById(R.id.rvGoods);
        name = findViewById(R.id.name);
        address = findViewById(R.id.address);
        expandRl = findViewById(R.id.expandRl);
        bckBtn = findViewById(R.id.bckBtn);
        /////////////////////////////////////////Language Changes//////////////////////////////////////////////////////
        textHeader = findViewById(R.id.textHeader);
        goodsinfo = findViewById(R.id.goodsinfo);
        totalgoodsinfo = findViewById(R.id.totalgoodsinfo);
        weightheading = findViewById(R.id.weightheading);
        measurementheading = findViewById(R.id.measurementheading);
        volumeheading = findViewById(R.id.volumeheading);
        descheading = findViewById(R.id.descheading);
        qtyheading = findViewById(R.id.qtyheading);
        totalWeightheading = findViewById(R.id.totalWeightheading);
        totalQtyheading = findViewById(R.id.totalQtyheading);
        totalVolumeheading = findViewById(R.id.totalVolumeheading);
        textHeader.setText(masterPojo.getJobHistoryTitle());
        goodsinfo.setText(masterPojo.getGoodsInformation());
        totalgoodsinfo.setText(masterPojo.getGoodsInformation());
        weightheading.setText(masterPojo.getWeight());
        measurementheading.setText(masterPojo.getMeasurement());
        volumeheading.setText(masterPojo.getVolume());
        descheading.setText(masterPojo.getDesc());
        qtyheading.setText(masterPojo.getQty());
        totalWeightheading.setText(masterPojo.getTotalWeight());
        totalQtyheading.setText(masterPojo.getTotalQty());
        totalVolumeheading.setText(masterPojo.getTotalVolume());
        phone = (ImageView) findViewById(R.id.phone);
        rvGoods.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ActivityJobHistoryDetail.this, LinearLayoutManager.VERTICAL, true);
        rvGoods.setLayoutManager(layoutManager);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }
    private void getBundle() {

        extras = getIntent().getExtras();
        if (extras != null) {
            JobDetailDataObject jobDetailData = (JobDetailDataObject) getIntent().getSerializableExtra("object");
            if (jobDetailData != null) {
                jobIdRef.setText(masterPojo.getReference() + "" + "");
                jobType = jobDetailData.getStatus().getJobType();
                setData(jobDetailData);
            }
        }


    }
    private void setData(JobDetailDataObject data) {

        try {
            origin.setText(data.getShipper().getCollectionAddr());
            desitination.setText(data.getShipper().getDeliveryAddr());
            totalQty.setText("");
            totalVolume.setText("");
            totalWeight.setText("");
            name.setText(data.getShipper().getFirstName());
            address.setText(data.getShipper().getAddressLine1());


            if (data.getStatus().getJobType() != null) {
                if (jobType.equalsIgnoreCase("Delivery")) {
                    totalgoodsinfo.setText(masterPojo.getCosigneeInfo());
                } else {
                    totalgoodsinfo.setText(masterPojo.getShipperInfo());
                }
            }

            if (data.getGoodinfo().size() < 3) {
                expandRl.setVisibility(View.INVISIBLE);
            } else {
                expandRl.setVisibility(View.INVISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        expandRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityJobHistoryDetail.this, ActivityExpandedGood.class));
            }
        });


        adapterGoodsDetail = new AdapterGoodsDetail(ActivityJobHistoryDetail.this, 0, data.getGoodinfo(), new AdapterGoodsDetail.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {
                    case R.id.mainRl:
                        break;

                }
            }
        });

        rvGoods.setAdapter(adapterGoodsDetail);
        adapterGoodsDetail.notifyDataSetChanged();
    }

}