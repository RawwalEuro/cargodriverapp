package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.Cosignee;
import com.eurosoft.cargodriverapp.Pojo.GoodsInfoAddObject;
import com.eurosoft.cargodriverapp.Pojo.GoodsQtyStatus;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterGoodsAdd extends RecyclerView.Adapter<AdapterGoodsAdd.ViewHolder>{

    private Context context;
    private ArrayList<GoodsInfoAddObject> listGoodsInfo;
    private OnItemClickListener mOnItemClickListener;
    private MasterPojo masterPojo;

    public AdapterGoodsAdd(Context ctx, ArrayList<GoodsInfoAddObject> listGoodsInfoAdd, MasterPojo masterPojoo, AdapterGoodsAdd.OnItemClickListener onItemClickListener) {
        context = ctx;
        listGoodsInfo = listGoodsInfoAdd;
        mOnItemClickListener = onItemClickListener;
        masterPojo = masterPojoo;
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position, GoodsInfoAddObject goodsInfoAddObject);
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_goods_add, parent, false);
        AdapterGoodsAdd.ViewHolder vh = new AdapterGoodsAdd.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GoodsInfoAddObject goodsInfoAddObject = listGoodsInfo.get(position);
        int count = holder.getAdapterPosition() + 1;

        holder.headingGoodsInfo.setText("Item " + count);
        holder.goodDesc.setText(goodsInfoAddObject.getGoodsDesc());
        holder.measurementValue.setText(goodsInfoAddObject.getMeasurementTypeDesc());
        holder.qtyValue.setText(goodsInfoAddObject.getGoodsQty());
        holder.volumeValue.setText(goodsInfoAddObject.getVolume());
        holder.weightValue.setText(goodsInfoAddObject.getWeight());

        holder.deltIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view,holder.getAdapterPosition(),goodsInfoAddObject);
            }
        });

        holder.editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view,holder.getAdapterPosition(),goodsInfoAddObject);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listGoodsInfo.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView goodDesc,measurementValue,qtyValue,volumeValue,weightValue,headingGoodsInfo;
        private ImageView deltIcon,editIcon;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            headingGoodsInfo = itemView.findViewById(R.id.headingGoodsInfo);
            goodDesc = itemView.findViewById(R.id.goodDesc);
            measurementValue = itemView.findViewById(R.id.measurementValue);
            qtyValue = itemView.findViewById(R.id.qtyValue);
            volumeValue = itemView.findViewById(R.id.volumeValue);
            weightValue = itemView.findViewById(R.id.weightValue);
            deltIcon = itemView.findViewById(R.id.deltIcon);
            editIcon = itemView.findViewById(R.id.editIcon);
        }
    }
}
