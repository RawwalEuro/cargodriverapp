package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.Language;
import com.eurosoft.cargodriverapp.Pojo.VehicleDetails;
import com.eurosoft.cargodriverapp.R;

import java.util.List;

public class AdapterChangeVehicle  extends RecyclerView.Adapter<AdapterChangeVehicle.ViewHolder>{


    private final List<VehicleDetails> vehicleDetailsList;
    private OnItemClickListener mOnItemClickListener;
    private Context context;
    int row_index=-1;

    public AdapterChangeVehicle(Context ctx, List<VehicleDetails> list, OnItemClickListener onItemClickListener) {
        context = ctx;
        vehicleDetailsList = list;
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, VehicleDetails vehicleDetails);
    }

    @NonNull
    @Override
    public AdapterChangeVehicle.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_change_vehicle, parent, false);
        AdapterChangeVehicle.ViewHolder vh = new AdapterChangeVehicle.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterChangeVehicle.ViewHolder holder, int position) {

        VehicleDetails vehicleDetails = vehicleDetailsList.get(position);
        holder.vehicleNumber.setText(vehicleDetails.getVehicleNumber()+"");


        if (row_index == position) {
            holder.rlBg.setBackgroundColor(Color.parseColor("#FF03A9F4"));
            holder.vehicleNumber.setTextColor(Color.parseColor("#FFFFFF"));
            holder.imageChecked.setColorFilter(Color.parseColor("#FFFFFF"));
            holder.imageChecked.setVisibility(View.VISIBLE);
        } else {

            holder.rlBg.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.vehicleNumber.setTextColor(Color.parseColor("#000000"));
            holder.imageChecked.setColorFilter(Color.parseColor("#000000"));
            holder.imageChecked.setVisibility(View.GONE);
        }

        holder.rlBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=holder.getAdapterPosition();
                mOnItemClickListener.onItemClick(view, vehicleDetails);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return vehicleDetailsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView truckImage,imageChecked;
        private TextView vehicleNumber;
        private RelativeLayout rlBg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            truckImage = itemView.findViewById(R.id.truckImage);
            vehicleNumber = itemView.findViewById(R.id.vehicleNumberPlate);
            imageChecked = itemView.findViewById(R.id.imageChecked);
            rlBg = itemView.findViewById(R.id.rlBg);
        }
    }
}
