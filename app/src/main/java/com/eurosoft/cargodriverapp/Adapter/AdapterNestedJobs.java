package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.addisonelliott.segmentedbutton.SegmentedButton;
import com.addisonelliott.segmentedbutton.SegmentedButtonGroup;
import com.eurosoft.cargodriverapp.Pojo.JobDetailDataObject;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobObject;
import com.eurosoft.cargodriverapp.R;

import java.util.List;

public class AdapterNestedJobs extends RecyclerView.Adapter<AdapterNestedJobs.ViewHolder> {


    private interfaceCallApi mInterfaceCallApi;
    private Context context;
    private List<PlannedJobObject> jobDetailDataObjects;
    private OnItemClickListener mOnItemClickListener;
    private MasterPojo mmasterpojo;

    public AdapterNestedJobs(Context ctx, List<PlannedJobObject> jobDetailDataObjectList, interfaceCallApi interfaceCallApi, OnItemClickListener onItemClickListener, MasterPojo masterpojo) {
        context = ctx;
        jobDetailDataObjects = jobDetailDataObjectList;
        mOnItemClickListener = onItemClickListener;
        mInterfaceCallApi = interfaceCallApi;
        mmasterpojo = masterpojo;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position, PlannedJobObject plannedJobObject);
    }

    public interface interfaceCallApi {
        public void interfaceCall(int jobId);
    }

    @Override
    public AdapterNestedJobs.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_nested_job, parent, false);
        AdapterNestedJobs.ViewHolder vh = new AdapterNestedJobs.ViewHolder(v);
        return vh;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PlannedJobObject plannedJobObject = jobDetailDataObjects.get(holder.getAdapterPosition());

        holder.pickup.setText(plannedJobObject.getCollectionAddr());
        holder.desitination.setText(plannedJobObject.getDeliveryAddr());
        holder.start.setText(mmasterpojo.getStart());
        holder.showDetails.setText(mmasterpojo.getShowDetails());
        holder.segmentButton.setDraggable(true);
        Log.e("JobStatus -Adp", plannedJobObject.getJobStatusId() + "");
        Log.e("JobStatus -Adp", plannedJobObject.getJobStatusId() + "");


        if (plannedJobObject.getJobStatusId() == 7) {
            holder.segmentButton.setVisibility(View.GONE);
            holder.start.setText("Completed");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.start.setBackgroundTintList(ColorStateList.valueOf(context.getColor(R.color.green_color)));
            }
            holder.start.setEnabled(false);
        } else {
            holder.segmentButton.setVisibility(View.VISIBLE);
            holder.start.setText("Start");
            holder.start.setEnabled(true);
        }
        holder.segmentButton.setOnPositionChangedListener(new SegmentedButtonGroup.OnPositionChangedListener() {
            @Override
            public void onPositionChanged(int position) {

                mInterfaceCallApi.interfaceCall(plannedJobObject.getId());
            }
        });

        holder.start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view, position, plannedJobObject);
            }
        });


        holder.showDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view, position, plannedJobObject);
            }
        });

        if (plannedJobObject.getJobStatusId() > 4) {
            //holder.segmentButton.setPosition(1, false);
            holder.segmentButton.setEnabled(false);
            holder.segmentWaiting.setVisibility(View.GONE);
            holder.segmentOnRoute.setVisibility(View.VISIBLE);
            holder.segmentButton.setBackground(context.getColor(R.color.teal_700));
            holder.segmentOnRoute.setTextColor(context.getColor(R.color.white));

        } else {
            //holder.segmentButton.setPosition(0, false);
            holder.segmentButton.setEnabled(true);
            holder.segmentButton.setVisibility(View.VISIBLE);
            holder.segmentWaiting.setVisibility(View.VISIBLE);
            holder.segmentOnRoute.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return jobDetailDataObjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView pickup, desitination, start, showDetails;
        private LinearLayout bottomLL;
        private SegmentedButtonGroup segmentButton;
        private SegmentedButton segmentWaiting;
        private SegmentedButton segmentOnRoute;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pickup = itemView.findViewById(R.id.pickup);
            desitination = itemView.findViewById(R.id.destinationoo);
            start = itemView.findViewById(R.id.start);
            showDetails = itemView.findViewById(R.id.showDetails);
            bottomLL = itemView.findViewById(R.id.bottomLL);
            segmentButton = itemView.findViewById(R.id.buttonGroup_roundSelectedButton);
            segmentWaiting = itemView.findViewById(R.id.segmentWaiting);
            segmentOnRoute = itemView.findViewById(R.id.segmentOnRoute);

        }
    }
}

