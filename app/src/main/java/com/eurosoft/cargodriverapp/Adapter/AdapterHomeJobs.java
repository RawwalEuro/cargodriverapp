package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.JobData;
import com.fxn.stash.Stash;

import java.util.List;

public class AdapterHomeJobs extends RecyclerView.Adapter<AdapterHomeJobs.ViewHolder> {

    private MasterPojo masterPojos;
    List<JobDetailData> lstJobs;
    private Context context;
    private AdapterHomeJobs.OnItemClickListenerHome mOnItemClickListener;
    private int countRead;
    int row_index = -1;


    public AdapterHomeJobs(Context ctx, List<JobDetailData> vehiclesList,MasterPojo masterPojo, AdapterHomeJobs.OnItemClickListenerHome onItemClickListener) {
        context = ctx;
        lstJobs = vehiclesList;
        masterPojos = masterPojo;
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListenerHome {
        public void onItemClick(View view, int position, int selectedOption,JobDetailData jobDetailData);
    }


    @Override
    public AdapterHomeJobs.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Stash.init(context);
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_jobs, parent, false);
        AdapterHomeJobs.ViewHolder vh = new AdapterHomeJobs.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterHomeJobs.ViewHolder holder, int position) {
        JobDetailData job = lstJobs.get(position);


        holder.origin.setText(job.getCollectionAddr() + "");
        holder.desitination.setText(job.getDeliveryAddr() + "");
        holder.jobId.setText(masterPojos.getReference() +job.getJobRefNo() + "");

        holder.start.setText(masterPojos.getStart()+ "");
        holder.recover.setText(masterPojos.getRecover() + "");
        holder.showDetails.setText(masterPojos.getShowDetails() + "");
        holder.datetime.setText(job.getCreateDate() + "");


        holder.fullCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mOnItemClickListener.onItemClick(v, holder.getAdapterPosition());
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition(), 0, job);
                row_index = holder.getAdapterPosition();
                notifyDataSetChanged();
                if (holder.btmView.getVisibility() == View.VISIBLE) {
                    holder.btmView.setVisibility(View.VISIBLE);
                } else {
                    holder.btmView.setVisibility(View.VISIBLE);
                }
            }
        });


        if (row_index == position) {

            holder.btmView.setVisibility(View.VISIBLE);
        } else {
            holder.btmView.setVisibility(View.VISIBLE);
        }


        holder.llStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Stash.getBoolean(Constants.JOB_IS_RUNNING) == true){
                    Toast.makeText(context.getApplicationContext(),"Can't start job while the job is running",Toast.LENGTH_SHORT).show();
                    return;
                }

                if(Stash.getInt(Constants.CURRENT_STATUS) != 1){
                    Toast.makeText(context.getApplicationContext(),masterPojos.getCantstartorrecoverajobwhenthedriverisonbreak(),Toast.LENGTH_SHORT).show();
                    return;
                }
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition(), 1, job);
            }
        });

        holder.llRecover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if(Stash.getBoolean(Constants.JOB_IS_RUNNING) == true){
                    Toast.makeText(context.getApplicationContext(),masterPojos.getCantrecoverjobwhenthejobisrunning(),Toast.LENGTH_SHORT).show();
                    return;
                }*/

                if(Stash.getInt(Constants.CURRENT_STATUS) != 1){
                    Toast.makeText(context.getApplicationContext(),masterPojos.getCantstartorrecoverajobwhenthedriverisonbreak(),Toast.LENGTH_SHORT).show();
                    return;
                }
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition(), 2, job);
            }
        });

        holder.llshowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition(), 3, job);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(lstJobs == null || lstJobs.size() == 0){
            return 0;
        }
        return lstJobs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView origin, desitination, jobId, vehicleId,start,recover,showDetails,datetime;
        private RelativeLayout mainRl;
        private CardView fullCard;
        private LinearLayout btmView, llRecover, llStart, llshowDetails;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            origin = itemView.findViewById(R.id.origin);
            desitination = itemView.findViewById(R.id.desitination);
            jobId = itemView.findViewById(R.id.jobId);

            start = itemView.findViewById(R.id.start);
            recover = itemView.findViewById(R.id.recover);
            showDetails = itemView.findViewById(R.id.showDetails);

            vehicleId = itemView.findViewById(R.id.vehicleId);
            mainRl = itemView.findViewById(R.id.mainRl);
            fullCard = itemView.findViewById(R.id.fullCard);
            btmView = itemView.findViewById(R.id.btmView);
            llRecover = itemView.findViewById(R.id.llRecover);
            llStart = itemView.findViewById(R.id.llStart);
            llshowDetails = itemView.findViewById(R.id.llshowDetails);
            datetime = itemView.findViewById(R.id.dateTimeTv);

        }
    }
}
