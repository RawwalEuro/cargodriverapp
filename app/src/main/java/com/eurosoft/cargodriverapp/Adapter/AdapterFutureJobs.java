package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.GoodInfo;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;

import java.util.List;

public class AdapterFutureJobs extends RecyclerView.Adapter<AdapterFutureJobs.ViewHolder>{


    private Context context;
    private List<String> stringList;
    private OnItemClickListener mOnItemClickListener;
    private MasterPojo mmasterPojo;

    public AdapterFutureJobs(Context ctx, List<String> stringsList, OnItemClickListener onItemClickListener, MasterPojo masterPojo) {
        context = ctx;
        stringList = stringsList;
        mOnItemClickListener = onItemClickListener;
        mmasterPojo=masterPojo;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @Override
    public AdapterFutureJobs.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_future_jobs, parent, false);
        AdapterFutureJobs.ViewHolder vh = new AdapterFutureJobs.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterFutureJobs.ViewHolder holder, int position) {
        String text = stringList.get(position);
        holder.planId.setText(mmasterPojo.getPlanNumber()+"205");

    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       private TextView planId,reciptId,date,location,distance,timesCovered;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            planId = itemView.findViewById(R.id.planId);
            reciptId = itemView.findViewById(R.id.reciptId);
            date = itemView.findViewById(R.id.date);
            location = itemView.findViewById(R.id.location);
            distance = itemView.findViewById(R.id.distance);
            timesCovered = itemView.findViewById(R.id.timesCovered);
        }
    }
}
