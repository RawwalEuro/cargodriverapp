package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.TemplateMessageList;
import com.eurosoft.cargodriverapp.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class TemplateMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    Context context;
    List<TemplateMessageList> templateMessageLists;
    private MessageSelectedListener mListener;

    public TemplateMessageAdapter(Context mcontext, List<TemplateMessageList> mtemplateMessageLists) {
        templateMessageLists = mtemplateMessageLists;
        context = mcontext;
        mListener = (MessageSelectedListener) context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.template_layout, parent, false);
        MessageList viewHolder = new MessageList(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        ((MessageList) holder).MessageText.setText("" + templateMessageLists.get(position).getMsgDesc());
        ((MessageList) holder).MessageText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onSelectedMessage(templateMessageLists.get(position).getMsgDesc());
            }
        });
    }

    @Override
    public int getItemCount() {
        return templateMessageLists.size();
    }

    public static class MessageList extends RecyclerView.ViewHolder {
        TextView MessageText;

        public MessageList(View itemView) {
            super(itemView);
            MessageText = (TextView) itemView.findViewById(R.id.item1);
        }
    }

    public interface MessageSelectedListener {
        void onSelectedMessage(String message);
    }
}
