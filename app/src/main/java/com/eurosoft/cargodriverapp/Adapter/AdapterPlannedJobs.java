package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.ActivityNestedJobs;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobs;
import com.eurosoft.cargodriverapp.R;

import java.util.List;

public class AdapterPlannedJobs extends RecyclerView.Adapter<AdapterPlannedJobs.ViewHolder> {


    private Context context;
    private List<PlannedJobs> plannedJobs;
    private OnItemClickListener mOnItemClickListener;
    private MasterPojo mmasterpojo;

    public AdapterPlannedJobs(Context ctx, List<PlannedJobs> plannedJobsList, OnItemClickListener onItemClickListener, MasterPojo masterpojo) {
        context = ctx;
        plannedJobs = plannedJobsList;
        mOnItemClickListener = onItemClickListener;
        mmasterpojo = masterpojo;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position, PlannedJobs plannedJobs);
    }

    @Override
    public AdapterPlannedJobs.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_planned_jobs, parent, false);
        AdapterPlannedJobs.ViewHolder vh = new AdapterPlannedJobs.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPlannedJobs.ViewHolder holder, int position) {
        PlannedJobs plannedJob = plannedJobs.get(position);
        holder.jobNumber.setText("Plan no " + plannedJob.getPlanNo());
        holder.location.setText(plannedJob.getJobAddrJsons().get(0).getDeliveryAddr());

        if (plannedJob.getConfirmed()) {
            holder.isConfirmed.setVisibility(View.VISIBLE);
        } else {
            holder.isConfirmed.setVisibility(View.GONE );
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // context.startActivity(new Intent(context, ActivityNestedJobs.class));
                mOnItemClickListener.onItemClick(view, holder.getAdapterPosition(), plannedJob);
            }
        });
    }

    @Override
    public int getItemCount() {
        return plannedJobs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView jobNumber, location, time, mints, kmCovereds;
        private CardView cardView;
        private ImageView isConfirmed;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            jobNumber = itemView.findViewById(R.id.jobNumber);
            location = itemView.findViewById(R.id.location);
            time = itemView.findViewById(R.id.time);
            mints = itemView.findViewById(R.id.mints);
            kmCovereds = itemView.findViewById(R.id.kmCovereds);
            cardView = itemView.findViewById(R.id.cardView);
            isConfirmed = itemView.findViewById(R.id.isConfirmed);
        }
    }
}

