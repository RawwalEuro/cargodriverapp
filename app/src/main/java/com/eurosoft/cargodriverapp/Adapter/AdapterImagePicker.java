package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class AdapterImagePicker extends RecyclerView.Adapter<AdapterImagePicker.ViewHolder> {

    private OnItemClickListener mOnItemClickListener;
    private ArrayList<String> lstPictures;
    private Context context;

    public AdapterImagePicker(Context ctx, ArrayList<String> lstImages, OnItemClickListener onItemClickListener) {
        context = ctx;
        lstPictures = lstImages;
        mOnItemClickListener = onItemClickListener;
    }


    @Override
    public AdapterImagePicker.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_image_picker, parent, false);
        AdapterImagePicker.ViewHolder vh = new AdapterImagePicker.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterImagePicker.ViewHolder holder, int position) {
        String base64 = lstPictures.get(position);
        holder.image.setImageBitmap(convertBase64ToBitmap(base64));
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnItemClickListener.onItemClick(view, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return lstPictures.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView text;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            text = itemView.findViewById(R.id.text);
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public static Bitmap convertBase64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }
}
