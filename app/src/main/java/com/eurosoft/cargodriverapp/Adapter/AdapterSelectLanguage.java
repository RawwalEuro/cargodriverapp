package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.Language;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;

import java.util.List;

public class AdapterSelectLanguage extends RecyclerView.Adapter<AdapterSelectLanguage.ViewHolder>{

    private Context context;
    private List<Language> languageList;
    private AdapterSelectLanguage.OnItemClickListener mOnItemClickListener;
    int row_index=-1;

    public AdapterSelectLanguage(Context ctx, List<Language> list, AdapterSelectLanguage.OnItemClickListener onItemClickListener) {
        context = ctx;
        languageList = list;
        mOnItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, Language language);
    }


    @NonNull
    @Override
    public AdapterSelectLanguage.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_change_language, parent, false);
        AdapterSelectLanguage.ViewHolder vh = new AdapterSelectLanguage.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterSelectLanguage.ViewHolder holder, int position) {
        Language language = languageList.get(position);
        holder.languaneName.setText(language.getName()+"");


        if (row_index == position) {
            holder.rlBg.setBackgroundColor(Color.parseColor("#FF03A9F4"));
            holder.languaneName.setTextColor(Color.parseColor("#FFFFFF"));
            holder.imageChecked.setColorFilter(Color.parseColor("#FFFFFF"));
            holder.imageChecked.setVisibility(View.VISIBLE);
        } else {

            holder.rlBg.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.languaneName.setTextColor(Color.parseColor("#000000"));
            holder.imageChecked.setColorFilter(Color.parseColor("#000000"));
            holder.imageChecked.setVisibility(View.GONE);
        }

        holder.rlBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=holder.getAdapterPosition();
                mOnItemClickListener.onItemClick(view, language);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return languageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       private ImageView flagImage,imageChecked;
       private TextView languaneName;
       private RelativeLayout rlBg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            flagImage = itemView.findViewById(R.id.flagImage);
            languaneName = itemView.findViewById(R.id.languaneName);
            imageChecked = itemView.findViewById(R.id.imageChecked);
            rlBg = itemView.findViewById(R.id.rlBg);
        }
    }
}
