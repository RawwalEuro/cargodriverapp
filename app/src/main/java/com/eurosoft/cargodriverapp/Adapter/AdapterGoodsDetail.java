package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.GoodInfo;
import com.eurosoft.cargodriverapp.Pojo.GoodInfoModelGoodInfoModelInner;
import com.eurosoft.cargodriverapp.R;
import com.eurosoft.cargodriverapp.Utils.JobData;

import java.util.ArrayList;
import java.util.List;

public class AdapterGoodsDetail extends RecyclerView.Adapter<AdapterGoodsDetail.ViewHolder>{

    private List<GoodInfo> goodsLists = new ArrayList<>();
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    private int countRead;

    public AdapterGoodsDetail(Context ctx,int count, List<GoodInfo> goodsList, OnItemClickListener onItemClickListener) {
        context = ctx;
        goodsLists = goodsList;
        mOnItemClickListener = onItemClickListener;
        countRead = count;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }


    @Override
    public AdapterGoodsDetail.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_goods, parent, false);
        AdapterGoodsDetail.ViewHolder vh = new AdapterGoodsDetail.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GoodInfo goodItem = goodsLists.get(position);

      /*  if(position == 0){
            holder.measurementHeading.setVisibility(View.VISIBLE);
            holder.volumeHeading.setVisibility(View.VISIBLE);
            holder.weightHeading.setVisibility(View.VISIBLE);
            holder.qtyHeading.setVisibility(View.VISIBLE);
            holder.descHeading.setVisibility(View.VISIBLE);
        } else {

            holder.measurementHeading.setVisibility(View.GONE);
            holder.volumeHeading.setVisibility(View.GONE);
            holder.weightHeading.setVisibility(View.GONE);
            holder.qtyHeading.setVisibility(View.GONE);
            holder.descHeading.setVisibility(View.GONE);
        }*/

        holder.measurement.setText(goodItem.getMeasurementTypeDesc());
        holder.desc.setText(goodItem.getGoodDesc() +"");
        holder.qty.setText(goodItem.getQuantity() + "");
        holder.volume.setText(goodItem.getVolume() + "");
        holder.weight.setText(goodItem.getWeight() + "");
    }


    @Override
    public int getItemCount() {
        if (countRead == 0) {
            if (goodsLists.size() <= 3) {
                return goodsLists.size();
            } else {
                return 3;
            }
        } else {
            return goodsLists.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView measurement,desc,qty,volume,weight,measurementHeading,descHeading,qtyHeading,volumeHeading,weightHeading;
        private CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            measurement = itemView.findViewById(R.id.measurement);
            desc = itemView.findViewById(R.id.desc);
            qty = itemView.findViewById(R.id.qty);
            volume = itemView.findViewById(R.id.volume);
            weight = itemView.findViewById(R.id.weight);
            weight = itemView.findViewById(R.id.weight);

          /*  measurementHeading = itemView.findViewById(R.id.measurementHeading);
            descHeading = itemView.findViewById(R.id.descHeading);
            qtyHeading = itemView.findViewById(R.id.qtyHeading);
            volumeHeading = itemView.findViewById(R.id.volumeHeading);
            weightHeading = itemView.findViewById(R.id.weightHeading);*/
        }
    }
}
