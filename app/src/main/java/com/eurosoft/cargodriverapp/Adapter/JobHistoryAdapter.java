
package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.ActivityJobHistoryDetail;
import com.eurosoft.cargodriverapp.ActivityJobStatus;
import com.eurosoft.cargodriverapp.HomeActivity;
import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;

import java.util.ArrayList;
import java.util.List;

public class JobHistoryAdapter extends RecyclerView.Adapter<JobHistoryAdapter.JobHistoryList> {
    Context context;
    List<JobDetailData> jobDetailDataList;
    private MasterPojo masterPojos;

    public JobHistoryAdapter(Context mcontext, List<JobDetailData> mjobDetailDataList, MasterPojo masterPojo) {
        jobDetailDataList = mjobDetailDataList;
        context = mcontext;
        masterPojos = masterPojo;
    }


    @NonNull
    @Override
    public JobHistoryList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.job_history_item, parent, false);
        JobHistoryList viewHolder = new JobHistoryList(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull JobHistoryList holder, int position) {
        JobDetailData job = jobDetailDataList.get(position);
        holder.origin.setText(job.getCollectionAddr() + "");
        holder.desitination.setText(job.getDeliveryAddr() + "");
        holder.jobId.setText(masterPojos.getReference() +job.getJobRefNo() + "");
        holder.fullCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                boolean showSlider = false;
                Stash.put(Constants.JOB_ID,job.getId());
                Intent i = new Intent(context, ActivityJobStatus.class);
                i.putExtra("showSlider", showSlider);
                i.putExtra("object", job);
                context.startActivity(i);

            }
        });


    }

    @Override
    public int getItemCount() {
        return jobDetailDataList.size();
    }

    public static class JobHistoryList extends RecyclerView.ViewHolder {
        private TextView origin, desitination, jobId, vehicleId;
        private CardView fullCard;

        public JobHistoryList(@NonNull View itemView) {
            super(itemView);

            origin = itemView.findViewById(R.id.origin);
            desitination = itemView.findViewById(R.id.desitination);
            jobId = itemView.findViewById(R.id.jobId);
            vehicleId = itemView.findViewById(R.id.vehicleId);
            fullCard = itemView.findViewById(R.id.fullCard);
        }
    }
    public void filterList(ArrayList<JobDetailData> filteredList) {
        jobDetailDataList = filteredList;
        notifyDataSetChanged();
    }



}

