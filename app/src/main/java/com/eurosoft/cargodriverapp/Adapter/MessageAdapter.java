package com.eurosoft.cargodriverapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.Message;
import com.eurosoft.cargodriverapp.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private List<Message> mMessages;
    private int[] mUsernameColors={R.color.black};
    Context mcontext;


    public MessageAdapter(Context context, List<Message> messages) {
        mMessages = messages;
        mcontext=context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout = -1;
        switch (viewType) {
        case Message.TYPE_MESSAGE:
            layout = R.layout.item_message_2;
            break;
        case Message.TYPE_LOG:
            layout = R.layout.item_log;
            break;
        case Message.TYPE_ACTION:
            layout = R.layout.item_action;
            break;
        }
        View v = LayoutInflater
                .from(parent.getContext())
                .inflate(layout, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Message message = mMessages.get(position);
        viewHolder.setMessage(message.getMessage());
       // viewHolder.setUsername(message.getUsername());
        viewHolder.setMsgDate(message.getMsgDate());

        if(mMessages.get(position).getUsername().equals("DriverApp,Android")){
            viewHolder.setLayout(R.drawable.my_message,RelativeLayout.ALIGN_PARENT_RIGHT);
        } else if(mMessages.get(position).getUsername().equals("WebApp")){
            viewHolder.setLayout(R.drawable.their_message,RelativeLayout.ALIGN_PARENT_LEFT);
        }
        else if(mMessages.get(position).getUsername().equals("DriverApp- iOS")){
            viewHolder.setLayout(R.drawable.my_message,RelativeLayout.ALIGN_PARENT_RIGHT);
        }
        else {
            viewHolder.setLayout(R.drawable.their_message,RelativeLayout.ALIGN_PARENT_LEFT);
        }

    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mMessages.get(position).getType();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
      //  private TextView mUsernameView;
        private TextView mMessageView;
        private TextView mDateView;
        private LinearLayout mMessageLayout;

        public ViewHolder(View itemView) {
            super(itemView);

        //    mUsernameView = (TextView) itemView.findViewById(R.id.username);
            mMessageView = (TextView) itemView.findViewById(R.id.message);
            mDateView = (TextView) itemView.findViewById(R.id.datetime);
            mMessageLayout = (LinearLayout) itemView.findViewById(R.id.messagetype);

        }

        public void setUsername(String username) {
       //     if (null == mUsernameView) return;
       //     mUsernameView.setText(username);
//            mUsernameView.setTextColor(getUsernameColor(username));
        }
        public void setMsgDate(String date_) {
            if (null == mDateView) return;
            mDateView.setText(date_);
        }
        public void setMessage(String message) {
            if (null == mMessageView) return;
            mMessageView.setText(message);
        }
        public void setLayout(int drawableid, int alignlayoutid){
            if (null == mMessageLayout) return;
            mMessageLayout.setBackground(mcontext.getResources().getDrawable(drawableid));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(alignlayoutid);
            mMessageLayout.setLayoutParams(params);
        }



        public void add(Message message) {
            if (mMessages == null) {
                mMessages = new ArrayList<>();
                mMessages.add(message);
                notifyItemInserted(mMessages.size() - 1);
            } else {
                if(message.getmMessage() == null){
                    return;
                }
                mMessages.add(message);
                notifyItemInserted(mMessages.size() - 1);
            }
        }

        public void addAll(List<Message> messageList) {
            for (Message result : messageList) {
                add(result);
            }
        }

        public void clearAll(){
            if(mMessages != null || mMessages.size() != 0){
                mMessages.clear();
                notifyDataSetChanged();

            }
        }


    }
}
