package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.Cosignee;
import com.eurosoft.cargodriverapp.Pojo.GoodInfo;
import com.eurosoft.cargodriverapp.Pojo.GoodsQtyStatus;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.R;

import java.util.List;

public class AdapterCosigneeNDelivery extends RecyclerView.Adapter<AdapterCosigneeNDelivery.ViewHolder>{


    private OnItemClickListener mOnItemClickListener;
    private Context context;
    private List<Cosignee> arrayListCosigneNShipper;
    private GoodsQtyStatus goodsQtyStatus;
    private MasterPojo masterPojo;

    public AdapterCosigneeNDelivery(Context ctx, GoodsQtyStatus goodsQtyStatu, MasterPojo masterPojoo, List<Cosignee> listCosigneNShipper, OnItemClickListener onItemClickListener) {
        context = ctx;
        arrayListCosigneNShipper = listCosigneNShipper;
        mOnItemClickListener = onItemClickListener;
        goodsQtyStatus = goodsQtyStatu;
        masterPojo = masterPojoo;
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    @Override
    public AdapterCosigneeNDelivery.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_cosignee_delivery, parent, false);
        AdapterCosigneeNDelivery.ViewHolder vh = new AdapterCosigneeNDelivery.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Cosignee cosignee = arrayListCosigneNShipper.get(position);

        if(cosignee.isCosignee()){

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                holder.shipperHeaderDetail.setBackgroundTintList(context.getColorStateList(R.color.send_button_color));
                holder.cardView.setBackgroundTintList(context.getColorStateList(R.color.selected_green));
            }
        }
        holder.name.setText(cosignee.getFirstName());
        holder.address.setText(cosignee.getCollectionAddr() +"");
        holder.totalgoodsinfo.setText(cosignee.isHeading() +"");

        holder.totalWeightheading.setText(masterPojo.getTotalWeight() +"");
        holder.totalQtyheading.setText(masterPojo.getTotalQty() +"");
        holder.totalVolumeheading.setText(masterPojo.getTotalVolume() +"");
        holder.phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+ cosignee.getPhone()));
                context.startActivity(intent);
            }
        });
        if(goodsQtyStatus != null){
        holder.totalWeight.setText(goodsQtyStatus.getWeight());
        holder.totalQty.setText(goodsQtyStatus.getGoodsQty());
        holder.totalVolume.setText(goodsQtyStatus.getVolume());
        }
    }

    @Override
    public int getItemCount() {

         if(arrayListCosigneNShipper != null){
             return arrayListCosigneNShipper.size();
         } else {
             return 0;
         }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name,address,totalWeight,totalQty,totalVolume,totalgoodsinfo,totalWeightheading,totalQtyheading,totalVolumeheading;
        private ImageView phone;
        private CardView cardView;
        private LinearLayout shipperHeaderDetail;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            address = itemView.findViewById(R.id.address);
            phone = itemView.findViewById(R.id.phone);
            totalWeight = itemView.findViewById(R.id.totalWeight);
            totalWeightheading = itemView.findViewById(R.id.totalWeightheading);
            totalQtyheading = itemView.findViewById(R.id.totalQtyheading);
            totalVolumeheading = itemView.findViewById(R.id.totalVolumeheading);
            totalQty = itemView.findViewById(R.id.totalQty);
            totalVolume = itemView.findViewById(R.id.totalVolume);


            cardView = itemView.findViewById(R.id.cardView);
            shipperHeaderDetail = itemView.findViewById(R.id.shipperHeaderDetail);
            totalgoodsinfo = itemView.findViewById(R.id.totalgoodsinfo);

        }


    }
}
