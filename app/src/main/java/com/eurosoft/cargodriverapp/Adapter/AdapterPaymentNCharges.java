package com.eurosoft.cargodriverapp.Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Pojo.GoodInfo;
import com.eurosoft.cargodriverapp.Pojo.PaymentNCharges;
import com.eurosoft.cargodriverapp.Pojo.PaymentNChargesField;
import com.eurosoft.cargodriverapp.R;

import java.util.List;

public class AdapterPaymentNCharges extends RecyclerView.Adapter<AdapterPaymentNCharges.ViewHolder> {


    private Context context;
    private AdapterPaymentNCharges.OnItemClickListener mOnItemClickListener;
    private List<PaymentNChargesField> paymentNChargesList;
    private EditText editText;
    private Double total = 0.0;
    private OnValuesChanged valuesChanged;

    public AdapterPaymentNCharges(Context ctx, List<PaymentNChargesField> list, AdapterPaymentNCharges.OnItemClickListener onItemClickListener, AdapterPaymentNCharges.OnValuesChanged onValuesChanged) {
        context = ctx;
        paymentNChargesList = list;
        mOnItemClickListener = onItemClickListener;
        valuesChanged = onValuesChanged;
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public interface OnValuesChanged {
        public void valueChanded(Double value);
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_payment_n_charges, parent, false);
        AdapterPaymentNCharges.ViewHolder vh = new AdapterPaymentNCharges.ViewHolder(v, new MyCustomEditTextListener());
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        PaymentNChargesField paymentNCharges = paymentNChargesList.get(position);
        holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());
        editText.setHint(paymentNCharges.getDesc());
        // editText.setText(paymentNCharges.getDesc());

    }

    public Double getTotal() {
        return total;
    }

    /*public int getValues(int i) {
        if (editText != null) {
            return Integer.parseInt(editText.getText().toString().trim());
        } else {
            return 0;
        }
    }*/


    public double getValues(int i) {
        if (editText != null) {
            return paymentNChargesList.get(i).getValuesInit();
        } else {
            return 0.0;
        }
    }

    public int getValuesHeader(int i) {
        if (editText != null) {
            return paymentNChargesList.get(i).getIndexNumber();
        } else {
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        return paymentNChargesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public MyCustomEditTextListener myCustomEditTextListener;

        public ViewHolder(@NonNull View itemView, MyCustomEditTextListener myCustomEditTextListener) {
            super(itemView);

            editText = itemView.findViewById(R.id.dynamicEt);
            this.myCustomEditTextListener = myCustomEditTextListener;
            editText.addTextChangedListener(myCustomEditTextListener);
        }
    }


    private class MyCustomEditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
            try {
                if (editable.toString().isEmpty()) {
                    paymentNChargesList.get(position).setEmpty(true);
                    return;
                }
                paymentNChargesList.get(position).setValuesInit(Double.parseDouble(editable.toString()));
                paymentNChargesList.get(position).setEmpty(false);

                if (paymentNChargesList.size() == 0) {
                    total = 0.0;
                    valuesChanged.valueChanded(0.0);
                    return;
                }
                total = 0.0;
                for (int i = 0; i < paymentNChargesList.size(); i++) {
                    total = total + paymentNChargesList.get(i).getValuesInit();
                    // total = total + 0;
                }
                valuesChanged.valueChanded(getTotal());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
