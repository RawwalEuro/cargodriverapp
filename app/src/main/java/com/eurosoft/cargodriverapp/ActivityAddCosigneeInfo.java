package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.AdapterGoodsAdd;
import com.eurosoft.cargodriverapp.Adapter.AdapterPaymentNCharges;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.AddCosigneeInfoMaster;
import com.eurosoft.cargodriverapp.Pojo.CityCodeResponse;
import com.eurosoft.cargodriverapp.Pojo.Cosignee;
import com.eurosoft.cargodriverapp.Pojo.CosigneeInfo;
import com.eurosoft.cargodriverapp.Pojo.CosigneeName;
import com.eurosoft.cargodriverapp.Pojo.Country;
import com.eurosoft.cargodriverapp.Pojo.GoodsInfoAddObject;
import com.eurosoft.cargodriverapp.Pojo.JobChargesInfo;
import com.eurosoft.cargodriverapp.Pojo.JobDetailDataObject;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterChargesCountryNShift;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.MeasurementTypes;
import com.eurosoft.cargodriverapp.Pojo.PaymentNChargesField;
import com.eurosoft.cargodriverapp.Pojo.ShiftSettings;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.InputValidatorHelper;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.eurosoft.cargodriverapp.spinerdialog.OnSpinerItemClick;
import com.eurosoft.cargodriverapp.spinerdialog.SpinnerDialog;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//Add Cosignee Details
public class ActivityAddCosigneeInfo extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private TextView headingTv;
    private RelativeLayout rlShift, rlDeliveryDate, rlFromTime, rlTillTime, rlCosigneeName, emailRl, numberRl, rlDeliveryAddress, rlCountry, rlLitreMeasurement, rlGoodDesc, quantityRl, volumeRl, weightRl;
    private TextView shiftTv, deliveryDateTv, fromTimeTv, tillTv, tvCountry, headingGoodsInfo, measurementTv, paymentTypeTv;
    private EditText cosigneeNameEt, emailEt, numberEt, deliveryEt, goodDescEt, quantityEt, volumeEt, weightEt, totalEt, editPrice, serviceEt, etPackaging, extraEt;
    private InputValidatorHelper inputValidator;
    private LinearLayout btnAddGoods, btnSave;
    private RecyclerView rvGoodsInfo;
    SpinnerDialog spinnerShift;
    SpinnerDialog spinnerCountry;
    SpinnerDialog spinnerMeasureMent;
    SpinnerDialog spinnerPaymentType;
    SpinnerDialog spinnerName;
    DatePicker datePicker;
    private ArrayList<String> shiftNameList = new ArrayList<>();
    final Calendar myCalendar = Calendar.getInstance();
    private String fromTime;
    private String tillTime, minutes;
    private ArrayList<String> shiftCountryList = new ArrayList<>();
    private ArrayList<String> measurementNameList = new ArrayList<>();
    private List<MeasurementTypes> measureList = new ArrayList<>();
    private ArrayList<String> paymentTypeList = new ArrayList<>();
    private MasterPojo masterPojo;
    private AdapterGoodsAdd adapterGoodsAdd;
    private LinearLayoutManager layoutManagerHorizontal;
    private ArrayList<GoodsInfoAddObject> listAddedGoods = new ArrayList<>();
    private ViewDialogEdit editDialogue;
    private ImageView bckIcon;
    private RecyclerView rvPaymentNCharges;
    private AdapterPaymentNCharges adapterPaymentNCharges;
    private List<PaymentNChargesField> paymentNChargesFieldArrayList;
    private ArrayList<JobChargesInfo> jobChargesInfoArrayList = new ArrayList<>();
    private CircularProgressBar circularProgressBar;
    private RelativeLayout rlProgressBar;
    private LinearLayout mainRl;
    private ArrayList<CosigneeName> listCosigneeName = new ArrayList<>();
    private ArrayList<String> listName = new ArrayList<>();
    private ImageView arrowDow, cross;
    private List<ShiftSettings> shiftListing;
    private List<Country> countrytListing;
    private CosigneeName cosigneeNameObj = new CosigneeName();
    private LoginResponse driverObj;
    private JobDetailDataObject jobDetailDataObject;
    private Integer shiftCode = 0;
    private boolean isManualFormFill = true;
    private Integer measureCode = 0;
    private AutoCompleteTextView rlDestination;
    private ArrayList<String> arr = new ArrayList<String>();
    private int destinationId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cosignee_info);
        Stash.init(this);
        inputValidator = new InputValidatorHelper();
        editDialogue = new ViewDialogEdit();
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        initViews();
        getBundle();
        callApiGetCharges();
        callApiLstCosigneeName();


    }

    private void getBundle() {

        try {
            jobDetailDataObject = (JobDetailDataObject) Stash.getObject(Constants.PASS_JOB_DETAIL_OBJ, JobDetailDataObject.class);
            setDataReceived(jobDetailDataObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setDataReceived(JobDetailDataObject jobDetailDataObject) {
        if (jobDetailDataObject == null) {
            return;
        }
        if (jobDetailDataObject.getConsignee() != null) {
            Cosignee cosignee = jobDetailDataObject.getConsignee();
            cosigneeNameEt.setText(cosignee.getFirstName());
            emailEt.setText(cosignee.getEmailAddress());
            numberEt.setText(cosignee.getPhone());
            deliveryEt.setText(cosignee.getDeliveryAddr());
            tvCountry.setText(cosignee.getCountryCode());

            if (cosignee.getAddressLine1() == null || cosignee.getAddressLine1().isEmpty()) {
                deliveryDateTv.setText(getDateTime() + "");
            } else {
                deliveryDateTv.setText(cosignee.getAddressLine1() + "");
            }

            if (cosignee.getFromTime() == null || cosignee.getFromTime().isEmpty()) {
                fromTimeTv.setText(getTime(myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE)));
            } else {
                fromTimeTv.setText(cosignee.getFromTime());
            }

            if (cosignee.getTillTime() == null || cosignee.getTillTime().isEmpty()) {
                tillTv.setText(getTime(myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE) + 20));
            } else {
                tillTv.setText(cosignee.getTillTime());
            }
        }
    }

    private void callApiGetCharges() {

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<MasterChargesCountryNShift>> call = apiService.getDetailsChargesCountryNShift();

        call.enqueue(new Callback<WebResponse<MasterChargesCountryNShift>>() {
            @Override
            public void onResponse(Call<WebResponse<MasterChargesCountryNShift>> call, Response<WebResponse<MasterChargesCountryNShift>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    progressVisiblityGone();

                    if (response.body().getData() == null) {
                        return;
                    }
                    paymentNChargesFieldArrayList = new ArrayList<>();
                    shiftListing = new ArrayList<>();
                    countrytListing = new ArrayList<>();
                    measureList = new ArrayList<>();
                    paymentNChargesFieldArrayList = response.body().getData().getCharges();
                    shiftListing = response.body().getData().getShiftSettings();
                    countrytListing = response.body().getData().getCountries();
                    measureList = response.body().getData().getMeasurementTypesList();


                    adapterPaymentNCharges = new AdapterPaymentNCharges(ActivityAddCosigneeInfo.this, paymentNChargesFieldArrayList, new AdapterPaymentNCharges.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            switch (view.getId()) {
                            }
                        }
                    }, new AdapterPaymentNCharges.OnValuesChanged() {
                        @Override
                        public void valueChanded(Double value) {
                            try {
                                totalEt.setText(value.toString());
                                Stash.put(jobDetailDataObject.getStatus().getId()+"_totalAmount", value.floatValue());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    rvPaymentNCharges.setAdapter(adapterPaymentNCharges);
                    adapterPaymentNCharges.notifyDataSetChanged();


                    if (shiftListing == null || shiftListing.size() == 0) {
                        return;
                    }
                    for (int i = 0; i < shiftListing.size(); i++) {
                        shiftNameList.add(shiftListing.get(i).getShiftDesc());
                    }
                    if (countrytListing == null || countrytListing.size() == 0) {
                        return;
                    }
                    for (int i = 0; i < countrytListing.size(); i++) {
                        shiftCountryList.add(countrytListing.get(i).getCode());
                    }

                    if (measureList == null || measureList.size() == 0) {
                        return;
                    }

                    for (int i = 0; i < measureList.size(); i++) {
                        measurementNameList.add(measureList.get(i).getMeasurementTypeDesc());
                    }

                    setShiftData();
                    setCountryData();
                    setMeasurementData();
                }

            }

            @Override
            public void onFailure(Call<WebResponse<MasterChargesCountryNShift>> call, Throwable t) {
                progressVisiblityGone();
            }
        });
    }


    private String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }


    private void callApiLstCosigneeName() {


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);


        CosigneeName cosigneeName = new CosigneeName(jobDetailDataObject.getStatus().getAccountInformationId() + "", "");
        Call<WebResponseList<CosigneeName>> call = apiService.getFindCosigneeName(cosigneeName);

        call.enqueue(new Callback<WebResponseList<CosigneeName>>() {
            @Override
            public void onResponse(Call<WebResponseList<CosigneeName>> call, Response<WebResponseList<CosigneeName>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    progressVisiblityGone();

                    if (response.body().getData().size() == 0) {
                        return;
                    } else {
                        listCosigneeName = response.body().getData();

                        listName = new ArrayList<>();
                        for (int i = 0; i < listCosigneeName.size(); i++) {
                            listName.add(listCosigneeName.get(i).getContactName());
                        }

                        spinnerName = new SpinnerDialog(ActivityAddCosigneeInfo.this, listName, "Select Name", "Close");// With No Animation
                        spinnerName.setShowKeyboard(false);
                        spinnerName.bindOnSpinerListener(new OnSpinerItemClick() {
                            @Override
                            public void onClick(String item, int position) {
                                cosigneeNameEt.setText(item + "");
                                CosigneeName cosigneeObj = listCosigneeName.get(position);
                                setEditTextDiasble(cosigneeNameEt);
                                cosigneeNameObj = listCosigneeName.get(position);
                                isManualFormFill = false;
                                setData(cosigneeObj);
                            }
                        });
                    }
                }

            }

            @Override
            public void onFailure(Call<WebResponseList<CosigneeName>> call, Throwable t) {
                progressVisiblityGone();
            }
        });
    }

    private void setEditTextDiasble(EditText editText) {
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setClickable(false);
        editText.setLongClickable(false);
        editText.setCursorVisible(false);

        arrowDow.setVisibility(View.GONE);
        cross.setVisibility(View.VISIBLE);
    }

    private void setEditTextClickable(EditText editText) {
        editText.setText("");
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.setClickable(true);
        editText.setLongClickable(true);
        editText.setCursorVisible(true);

        arrowDow.setVisibility(View.VISIBLE);
        cross.setVisibility(View.GONE);
        clearFields();

        cosigneeNameObj = new CosigneeName();
    }

    private void setData(CosigneeName cosigneeObj) {
        emailEt.setText(cosigneeObj.getEmail() + "");
        numberEt.setText(cosigneeObj.getPhone() + "");
        deliveryEt.setText(cosigneeObj.getCollectionAddress() + "");
        tvCountry.setText(cosigneeObj.getCountryCode() + "");


    }

    private void clearFields() {
        emailEt.setText("");
        numberEt.setText("");
        deliveryEt.setText("");
        tvCountry.setText("");
    }

    private void setShiftData() {


        spinnerShift = new SpinnerDialog(ActivityAddCosigneeInfo.this, shiftNameList, "Select Shift", "Close");// With No Animation
        spinnerShift.setShowKeyboard(false);

        shiftTv.setText(shiftNameList.get(0));
        shiftCode = shiftListing.get(0).getId();
        spinnerShift.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                Toast.makeText(ActivityAddCosigneeInfo.this, item + "  ", Toast.LENGTH_SHORT).show();
                shiftCode = shiftListing.get(position).getId();
                shiftTv.setText(item);
            }
        });
    }

    private void setMeasurementData() {

        spinnerMeasureMent = new SpinnerDialog(ActivityAddCosigneeInfo.this, measurementNameList, "Select Measurement", "Close");// With No Animation
        spinnerMeasureMent.setShowKeyboard(false);

        measurementTv.setText(measurementNameList.get(0));
        measureCode = measureList.get(0).getId();
        spinnerMeasureMent.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                Toast.makeText(ActivityAddCosigneeInfo.this, item + "  ", Toast.LENGTH_SHORT).show();
                measureCode = measureList.get(position).getId();
                measurementTv.setText(item);
            }
        });
    }


    private void setCountryData() {


        spinnerCountry = new SpinnerDialog(ActivityAddCosigneeInfo.this, shiftCountryList, "Select Country", "Close");// With No Animation
        spinnerCountry.setShowKeyboard(false);

        tvCountry.setText(shiftCountryList.get(0));

        spinnerCountry.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                Toast.makeText(ActivityAddCosigneeInfo.this, item + "  ", Toast.LENGTH_SHORT).show();
                tvCountry.setText(item);
            }
        });
    }


    private void setPaymentType() {
        paymentTypeList = new ArrayList<>();
        paymentTypeList.add("Cash");
        paymentTypeList.add("Card");

        spinnerPaymentType = new SpinnerDialog(ActivityAddCosigneeInfo.this, paymentTypeList, "Select Payment Type", "Close");// With No Animation
        spinnerPaymentType.setShowKeyboard(false);

        paymentTypeTv.setText(paymentTypeList.get(0));

        spinnerPaymentType.bindOnSpinerListener(new OnSpinerItemClick() {
            @Override
            public void onClick(String item, int position) {
                Toast.makeText(ActivityAddCosigneeInfo.this, item + "  ", Toast.LENGTH_SHORT).show();
                paymentTypeTv.setText(item);
            }
        });
    }


    private void initViews() {
        headingTv = findViewById(R.id.headingTv);
        rlShift = findViewById(R.id.rlShift);
        shiftTv = findViewById(R.id.shiftTv);
        rlDeliveryDate = findViewById(R.id.rlDeliveryDate);
        deliveryDateTv = findViewById(R.id.deliveryDateTv);
        rlFromTime = findViewById(R.id.rlFromTime);
        fromTimeTv = findViewById(R.id.fromTimeTv);
        rlTillTime = findViewById(R.id.rlTillTime);
        tillTv = findViewById(R.id.tillTv);
        rlCosigneeName = findViewById(R.id.rlCosigneeName);
        cosigneeNameEt = findViewById(R.id.cosigneeNameEt);
        emailRl = findViewById(R.id.emailRl);
        emailEt = findViewById(R.id.emailEt);
        numberRl = findViewById(R.id.numberRl);
        numberEt = findViewById(R.id.numberEt);
        rlDeliveryAddress = findViewById(R.id.rlDeliveryAddress);
        deliveryEt = findViewById(R.id.deliveryEt);
        rlCountry = findViewById(R.id.rlCountry);
        tvCountry = findViewById(R.id.tvCountry);
        bckIcon = findViewById(R.id.bckIcon);

        paymentTypeTv = findViewById(R.id.paymentTypeTv);
        totalEt = findViewById(R.id.totalEt);
        editPrice = findViewById(R.id.editPrice);
        serviceEt = findViewById(R.id.serviceEt);
        etPackaging = findViewById(R.id.etPackaging);
        extraEt = findViewById(R.id.extraEt);


        rvGoodsInfo = findViewById(R.id.rvGoodsInfo);
        rvPaymentNCharges = findViewById(R.id.rvPaymentNCharges);
        btnSave = findViewById(R.id.btnSave);
        rlDestination = findViewById(R.id.rlDestination);


        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        arrowDow = findViewById(R.id.arrowDow);
        cross = findViewById(R.id.cross);


        //Goods Information Init
        headingGoodsInfo = findViewById(R.id.headingGoodsInfo);
        rlLitreMeasurement = findViewById(R.id.rlLitreMeasurement);
        measurementTv = findViewById(R.id.measurementTv);
        rlGoodDesc = findViewById(R.id.rlGoodDesc);
        goodDescEt = findViewById(R.id.goodDescEt);
        quantityRl = findViewById(R.id.quantityRl);
        quantityEt = findViewById(R.id.quantityEt);
        volumeRl = findViewById(R.id.volumeRl);
        volumeEt = findViewById(R.id.volumeEt);
        weightRl = findViewById(R.id.weightRl);
        weightEt = findViewById(R.id.weightEt);
        btnAddGoods = findViewById(R.id.btnAddGoods);


        rlDestination.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.toString().length() < 1) {
                    return;
                }
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String querry = editable.toString();
                            callApiGetCityCode(querry);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });

            }
        });

        setUpAutoComplete();
        setPaymentType();
        //Set data
        deliveryDateTv.setText(getDateTime() + "");
        fromTimeTv.setText(getTime(myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE)));
        tillTv.setText(getTime(myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get(Calendar.MINUTE) + 20));


        fromTimeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimeFrom();
            }
        });

        deliveryDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDate();
            }
        });


        deliveryDateTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogDate();
            }
        });

        tillTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimeTill();
            }
        });

        rlShift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerShift.showSpinerDialog();
            }
        });


        rlCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerCountry.showSpinerDialog();
            }
        });

        rlLitreMeasurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerMeasureMent.showSpinerDialog();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateFields();
            }
        });

        btnAddGoods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateAddGoods();
            }
        });

        paymentTypeTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                spinnerPaymentType.showSpinerDialog();
            }
        });

        bckIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityAddCosigneeInfo.this, ActivityJobStatus.class);
                finish();
                startActivity(intent);
            }
        });


        arrowDow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    spinnerName.showSpinerDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEditTextClickable(cosigneeNameEt);
                isManualFormFill = true;
            }
        });

        rvGoodsInfo.setHasFixedSize(true);
        layoutManagerHorizontal = new LinearLayoutManager(ActivityAddCosigneeInfo.this, LinearLayoutManager.HORIZONTAL, false);
        rvGoodsInfo.setLayoutManager(layoutManagerHorizontal);

        rvPaymentNCharges.setHasFixedSize(true);


    }

    private void callApiGetCityCode(String querry) {


        CityCodeResponse cityCodeResponse = new CityCodeResponse(querry);
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseList<CityCodeResponse>> call = apiService.getCityCode(cityCodeResponse);

        call.enqueue(new Callback<WebResponseList<CityCodeResponse>>() {
            @Override
            public void onResponse(Call<WebResponseList<CityCodeResponse>> call, Response<WebResponseList<CityCodeResponse>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }


                if (response.code() == 200 || response.body().getData() != null) {

                    if (response.body().getData().size() == 0) {
                        return;
                    }
                    arr = new ArrayList<>();
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        arr.add(response.body().getData().get(i).getCode() + " , " + response.body().getData().get(i).getCountryCode() + " , " + response.body().getData().get(i).getId());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                            (getApplicationContext(), android.R.layout.select_dialog_item, arr);


                    rlDestination.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.e("Values", adapterView.getItemAtPosition(i).toString());

                            String fetchedString = adapterView.getItemAtPosition(i).toString();
                            String[] separated = fetchedString.split(",");
                            try {
                                rlDestination.setText(separated[0]);
                                tvCountry.setText(separated[1]);
                                destinationId = Integer.parseInt(separated[2]);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    rlDestination.setThreshold(2);
                    rlDestination.setAdapter(adapter);
                }

            }

            @Override
            public void onFailure(Call<WebResponseList<CityCodeResponse>> call, Throwable t) {
                progressVisiblityGone();
            }
        });


    }

    private void setUpAutoComplete() {

    }


    private String getTime(int hr, int min) {
        Time tme = new Time(hr, min, 0);//seconds by default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("hh:mm a");
        return formatter.format(tme);
    }

    private void showTimeFrom() {
        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(ActivityAddCosigneeInfo.this, R.style.MyTimePickerDialogTheme,
                new TimePickerDialog.OnTimeSetListener() {
                    String am_pm = "";


                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);


                        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                            am_pm = "PM";

                        fromTime = (datetime.get(Calendar.HOUR) == 0) ? "12" : datetime.get(Calendar.HOUR) + "";
                        if (datetime.get(Calendar.HOUR) < 10) {
                            NumberFormat f = new DecimalFormat("00");
                            fromTime = String.valueOf(f.format((datetime.get(Calendar.HOUR))));
                        }
                        if (datetime.get(Calendar.HOUR) > 10) {
                            fromTime = String.valueOf(datetime.get(Calendar.HOUR));
                        }
                        if (datetime.get(Calendar.MINUTE) < 10) {
                            NumberFormat f = new DecimalFormat("00");
                            minutes = String.valueOf(f.format((datetime.get(Calendar.MINUTE))));
                            Log.e("Minute", minutes);
                        }
                        if (datetime.get(Calendar.MINUTE) > 10) {
                            minutes = String.valueOf(datetime.get(Calendar.MINUTE));
                        }
                        fromTimeTv.setText(fromTime + ":" + minutes + " " + am_pm);
                    }
                }, hour, minute, false);
        timePickerDialog.show();
    }

    private void showTimeTill() {
        int hour = myCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = myCalendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(ActivityAddCosigneeInfo.this, R.style.MyTimePickerDialogTheme,
                new TimePickerDialog.OnTimeSetListener() {
                    String am_pm = "";

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar datetime = Calendar.getInstance();
                        datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        datetime.set(Calendar.MINUTE, minute);

                        if (datetime.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else if (datetime.get(Calendar.AM_PM) == Calendar.PM)
                            am_pm = "PM";

                        tillTime = (datetime.get(Calendar.HOUR) == 0) ? "12" : datetime.get(Calendar.HOUR) + "";
                        if (datetime.get(Calendar.HOUR) < 10) {
                            NumberFormat f = new DecimalFormat("00");
                            tillTime = String.valueOf(f.format((datetime.get(Calendar.HOUR))));
                        }

                        if (datetime.get(Calendar.HOUR) > 10) {
                            tillTime = String.valueOf(datetime.get(Calendar.HOUR));
                        }

                        if (datetime.get(Calendar.MINUTE) < 10) {
                            NumberFormat f = new DecimalFormat("00");
                            minutes = String.valueOf(f.format((datetime.get(Calendar.MINUTE))));
                        }
                        if (datetime.get(Calendar.MINUTE) > 10) {
                            minutes = String.valueOf(datetime.get(Calendar.MINUTE));
                        }
                        tillTv.setText(tillTime + ":" + minutes + " " + am_pm);
                    }
                }, hour, minute, false);
        timePickerDialog.show();
    }

    private void showDialogDate() {
        DatePickerDialog dialog = new DatePickerDialog(ActivityAddCosigneeInfo.this, R.style.MyTimePickerDialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                deliveryDateTv.setText(i1 + "/" + i2 + "/" + i + "");
            }
        }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    private void validateAddGoods() {
        String measure = measurementTv.getText().toString().trim();
        String goodDesc = goodDescEt.getText().toString().trim();
        String quantity = quantityEt.getText().toString().trim();
        String volume = volumeEt.getText().toString().trim();
        String weight = weightEt.getText().toString().trim();

        if (inputValidator.isNullOrEmpty(measure)) {
            toast("Please enter measurement type");
            return;
        }

        if (inputValidator.isNullOrEmpty(goodDesc)) {
            toast("Please enter good desc");
            return;
        }

        if (inputValidator.isNullOrEmpty(quantity)) {
            toast("Please enter quantity");
            return;
        }

        if (inputValidator.isNullOrEmpty(volume)) {
            toast("Please enter volume");
            return;
        }

        if (inputValidator.isNullOrEmpty(weight)) {
            toast("Please enter weight");
            return;
        }
        GoodsInfoAddObject goodsInfoAddObject = new GoodsInfoAddObject(quantity, quantity, volume, weight, goodDesc, measureCode, jobDetailDataObject.getStatus().getJobDataId(), measure);

        listAddedGoods.add(goodsInfoAddObject);


        adapterGoodsAdd = new AdapterGoodsAdd(ActivityAddCosigneeInfo.this, listAddedGoods, masterPojo, new AdapterGoodsAdd.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, GoodsInfoAddObject goodsInfoAddObject) {
                switch (view.getId()) {

                    case R.id.deltIcon:
                        listAddedGoods.remove(position);
                        adapterGoodsAdd.notifyDataSetChanged();
                        break;


                    case R.id.editIcon:
                        editDialogue.showDialog(ActivityAddCosigneeInfo.this, goodsInfoAddObject, position);
                        break;
                }
            }
        });

        clearGoodsView();

        rvGoodsInfo.setAdapter(adapterGoodsAdd);
        rvGoodsInfo.smoothScrollToPosition(listAddedGoods.size() - 1);
        adapterGoodsAdd.notifyDataSetChanged();

        Toast.makeText(getApplicationContext(), "Item added", Toast.LENGTH_SHORT).show();
    }


    private void clearGoodsView() {
        goodDescEt.setText("");
        quantityEt.setText("");
        volumeEt.setText("");
        weightEt.setText("");
        // setCursorDrawableColor(goodDescEt);
        goodDescEt.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(goodDescEt, InputMethodManager.SHOW_IMPLICIT);
        hideSoftKeyboard(ActivityAddCosigneeInfo.this);


    }


    public static void hideSoftKeyboard(Activity activity) {

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    private void validateFields() {
        String shift = shiftTv.getText().toString().trim();
        String deliveryDate = deliveryDateTv.getText().toString().trim();
        String fromTime = fromTimeTv.getText().toString().trim();
        String tillTime = tillTv.getText().toString().trim();
        String cosigneeName = cosigneeNameEt.getText().toString().trim();
        String email = emailEt.getText().toString().trim();
        String number = numberEt.getText().toString().trim();
        String deliveryAddress = deliveryEt.getText().toString().trim();
        String country = tvCountry.getText().toString().trim();

        String paymentType = paymentTypeTv.getText().toString().trim();
        String total = totalEt.getText().toString().trim();


        if (inputValidator.isNullOrEmpty(deliveryDate)) {
            toast("Please enter delivery date");
            return;
        }

        if (inputValidator.isNullOrEmpty(shift)) {
            toast("Please enter shift");
            return;
        }

        if (inputValidator.isNullOrEmpty(fromTime)) {
            toast("Please enter from time");
            return;
        }

        if (inputValidator.isNullOrEmpty(tillTime)) {
            toast("Please enter till time");
            return;
        }

        if (inputValidator.isNullOrEmpty(cosigneeName)) {
            toast("Please enter cosignee name");
            return;
        }

        if (inputValidator.isNullOrEmpty(email)) {
            toast("Please enter email");
            return;
        }

        if (!inputValidator.isValidEmail(email)) {
            toast("Please enter email");
            return;
        }

        if (inputValidator.isNullOrEmpty(number)) {
            toast("Please enter number");
            return;
        }

        if (inputValidator.isNullOrEmpty(deliveryAddress)) {
            toast("Please enter delivery");
            return;
        }

        if (inputValidator.isNullOrEmpty(country)) {
            toast("Please enter country");
            return;
        }

        if (inputValidator.isNullOrEmpty(paymentType)) {
            toast("Please select payment type");
            return;
        }


        if (inputValidator.isNullOrEmpty(total)) {
            toast("Please enter total");
            return;
        }


        for (int i = 0; i < paymentNChargesFieldArrayList.size(); i++) {

            if (paymentNChargesFieldArrayList.get(i).isEmpty()) {
                Toast.makeText(getApplicationContext(), "Fill all fields", Toast.LENGTH_SHORT).show();
                return;
            }

            Double amount = adapterPaymentNCharges.getValues(i);
            int indexHeader = adapterPaymentNCharges.getValuesHeader(i);
            Log.e("values", amount + " ");
            Log.e("values", indexHeader + " ");

            JobChargesInfo jobChargesInfo = new JobChargesInfo();
            jobChargesInfo.setAmount(amount + "");
            jobChargesInfo.setChargeId(indexHeader);
            jobChargesInfo.setJobDataId(jobDetailDataObject.getStatus().getJobDataId());
            jobChargesInfoArrayList.add(jobChargesInfo);

        }
        // Toast.makeText(getApplicationContext(), "All filled", Toast.LENGTH_SHORT).show();


        CosigneeInfo cosigneeInfo = new CosigneeInfo();
        cosigneeInfo.setTimeShift(shiftCode);
        cosigneeInfo.setDeliveryDate(getDate());
        cosigneeInfo.setFromTime(fromTime);
        cosigneeInfo.setTillTime(tillTime);
        cosigneeInfo.setCosigneeName(cosigneeName);
        cosigneeInfo.setEmail(email);
        cosigneeInfo.setNumber(number);
        cosigneeInfo.setDeliveryAddress(deliveryAddress);
        cosigneeInfo.setCountry(country);
        cosigneeInfo.setDestinationId(destinationId);

        try {
            if (jobDetailDataObject.getConsignee() == null) {
                cosigneeInfo.setJobAddrId(0);
            }
            cosigneeInfo.setJobAddrId(jobDetailDataObject.getConsignee().getJobAddrId());
        } catch (Exception e) {
            e.printStackTrace();
        }


        //TODO

        if (isManualFormFill) {
            cosigneeInfo.setAddressLine1(deliveryAddress);
            cosigneeInfo.setAccNumber(jobDetailDataObject.getShipper().getAccountNumber());
            cosigneeInfo.setCompanyName(jobDetailDataObject.getShipper().getCompanyName());
            cosigneeInfo.setAccountAddressId(jobDetailDataObject.getStatus().getAccountInformationId());
        } else {
            cosigneeInfo.setAddressLine1(cosigneeNameObj.getAddressLine1());
            cosigneeInfo.setAccNumber(cosigneeNameObj.getAccountNumber());
            cosigneeInfo.setCompanyName(cosigneeNameObj.getCompanyName());
            cosigneeInfo.setAccountAddressId(0);
        }

        cosigneeInfo.setAddressLine2(cosigneeNameObj.getAddressLine2());
        cosigneeInfo.setAddressLine3(cosigneeNameObj.getAddressLine3());
        cosigneeInfo.setAddressLine4(cosigneeNameObj.getAddressLine4());
        cosigneeInfo.setAddressLine4(cosigneeNameObj.getAddressLine4());
        cosigneeInfo.setInvoiceCurrency(cosigneeNameObj.getInvoiceCurrency());
        cosigneeInfo.setPostal_Code(cosigneeNameObj.getPostalCode());
        cosigneeInfo.setPostalCode(cosigneeNameObj.getPostalCode());

        cosigneeInfo.setCreatedby(driverObj.getId());
        cosigneeInfo.setCreateDate("");
        cosigneeInfo.setCountryCode(country);
        cosigneeInfo.setJobStatus(jobDetailDataObject.getStatus().getJobStatus());
        cosigneeInfo.setJobStatusId(jobDetailDataObject.getStatus().getJobStatusId());
        cosigneeInfo.setDriverId(driverObj.getId());
        cosigneeInfo.setCollectionAddr(jobDetailDataObject.getStatus().getConsigneeCollectionAddress() + "");
        cosigneeInfo.setJobType("Delivery");
        cosigneeInfo.setJobTypeId(jobDetailDataObject.getStatus().getJobTypeId());
        cosigneeInfo.setJobDataId(jobDetailDataObject.getStatus().getJobDataId());

        cosigneeInfo.setShipper("0");
        cosigneeInfo.setConsignee("-1");


        AddCosigneeInfoMaster addCosigneeInfo = new AddCosigneeInfoMaster(cosigneeInfo, jobChargesInfoArrayList, listAddedGoods);

        callApiSendCosigneeInfoMaster(addCosigneeInfo);

    }

    private void callApiSendCosigneeInfoMaster(AddCosigneeInfoMaster addCosigneeInfoMaster) {
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        progressVisiblityVisible();
        Call<WebResponse<AddCosigneeInfoMaster>> call = apiService.sendCosigneeInfoMaster(addCosigneeInfoMaster);

        call.enqueue(new Callback<WebResponse<AddCosigneeInfoMaster>>() {
            @Override
            public void onResponse(Call<WebResponse<AddCosigneeInfoMaster>> call, Response<WebResponse<AddCosigneeInfoMaster>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }


                if (response.code() == 200) {
                    Toast.makeText(getApplicationContext(), "Sucessfully Added", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ActivityAddCosigneeInfo.this, ActivityJobStatus.class);
                    intent.putExtra("showSlider", true);
                    finish();
                    startActivity(intent);
                }

            }

            @Override
            public void onFailure(Call<WebResponse<AddCosigneeInfoMaster>> call, Throwable t) {
                progressVisiblityGone();
            }
        });
    }

    private void toast(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }


    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat dateFormat = new SimpleDateFormat(myFormat, Locale.US);
        deliveryDateTv.setText(dateFormat.format(myCalendar.getTime()));
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }

    public class ViewDialogEdit {

        public void showDialog(Activity activity, GoodsInfoAddObject goodsInfoAddObject, int position) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_edit_good_info);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


            EditText goodDescEtDialog = (EditText) dialog.findViewById(R.id.goodDescEt);
            EditText quantityEtDialog = (EditText) dialog.findViewById(R.id.quantityEt);
            EditText volumeEtDialog = (EditText) dialog.findViewById(R.id.volumeEt);
            EditText weightEtDialog = (EditText) dialog.findViewById(R.id.weightEt);
            TextView measurementTvDialog = (TextView) dialog.findViewById(R.id.measurementTv);
            LinearLayout btnCancelDialog = (LinearLayout) dialog.findViewById(R.id.btnCancel);
            LinearLayout btnUpdateDialog = (LinearLayout) dialog.findViewById(R.id.btnUpdate);


            try {
                measurementTvDialog.setText(goodsInfoAddObject.getMeasurementTypeDesc());
                goodDescEtDialog.setText(goodsInfoAddObject.getGoodsDesc());
                goodDescEtDialog.setText(goodsInfoAddObject.getGoodsDesc());
                quantityEtDialog.setText(goodsInfoAddObject.getQuantity());
                volumeEtDialog.setText(goodsInfoAddObject.getVolume());
                weightEtDialog.setText(goodsInfoAddObject.getWeight());
            } catch (Exception e) {
                e.printStackTrace();
            }


            measurementTvDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    spinnerMeasureMent.showSpinerDialog();
                }
            });


            btnUpdateDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String measure = measurementTvDialog.getText().toString().trim();
                    String goodDesc = goodDescEtDialog.getText().toString().trim();
                    String quantity = quantityEtDialog.getText().toString().trim();
                    String volume = volumeEtDialog.getText().toString().trim();
                    String weight = weightEtDialog.getText().toString().trim();

                    if (inputValidator.isNullOrEmpty(measure)) {
                        toast("Please enter measurement type");
                        return;
                    }

                    if (inputValidator.isNullOrEmpty(goodDesc)) {
                        toast("Please enter good desc");
                        return;
                    }

                    if (inputValidator.isNullOrEmpty(quantity)) {
                        toast("Please enter quantity");
                        return;
                    }

                    if (inputValidator.isNullOrEmpty(volume)) {
                        toast("Please enter volume");
                        return;
                    }

                    if (inputValidator.isNullOrEmpty(weight)) {
                        toast("Please enter weight");
                        return;
                    }

                    GoodsInfoAddObject goodsInfoAddObject = new GoodsInfoAddObject(quantity, quantity, volume, weight, goodDesc, measureCode, jobDetailDataObject.getStatus().getJobDataId(), measure);
                    listAddedGoods.set(position, goodsInfoAddObject);
                    adapterGoodsAdd.notifyDataSetChanged();
                    dialog.dismiss();

                }
            });


            btnCancelDialog.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            dialog.show();
        }
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        // disableEnableControls(true, mainRl);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
        //   disableEnableControls(false, mainRl);
    }
}