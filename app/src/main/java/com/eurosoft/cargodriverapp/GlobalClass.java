package com.eurosoft.cargodriverapp;


import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleEventObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LifecycleRegistry;
import androidx.lifecycle.LifecycleRegistryOwner;
import androidx.lifecycle.OnLifecycleEvent;
import com.sumup.merchant.api.SumUpState;

import io.socket.client.IO;
import io.socket.client.Socket;

public class GlobalClass extends Application   {

	// public ArrayList<fojModel> listofFojJobs = null;
	@OnLifecycleEvent(Lifecycle.Event.ON_STOP)
	public void onAppBackgrounded() {
		//App in background
	}

	@OnLifecycleEvent(Lifecycle.Event.ON_START)
	public void onAppForegrounded() {
//		Log.e("MYMESSAGE","App in foreground");
		// App in foreground
	}

	@Override
	public void onCreate() {
		super.onCreate();
		try {
			SumUpState.init(this);
		}catch (Exception e){

		}
	}

	private boolean IsExtraChargesFinish;

	public boolean getIsExtraFinish() {

		return IsExtraChargesFinish;
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		Log.e("TAG","App memory is going low");
	}

	public void setIsExtraFinish(boolean IsExtraFinish) {

		IsExtraChargesFinish = IsExtraFinish;

	}





	public void createFloatingWidget() {
		//Check if the application has draw over other apps permission or not?
		//This permission is by default available for API<23. But for API > 23
		//you have to ask for the permission in runtime.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
			//If the draw over permission is not available open the settings screen
			//to grant the permission.

		} else {
			//If permission is granted start floating widget service
		}

	}

}