package com.eurosoft.cargodriverapp;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.AdapterCosigneeNDelivery;
import com.eurosoft.cargodriverapp.Adapter.AdapterGoodsDetail;
import com.eurosoft.cargodriverapp.Fragment.BottomSheetImageList;
import com.eurosoft.cargodriverapp.Fragment.BottomSheetSignature;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.Cosignee;
import com.eurosoft.cargodriverapp.Pojo.ImageSheetModel;
import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.JobDetailDataObject;
import com.eurosoft.cargodriverapp.Pojo.JobNotificationStatus;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PaymentGatewayDetail;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobObject;
import com.eurosoft.cargodriverapp.Pojo.ResponseJobNotification;
import com.eurosoft.cargodriverapp.Pojo.SendMUultipleImages;
import com.eurosoft.cargodriverapp.Pojo.SendSignature;
import com.eurosoft.cargodriverapp.SlideView.SlideView;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.ImageUtil;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.paymentgateway.SumupPayment;
import com.fxn.stash.Stash;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.sumup.merchant.Models.TransactionInfo;
import com.sumup.merchant.api.SumUpAPI;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivtityPlanJobStatus extends AppCompatActivity implements BottomSheetSignature.BottomSignatureSucessListener {

    private LoginResponse driverObj;
    private MasterPojo masterPojo;
    private SlideView acceptSlideView;
    private int currentStatus;
    private boolean jobRunning;
    private TextView jobIdRef;
    private TextView currentStatusTv;
    private String jobIdToSend;
    private TextView origin, desitination, pickup,paynow, dropOff, totalWeight, totalVolume, totalQty, name, address, textHeader, goodsinfo, totalgoodsinfo, measurementheading, descheading, qtyheading, volumeheading, weightheading, totalWeightheading, totalQtyheading, totalVolumeheading;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout mainRl;
    private AdapterGoodsDetail adapterGoodsDetail;
    private AdapterCosigneeNDelivery adapterCosigneeNDelivery;
    private RecyclerView rvGoods;
    private RecyclerView rvCosigneesNDelivery;
    private LinearLayoutManager layoutManager;
    private RelativeLayout expandRl;
    private String jobType = "";
    private boolean isSignatureAdded = false;
    private BottomSheetSignature bottomSheetSignature;
    private ProgressBar pgsBar;
    TextView Wait;
    private Handler hdlr = new Handler();
    int j = 0;
    private ImageView phone;
    private RelativeLayout bottomSwipeRl,serviceTypebg;
    private ImageView bckBtn;
    private Bundle extras;
    private boolean value;
    private ViewDialog alertDialoge;
    private Button btnNavigate;
    private Double lat = 28.710460;
    private Double longitude = 77.098280;
    private String latitude, longitudes;
    private String[] deliverList, collectList;
    private LinearLayoutManager layoutManagerHorizontal;
    private TextView paymentType;
    private ImageView profile;
    private static final int CAMERA_REQUEST = 1888;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private CardView cardAddInfo;
    private JobDetailDataObject jobDetailDataModel;
    private boolean isFillLater = false;
    private TextView expand;
    private boolean isImagesUploaded;

    private void AskForConsignee(){

        new AlertDialog.Builder(ActivtityPlanJobStatus.this)
                .setMessage("Do you want to add the consignee details")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(ActivtityPlanJobStatus.this, ActivityAddCosigneeInfo.class);
                        Stash.put(Constants.PASS_JOB_DETAIL_OBJ, jobDetailDataModel);
                        finish();
                        // i.putExtra(Constants.PASS_JOB_DETAIL_OBJ, jobDetailDataModel);
                        startActivity(i);
                    }
                })
                .setNegativeButton("FillLater", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        fillater = 1;
                    }
                }).show();

    }
    int fillater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_job_status);
        Stash.init(this);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        alertDialoge = new ViewDialog();
        initViews();
        getBundle();
        //callApiGetJobDetail();
    }


    private void getBundle() {

        extras = getIntent().getExtras();
        if (extras != null) {
            value = extras.getBoolean("showSlider");
            PlannedJobObject plannedJobObject = (PlannedJobObject) getIntent().getSerializableExtra("object");

            textHeader.setText("Job Details");

            jobIdRef.setText(masterPojo.getReference() + plannedJobObject.getJobRefNo() + "");
            jobType = plannedJobObject.getJobType();
            //setData(jobDetailData);
            jobIdToSend = plannedJobObject.getId() + "";

            Log.e("jobIdToSend", jobIdToSend);
            callApiGetJobDetail();

            bckBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivtityPlanJobStatus.this, HomeActivity.class);
                    intent.putExtra("isReloadTrue", false);
                    startActivity(intent);
                    finish();
                }
            });

            if (value == false) {
                bottomSwipeRl.setVisibility(View.GONE);
                btnNavigate.setVisibility(View.GONE);
//                camera.setVisibility(View.GONE);
            } else {
                bottomSwipeRl.setVisibility(View.VISIBLE);
                btnNavigate.setVisibility(View.VISIBLE);
//                camera.setVisibility(View.VISIBLE);
            }

        } else {
            Log.e("NotCalled", "NotCalled");
        }
    }

    private void initViews() {
        acceptSlideView = findViewById(R.id.acceptSlide);
        jobIdRef = findViewById(R.id.jobIdRef);
        currentStatusTv = findViewById(R.id.currentStatus);
        origin = findViewById(R.id.origin);
        desitination = findViewById(R.id.desitination);
        btnNavigate = findViewById(R.id.btnNavigate);
        paymentType = findViewById(R.id.paymentType);
        ((TextView) findViewById(R.id.addCosignee)).setText(jobDetailDataModel.getConsignee()==null?"Add Consignee Info":"Manage Consignee Info");
        paymentType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askforPaymentType(false);
            }
        });
        Wait = (TextView) findViewById(R.id.waitForthis);

        totalWeight = findViewById(R.id.totalWeight);
        totalVolume = findViewById(R.id.totalVolume);
        bottomSwipeRl = findViewById(R.id.bottomSwipeRl);
        totalQty = findViewById(R.id.totalQty);
        rvGoods = findViewById(R.id.rvGoods);
        name = findViewById(R.id.name);
        address = findViewById(R.id.address);
        expandRl = findViewById(R.id.expandRl);
        bckBtn = findViewById(R.id.bckBtn);
        expand = findViewById(R.id.expand);
        serviceTypebg = findViewById(R.id.serviceTypebg);
        rvCosigneesNDelivery = findViewById(R.id.rVCosigneeNShipper);
        pickup = (TextView) findViewById(R.id.pickup);
        dropOff = (TextView) findViewById(R.id.dropOff);
//        camera = (ImageView) findViewById(R.id.camera);
        cardAddInfo = (CardView) findViewById(R.id.cardAddInfo);
        paynow = (TextView) findViewById(R.id.paynow);
        paynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiGetPaymentDetails();
            }
        });
        /////////////////////////////////////////Language Changes//////////////////////////////////////////////////////
        textHeader = findViewById(R.id.textHeader);
        goodsinfo = findViewById(R.id.goodsinfo);
        totalgoodsinfo = findViewById(R.id.totalgoodsinfo);
        weightheading = findViewById(R.id.weightheading);
        measurementheading = findViewById(R.id.measurementheading);
        volumeheading = findViewById(R.id.volumeheading);
        descheading = findViewById(R.id.descheading);
        qtyheading = findViewById(R.id.qtyheading);
        textHeader.setText(masterPojo.getCurrentJob());
        goodsinfo.setText(masterPojo.getGoodsInformation());
        weightheading.setText(masterPojo.getWeight());
        measurementheading.setText(masterPojo.getMeasurement());
        volumeheading.setText(masterPojo.getVolume());
        //  descheading.setText(masterPojo.getDesc());
        qtyheading.setText(masterPojo.getQty());
        acceptSlideView.setText(masterPojo.getSwipeToAccept());
        btnNavigate.setText(masterPojo.getNavigate());
        pickup.setText(masterPojo.getPickup());
        dropOff.setText(masterPojo.getDropoff());
        Wait.setText(masterPojo.getPleaseWait());
        pgsBar = (ProgressBar) findViewById(R.id.progress_bar1);

        phone = (ImageView) findViewById(R.id.phone);


        rvGoods.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(ActivtityPlanJobStatus.this, LinearLayoutManager.VERTICAL, true);
        rvGoods.setLayoutManager(layoutManager);


        rvCosigneesNDelivery.setHasFixedSize(true);
        layoutManagerHorizontal = new LinearLayoutManager(ActivtityPlanJobStatus.this, LinearLayoutManager.HORIZONTAL, false);
        rvCosigneesNDelivery.setLayoutManager(layoutManagerHorizontal);
//        jobId.setText(masterPojo.getReference() + Stash.getString(Constants.JOB_ID));

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);


//        camera.setOnClickListener(new View.OnClickListener() {
//
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View view) {
//
//                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                    // requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
//                    //ActivityCompat.requestPermissions(ActivtityPlanJobStatus.this, new String[] {Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
//                    requestMultiplePermissions();
//                } else {
//                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                    startActivityForResult(cameraIntent, CAMERA_REQUEST);
//
//                }
//            }
//        });


        acceptSlideView.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {

                Log.e("JobType", jobType);
                Log.e("CurrentStatus", currentStatus + "");
                if (currentStatus == 7 ) {
                    AskForImages();
                }


                else if (currentStatus == 6) {
                    if ((jobType.equalsIgnoreCase("Collection")||jobType.equalsIgnoreCase("Collection Only")) && isSignatureAdded == false) {
                        AskForImages();
                        return;
                    } else {
                       setupStatusWork();
                    }
                }else{setupStatusWork();}
            }
        });

        cardAddInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivtityPlanJobStatus.this, ActivityAddCosigneeInfo.class);

                Stash.put(Constants.PASS_JOB_DETAIL_OBJ, jobDetailDataModel);
                // i.putExtra(Constants.PASS_JOB_DETAIL_OBJ, jobDetailDataModel);
                startActivity(i);
            }
        });
    }


    private void requestMultiplePermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {  // check if all permissions are granted
                            Toast.makeText(getApplicationContext(), "All permissions are granted by user!", Toast.LENGTH_SHORT).show();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) { // check for permanent denial of any permission
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted--", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                ActivityCompat.requestPermissions(ActivtityPlanJobStatus.this, new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
            }
        }
    }
    private ArrayList<String> imagesList = new ArrayList<>();
    BottomSheetImageList  bottomSheetImageList;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            try {


                Bitmap photo = (Bitmap) data.getExtras().get("data");
                //camera.setImageBitmap(photo);
                String base64String = ImageUtil.convert(photo);

                if(bottomSheetImageList == null){
                    imagesList.add(base64String);
                    bottomSheetImageList = new BottomSheetImageList(new ImageSheetModel(ActivtityPlanJobStatus.this,imagesList));
                    bottomSheetImageList.show(((AppCompatActivity) ActivtityPlanJobStatus.this).getSupportFragmentManager(),"");
                }else{
                    imagesList = bottomSheetImageList.getImageList();
                    imagesList.add(base64String);
                    bottomSheetImageList.RefreshImageList(imagesList);
                }
//                adapterImagePicker = new AdapterImagePicker(ActivityJobStatus.this, imagesList, new AdapterImagePicker.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        imagesList.remove(position);
//                        adapterImagePicker.notifyDataSetChanged();
//                    }
//                });
//                imageRecycler.setHasFixedSize(true);
//                imageRecycler.setAdapter(adapterImagePicker);

                //TODO
                //callApiSendCapterImage(jobIdToSend, base64String);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else    if (requestCode == 1) {
            if (resultCode == SumUpAPI.Response.ResultCode.SUCCESSFUL || resultCode == SumUpAPI.Response.ResultCode.ERROR_ALREADY_LOGGED_IN) {
                double totalAmount  =  jobDetailDataModel.getStatus().getTotalAmount();
                if(Stash.getFloat(jobDetailDataModel.getStatus().getId()+"_totalAmount")>0){
                    totalAmount = Stash.getFloat(jobDetailDataModel.getStatus().getId()+"_totalAmount");
                }

                SumupPayment.getInstance().sumUpCheckout(ActivtityPlanJobStatus.this,totalAmount);
            }
        }
        else    if (requestCode == 2) {
            if (resultCode == SumUpAPI.Response.ResultCode.SUCCESSFUL) {

                if (data != null) {

                    Bundle extra = data.getExtras();
                    if (extra != null && extra.getString(SumUpAPI.Response.TX_CODE) != null && !extra.getString(SumUpAPI.Response.TX_CODE).equals("")) {
                        String txCode = extra.getString(SumUpAPI.Response.TX_CODE);

                        try {
                            TransactionInfo transactionInfo = (TransactionInfo) extra.get(SumUpAPI.Response.TX_INFO);
                            txCode += "\r\n" + transactionInfo.getAmount() + "\r\n" + transactionInfo.getCard().getLast4Digits();

                        } catch (Exception e) {

                        }


                        Stash.put(jobIdToSend+"transId",txCode);
                        paymentStatus.setText("Payment Status:(Paid)");
                        paymentStatus.setTextColor(getResources().getColor(R.color.white));

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActivtityPlanJobStatus.this);
                        alertDialog.setTitle("Payment successful!")

                                .setMessage("Transaction id: " + txCode)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // TODO
                                        arg0.dismiss();
                                        setupStatusWork();
                                    }
                                })
                                .setCancelable(false)
                                .create()
                                .show();


                    } else {
                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActivtityPlanJobStatus.this);
                        alertDialog.setTitle("Payment Failed!")
//
//								.setMessage("Transaction id: " + txCode)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        // TODO
//									Intent i = new Intent(ConnectPaymentSystem.this, NotificationDetail.class);
//									startActivity(i);
                                        arg0.dismiss();
                                    }
                                })
                                .setCancelable(false)
                                .create()
                                .show();
                    }
//					mTxInfo.setText(transactionInfo == null ? "" : "Transaction Info : " + transactionInfo);
                } else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActivtityPlanJobStatus.this);
                    alertDialog.setTitle("Payment Failed!")
//
//								.setMessage("Transaction id: " + txCode)
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface arg0, int arg1) {
                                    // TODO
//									Intent i = new Intent(ConnectPaymentSystem.this, NotificationDetail.class);
//									startActivity(i);
                                    arg0.dismiss();
                                }
                            })
                            .setCancelable(false)
                            .create()
                            .show();
                }
            } else {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(ActivtityPlanJobStatus.this);
                alertDialog.setTitle("Payment Failed!")
//
//								.setMessage("Transaction id: " + txCode)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                // TODO
//									Intent i = new Intent(ConnectPaymentSystem.this, NotificationDetail.class);
//									startActivity(i);
                                arg0.dismiss();
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            }
        }
    }

    private void openSignatureSheet(){
        bottomSheetSignature = BottomSheetSignature.newInstance();
        bottomSheetSignature.show(((AppCompatActivity) ActivtityPlanJobStatus.this).getSupportFragmentManager(),
                "");
    }
    private void callApiSendStatus(int i, String status) {

        JobNotificationStatus jobNotificationStatus;

        if(i == 7){
            jobNotificationStatus = new JobNotificationStatus(jobIdToSend, driverObj.getId() + "", i + "", status + "", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID), "DriverApp,Android",Stash.getString(jobIdToSend+"transId"),fillater);
        }else {
            jobNotificationStatus = new JobNotificationStatus(jobIdToSend, driverObj.getId() + "", i + "", status + "", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID), "DriverApp,Android");
        }
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<ResponseJobNotification>> call = apiService.sendStatus(jobNotificationStatus);

        call.enqueue(new Callback<WebResponse<ResponseJobNotification>>() {
            @Override
            public void onResponse(Call<WebResponse<ResponseJobNotification>> call, Response<WebResponse<ResponseJobNotification>> response) {
                if (response.code() != 200 || response.body() == null) {
                    ResetProgressonResponse();
                    return;
                }
                if (!response.body().getSuccess()) {


                    if (i == 19) {
                        Intent intent = new Intent(ActivtityPlanJobStatus.this, HomeActivity.class);
                        startActivity(intent);

                        Stash.put(Constants.JOB_IS_RUNNING, false);
                        finish();
                    }

                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();

                    if (currentStatus == 5) {
                        changeStatus(20);
                        currentStatus = 20;
                        ResetProgressonResponse();



                        return;
                    }
                    if (currentStatus == 20) {
                        changeStatus(6);
                        currentStatus = 6;
                        ResetProgressonResponse();


                        if (jobType.equalsIgnoreCase("Collection")||jobType.equalsIgnoreCase("Collection Only")) {
                            if(!isImagesUploaded){
                                AskForImages();
                            }else if(!isSignatureAdded){
                                openSignatureSheet();
                            }

                        }




                        return;
                    }

                    if (currentStatus == 6) {
                        changeStatus(7);
                        currentStatus = 7;
                        ResetProgressonResponse();






                        if (jobType.equalsIgnoreCase("Delivery")||jobType.equalsIgnoreCase("Delivery Only")) {

                            if(!isImagesUploaded){
                                AskForImages();
                            }else if(!isSignatureAdded){
                                openSignatureSheet();
                            }
                            Stash.put(Constants.JOB_IS_RUNNING, false);
                            return;
                        }
                        if (currentStatus == 7 && jobType.equalsIgnoreCase("Collection") || jobType.equalsIgnoreCase("Collection Only")) {
                            Stash.put(Constants.IS_LANG_CHANGED, true);
                            finish();
                            return;
                        }

                        return;
                    }


                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    ResetProgressonResponse();
                    Toast.makeText(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<WebResponse<ResponseJobNotification>> call, Throwable t) {
                ResetProgressonResponse();
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void askforPaymentType(boolean isStatus) {

        selectedPayMode = -1;
        LayoutInflater layoutInflater = LayoutInflater.from(ActivtityPlanJobStatus.this);

        View promptView = layoutInflater.inflate(R.layout.driverlist, null);

        AlertDialog.Builder alertDialogBuilderr = new AlertDialog.Builder(ActivtityPlanJobStatus.this);

        alertDialogBuilderr.setView(promptView);

        // addItemsOnNv_Spinner();

        ListView lv_Paymenttyep = (ListView) promptView.findViewById(R.id.lv_Driver);
        final List<String> lstPaymenttyep = new ArrayList<String>();
        lstPaymenttyep.clear();



        lstPaymenttyep.add(isStatus?"Paid":"Cash");
        lstPaymenttyep.add(isStatus?"Not Paid":"Card");

        ArrayAdapter<String> adb = new ArrayAdapter<String>(ActivtityPlanJobStatus.this, R.layout.checkboxrow, lstPaymenttyep) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                RadioButton text = (RadioButton) super.getView(position, convertView, parent);
                if(isStatus) {
                    if ((jobDetailDataModel.getStatus().getPaid()== 1&&lstPaymenttyep.get(position).toLowerCase().equals("paid"))||jobDetailDataModel.getStatus().getPaid()== 0&&lstPaymenttyep.get(position).toLowerCase().equals("not paid"))
                        text.setChecked(true);
                    else
                        text.setChecked(false);
                }else {
                    if (jobDetailDataModel.getStatus().getPaymentMode()!=null&&jobDetailDataModel.getStatus().getPaymentMode().trim().toLowerCase().equals(lstPaymenttyep.get(position).toLowerCase()))
                        text.setChecked(true);
                    else
                        text.setChecked(false);
                }
                return text;


            }
        };

        lv_Paymenttyep.setAdapter(adb);

        alertDialogBuilderr.setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                if(selectedPayMode>=0){
                    if(isStatus) {
                        paymentStatus.setText("Payment Status:"+lstPaymenttyep.get(selectedPayMode));
                        paymentStatus.setTextColor(getResources().getColor(lstPaymenttyep.get(selectedPayMode).toLowerCase().equals("paid")?R.color.white:R.color.red));
                        jobDetailDataModel.getStatus().setPaid(lstPaymenttyep.get(selectedPayMode).toLowerCase().equals("paid")?1:0);
                    }else{
                        jobDetailDataModel.getStatus().setPaymentMode(lstPaymenttyep.get(selectedPayMode));
                        paymentType.setText("Payment Type:"+lstPaymenttyep.get(selectedPayMode));
                    }

                }
/**
 * 	Date: 27-July-2016
 * 	END ->
 */
            }
        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();

            }
        });

        final AlertDialog alertDe = alertDialogBuilderr.create();

        alertDe.setTitle("Please select Payment Mode");
        alertDe.show();
        lv_Paymenttyep.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                jobDetailDataModel.getStatus().setPaymentMode(lstPaymenttyep.get(position));
                selectedPayMode = position;
                for (int i = 0; i < parent.getChildCount(); i++) {
                    View child = parent.getChildAt(i);
                    if (child instanceof RadioButton)
                        ((RadioButton) child).setChecked(false);
                }
                ((RadioButton) view).setChecked(true);
            }
        });
    }
    int selectedPayMode = -1;
    @Override
    public void onMainSignatureSucessClicked(String bytes) {
        isSignatureAdded = true;
        callApiSendImage(jobIdToSend, bytes);
    }
    TextView paymentStatus ;
    private void setupPaymentStatus(int status){
        paymentStatus = findViewById(R.id.paymentStatus);
        paymentStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askforPaymentType(true);
            }
        });
        paymentStatus.setText(status==1?"(Paid)":"(Not Paid)");
        if(status==0) {
            paymentStatus.setTextColor(getResources().getColor(R.color.red));
        }
    }
    private void callApiGetJobDetail() {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<JobDetailDataObject>> call = apiService.getJobDetailsForAndroidId(jobIdToSend + "");

        call.enqueue(new Callback<WebResponse<JobDetailDataObject>>() {
            @Override
            public void onResponse(Call<WebResponse<JobDetailDataObject>> call, Response<WebResponse<JobDetailDataObject>> response) {


                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    progressVisiblityGone();

                    try {
                        jobType = response.body().getData().getStatus().getJobType();
                        jobIdRef.setText(masterPojo.getReference() + response.body().getData().getStatus().getJobRefNo() + "");
                        if (response.body().getData().getStatus() != null) {
                            jobDetailDataModel = response.body().getData();
                            setupPaymentStatus(jobDetailDataModel.getStatus().getPaid());
                            currentStatus = response.body().getData().getStatus().getJobStatusId();
                            changeStatus(currentStatus);
                            setData(response.body().getData());
                            // btnNavigate.setVisibility(View.VISIBLE);
                        } else {
                            // btnNavigate.setVisibility(View.GONE);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<WebResponse<JobDetailDataObject>> call, Throwable t) {
                progressVisiblityGone();
            }
        });

    }


    private void changeStatus(int status) {
        if(status!= 20&&status>5){
            if(jobDetailDataModel!=null&&jobDetailDataModel.getStatus().getPaymentMode().toLowerCase().trim().equals("card")) {
                paynow.setVisibility(View.VISIBLE);
            }
        }
        if (status == 5) {
            acceptSlideView.setText(masterPojo.getSwipeToArrive());
        }  if (status == 20) {
            acceptSlideView.setText(masterPojo.getPresstoWC());
        }

        else if (status == 6) {
            if (jobType.equalsIgnoreCase("Collection") || jobType.equalsIgnoreCase("Collection Only")) {
                acceptSlideView.setText("Collected");
            } else {
                acceptSlideView.setText(masterPojo.getPresstodeliver());
            }
        }

    }


    private void setData(JobDetailDataObject data) {
        try {

            jobType = data.getStatus().getJobType();
            serviceTypebg.getBackground().setColorFilter(Color.parseColor(data.getStatus().getColorCode()), PorterDuff.Mode.SRC_ATOP);
            expand.setText(jobDetailDataModel.getStatus().getServiceDesc());
            serviceTypebg.setVisibility(View.VISIBLE);
            paymentType.setText(masterPojo.getPaymentType() + ": " + data.getStatus().getPaymentMode());

            if (data.getStatus().getFillLater() == null) {
                isFillLater = false;
            } else {
                isFillLater = data.getStatus().getFillLater();
            }
            if (isFillLater) {
                cardAddInfo.setVisibility(View.VISIBLE);
            } else {
                cardAddInfo.setVisibility(View.VISIBLE);
            }
            if (data.getStatus().getJobType().equalsIgnoreCase("Collection")) {
                Log.e("Collection", "Collection");
                extractedForShipper(data);
            } else if (data.getStatus().getJobType().equalsIgnoreCase("Delivery")) {
                Log.e("Delivery", "Delivery");
                extractedForDelivery(data);
            } else if (data.getStatus().getJobType().equalsIgnoreCase("Delivery Only")) {
                extractedForDeliveryOnly(data);
            } else if (data.getStatus().getJobType().equalsIgnoreCase("Collection Only")) {
                extractedForCollectionOnly(data);
            }
            changeStatus(currentStatus);

        } catch (Exception e) {
            Log.e("Catch", "Problem is here");
            e.printStackTrace();
        }


    }
    private void AskForImages(){
        if(!isImagesUploaded) {
            new AlertDialog.Builder(ActivtityPlanJobStatus.this)
                    .setMessage("Do you want to upload the product images?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            gotoGallery();
                        }
                    })
                    .setNegativeButton("Skip", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            isImagesUploaded =true;
                            setupStatusWork();
                        }
                    }).show();
        }else{
            setupStatusWork();
        }

    }
    private void setupStatusWork(){
        if (currentStatus == 7 && isSignatureAdded == false) {
            Toast.makeText(getApplicationContext(), "Please add signature", Toast.LENGTH_SHORT).show();
            bottomSheetSignature = BottomSheetSignature.newInstance();
            bottomSheetSignature.show(((AppCompatActivity) ActivtityPlanJobStatus.this).getSupportFragmentManager(),
                    "");
        }

        if (currentStatus == 5) {
            Progress();
            callApiSendStatus(20, "Arrive");
        }
        else   if (currentStatus == 20) {
            Progress();
            callApiSendStatus(6, "With Customer");
        }
        else if (currentStatus == 6) {

            if(jobDetailDataModel!=null&&jobDetailDataModel.getStatus().getPaymentMode().toLowerCase().trim().equals("card")&&Stash.getString(jobIdToSend+"transId").equals("")) {
                callApiGetPaymentDetails();
                return;
            }
            if (isFillLater&&fillater==0) {
                AskForConsignee();
            }
            if (jobType.equalsIgnoreCase("Collection")||jobType.equalsIgnoreCase("Collection Only")) {
                if(!isImagesUploaded){
                    AskForImages();
                    return;
                }else if(!isSignatureAdded){
                    openSignatureSheet();
                    return;
                }
            }
            Progress();

//            if (isFillLater) {
//                callApiSendStatus(19, "Fill Later");
//            } else {
            callApiSendStatus(7, jobType.equalsIgnoreCase("Collection")||jobType.equalsIgnoreCase("Collection Only")?"Collect":"Deliver");
//            }


        }
    }
    private void setupPaymentFlow(PaymentGatewayDetail paymentGatewayDetail){
        if(paymentGatewayDetail.getGatewayName().trim().equalsIgnoreCase("sumup")){
            SumupPayment.getInstance().sumUpLogin(paymentGatewayDetail.getMerchantId(),ActivtityPlanJobStatus.this);
        }
    }
    private void callApiGetPaymentDetails() {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<PaymentGatewayDetail>> call = apiService.getPaymentGatewayDetails(jobIdToSend + "");

        call.enqueue(new Callback<WebResponse<PaymentGatewayDetail>>() {
            @Override
            public void onResponse(Call<WebResponse<PaymentGatewayDetail>> call, Response<WebResponse<PaymentGatewayDetail>> response) {


                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    progressVisiblityGone();

                    try {
                        setupPaymentFlow(response.body().getData());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    Toast.makeText(ActivtityPlanJobStatus.this,response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<WebResponse<PaymentGatewayDetail>> call, Throwable t) {
                progressVisiblityGone();
            }
        });

    }
    public void gotoGallery(){
        if (getPackageManager().checkPermission(Manifest.permission.CAMERA,getPackageName()) != PackageManager.PERMISSION_GRANTED) {
            // requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);

            //ActivityCompat.requestPermissions(ActivtityPlanJobStatus.this, new String[] {Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
            requestMultiplePermissions();
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);

        }
    }

    private void extractedForCollectionOnly(JobDetailDataObject data) {

        adapterGoodsDetail = new AdapterGoodsDetail(ActivtityPlanJobStatus.this, 0, data.getGoodinfo(), new AdapterGoodsDetail.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });
        rvGoods.setAdapter(adapterGoodsDetail);
        adapterGoodsDetail.notifyDataSetChanged();
        origin.setText(data.getShipper().getCollectionAddr());
        desitination.setText(data.getShipper().getDeliveryAddr());


        ArrayList<Cosignee> arrayList = new ArrayList();


        Cosignee cosignee = new Cosignee();

        cosignee.setHeading("Shipper Info");
        cosignee.setFirstName(data.getShipper().getFirstName());
        cosignee.setCollectionAddr(data.getShipper().getCollectionAddr());
        cosignee.setPhone(data.getShipper().getPhone());
        cosignee.setCollectionLatLng(data.getShipper().getCollectionLatLng());
        try {
            cosignee.setCollectionLatLng(data.getShipper().getCollectionLatLng());
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            cosignee.setDeliveryLatLng(data.getShipper().getCollectionLatLng());
        } catch (Exception e) {
            e.printStackTrace();
        }
        cosignee.setCosignee(false);
        arrayList.add(cosignee);

        adapterCosigneeNDelivery = new AdapterCosigneeNDelivery(ActivtityPlanJobStatus.this, data.getGoodsQtyStatus(), masterPojo, arrayList, new AdapterCosigneeNDelivery.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });

        rvCosigneesNDelivery.setAdapter(adapterCosigneeNDelivery);
        adapterCosigneeNDelivery.notifyDataSetChanged();

        if (data.getGoodinfo().size() < 3) {
            expandRl.setVisibility(View.INVISIBLE);
        } else {
            expandRl.setVisibility(View.VISIBLE);
        }


        expandRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Stash.put(Constants.LIST_GOODS, data.getGoodinfo());
                startActivity(new Intent(ActivtityPlanJobStatus.this, ActivityExpandedGood.class));
            }
        });

        try {
            collectList = cosignee.getCollectionLatLng().split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }

        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppStatus.getInstance(ActivtityPlanJobStatus.this).isOnline()) {
                    alertDialoge.showDialog(ActivtityPlanJobStatus.this);

                } else {
                    Toast.makeText(ActivtityPlanJobStatus.this, masterPojo.getCheckInternet(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void extractedForShipper(JobDetailDataObject data) {

        Log.e("Extracted-Shipper", "Extracted-Shipper");
        adapterGoodsDetail = new AdapterGoodsDetail(ActivtityPlanJobStatus.this, 0, data.getGoodinfo(), new AdapterGoodsDetail.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });

        rvGoods.setAdapter(adapterGoodsDetail);
        adapterGoodsDetail.notifyDataSetChanged();
        origin.setText(data.getShipper().getCollectionAddr());
        desitination.setText(data.getShipper().getDeliveryAddr());

        ArrayList<Cosignee> arrayList = new ArrayList();


        Cosignee cosignee = new Cosignee();

        cosignee.setHeading("Shipper Info");
        cosignee.setFirstName(data.getShipper().getFirstName());
        cosignee.setCollectionAddr(data.getShipper().getCollectionAddr());
        cosignee.setPhone(data.getShipper().getPhone());
        cosignee.setCosignee(false);
        arrayList.add(cosignee);


        data.getConsignee().setCosignee(true);
        data.getConsignee().setHeading("Consignee Info");
        data.getConsignee().setCollectionAddr(data.getShipper().getDeliveryAddr());
        arrayList.add(data.getConsignee());

        adapterCosigneeNDelivery = new AdapterCosigneeNDelivery(ActivtityPlanJobStatus.this, data.getGoodsQtyStatus(), masterPojo, arrayList, new AdapterCosigneeNDelivery.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });

        rvCosigneesNDelivery.setAdapter(adapterCosigneeNDelivery);
        adapterCosigneeNDelivery.notifyDataSetChanged();


        try {
            collectList = data.getShipper().getCollectionLatLng().split(",");

        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            deliverList = data.getConsignee().getCollectionLatLng().split(",");

        } catch (Exception e) {
            e.printStackTrace();
        }


        if (data.getGoodinfo().size() < 3) {
            expandRl.setVisibility(View.INVISIBLE);
        } else {
            expandRl.setVisibility(View.INVISIBLE);
        }


        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppStatus.getInstance(ActivtityPlanJobStatus.this).isOnline()) {
                    Toast.makeText(getApplicationContext(), "Navigate", Toast.LENGTH_SHORT).show();
                    alertDialoge.showDialog(ActivtityPlanJobStatus.this);
                } else {
                    Toast.makeText(ActivtityPlanJobStatus.this, masterPojo.getCheckInternet(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void extractedForDeliveryOnly(JobDetailDataObject data) {

        adapterGoodsDetail = new AdapterGoodsDetail(ActivtityPlanJobStatus.this, 0, data.getGoodinfo(), new AdapterGoodsDetail.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });
        rvGoods.setAdapter(adapterGoodsDetail);
        adapterGoodsDetail.notifyDataSetChanged();
        origin.setText(data.getConsignee().getCollectionAddr());
        desitination.setText(data.getConsignee().getDeliveryAddr());


        ArrayList<Cosignee> arrayList = new ArrayList();
        data.getConsignee().setCosignee(true);
        data.getConsignee().setHeading("Consignee Info");
        arrayList.add(data.getConsignee());

        adapterCosigneeNDelivery = new AdapterCosigneeNDelivery(ActivtityPlanJobStatus.this, data.getGoodsQtyStatus(), masterPojo, arrayList, new AdapterCosigneeNDelivery.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });

        rvCosigneesNDelivery.setAdapter(adapterCosigneeNDelivery);
        adapterCosigneeNDelivery.notifyDataSetChanged();

        if (data.getGoodinfo().size() < 3) {
            expandRl.setVisibility(View.INVISIBLE);
        } else {
            expandRl.setVisibility(View.VISIBLE);
        }


        expandRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Stash.put(Constants.LIST_GOODS, data.getGoodinfo());
                startActivity(new Intent(ActivtityPlanJobStatus.this, ActivityExpandedGood.class));
            }
        });


        try {
            collectList = data.getConsignee().getCollectionLatLng().split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }
     try {
            deliverList = data.getConsignee().getDeliveryLatLng().split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }


        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppStatus.getInstance(ActivtityPlanJobStatus.this).isOnline()) {
                    alertDialoge.showDialog(ActivtityPlanJobStatus.this);

                } else {
                    Toast.makeText(ActivtityPlanJobStatus.this, masterPojo.getCheckInternet(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void extractedForDelivery(JobDetailDataObject data) {

        Log.e("Extracted-Delivery", "Extracted-Delivery");
        adapterGoodsDetail = new AdapterGoodsDetail(ActivtityPlanJobStatus.this, 0, data.getGoodinfo(), new AdapterGoodsDetail.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });
        rvGoods.setAdapter(adapterGoodsDetail);
        adapterGoodsDetail.notifyDataSetChanged();
        origin.setText(data.getConsignee().getCollectionAddr());
        desitination.setText(data.getConsignee().getDeliveryAddr());


        ArrayList<Cosignee> arrayList = new ArrayList();
        data.getConsignee().setCosignee(true);
        data.getConsignee().setHeading("Consignee Info");
        arrayList.add(data.getConsignee());

        Cosignee cosignee = new Cosignee();

        cosignee.setFirstName(data.getShipper().getFirstName());
        cosignee.setCollectionAddr(data.getShipper().getCollectionAddr());
        cosignee.setPhone(data.getShipper().getPhone());
        cosignee.setHeading("Shipper Info");
        cosignee.setCosignee(false);
        cosignee.setCollectionAddr(data.getConsignee().getDeliveryAddr());
        arrayList.add(cosignee);
        adapterCosigneeNDelivery = new AdapterCosigneeNDelivery(ActivtityPlanJobStatus.this, data.getGoodsQtyStatus(), masterPojo, arrayList, new AdapterCosigneeNDelivery.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });

        rvCosigneesNDelivery.setAdapter(adapterCosigneeNDelivery);
        adapterCosigneeNDelivery.notifyDataSetChanged();

        if (data.getGoodinfo().size() < 3) {
            expandRl.setVisibility(View.INVISIBLE);
        } else {
            expandRl.setVisibility(View.VISIBLE);
        }


        expandRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Stash.put(Constants.LIST_GOODS, data.getGoodinfo());
                startActivity(new Intent(ActivtityPlanJobStatus.this, ActivityExpandedGood.class));
            }
        });


        try {
            collectList = data.getConsignee().getCollectionLatLng().split(",");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            deliverList = data.getConsignee().getCollectionLatLng().split(",");

        } catch (Exception e) {
            e.printStackTrace();
        }

        btnNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppStatus.getInstance(ActivtityPlanJobStatus.this).isOnline()) {
                    alertDialoge.showDialog(ActivtityPlanJobStatus.this);

                } else {
                    Toast.makeText(ActivtityPlanJobStatus.this, masterPojo.getCheckInternet(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void callApiSendImage(String i, String bytes) {

        SendSignature sendSignature = new SendSignature();
        sendSignature.setJobAddrId(i);
        sendSignature.setImgBase64(bytes);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<SendSignature>> call = apiService.sendSignature(sendSignature);

        progressVisiblityVisible();
        call.enqueue(new Callback<WebResponse<SendSignature>>() {
            @Override
            public void onResponse(Call<WebResponse<SendSignature>> call, Response<WebResponse<SendSignature>> response) {

                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();

                    try {
                        // Toast.makeText(ActivityJobStatus.this, "Image saved sucess", Toast.LENGTH_SHORT).show();

                        if (jobType.equalsIgnoreCase("Delivery") || jobType.equalsIgnoreCase("Delivery Only")) {
                            finish();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        progressVisiblityGone();
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<SendSignature>> call, Throwable t) {
                Toast.makeText(ActivtityPlanJobStatus.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                progressVisiblityGone();
            }
        });
    }


    private void callApiSendCapterImage(String i, String bytes) {

        SendSignature sendSignature = new SendSignature();
        sendSignature.setJobAddrId(i);
        sendSignature.setImgBase64(bytes);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<SendSignature>> call = apiService.sendImage(sendSignature);

        progressVisiblityVisible();
        call.enqueue(new Callback<WebResponse<SendSignature>>() {
            @Override
            public void onResponse(Call<WebResponse<SendSignature>> call, Response<WebResponse<SendSignature>> response) {

                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();

                    try {
                        Toast.makeText(ActivtityPlanJobStatus.this, "Image send sucess", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressVisiblityGone();
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<SendSignature>> call, Throwable t) {
                Toast.makeText(ActivtityPlanJobStatus.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                progressVisiblityGone();
            }
        });
    }
    public void callApiSendAllCapterImages(String i, ArrayList<String> bytes,BottomSheetImageList btSheet) {

        SendMUultipleImages sendSignature = new SendMUultipleImages();
        sendSignature.setJobAddrId(i);
        sendSignature.setImgBase64(bytes);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<SendSignature>> call = apiService.sendMultipleImages(sendSignature);

        progressVisiblityVisible();
        call.enqueue(new Callback<WebResponse<SendSignature>>() {
            @Override
            public void onResponse(Call<WebResponse<SendSignature>> call, Response<WebResponse<SendSignature>> response) {

                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    isImagesUploaded =true;
                    progressVisiblityGone();

                    try {
                        btSheet.dismiss();
                        setupStatusWork();
                        Toast.makeText(ActivtityPlanJobStatus.this, "Image Uploaded Successfully", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                        progressVisiblityGone();
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<SendSignature>> call, Throwable t) {
                Toast.makeText(ActivtityPlanJobStatus.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                progressVisiblityGone();
            }
        });
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }


    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }

    public void Progress() {
        OnProgress();
        j = pgsBar.getProgress();
        new Thread(new Runnable() {
            public void run() {
                while (j < 100) {
                    j += 1;
                    // Update the progress bar and display the current value in text view
                    hdlr.post(new Runnable() {
                        public void run() {
                            pgsBar.setProgress(j);
                        }
                    });
                    try {
                        // Sleep for 100 milliseconds to show the progress slowly.
                        Thread.sleep(190);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (j == 100) {
                            ResetProgress();
                        }
                    }
                });
            }
        }).start();

    }

    public void ResetProgress() {
        pgsBar.setProgress(100);
        pgsBar.setVisibility(View.GONE);
        Wait.setVisibility(View.GONE);
        acceptSlideView.setVisibility(View.VISIBLE);
        pgsBar.setProgress(0);

    }

    public void OnProgress() {
        pgsBar.setVisibility(View.VISIBLE);
        Wait.setVisibility(View.VISIBLE);
    }

    public void ResetProgressonResponse() {
        j = 100;
        pgsBar.setProgress(99);
    }

    public void navigationwithGoogleMap(String latitude, String longitudes) {
        Intent i = null;


      /*  String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitudes);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);*/

        i = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + latitude + ',' + longitudes));
        startActivity(i);
    }

    public void navigationwithWaze(String lat, String longitude) {
        Intent i = null;
        String url;
        try {
            url = "waze://?q=" + lat + "," + longitude;
            i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            //    Log.e("sdfdfdsf","dropoff in"+dropoff);
            startActivity(i);
        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
            startActivity(intent);
        }
    }

    public void navigationwithHerewego(String lat, String longitude, String pickup) {
        try {
            Intent intent = new Intent();
            intent.setAction("com.here.maps.DIRECTIONS");
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            //    Log.e("sdfdfdsf","pickup"+pickup);

            pickup = "Kavish Crown 401";

            pickup = pickup.replace("/", "-");
            intent.setData(Uri.parse("here.directions://v1.0/mylocation/" + lat + "," + longitude + "," + pickup.replaceAll("\\+", " ") + "?ref=<Referrer>&m=d"));

            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.here.app.maps"));
            startActivity(intent);
        }
    }

    public class ViewDialog {

        public void showDialog(Activity activity) {

            Log.e("ShowDialog", "ShowDialog");
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_navigate_with);


            Button maps = (Button) dialog.findViewById(R.id.mapsBtn);
            Button hereWeGo = (Button) dialog.findViewById(R.id.hereWeGo);
            Button waze = (Button) dialog.findViewById(R.id.waze);
            Button dismiss = (Button) dialog.findViewById(R.id.dismiss);
            TextView areYouSure = (TextView) dialog.findViewById(R.id.areYouSure);

            maps.setText(masterPojo.getGoogleMaps());
            hereWeGo.setText(masterPojo.getHereWeGo());
            waze.setText(masterPojo.getWaze());
            dismiss.setText(masterPojo.getDismiss());
            areYouSure.setText(masterPojo.getNavigateWith());


            if (driverObj.getDriverAppMapNavigationTypeId().equalsIgnoreCase("1")) {
                hereWeGo.setVisibility(View.VISIBLE);
                maps.setVisibility(View.VISIBLE);
                waze.setVisibility(View.VISIBLE);
            } else if (driverObj.getDriverAppMapNavigationTypeId().equalsIgnoreCase("2")) {
                hereWeGo.setVisibility(View.GONE);
                maps.setVisibility(View.VISIBLE);
                waze.setVisibility(View.GONE);
            } else if (driverObj.getDriverAppMapNavigationTypeId().equalsIgnoreCase("3")) {
                hereWeGo.setVisibility(View.GONE);
                maps.setVisibility(View.GONE);
                waze.setVisibility(View.VISIBLE);
            } else if (driverObj.getDriverAppMapNavigationTypeId().equalsIgnoreCase("4")) {
                hereWeGo.setVisibility(View.VISIBLE);
                maps.setVisibility(View.GONE);
                waze.setVisibility(View.GONE);
            } else {
                maps.setVisibility(View.VISIBLE);
                waze.setVisibility(View.GONE);
                hereWeGo.setVisibility(View.GONE);
            }


            dismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            maps.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (currentStatus == 5) {
                        if (collectList == null) {
                            return;
                        }

                        try {
                            String latitude = collectList[0];
                            String longitude = collectList[1];
                            navigationwithGoogleMap(latitude, longitude);
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (currentStatus == 6 || currentStatus == 7) {
                        if (deliverList == null) {
                            return;
                        }

                        try {
                            String latitude = deliverList[0];
                            String longitude = deliverList[1];
                            navigationwithGoogleMap(latitude, longitude);
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {


                        if (deliverList == null) {
                            return;
                        }
                        try {
                            String latitude = deliverList[0];
                            String longitude = deliverList[1];
                            navigationwithGoogleMap(latitude, longitude);
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }
            });


            hereWeGo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (currentStatus == 5) {
                        if (collectList == null) {
                            Log.e("Returned", "Returned");
                            return;
                        }
                        try {
                            String latitude = collectList[0];
                            String longitude = collectList[1];
                            navigationwithHerewego(latitude, longitude, "");

                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (currentStatus == 6 || currentStatus == 7) {
                        if (deliverList == null) {
                            return;
                        }

                        try {
                            String latitude = deliverList[0];
                            String longitude = deliverList[1];
                            navigationwithHerewego(latitude, longitude, "");
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {


                        if (deliverList == null) {
                            return;
                        }
                        try {
                            String latitude = deliverList[0];
                            String longitude = deliverList[1];
                            navigationwithHerewego(latitude, longitude, "");
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                    dialog.dismiss();
                }
            });

            waze.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (currentStatus == 5) {
                        if (collectList == null) {
                            return;
                        }

                        try {
                            String latitude = collectList[0];
                            String longitude = collectList[1];
                            navigationwithWaze(latitude, longitude);

                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    } else if (currentStatus == 6 || currentStatus == 7) {
                        if (deliverList == null) {
                            return;
                        }

                        try {
                            String latitude = deliverList[0];
                            String longitude = deliverList[1];
                            navigationwithWaze(latitude, longitude);
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {


                        if (deliverList == null) {
                            return;
                        }
                        try {
                            String latitude = deliverList[0];
                            String longitude = deliverList[1];
                            navigationwithWaze(latitude, longitude);
                            dialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }
                }
            });

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }
    }

}