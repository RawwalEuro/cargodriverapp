package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.cargodriverapp.Adapter.AdapterFutureJobs;
import com.eurosoft.cargodriverapp.Adapter.AdapterGoodsDetail;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

public class ActivityFutureJobs extends AppCompatActivity {

    private CardView cardView;
    private ImageView bckIcon;
    private TextView textHeader,radiobutton0,radiobutton1;
    private RadioGroup radioGroupToggle;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private LinearLayout mainRl;
    private RecyclerView rvFutureNPlanned;
    private LinearLayoutManager layoutManager;
    private AdapterFutureJobs adapterFutureJobs;
    private MasterPojo masterPojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_future_jobs);

        initViews();
    }

    private void initViews() {
        bckIcon = findViewById(R.id.finish);
        textHeader = findViewById(R.id.textHeader);
        radioGroupToggle = findViewById(R.id.radioGroupToggle);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        rvFutureNPlanned = findViewById(R.id.rvFutureNPlanned);
        masterPojo=new MasterPojo();
        ///////////////////////////////////////// Language Changes ////////////////////////////////////////
        radiobutton0 =findViewById(R.id.radio0);
        radiobutton1 =findViewById(R.id.radio1);
        radiobutton0.setText(masterPojo.getPlannedJobs());
        radiobutton1.setText(masterPojo.getSingleJob());
        textHeader.setText(masterPojo.getFutureJobsTitle());
        ///////////////////////////////////////// Language Changes ////////////////////////////////////////




        bckIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rvFutureNPlanned.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ActivityFutureJobs.this, LinearLayoutManager.VERTICAL, false);
        rvFutureNPlanned.setLayoutManager(layoutManager);


        List<String> strings = new ArrayList<>();
        strings.add("");
        strings.add("");
        strings.add("");
        strings.add("");
        adapterFutureJobs  = new AdapterFutureJobs(ActivityFutureJobs.this, strings, new AdapterFutureJobs.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        },masterPojo);

        rvFutureNPlanned.setAdapter(adapterFutureJobs);
        adapterFutureJobs.notifyDataSetChanged();

        radioGroupToggle.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

            }
        });

    }



    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        // disableEnableControls(true, mainRl);
    }


    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
        //   disableEnableControls(false, mainRl);
    }
}