package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.AdapterChangeVehicle;
import com.eurosoft.cargodriverapp.Adapter.AdapterPlannedJobs;
import com.eurosoft.cargodriverapp.Adapter.AdapterSelectLanguage;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.ConfimedPlan;
import com.eurosoft.cargodriverapp.Pojo.JobDetailDataObject;
import com.eurosoft.cargodriverapp.Pojo.Language;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobs;
import com.eurosoft.cargodriverapp.Pojo.VehicleDetails;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySelectLanguage extends AppCompatActivity {

    private TextView textHeader;
    private ImageView bckBtn;
    private RecyclerView changeLanguageRv;
    private Button btnDone;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private AdapterSelectLanguage adapterSelectLanguage;
    private AdapterChangeVehicle adapterChangeVehicle;
    private LinearLayoutManager layoutManager;
    private MasterPojo masterPojo;
    private boolean notSelected = false;
    private String code = "";
    private ImageView profile;
    private TextView changeLanguage;
    private ImageView arrowDown, arrowUp, arrowUpV, arrowDownV;
    private RelativeLayout rlChangeLang, rlChangeVehicle;
    private LoginResponse driverObj;
    private RecyclerView changeVehicleRv;
    private LinearLayoutManager layoutManagerVehicles;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_language);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        initViews();
        callApiGetAllLanguages();
        callApiGetAllVehicles();
    }

    private void callApiGetAllLanguages() {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseList<Language>> call = apiService.getAllLanguages();

        call.enqueue(new Callback<WebResponseList<Language>>() {
            @Override
            public void onResponse(Call<WebResponseList<Language>> call, Response<WebResponseList<Language>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    try {
                        adapterSelectLanguage = new AdapterSelectLanguage(ActivitySelectLanguage.this, response.body().getData(), new AdapterSelectLanguage.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, Language language) {
                                notSelected = true;
                                code = language.getShortName();
                                callApiForLanguage(code);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    changeLanguageRv.setAdapter(adapterSelectLanguage);
                    adapterSelectLanguage.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<WebResponseList<Language>> call, Throwable t) {
                progressVisiblityGone();
            }
        });

    }


    private void callApiGetAllVehicles() {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        VehicleDetails vehicleDetails = new VehicleDetails(driverObj.getId());
        Call<WebResponseList<VehicleDetails>> call = apiService.getAllVehicles(vehicleDetails);

        call.enqueue(new Callback<WebResponseList<VehicleDetails>>() {
            @Override
            public void onResponse(Call<WebResponseList<VehicleDetails>> call, Response<WebResponseList<VehicleDetails>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Log.e("200", "200");
                    try {
                        adapterChangeVehicle = new AdapterChangeVehicle(ActivitySelectLanguage.this, response.body().getData(), new AdapterChangeVehicle.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, VehicleDetails vehicleDetails1) {
                                // notSelected = true;
                                //  code = language.getShortName();

                                Log.e("ABC-VEHICICLE", vehicleDetails1.getVehicleId() + "");
                                Log.e("ABC-VEHICICLE", vehicleDetails1.getVehicleNumber() + "");
                                callApiUpdateVehicle(vehicleDetails1.getVehicleId(), vehicleDetails1.getVehicleNumber());
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    changeVehicleRv.setAdapter(adapterChangeVehicle);
                    adapterChangeVehicle.notifyDataSetChanged();
                }

            }

            @Override
            public void onFailure(Call<WebResponseList<VehicleDetails>> call, Throwable t) {
                progressVisiblityGone();
            }
        });

    }


    private void callApiUpdateVehicle(Integer vehicleIdd, String numberPlate) {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        VehicleDetails vehicleDetails = new VehicleDetails(driverObj.getId(), vehicleIdd, "DriverApp,Android");
        Call<WebResponseList<VehicleDetails>> call = apiService.updateSelectedVehicles(vehicleDetails);

        call.enqueue(new Callback<WebResponseList<VehicleDetails>>() {
            @Override
            public void onResponse(Call<WebResponseList<VehicleDetails>> call, Response<WebResponseList<VehicleDetails>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {

                    Toast.makeText(getApplicationContext(), "Vehicle Updated Sucessfully", Toast.LENGTH_SHORT).show();

                    if (Stash.getString(Constants.CONFIRMED_VEHICLE).equalsIgnoreCase("")) {
                        driverObj.setVehicleId(vehicleIdd + "");
                        Stash.put(Constants.DRIVER_VEHICLE, numberPlate);
                        Stash.put(Constants.DRIVER_OBJ, driverObj);
                        Log.e("NumberPlate", numberPlate);
                        progressVisiblityGone();
                        Stash.put(Constants.IS_LANG_CHANGED, true);
                        finish();

                        //progressVisiblityGone();
                        // finish();
                        return;
                    } else {
                        String confirmedVehicleId = Stash.getString(Constants.CONFIRMED_VEHICLE);
                        driverObj.setVehicleId(vehicleIdd + "");
                        Stash.put(Constants.DRIVER_VEHICLE, numberPlate);
                        Stash.put(Constants.DRIVER_OBJ, driverObj);
                        Log.e("NumberPlate", numberPlate);
                        progressVisiblityGone();
                        Stash.put(Constants.IS_LANG_CHANGED, true);
                        finish();
                        //callApiGetConfirmedPlan(confirmedVehicleId);
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<VehicleDetails>> call, Throwable t) {
                progressVisiblityGone();
            }
        });

    }

    private void callApiGetConfirmedPlan(String confirmedVehicleId) {
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        ConfimedPlan confimedPlan = new ConfimedPlan(driverObj.getId() + "", confirmedVehicleId);

        Call<WebResponseList<PlannedJobs>> call = apiService.getConfimedPlan(confimedPlan);

        call.enqueue(new Callback<WebResponseList<PlannedJobs>>() {
            @Override
            public void onResponse(Call<WebResponseList<PlannedJobs>> call, Response<WebResponseList<PlannedJobs>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();
                    Stash.put(Constants.DRIVER_VEHICLE, "");
                    Stash.put(Constants.IS_LANG_CHANGED, true);
                    finish();
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<PlannedJobs>> call, Throwable t) {
                progressVisiblityGone();
            }
        });

    }

    private void initViews() {
        textHeader = findViewById(R.id.textHeader);
        bckBtn = findViewById(R.id.bckBtn);
        changeLanguageRv = findViewById(R.id.changeLanguageRv);
        changeVehicleRv = findViewById(R.id.changeVehicleRv);
        btnDone = findViewById(R.id.btnDone);
        changeLanguage = findViewById(R.id.changeLanguage);
        arrowUp = findViewById(R.id.arrowUp);
        arrowDown = findViewById(R.id.arrowDown);
        rlChangeLang = findViewById(R.id.rlChangeLang);
        rlChangeVehicle = findViewById(R.id.rlChangeVehicle);
        arrowUpV = findViewById(R.id.arrowUpV);
        arrowDownV = findViewById(R.id.arrowDownV);

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);

        changeLanguageRv.setHasFixedSize(true);
        changeVehicleRv.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(ActivitySelectLanguage.this, LinearLayoutManager.VERTICAL, false);
        layoutManagerVehicles = new LinearLayoutManager(ActivitySelectLanguage.this, LinearLayoutManager.VERTICAL, false);
        changeLanguageRv.setLayoutManager(layoutManager);
        changeVehicleRv.setLayoutManager(layoutManagerVehicles);


        profile = (ImageView) findViewById(R.id.profile);


        rlChangeVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (changeVehicleRv.getVisibility() == View.VISIBLE) {
                    changeVehicleRv.setVisibility(View.GONE);
                    arrowUpV.setVisibility(View.GONE);
                    arrowDownV.setVisibility(View.VISIBLE);
                } else {
                    changeVehicleRv.setVisibility(View.VISIBLE);
                    arrowUpV.setVisibility(View.VISIBLE);
                    arrowDownV.setVisibility(View.GONE);
                }
            }
        });
        findViewById(R.id.privacyPolicyLyt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://cabtreasure.com/privacy-policy-cargo-driver-app/")));
                } catch (Exception e) {
                }
            }
        });


        rlChangeLang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (changeLanguageRv.getVisibility() == View.VISIBLE) {
                    changeLanguageRv.setVisibility(View.GONE);
                    arrowUp.setVisibility(View.GONE);
                    arrowDown.setVisibility(View.VISIBLE);
                } else {
                    changeLanguageRv.setVisibility(View.VISIBLE);
                    arrowUp.setVisibility(View.VISIBLE);
                    arrowDown.setVisibility(View.GONE);
                }
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChangeDayNightMode();

            }
        });

        textHeader.setText(masterPojo.getSelectLanguage());
        btnDone.setText(masterPojo.getDone());

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!notSelected) {
                    Toast.makeText(getApplicationContext(), masterPojo.getLanguageNotSelected(), Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    callApiForLanguage(code);
                }
            }
        });


        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void ChangeDayNightMode() {
        switch (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
            case Configuration.UI_MODE_NIGHT_YES:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                //    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case Configuration.UI_MODE_NIGHT_NO:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                //  overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
        }
    }


    private void callApiForLanguage(String code) {
        progressVisiblityVisible();
        MasterPojo masterPojo = new MasterPojo(code);
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<MasterPojo>> call = apiService.getSelectedLanguage(masterPojo);

        call.enqueue(new Callback<WebResponse<MasterPojo>>() {
            @Override
            public void onResponse(Call<WebResponse<MasterPojo>> call, Response<WebResponse<MasterPojo>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess() && response.body().getData() != null) {
                    Stash.put(Constants.MASTER_POJO, response.body().getData());
                    progressVisiblityGone();
                    MasterPojo reloadedMasterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
                    Stash.put(Constants.IS_LANG_CHANGED, true);
                    textHeader.setText(reloadedMasterPojo.getSelectLanguage());
                    btnDone.setText(reloadedMasterPojo.getDone());
                    finish();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<MasterPojo>> call, Throwable t) {
                progressVisiblityGone();
            }
        });
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }


    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
    }

}