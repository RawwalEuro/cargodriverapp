package com.eurosoft.cargodriverapp;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eurosoft.cargodriverapp.Adapter.MessageAdapter;
import com.eurosoft.cargodriverapp.Adapter.TemplateMessageAdapter;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.Message;
import com.eurosoft.cargodriverapp.Pojo.MessageHistory;
import com.eurosoft.cargodriverapp.Pojo.RequestMessage;
import com.eurosoft.cargodriverapp.Pojo.RequestSendMessage;
import com.eurosoft.cargodriverapp.Pojo.TemplateMessageList;
import com.eurosoft.cargodriverapp.Services.GoogleService;
import com.eurosoft.cargodriverapp.Services.SignalRService;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.Db.DatabaseClient;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.socket.client.Ack;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MessageActivity extends AppCompatActivity implements SignalRService.GetNewChatMsg, TemplateMessageAdapter.MessageSelectedListener {


    private static final String TAG = "MainFragment";
    private static final int REQUEST_LOGIN = 0;
    private static final int TYPING_TIMER_LENGTH = 600;
    private RecyclerView mMessagesView;
    private EditText mInputMessageView;
    private List<Message> mMessages = new ArrayList<Message>();
    private List<Message> getmMessages = new ArrayList<Message>();
    private RecyclerView.Adapter mAdapter;
    private boolean mTyping = false;
    private Handler mTypingHandler = new Handler();
    private String mUsername;
    private Socket mSocket;
    private Boolean isConnected = true;
    private LoginResponse driverObj;
    RecyclerView templateMessageView;
    TemplateMessageAdapter templateMessageAdapter;
    ApiInterface apiService;
    MasterPojo masterPojo;
    private LinearLayoutManager layoutManager;
    private boolean isLoadMore = false;
    private SwipeRefreshLayout pullToRefresh;
    private int toRange = 0;
    private int fromRange = 10;
    private int totalCount;
    private int totalCycles, page;
    private boolean closeAfterMsg = false;
    private ImageView bckPress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);

        getBundle();
        Stash.init(this);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        mMessagesView = (RecyclerView) findViewById(R.id.messages);
        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        bckPress = (ImageView) findViewById(R.id.bckPress);

        layoutManager = new LinearLayoutManager(this);
        mMessagesView.setLayoutManager(layoutManager);
        mInputMessageView = (EditText) findViewById(R.id.message_input);
        mInputMessageView.setHint(masterPojo.getEnterMessageHere());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        Stash.put(Constants.CHATACTIVITY_IS_RUNNING, true);
        apiService = APIClient.getClient().create(ApiInterface.class);
        getTemplatleSavedMessageFromServer();
        mInputMessageView.requestFocus();

        callApiGetChatHistory(0, 10, true);

        //getChatList();
        mAdapter = new MessageAdapter(MessageActivity.this, getmMessages);
        mMessagesView.setAdapter(mAdapter);
        scrollToBottom();


        bckPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                try {
                    if (page == totalCycles) {
                        pullToRefresh.setRefreshing(false);
                        pullToRefresh.setEnabled(false);
                        return;
                    }
                    toRange = toRange + 10;
                    fromRange = fromRange + 10;
                    callApiGetChatHistory(toRange, fromRange, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //page = page + 1;
            }
        });


        mInputMessageView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == R.id.send || id == EditorInfo.IME_NULL) {
                    attemptSend();
                    return true;
                }
                return false;
            }
        });
        mInputMessageView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ImageButton sendButton = (ImageButton) findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSend();
            }
        });
        startAlarmWithSignalRStart();


    }

    private void getBundle() {
        try {
            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                closeAfterMsg = extras.getBoolean(Constants.CLOSE_CHAT);
                //The key argument here must match that used in the other activity
            } else {
                closeAfterMsg = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void callApiGetChatHistory(int start, int end, boolean isReverse) {

        MessageHistory messageHistory = new MessageHistory(driverObj.getId(), 0, end);

        Call<WebResponseList<MessageHistory>> call = apiService.getMessageHistory(messageHistory);
        call.enqueue(new Callback<WebResponseList<MessageHistory>>() {
            @Override
            public void onResponse(Call<WebResponseList<MessageHistory>> call, Response<WebResponseList<MessageHistory>> response) {

                try {

                    pullToRefresh.setRefreshing(false);
                    if (response.code() != 200 || response.body() == null) {
                        return;
                    }
                    if (response.body().getSuccess()) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!response.body().getSuccess() && response.code() == 200) {

                        if (simpleDateFormat == null) {
                            simpleDateFormat = new SimpleDateFormat("dd/MMM HH:mm");
                        }


                        if (response.body().getData() == null) {
                            return;
                        }

                        if (response.body().getMessage() == null || response.body().getMessage().equalsIgnoreCase("")) {
                            totalCount = 10;
                            return;
                        }

                        try {
                            totalCount = Integer.parseInt(response.body().getMessage());
                            totalCycles = totalCount / 10;
                            totalCycles = Math.round(totalCycles);
                            totalCycles = totalCycles + 1;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        Log.e("totalCount", totalCount + "");
                        if (totalCount <= 10) {
                            pullToRefresh.setRefreshing(false);
                            pullToRefresh.setEnabled(false);
                        }
                        getmMessages = new ArrayList<>();

                        for (int i = 0; i < response.body().getData().size(); i++) {
                            MessageHistory messageHistory = response.body().getData().get(i);


                            if (messageHistory.getMessageFrom().equalsIgnoreCase("DriverApp,Android")) {

                                getmMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                                        .username("DriverApp,Android").message(messageHistory.getMessage()).mDate(simpleDateFormat.format(Calendar.getInstance().getTime())).build());
                            } else if (messageHistory.getMessageFrom().equalsIgnoreCase("WebApp")) {
                                getmMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                                        .username("WebApp").message(messageHistory.getMessage()).mDate(simpleDateFormat.format(Calendar.getInstance().getTime())).build());
                            } else if (messageHistory.getMessageFrom().equalsIgnoreCase("DriverApp- iOS")) {
                                getmMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                                        .username("DriverApp- iOS").message(messageHistory.getMessage()).mDate(simpleDateFormat.format(Calendar.getInstance().getTime())).build());
                            } else {
                                getmMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                                        .username("").message(messageHistory.getMessage()).mDate(simpleDateFormat.format(Calendar.getInstance().getTime())).build());
                            }


                        }
                        Collections.reverse(getmMessages);

                        mAdapter = new MessageAdapter(MessageActivity.this, getmMessages);
                        mMessagesView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();
                        if (isReverse) {
                            scrollToBottom();
                        } else {
                            scrollToTop();
                        }
                        page = page + 1;
                        if (totalCycles == page) {
                            pullToRefresh.setRefreshing(false);
                            pullToRefresh.setEnabled(false);
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<WebResponseList<MessageHistory>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "" + masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK != resultCode) {
            finish();
            return;
        }
        mUsername = data.getStringExtra("username");
        int numUsers = data.getIntExtra("numUsers", 1);
        addLog(getResources().getString(R.string.message_welcome));

    }

    private void addLog(String message) {
        mMessages.add(0, new Message.Builder(Message.TYPE_LOG)
                .message(message).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        //  scrollToBottom();
    }


    SimpleDateFormat simpleDateFormat;

    private void addMessage(String username, String msg, boolean addInRoom) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (msg.equals("")) {
                    return;
                }
                if (simpleDateFormat == null) {
                    simpleDateFormat = new SimpleDateFormat("dd/MMM HH:mm");
                }


                if (addInRoom == true) {
                    getmMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                            .username(username).message(msg).mDate(simpleDateFormat.format(Calendar.getInstance().getTime())).build());
                }


                mAdapter = new MessageAdapter(MessageActivity.this, getmMessages);
                mMessagesView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                scrollToBottom();

            }
        });

    }

    private void addTyping(String username) {
        mMessages.add(0, new Message.Builder(Message.TYPE_ACTION)
                .username(username).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        //  scrollToBottom();
    }


    private void startAlarmWithSignalRStart() {
        Intent intent = new Intent();
        intent.setClass(this, SignalRService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private SignalRService mService;

    private boolean mBound = false;
    private final ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to SignalRService, cast the IBinder and get SignalRService instance
            SignalRService.LocalBinder binder = (SignalRService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            mService.setOnGetNewChatMsg(MessageActivity.this);
            mMessages = mService.getMessageList();
            mAdapter = new MessageAdapter(MessageActivity.this, getmMessages);
            mMessagesView.setAdapter(mAdapter);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    private void attemptSend() {
//        if (null == mUsername) return;
//        if (!mSocket.connected()) return;

        mTyping = false;

        String message = mInputMessageView.getText().toString().trim();
        if (message.equals("")) {
            // Toast.makeText(MessageActivity.this,masterPojo.getCantSendEmptyText(),Toast.LENGTH_LONG).show();
            return;
        }

        CargoApp app = CargoApp.getInstance(this);
        mSocket = app.getSocket();
        RequestSendMessage requestSendMessage = new RequestSendMessage();
        requestSendMessage.setLoginId(driverObj.getId() + "");
        requestSendMessage.setToId("1");

        RequestMessage requestMessage = new RequestMessage();
        requestMessage.setMessage(message);
        requestMessage.setId(0);
        requestMessage.setFromname("DriverApp,Android");
        requestMessage.setFromsocketid(Stash.getString(Constants.SOCKET_ID));
        requestMessage.setToDeviceId(0);
        requestMessage.setPurpose("Conversation");
        requestMessage.setDriverName(driverObj.getFName());
        requestMessage.setDriverNumber(driverObj.getDriverNumber());
        requestMessage.setDriverId(driverObj.getId() + "");

        requestSendMessage.setRequest(requestMessage);

        Call<WebResponse<LoginResponse>> call = apiService.sendMessage(requestSendMessage);
        mInputMessageView.setText("");

        call.enqueue(new Callback<WebResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<WebResponse<LoginResponse>> call, Response<WebResponse<LoginResponse>> response) {
                try {
                    if (response.code() != 200 || response.body() == null) {
                        return;
                    }
                    if (response.body().getSuccess()) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!response.body().getSuccess() && response.code() == 200) {
                        Toast.makeText(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT).show();
                        mInputMessageView.setText("");
                        addMessage("DriverApp,Android", response.body().getMessage(), true);
                        Log.e("closeAfterMsg", closeAfterMsg + "");
                        if (closeAfterMsg) {
                            finish();
                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), e.getMessage() + "", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<WebResponse<LoginResponse>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), "" + masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void scrollToBottom() {
        mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
    }


    private void scrollToTop() {
        //  mMessagesView.smoothScrollToPosition(0);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void getMessage(Message message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //getChatList();
                getmMessages.add(message);
                mAdapter = new MessageAdapter(MessageActivity.this, getmMessages);
                mMessagesView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                scrollToBottom();
            }
        });

        //addMessage(message.getUsername(), message.getMessage() == null ? "" : message.getMessage(),false);
    }

    @Override
    public void getMessageList(ArrayList<Message> messageList) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });

    }

    @Override
    public void onSelectedMessage(String message) {
        mInputMessageView.setText(message);
        mInputMessageView.setSelection(mInputMessageView.getText().length());

    }

    public void SetupRecycleViewofTemplateMessage(List<TemplateMessageList> templateMessageLists) {
        templateMessageView = (RecyclerView) findViewById(R.id.messagelist_recylceview);
        templateMessageView.setLayoutManager(new LinearLayoutManager(this));
        templateMessageAdapter = new TemplateMessageAdapter(MessageActivity.this, templateMessageLists);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(MessageActivity.this,
                DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.custom_divider));
        templateMessageView.addItemDecoration(dividerItemDecoration);
        templateMessageView.setAdapter(templateMessageAdapter);
    }

    public void getTemplatleSavedMessageFromServer() {
        Call<WebResponseList<TemplateMessageList>> call = apiService.gettemplatlemessage();
        call.enqueue(new Callback<WebResponseList<TemplateMessageList>>() {
            @Override
            public void onResponse(Call<WebResponseList<TemplateMessageList>> call, Response<WebResponseList<TemplateMessageList>> response) {

                try {
                    if (response.code() != 200 || response.body() == null) {
                        return;
                    }
                    if (response.code() == 200 && response.body().getData() != null) {
                        SetupRecycleViewofTemplateMessage(response.body().getData());
                        // Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (!response.body().getSuccess() && response.code() == 200) {
                        //  Toast.makeText(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    //  Toast.makeText(getApplicationContext(), ""+e.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<WebResponseList<TemplateMessageList>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Stash.put(Constants.CHATACTIVITY_IS_RUNNING, false);
    }


    private void addMessageToList(Message message) {
        class AddMessage extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .chatDao()
                        .insert(message);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                //getChatList();
                //progressVisiblityGone();
                //getChatList();
            }
        }

        AddMessage st = new AddMessage();
        st.execute();
    }

    private List<Message> getChatList() {
        class GetChatList extends AsyncTask<Void, Void, List<Message>> {

            @Override
            protected List<Message> doInBackground(Void... voids) {
                getmMessages = new ArrayList();

                getmMessages = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .chatDao()
                        .getAll();
                return getmMessages;
            }

            @Override
            protected void onPostExecute(List<Message> messages) {
                super.onPostExecute(messages);


                if (messages == null || messages.size() == 0) {
                    return;
                } else {


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //  getChatList();
                            mAdapter = new MessageAdapter(MessageActivity.this, getmMessages);
                            mMessagesView.setAdapter(mAdapter);
                            mAdapter.notifyDataSetChanged();
                            scrollToBottom();

                        }
                    });


                }
            }
        }

        GetChatList gt = new GetChatList();
        gt.execute();

        return getmMessages;
    }


    private void delAllMessages() {
        class DeleteProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .chatDao()
                        .deleteAllMessages();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                mAdapter = new MessageAdapter(MessageActivity.this, getmMessages);
                mMessagesView.setAdapter(mAdapter);
            }
        }

        DeleteProduct st = new DeleteProduct();
        st.execute();
    }

}


/*   private Emitter.Listener onConnect = new Emitter.Listener() {
         @Override
         public void call(Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     Toast.makeText(getApplicationContext(),
                             "Connected", Toast.LENGTH_SHORT).show();
 //                    }
                 }
             });
         }
     };


     private Emitter.Listener onDisconnect = new Emitter.Listener() {
         @Override
         public void call(Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     Log.i(TAG, "diconnected");
                     isConnected = false;
                     Toast.makeText(getApplicationContext(),
                             "Disconnected", Toast.LENGTH_SHORT).show();
                 }
             });
         }
     };


     private Emitter.Listener onConnectError = new Emitter.Listener() {
         @Override
         public void call(Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     Log.e(TAG, "Error connecting" + args.toString());
                     Toast.makeText(getApplicationContext(),
                             "Error connecting", Toast.LENGTH_SHORT).show();
                 }
             });
         }
     };
     private Emitter.Listener sendingError = new Emitter.Listener() {
         @Override
         public void call(Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     Log.e(TAG, "Error msg" + args.toString());
                     Toast.makeText(getApplicationContext(),
                             "Error connecting", Toast.LENGTH_SHORT).show();
                 }
             });
         }
     };
     private Emitter.Listener onNewMessage = new Emitter.Listener() {
         @Override
         public void call(final Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     Log.e("TAG", "NEW MESSAGE_" + args[0].toString());


                     try {
                         JSONObject data = new JSONObject(args[0].toString());
                         String username;
                         String message;

 //                        username = data.getString("username");
                         message = data.getString("message");
                         removeTyping("username");
                         addMessage("username", message);
                     } catch (JSONException e) {
                         Log.e(TAG, e.getMessage());
                         return;
                     }
                 }
             });
         }
     };


     private Emitter.Listener onUserJoined = new Emitter.Listener() {
         @Override
         public void call(final Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     JSONObject data = (JSONObject) args[0];
                     String username;
                     int numUsers;
                     try {
                         username = data.getString("username");
                         numUsers = data.getInt("numUsers");
                     } catch (JSONException e) {
                         Log.e(TAG, e.getMessage());
                         return;
                     }

                     addLog(getResources().getString(R.string.message_user_joined, username));

                 }
             });
         }
     };

     private Emitter.Listener onUserLeft = new Emitter.Listener() {
         @Override
         public void call(final Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     JSONObject data = (JSONObject) args[0];
                     String username;
                     int numUsers;
                     try {
                         username = data.getString("username");
                         numUsers = data.getInt("numUsers");
                     } catch (JSONException e) {
                         Log.e(TAG, e.getMessage());
                         return;
                     }

                     addLog(getResources().getString(R.string.message_user_left, username));

                     removeTyping(username);
                 }
             });
         }
     };

     private Emitter.Listener onTyping = new Emitter.Listener() {
         @Override
         public void call(final Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     JSONObject data = (JSONObject) args[0];
                     String username;
                     try {
                         username = data.getString("username");
                     } catch (JSONException e) {
                         Log.e(TAG, e.getMessage());
                         return;
                     }
                     addTyping(username);
                 }
             });
         }
     };

     private Emitter.Listener onStopTyping = new Emitter.Listener() {
         @Override
         public void call(final Object... args) {
             runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                     JSONObject data = (JSONObject) args[0];
                     String username;
                     try {
                         username = data.getString("username");
                     } catch (JSONException e) {
                         Log.e(TAG, e.getMessage());
                         return;
                     }
                     removeTyping(username);
                 }
             });
         }
     };

     private Runnable onTypingTimeout = new Runnable() {
         @Override
         public void run() {
             if (!mTyping) return;

             mTyping = false;
             mSocket.emit("stop typing", new Ack() {
                 @Override
                 public void call(Object... args) {

                 }
             });
         }
     };


    @Override
      public void getMessage(Message message) {
          addMessage(message.getUsername(),message.getMessage());
      }

      @Override
      public void getMessageList(ArrayList<Message> messageList) {
          runOnUiThread(new Runnable() {
              @Override
              public void run() {

              }
          });
          mMessages = messageList;
          mAdapter = new MessageAdapter(MessageActivity.this, messageList);
          mMessagesView.setAdapter(mAdapter);
          scrollToBottom();
      }


//        Constants.CHAT_SERVER_URL = message;
//        getActivity().stopService(new Intent(getActivity(),SignalRService.class));
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                getActivity().startService(new Intent(getActivity(),SignalRService.class));
//                mInputMessageView.setText("");
//            }
//        },2000);


     /*   //addMessage(driverObj.getId().toString(), message);
        JSONObject jsonObject=new JSONObject();
        try {
            jsonObject.put("message",message);
            jsonObject.put("id",0);
            jsonObject.put("fromname","DriverApp,Android");
            jsonObject.put("fromsocketid", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID));
            jsonObject.put("ToDeviceId", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
  */