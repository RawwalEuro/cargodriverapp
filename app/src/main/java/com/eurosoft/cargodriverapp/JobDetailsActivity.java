package com.eurosoft.cargodriverapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bcgdv.asia.lib.ticktock.TickTockView;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.JobNotificationStatus;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PlannedAccept;
import com.eurosoft.cargodriverapp.Pojo.ResponseJobNotification;
import com.eurosoft.cargodriverapp.SlideView.SlideView;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.JobData;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseString;
import com.fxn.stash.Stash;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//Job Offer Activity
public class JobDetailsActivity extends AppCompatActivity {

    private Button btnAccept;
    private ProgressBar progressBar;
    private TextView progressText;
    int i = 60;
    private RelativeLayout btnReject;
    private String jobIdToSend;
    private TextView origin, desitination, date, time, declinetxt, pickup, dropoff;
    private LoginResponse driverObj;
    private SlideView acceptSlide;
    private JobData jobData;
    private MasterPojo masterPojo;
    private ProgressBar pgsBar;
    TextView Wait;
    private TickTockView mCountDown = null;
    private Handler hdlr = new Handler();
    int j = 0;
    private Handler handler;
    MediaPlayer soundPlayer;
    private int jobType = 0;
    private RelativeLayout rlFojJob;
    private RelativeLayout rlBottom;
    private TextView headerNextJobs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);
        Stash.init(this);
        handler = new Handler();
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);

        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);

        getBundle();

        try {
            if (jobType == 0) {
                jobIdToSend = Stash.getString(Constants.JOB_ID);
            } else if (jobType == 1) {
                jobIdToSend = Stash.getString(Constants.JOB_ID_FOLLOW_ON);
            } else if (jobType == 2) {
                jobIdToSend = Stash.getString(Constants.PLANNED_ID);
            } else {
                jobIdToSend = Stash.getString(Constants.JOB_ID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        orderProgress();
        initViews();
        playMusic();


        if (AppStatus.getInstance(this).isOnline()) {

            if (jobType == 2) {
                rlBottom.setVisibility(View.GONE);
                return;
            } else {
                rlBottom.setVisibility(View.VISIBLE);
            }

            callApiGetJobDetails();
            return;
        } else {
            Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
        }

    }

    private void getBundle() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            jobType = extras.getInt(Constants.JOB_TYPE);
            //The key argument here must match that used in the other activity
        } else {
        }
    }

    public void orderProgress() {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (i >= 0) {
                    progressText.setText("" + i + "");
                    progressBar.setProgress(i);
                    if (i == 0) {
                        try {
                            // callApiSendStatus(8);
                        } catch (Exception e) {

                        }
                    }
                    i--;
                    handler.postDelayed(this, 1000);
                } else {
                    handler.removeCallbacks(this);
                }
            }
        }, 200);
    }

    private void callApiGetJobDetails() {

        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
            return;
        }

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<JobData>> call = apiService.getJobData(jobIdToSend);

        call.enqueue(new Callback<WebResponse<JobData>>() {
            @Override
            public void onResponse(Call<WebResponse<JobData>> call, Response<WebResponse<JobData>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }

                if (!response.body().getSuccess()) {

                    jobData = response.body().getData();
                    setData(jobData);
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    // Toast.makeText(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<WebResponse<JobData>> call, Throwable t) {

            }
        });

    }

    private void setData(JobData jobData) {

        if (jobData == null) {
            return;
        } else {
            origin.setText(jobData.getCollectionAddr() + "");
            desitination.setText(jobData.getDeliveryAddr() + "");
            date.setText(jobData.getDate());
            time.setText(jobData.getFromTime() + " - " + jobData.getTillTime());
        }
    }

    private void initViews() {
        btnAccept = findViewById(R.id.btnAccept);
        btnReject = findViewById(R.id.btnReject);
        acceptSlide = findViewById(R.id.accept);
        declinetxt = findViewById(R.id.decline);

        origin = findViewById(R.id.origin);
        desitination = findViewById(R.id.desitination);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        pickup = findViewById(R.id.pickup);
        dropoff = findViewById(R.id.dropoff);
        pgsBar = (ProgressBar) findViewById(R.id.progress_bar1);
        Wait = (TextView) findViewById(R.id.wait);
        rlFojJob = (RelativeLayout) findViewById(R.id.rlFojJob);
        headerNextJobs = (TextView) findViewById(R.id.headerNextJobs);
        rlBottom = (RelativeLayout) findViewById(R.id.rlBottom);

        progressBar = findViewById(R.id.progress_bar);
        progressText = findViewById(R.id.progress_text);


        declinetxt.setText(masterPojo.getDecline());
        acceptSlide.setText(masterPojo.getSwipeToAccept());
        pickup.setText(masterPojo.getPickup() + "");
        dropoff.setText(masterPojo.getDropoff() + "");

        if (jobType == 1) {
            rlFojJob.setVisibility(View.VISIBLE);
            headerNextJobs.setText("Next Job");
        } else if (jobType == 0) {
            rlFojJob.setVisibility(View.GONE);
        } else if (jobType == 2) {
            rlFojJob.setVisibility(View.VISIBLE);
            headerNextJobs.setText("Planned Job");
        }

        acceptSlide.setOnSlideCompleteListener(new SlideView.OnSlideCompleteListener() {
            @Override
            public void onSlideComplete(SlideView slideView) {
                if (AppStatus.getInstance(getApplicationContext()).isOnline()) {
                    Progress();
                    if (jobType == 0) {
                        callApiSendStatusAccept(5, "OnRoute");
                    } else if (jobType == 1) {
                        callApiSendStatusAccept(15, "NextCollection");
                    } else if (jobType == 2) {
                        //callApiSendStatusAccept(10, "Pending Start");
                        callApiAcceptPlan(17, "ONROUTE","Pending Accept",4+"");

                    }
                } else {
                    Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppStatus.getInstance(getApplicationContext()).isOnline()) {

                    if(jobType == 2){
                        callApiAcceptPlan(8, "Rejected","Rejected",8+"");
                    } else {
                    callApiSendStatus(8);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    private void callApiAcceptPlan(int planStatusId, String planStatus , String jobStatus, String jobStatusId) {

        PlannedAccept plannedAccept = new PlannedAccept(jobIdToSend, driverObj.getId() + "", jobStatusId + "", jobStatus, planStatusId + "", planStatus, "DriverApp,Android", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID));


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseString<String>> call = apiService.plannedAccept(plannedAccept);

        call.enqueue(new Callback<WebResponseString<String>>() {
            @Override
            public void onResponse(Call<WebResponseString<String>> call, Response<WebResponseString<String>> response) {


                if (response.code() != 200 || response.body() == null) {
                    ResetProgressonResponse();
                    ResetMusic();
                    return;
                }
                if (!response.body().getSuccess()) {

                    ResetMusic();
                    Intent i = new Intent(JobDetailsActivity.this, HomeActivity.class);
                    i.putExtra("isReloadTrue", false);
                    i.putExtra("isPlannedSelected", true);
                    startActivity(i);
                    finish();

                }
            }

            @Override
            public void onFailure(Call<WebResponseString<String>> call, Throwable t) {
                ResetProgressonResponse();

                Log.e("Error",t.getMessage() + "");
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
       /* Calendar end = Calendar.getInstance();
        end.add(Calendar.MINUTE, 4);
        end.add(Calendar.SECOND, 0);

        Calendar start = Calendar.getInstance();
        start.add(Calendar.MINUTE, -1);
        if (mCountDown != null) {
            mCountDown.start(start, end);
        }*/
    }

    @Override
    protected void onStop() {
        super.onStop();
        //mCountDown.stop();
    }

    private void callApiSendStatus(int i) {

        JobNotificationStatus jobNotificationStatus = new JobNotificationStatus(jobIdToSend, driverObj.getId() + "", i + "", "Reject", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID), "DriverApp,Android");

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<ResponseJobNotification>> call = apiService.sendStatus(jobNotificationStatus);

        call.enqueue(new Callback<WebResponse<ResponseJobNotification>>() {
            @Override
            public void onResponse(Call<WebResponse<ResponseJobNotification>> call, Response<WebResponse<ResponseJobNotification>> response) {
                if (response.code() != 200 || response.body() == null) {
                    ResetMusic();
                    return;
                }
                if (!response.body().getSuccess()) {
                    ResetMusic();
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(JobDetailsActivity.this, HomeActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<WebResponse<ResponseJobNotification>> call, Throwable t) {

                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void callApiSendStatusAccept(int i, String jobStatus) {

        JobNotificationStatus jobNotificationStatus = new JobNotificationStatus(jobIdToSend, driverObj.getId() + "", i + "", jobStatus + "", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID), "DriverApp,Android");


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<ResponseJobNotification>> call = apiService.sendStatus(jobNotificationStatus);

        call.enqueue(new Callback<WebResponse<ResponseJobNotification>>() {
            @Override
            public void onResponse(Call<WebResponse<ResponseJobNotification>> call, Response<WebResponse<ResponseJobNotification>> response) {
                if (response.code() != 200 || response.body() == null) {
                    ResetProgressonResponse();
                    ResetMusic();
                    return;
                }
                if (!response.body().getSuccess()) {
                    if (jobType == 0) {
                        boolean showSlider = true;
                        ResetProgressonResponse();
                        ResetMusic();
                        Intent i = new Intent(JobDetailsActivity.this, ActivityJobStatus.class);
                        i.putExtra("showSlider", showSlider);
                        startActivity(i);
                        finish();
                    } else if (jobType == 1) {
                        Stash.put(Constants.IS_FOJ_ACCEPTED, true);
                        Toast.makeText(getApplicationContext(), "Job Added to Next Collection", Toast.LENGTH_SHORT).show();
                        ResetMusic();
                        Intent i = new Intent(JobDetailsActivity.this, HomeActivity.class);
                        i.putExtra("isReloadTrue", false);
                        startActivity(i);
                        finish();
                    } else if (jobType == 2) {
                        Toast.makeText(getApplicationContext(), "Job Added to Planned Job", Toast.LENGTH_SHORT).show();
                        ResetMusic();
                        Intent i = new Intent(JobDetailsActivity.this, HomeActivity.class);
                        i.putExtra("isReloadTrue", false);
                        startActivity(i);
                        finish();
                    } else {
                        boolean showSlider = true;
                        ResetProgressonResponse();
                        ResetMusic();
                        Intent i = new Intent(JobDetailsActivity.this, ActivityJobStatus.class);
                        i.putExtra("showSlider", showSlider);
                        startActivity(i);
                        finish();
                    }
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    if (jobType == 0) {
                        boolean showSlider = true;
                        ResetProgressonResponse();
                        ResetMusic();
                        Intent i = new Intent(JobDetailsActivity.this, ActivityJobStatus.class);
                        i.putExtra("showSlider", showSlider);
                        startActivity(i);
                        finish();
                    } else if (jobType == 1) {
                        ResetMusic();
                        Toast.makeText(getApplicationContext(), "Job Added to Next Collection", Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(JobDetailsActivity.this, HomeActivity.class);
                        i.putExtra("isReloadTrue", false);
                        startActivity(i);
                        finish();
                    } else if (jobType == 2) {
                        Toast.makeText(getApplicationContext(), "Job Added to Planned Jobs", Toast.LENGTH_SHORT).show();
                        ResetMusic();
                        Intent i = new Intent(JobDetailsActivity.this, HomeActivity.class);
                        i.putExtra("isReloadTrue", false);
                        startActivity(i);
                        finish();
                    } else {
                        boolean showSlider = true;
                        ResetProgressonResponse();
                        ResetMusic();
                        Intent i = new Intent(JobDetailsActivity.this, ActivityJobStatus.class);
                        i.putExtra("showSlider", showSlider);
                        startActivity(i);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ResponseJobNotification>> call, Throwable t) {
                ResetProgressonResponse();
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void Progress() {
        OnProgress();
        j = pgsBar.getProgress();
        new Thread(new Runnable() {
            public void run() {
                while (j < 100) {
                    j += 1;
                    // Update the progress bar and display the current value in text view
                    hdlr.post(new Runnable() {
                        public void run() {
                            pgsBar.setProgress(j);
                        }
                    });
                    try {
                        // Sleep for 100 milliseconds to show the progress slowly.
                        Thread.sleep(190);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (j == 100) {
                            ResetProgress();
                        }
                    }
                });
            }
        }).start();

    }

    public void ResetProgress() {
        pgsBar.setProgress(100);
        pgsBar.setVisibility(View.GONE);
        Wait.setVisibility(View.GONE);
        acceptSlide.setVisibility(View.VISIBLE);
        pgsBar.setProgress(0);

    }

    public void OnProgress() {
        pgsBar.setVisibility(View.VISIBLE);
        Wait.setVisibility(View.VISIBLE);
    }

    public void ResetProgressonResponse() {
        j = 100;
        pgsBar.setProgress(99);
    }

    Runnable musicStopRunable;

    private void playMusic() {
        soundPlayer = MediaPlayer.create(JobDetailsActivity.this, R.raw.mixkit);
        musicStopRunable = new Runnable() {
            @Override
            public void run() {
                soundPlayer.start();
                musicStopHandler.postDelayed(musicStopRunable, 1000);
            }
        };
        if (musicStopHandler != null) {
            musicStopHandler.removeCallbacks(musicStopRunable);
            musicStopHandler.post(musicStopRunable);
        }

    }

    Handler musicStopHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
        }
    };

    public void ResetMusic() {
        soundPlayer.stop();
        if (musicStopHandler != null) {
            musicStopHandler.removeCallbacks(musicStopRunable);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ResetMusic();
    }
}