package com.eurosoft.cargodriverapp;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Adapter.AdapterHomeJobs;
import com.eurosoft.cargodriverapp.Adapter.AdapterPlannedJobs;
import com.eurosoft.cargodriverapp.Fragment.BottomSheetSignature;
import com.eurosoft.cargodriverapp.Fragment.FragmentLeftMenu;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.ChangeStatus;
import com.eurosoft.cargodriverapp.Pojo.ConfimedPlan;
import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.JobNotificationStatus;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobs;
import com.eurosoft.cargodriverapp.Pojo.RequestLogin;
import com.eurosoft.cargodriverapp.Pojo.RequestLoginBody;
import com.eurosoft.cargodriverapp.Pojo.ResponseJobNotification;
import com.eurosoft.cargodriverapp.Services.GoogleService;
import com.eurosoft.cargodriverapp.Services.SignalRService;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.JobData;
import com.eurosoft.cargodriverapp.Utils.ServiceTools;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements BottomSheetSignature.BottomSignatureSucessListener {

    private LoginResponse driverObj;
    private RecyclerView rvJobs;
    private AdapterHomeJobs mAdapter;
    private AdapterPlannedJobs mPlannedAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<String> strings;
    int selectedindex = 1;
    public static int BREAK_INDEX = 0;
    public static int AVAIALBLE_INDEX = 1;
    public static int PANIC_INDEX = 2;

    private TextView driverId;
    private TextView statusText;

    private Button btnConnect, btnLogout;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout mainRl;
    private Button btnSendMsg;
    Geocoder geocoder;
    private static final int REQUEST_PERMISSIONS = 100;
    boolean boolean_permission;
    private static final int REQUEST_LOCATION = 99;
    public static String str_receiver = "servicetutorial.service.receiver";
    private HomeActivity mContext;
    private ImageView profile;
    private BottomSheetSignature bottomSheetSignature;
    private TextView driver;
    private TextView vehicleId;
    private DrawerLayout mDrawer;
    private ImageView navIcon;
    private String vehicleNum = "";
    private TextView currentJob;
    private ImageView refreshIcon;
    private ViewDialog alertDialoge;
    private ShowVehicleDialog vehicleDialog;
    private MasterPojo masterPojo;
    private JobNotificationStatus jobNotificationStatus;
    TextView vehicle;
    TextView statusHeading;
    TextView title;
    TextView pendingJobs, panictext, availabletext, breaktext;
    private TextView wait;
    private Bundle extras;
    private boolean isReloadTrue = true;
    private RadioGroup radioGroup;
    private RecyclerView rvPlannedJobs;
    private DialogGoOnBreak dialogGoBreak;
    private boolean isPlannedSelected;
    private ArrayList<PlannedJobs> plannedJobList = new ArrayList<>();
    private int previousPos;
    boolean isPanic = false;
    boolean isBreak = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Stash.init(this);
        setupHome();
        try {
            selectedindex = Stash.getInt(Constants.DRIVER_STATUS);
        } catch (Exception e) {
            e.printStackTrace();
            selectedindex = 1;
        }
    }

    private void setupHome() {
        alertDialoge = new ViewDialog();
        dialogGoBreak = new DialogGoOnBreak();
        vehicleDialog = new ShowVehicleDialog();
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        initViews();
        startListenService();

        callApiGetPendingJobs(driverObj.getId());
        getCurrentJobRunning();
        getBundle();
        draggableButtonWork();
        //callApiGetConfirmedPlanReload(driverObj.getVehicleId());
        //currentStatus();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isReloadTrue = true;
            }
        }, 3000);

        if (isPlannedSelected) {
            radioGroup.check(R.id.radio1);
            setUpPlannedJobs();
        }

        setupConfirmedApiAndDialog();
    }

    private void setupConfirmedApiAndDialog() {
        if (!Stash.getString(Constants.ADDITIONAL_MSG, "").equalsIgnoreCase("")) {
            vehicleDialog.showDialog(HomeActivity.this);
            if (Stash.getBoolean(Constants.IS_LAUNCH)) {
                // callApiGetConfirmedPlanReload(Stash.getString(Constants.CONFIRMED_VEHICLE));
                Stash.put(Constants.IS_LAUNCH, false);
            }
        } else {
            if (Stash.getBoolean(Constants.IS_LAUNCH)) {
                //callApiGetConfirmedPlanReload(driverObj.getVehicleId());
                Stash.put(Constants.IS_LAUNCH, false);
            }

        }
    }


    private void getBundle() {
        try {
            extras = getIntent().getExtras();
            if (extras != null) {
                isReloadTrue = extras.getBoolean("isReloadTrue");
                isPlannedSelected = extras.getBoolean("isPlannedSelected");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void callApiGetConfirmedPlanReload(String confirmedVehicleId) {
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        ConfimedPlan confimedPlan = new ConfimedPlan(driverObj.getId() + "", confirmedVehicleId);

        Call<WebResponseList<PlannedJobs>> call = apiService.getConfimedPlan(confimedPlan);

        call.enqueue(new Callback<WebResponseList<PlannedJobs>>() {
            @Override
            public void onResponse(Call<WebResponseList<PlannedJobs>> call, Response<WebResponseList<PlannedJobs>> response) {
                if (response.code() != 200 || response.body() == null) {

                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    plannedJobList = new ArrayList<>();
                    if (response.body().getData().size() != 0) {

                        for (int i = 0; i < response.body().getData().size(); i++) {
                            plannedJobList.add(response.body().getData().get(i));
                        }
                    }

                }
            }

            @Override
            public void onFailure(Call<WebResponseList<PlannedJobs>> call, Throwable t) {
                progressVisiblityGone();
            }
        });

    }


    private void startListenService() {
        Intent serviceIntent = new Intent(this, SignalRService.class);
        serviceIntent.setAction(Constants.STARTFOREGROUND_ACTION);
        serviceIntent.putExtra("inputExtra", "We are looking for your location updates");
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    private void callApiGetPendingJobs(Integer id) {

        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
            progressVisiblityGone();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseList<JobDetailData>> call = apiService.getPendingJobs(id + "", 4);

        call.enqueue(new Callback<WebResponseList<JobDetailData>>() {
            @Override
            public void onResponse(Call<WebResponseList<JobDetailData>> call, Response<WebResponseList<JobDetailData>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }


                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();
                    mAdapter = new AdapterHomeJobs(HomeActivity.this, response.body().getData(), masterPojo,
                            new AdapterHomeJobs.OnItemClickListenerHome() {
                                @Override
                                public void onItemClick(View view, int position, int selectedOption, JobDetailData jobDetailData) {
                                    switch (view.getId()) {

                                        case R.id.fullCard:


                                            /*Intent i = new Intent(HomeActivity.this, ActivityJobStatus.class);
                                            i.putExtra("showSlider", showSlider);
                                            i.putExtra("object", jobDetailData);
                                            startActivity(i);*/

                                            break;


                                        case R.id.llshowDetails:
                                            boolean showSlider = false;
                                            Stash.put(Constants.JOB_ID, jobDetailData.getId());
                                            Intent i = new Intent(HomeActivity.this, ActivityJobStatus.class);
                                            i.putExtra("showSlider", showSlider);
                                            i.putExtra("object", jobDetailData);
                                            Log.e("ID", jobDetailData.getId() + "");
                                            startActivity(i);
                                            break;


                                        case R.id.llRecover:
                                            alertDialoge.showDialog(HomeActivity.this, true, jobDetailData);
                                            break;


                                        case R.id.llStart:
                                            alertDialoge.showDialog(HomeActivity.this, false, jobDetailData);
                                            break;
                                    }
                                }
                            });

                    rvJobs.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<JobDetailData>> call, Throwable t) {
                progressVisiblityGone();
            }
        });


    }


    private void callApiSendStatusAccept(int i, JobDetailData jobDetailData, boolean isRecover) {
        progressVisiblityVisible();

        if (isRecover) {
            jobNotificationStatus = new JobNotificationStatus(jobDetailData.getId() + "", driverObj.getId() + "", i + "", "Start" + "", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID), "DriverApp,Android");
        } else {

            jobNotificationStatus = new JobNotificationStatus(jobDetailData.getId() + "", driverObj.getId() + "", i + "", "Recover" + "", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID), "DriverApp,Android");
        }
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<ResponseJobNotification>> call = apiService.sendStatus(jobNotificationStatus);

        call.enqueue(new Callback<WebResponse<ResponseJobNotification>>() {
            @Override
            public void onResponse(Call<WebResponse<ResponseJobNotification>> call, Response<WebResponse<ResponseJobNotification>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    if (isRecover == false) {
                        callApiGetPendingJobs(driverObj.getId());
                        return;
                    }

                    boolean showSlider = true;
                    Stash.put(Constants.JOB_ID, jobDetailData.getId());
                    Intent i = new Intent(HomeActivity.this, ActivityJobStatus.class);
                    i.putExtra("showSlider", showSlider);
                    startActivity(i);
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    if (isRecover == false) {
                        callApiGetPendingJobs(driverObj.getId());
                        return;
                    }
                    boolean showSlider = true;
                    Stash.put(Constants.JOB_ID, jobDetailData.getId());
                    Intent i = new Intent(HomeActivity.this, ActivityJobStatus.class);
                    i.putExtra("showSlider", showSlider);
                    startActivity(i);
                    progressVisiblityGone();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ResponseJobNotification>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong() + "", Toast.LENGTH_SHORT).show();
                progressVisiblityGone();
            }
        });
    }


    public class ViewDialog {

        public void showDialog(Activity activity, boolean isRecover, JobDetailData jobDetailData) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_recover_restart);


            TextView areYouSure = (TextView) dialog.findViewById(R.id.areYouSure);

            Button btnRecover = (Button) dialog.findViewById(R.id.btnRecover);
            Button btnReject = (Button) dialog.findViewById(R.id.btnLeave);


            if (isRecover) {
                areYouSure.setText(masterPojo.getAreyousureyouwanttorecoverthisjob());
                btnRecover.setText(masterPojo.getRecover());
            } else {
                areYouSure.setText(masterPojo.getAreyousureyouwanttostartthisjob());
                btnRecover.setText(masterPojo.getStart());
            }


            btnRecover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isRecover) {
                        dialog.dismiss();
                        callApiSendStatusAccept(9, jobDetailData, false);
                    } else {
                        dialog.dismiss();
                        callApiSendStatusAccept(10, jobDetailData, true);
                    }
                }
            });

            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }
    }


    public class ShowVehicleDialog {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_change_your_vehicle);


            TextView changeVehicleHeading = (TextView) dialog.findViewById(R.id.changeVehicleHeading);

            Button changeVehicle = (Button) dialog.findViewById(R.id.changeVehicle);
            Button skip = (Button) dialog.findViewById(R.id.skip);
            TextView vehicleIdText = (TextView) dialog.findViewById(R.id.vehicleIdText);

            changeVehicleHeading.setText("You have a confirmed plan in que. \n To get the plan change vehicle ");
            vehicleIdText.setText(Stash.getString(Constants.ADDITIONAL_MSG));


            changeVehicle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Stash.clear(Constants.ADDITIONAL_MSG);
                    startActivity(new Intent(HomeActivity.this, ActivitySelectLanguage.class));
                    dialog.dismiss();
                }
            });

            skip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Stash.clear(Constants.ADDITIONAL_MSG);
                    dialog.dismiss();
                }
            });

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }
    }


    private void getCurrentLat() {
        try {
            mContext = this;
            geocoder = new Geocoder(this, Locale.getDefault());
            fn_permission();
            Intent intent = new Intent(getApplicationContext(), GoogleService.class);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            vehicleId.setText(Stash.getString(Constants.DRIVER_VEHICLE));
            Log.e("Vehicle Number ", Stash.getString(Constants.DRIVER_VEHICLE) + "ABC");
        } catch (Exception e) {
            Log.e("Catch", "Catch");
            e.printStackTrace();
        }


        registerReceiver(broadcastReceiver, new IntentFilter(str_receiver));
        if (Stash.getBoolean(Constants.IS_LANG_CHANGED) == true) {
            Intent intent = getIntent();
            finish();
            startActivity(intent);
            Stash.put(Constants.IS_LANG_CHANGED, false);
        }
        getCurrentJobRunning();


        if (mDrawer != null) {
            mDrawer.closeDrawer(GravityCompat.START);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (requestCode == REQUEST_LOCATION) {
                    if (grantResults.length >= 1
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // We can now safely use the API we requested access to
                        boolean_permission = true;
                        getCurrentLat();
                        fn_permission();
                        // startLocationUpdates();
                    } else {
                    }
                    break;
                }
        }
    }


    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
            ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);


            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    REQUEST_PERMISSIONS);

        } else {
            boolean_permission = true;
        }
    }

    private void initViews() {

        btnLogout = findViewById(R.id.btnLogout);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        btnSendMsg = findViewById(R.id.btnSendMsg);
        profile = findViewById(R.id.profile);
        driver = findViewById(R.id.driver);
        vehicle = findViewById(R.id.vehicle);
        statusHeading = findViewById(R.id.status);
        vehicleId = findViewById(R.id.vehicleId);
        title = findViewById(R.id.textHeader);
        mDrawer = findViewById(R.id.drawer_layout);
        navIcon = findViewById(R.id.navIcon);
        currentJob = findViewById(R.id.currentJob);
        refreshIcon = findViewById(R.id.refreshIcon);
        pendingJobs = findViewById(R.id.pendingJobs);
        panictext = findViewById(R.id.panicLabel);
        availabletext = findViewById(R.id.availableLabel);
        breaktext = findViewById(R.id.breakLabel);
        radioGroup = findViewById(R.id.radioGroup);


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.radio0:
                        setUpPendingJobs();
                        rvJobs.setVisibility(View.VISIBLE);
                        rvPlannedJobs.setVisibility(View.GONE);

                        break;


                    case R.id.radio1:

                        setUpPlannedJobs();

                        break;
                }
            }
        });

        l1 = findViewById(R.id.linearLayout1);//bottom
        l2 = findViewById(R.id.linearLayout2);//bottom
        l3 = findViewById(R.id.linearLayout3);//bottom

        draggabletxt = (TextView) findViewById(R.id.draggabletxt);
        draggableimg = findViewById(R.id.segmented_Iv);
        /*wait = findViewById(R.id.waitForthis);*/
        ////////////////////////////////// Language Changes ///////////////////////////////
        driver.setText(masterPojo.getDriver());
        vehicle.setText(masterPojo.getVehicle());
        statusHeading.setText(masterPojo.getStatus());
//        title.setText(masterPojo.getTreasureCargo());
        pendingJobs.setText(masterPojo.getTodayJob());
        panictext.setText(masterPojo.getBreaks());
        availabletext.setText(masterPojo.getAvailable());
        breaktext.setText(masterPojo.getPanic());
        currentJob.setText(masterPojo.getGoToCurrentJob());
        // wait.setText(masterPojo.getPleaseWait() + "");
        ////////////////////////////////// Language Changes ///////////////////////////////


        rvJobs = findViewById(R.id.rvJobs);
        rvPlannedJobs = findViewById(R.id.rvPlannedJobs);
        driverId = findViewById(R.id.driverId);
        statusText = findViewById(R.id.statusText);

        statusText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //     startActivity(new Intent(HomeActivity.this,ActivityAddCosigneeInfo.class));
            }
        });


        rvJobs.setHasFixedSize(true);
        rvPlannedJobs.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(HomeActivity.this, LinearLayoutManager.VERTICAL, false);
        rvJobs.setLayoutManager(layoutManager);
        //rvPlannedJobs.setLayoutManager(new LinearLayoutManager(HomeActivity.this));


        strings = new ArrayList<>();

        vehicleNum = Stash.getString(Constants.DRIVER_VEHICLE);
        if (vehicleNum == null || vehicleNum.isEmpty()) {
            vehicleNum = "--";
        } else {
            vehicleNum = Stash.getString(Constants.DRIVER_VEHICLE);
        }
        vehicleId.setText(vehicleNum);

        driverId.setText(driverObj.getDriverNumber() + "");
        for (int i = 0; i <= 2; i++) {
            strings.add("SLATEY RD, OXTON , BIKEN HEAD");
        }

        driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), "556465", Toast.LENGTH_SHORT).show();
            }
        });

        btnSendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, MessageActivity.class));
            }
        });

        //    mDrawer.setScrimColor(getResources().getColor());
        getSupportFragmentManager().beginTransaction().replace(R.id.navigation_container, FragmentLeftMenu.newInstance()).commitAllowingStateLoss();
        navIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawer.openDrawer(GravityCompat.START, true);
            }
        });


        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driverObj == null) {
                    return;
                }
                if (Stash.getBoolean(Constants.JOB_IS_RUNNING) == true) {
                    Toast.makeText(getApplicationContext(), masterPojo.getCantLogoutwhenajobisrunning(), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (AppStatus.getInstance(getApplicationContext()).isOnline()) {
                    callApiLogout(driverObj.getId(), driverObj.getDeviceId());
                } else {
                    return;
                }
            }
        });


        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ChangeDayNightMode();

            }
        });


        refreshIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callApiGetPendingJobs(driverObj.getId());
            }
        });

        getCurrentJobRunning();
    }

    private void setUpPlannedJobs() {
        callApiPlannedJobs(driverObj.getId());
        rvJobs.setVisibility(View.GONE);
        rvPlannedJobs.setVisibility(View.VISIBLE);
    }

    private void callApiPlannedJobs(Integer id) {
        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
            progressVisiblityGone();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);

        PlannedJobs plannedJobs = new PlannedJobs(driverObj.getId() + "", "", driverObj.getVehicleId());

        Call<WebResponseList<PlannedJobs>> call = apiService.getPlannedJobs(plannedJobs);

        call.enqueue(new Callback<WebResponseList<PlannedJobs>>() {
            @Override
            public void onResponse(Call<WebResponseList<PlannedJobs>> call, Response<WebResponseList<PlannedJobs>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {

                    progressVisiblityGone();

                    if (response.body().getData() == null || response.body().getData().size() == 0) {
                        plannedJobList = new ArrayList<>();
                        plannedJobList.addAll(new ArrayList<>());

                        plannedJobList = new ArrayList<>();
                        plannedJobList.addAll(response.body().getData());
                        mPlannedAdapter = new AdapterPlannedJobs(HomeActivity.this, plannedJobList, new AdapterPlannedJobs.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position, PlannedJobs plannedJobs) {
                                switch (view.getId()) {

                                    case R.id.cardView:

                                        Intent intent = new Intent(HomeActivity.this, ActivityNestedJobs.class);
                                        intent.putExtra(Constants.PLANNED_JOBS, plannedJobs);
                                        radioGroup.check(R.id.radio0);
                                        setUpPendingJobs();
                                        startActivity(intent);
                                        //setUpPendingJobs();

                                        break;

                                }
                            }
                        }, masterPojo);


                        rvPlannedJobs.setAdapter(mPlannedAdapter);
                        mPlannedAdapter.notifyDataSetChanged();
                        return;
                    }

                    plannedJobList = new ArrayList<>();
                    plannedJobList.addAll(response.body().getData());
                    mPlannedAdapter = new AdapterPlannedJobs(HomeActivity.this, plannedJobList, new AdapterPlannedJobs.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position, PlannedJobs plannedJobs) {
                            switch (view.getId()) {

                                case R.id.cardView:

                                    Intent intent = new Intent(HomeActivity.this, ActivityNestedJobs.class);
                                    intent.putExtra(Constants.PLANNED_JOBS, plannedJobs);
                                    startActivity(intent);

                                    break;

                            }
                        }
                    }, masterPojo);


                    rvPlannedJobs.setAdapter(mPlannedAdapter);
                    mPlannedAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<PlannedJobs>> call, Throwable t) {
                progressVisiblityGone();

            }
        });


    }


    private void setUpPendingJobs() {
        rvJobs.setVisibility(View.VISIBLE);
        rvPlannedJobs.setVisibility(View.GONE);
        callApiGetPendingJobs(driverObj.getId());
    }


    private void getCurrentJobRunning() {

        if (Stash.getBoolean(Constants.JOB_IS_RUNNING) == true) {
            currentJob.setVisibility(View.VISIBLE);
            currentJob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(HomeActivity.this, ActivityJobStatus.class);
                    intent.putExtra("showSlider", true);
                    startActivity(intent);
                    //finish();
                }
            });
        } else {
            currentJob.setVisibility(View.GONE);
        }
    }

boolean isFirstLaunch=true;
    private void gotoPos(float pos, int index) {//bottom

        Stash.put(Constants.DRIVER_STATUS, index);

        //getCurrentDriverStatus(index);


        if (isBreak) {
            if (index == PANIC_INDEX) {
                revertBackToOldPos(0, PANIC_INDEX);
                return;
            }
        } else if (isPanic) {
            if (index == BREAK_INDEX) {
                revertBackToOldPos(endL2, BREAK_INDEX);
                return;
            }
        }


        Log.e("pos", index + "");
        Log.e("previousPos", previousPos + "");


        if ((index == PANIC_INDEX && selectedindex == BREAK_INDEX)
                ||
                (index == Constants.BREAK_INDEX && selectedindex == PANIC_INDEX)) {
            index = selectedindex;
            pos = index == PANIC_INDEX ? 0 : endL2;
        }
        if (index == PANIC_INDEX) {
            draggableButton.setBackgroundTintList(getResources().getColorStateList(R.color.red_color));
            draggableimg.setImageDrawable(getResources().getDrawable(R.drawable.ic_panic));
            draggabletxt.setText(masterPojo.getPanic());
            statusText.setText(masterPojo.getPanic());
            isPanic = true;
            isBreak = false;
            if (index != selectedindex && isReloadTrue == true) {
                selectedindex = index;
                statusText.setText(masterPojo.getPanic());
                callChangeStatus(driverObj.getId() + "", 4, true);

            } else {
                selectedindex = index;
                statusText.setText(masterPojo.getPanic());
                draggableButton.setX(AVAIALBLE_INDEX);


            }
        } else if (index == Constants.AVAIALBLE_INDEX) {
            draggableButton.setBackgroundTintList(getResources().getColorStateList(R.color.selected_green));
            draggableimg.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_check));
            draggabletxt.setText(masterPojo.getAvailable());
            statusText.setText(masterPojo.getAvailable());
            isPanic = false;
            isBreak = false;

            if (index != selectedindex) {
                if (selectedindex == 2) {
                } else if (selectedindex == 0) {

                }
                selectedindex = index;

            }
            if (isFirstLaunch) {
                isFirstLaunch = false;
            } else {
                callChangeStatus(driverObj.getId() + "", 1, false);
            }
        } else if (index == Constants.BREAK_INDEX) {

            isPanic = false;
            isBreak = true;

            draggableButton.setBackgroundTintList(getResources().getColorStateList(R.color.yellow));
            draggableimg.setImageDrawable(getResources().getDrawable(R.drawable.ic_coffee));
            draggabletxt.setText(masterPojo.getBreaks());

             dialogGoBreak.showDialog(HomeActivity.this);


            if (index != selectedindex) {
                selectedindex = index;
            }
        }
        draggableButton.setX(pos);
        animate(draggableButton, (int) draggableButton.getX());
    }


    LinearLayout l1, l2, l3;//bottom
    float startL1, startL2, startL3, endL1, endL2, endL3, dX;//bottom
    TextView draggabletxt;//bottom
    ImageView draggableimg;//bottom
    RelativeLayout draggableButton;//bottom

    private void draggableButtonWork() {//bottom

        l1.post(new Runnable() {
            @Override
            public void run() {
                startL1 = l1.getX();
                endL1 = startL1 + l1.getWidth();
                if (selectedindex == 1) {
                    gotoPos(endL1, AVAIALBLE_INDEX);
                }

            }
        });

        l2.post(new Runnable() {
            @Override
            public void run() {
                startL2 = l2.getX();
                endL2 = startL2 + l2.getWidth();
                if (selectedindex == 2) {
                    gotoPos(endL2, PANIC_INDEX);


                }
            }
        });
        l3.post(new Runnable() {
            @Override
            public void run() {
                startL3 = l3.getX();
                endL3 = startL3 + l3.getWidth();
                if (selectedindex == 0) {
                    gotoPos(0, BREAK_INDEX);
                }
            }
        });
        draggableButton = (RelativeLayout) findViewById(R.id.draggableView);

        final ViewGroup.LayoutParams params = draggableButton.getLayoutParams();
        params.height = RelativeLayout.LayoutParams.MATCH_PARENT;
        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        frameLayout.post(new Runnable() {
            @Override
            public void run() {
                int totalWidth = frameLayout.getWidth();
                params.width = (int) (totalWidth / 3);
                draggableButton.setLayoutParams(params);
            }
        });
        draggableButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        dX = draggableButton.getX() - event.getRawX();
                        break;
                    case MotionEvent.ACTION_MOVE:


                        draggableButton.setX(event.getRawX() + dX);
                        animate(draggableButton, (int) draggableButton.getX());
                        break;
                    case MotionEvent.ACTION_UP:
                        /*if (Stash.getBoolean(Constants.JOB_IS_RUNNING)) {
                            resetDriverStateOffBreak();
                            return false;
                        }*/
                        extractedDoDragSetup();
                    default:
                        return false;
                }
                return true;
            }
        });

//    draggableButton.setOnTouchListener((v, event) -> {
//
//    });
    }


    private void extractedDoDragSetup() {


        try {
            selectedindex = Stash.getInt(Constants.DRIVER_STATUS,1);

        } catch (Exception e) {
            selectedindex = 1;
            e.printStackTrace();
        }


        float first2differnce = draggableButton.getX() - startL1;
        float second2differnce = startL2 - draggableButton.getX();
        if (second2differnce < 0) {
            first2differnce = draggableButton.getX() - startL2;
            second2differnce = startL3 - draggableButton.getX();
            if (first2differnce > second2differnce) {


                previousPos = 1;
                gotoPos(endL2, 2);

            } else {
                previousPos = 2;
                gotoPos(endL1, 1);
            }
        } else {
            if (first2differnce > second2differnce) {

                previousPos = 0;
                gotoPos(endL1, 1);
            } else {///panic

                if (second2differnce > 190) {
                    previousPos = 1;
                    gotoPos(0, 0);
                }

            }
        }
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String vehicleIds = intent.getStringExtra("vehicleId");
                String vehicleNumber = intent.getStringExtra("vehicleNumber");
                driverObj.setVehicleId(vehicleIds + "");
                Stash.put(Constants.DRIVER_VEHICLE, vehicleNumber);
                Stash.put(Constants.DRIVER_OBJ, driverObj);
                vehicleId.setText(vehicleNumber);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    private void revertBackToOldPos(float pos, int index) {//bottom
        if (index == 0) {
            draggableButton.setBackgroundTintList(getResources().getColorStateList(R.color.red_color));
            draggableimg.setImageDrawable(getResources().getDrawable(R.drawable.ic_panic));

            draggabletxt.setText(masterPojo.getPanic());
            if (index != selectedindex) {
                selectedindex = index;
            }

        } else if (index == 1) {
            draggableButton.setBackgroundTintList(getResources().getColorStateList(R.color.selected_green));
            draggableimg.setImageDrawable(getResources().getDrawable(R.drawable.ic_icon_check));
            draggabletxt.setText(masterPojo.getAvailable());
            if (index != selectedindex) {
                selectedindex = index;
            }
        } else if (index == 2) {
            draggableButton.setBackgroundTintList(getResources().getColorStateList(R.color.yellow));
            draggableimg.setImageDrawable(getResources().getDrawable(R.drawable.ic_coffee));
            draggabletxt.setText(masterPojo.getBreaks());
            if (index != selectedindex) {
                selectedindex = index;
            }
        }
        draggableButton.setX(pos);
        animate(draggableButton, (int) draggableButton.getX());
    }

    private void animate(View view, int moveValue) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", moveValue);
        animation.setDuration(2000);
        animation.start();
    }


    private void callApiLogout(int id, String deviceId) {
        progressVisiblityVisible();
        Log.e("id", id + "");
        Log.e("deviceId", deviceId);

        RequestLoginBody requestLoginBody = new RequestLoginBody(deviceId, id);


        RequestLogin requestLogin = new RequestLogin();
        requestLogin.setRequest(requestLoginBody);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<LoginResponse>> call = apiService.logout(requestLogin);


        call.enqueue(new Callback<WebResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<WebResponse<LoginResponse>> call, Response<WebResponse<LoginResponse>> response) {

                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();

                    try {

                        Boolean isServiceRunning = ServiceTools.isServiceRunning(
                                HomeActivity.this,
                                GoogleService.class);
                        Log.e("isServiceRunning", isServiceRunning + "");
                        if (isServiceRunning) {
                            Intent stopIntent = new Intent(HomeActivity.this, GoogleService.class);
                            stopIntent.setAction(Constants.STOPFOREGROUND_ACTION);
                            startService(stopIntent);
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Stash.clearAll();
                    Intent i = new Intent(HomeActivity.this, ActivityLogin.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);

                }
            }

            @Override
            public void onFailure(Call<WebResponse<LoginResponse>> call, Throwable t) {
                progressVisiblityGone();
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void callChangeStatus(String driverId, int status, boolean isPanic) {


        progressVisiblityVisible();
        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
            return;
        }

        ChangeStatus changeStatus = new ChangeStatus(driverId, status, isPanic);


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<JobData>> call = apiService.changeDriverStatus(changeStatus);


        call.enqueue(new Callback<WebResponse<JobData>>() {
            @Override
            public void onResponse(Call<WebResponse<JobData>> call, Response<WebResponse<JobData>> response) {

                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();

                    try {
                        Stash.put(Constants.CURRENT_STATUS, status);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<JobData>> call, Throwable t) {
                progressVisiblityGone();
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        // disableEnableControls(true, mainRl);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
        //   disableEnableControls(false, mainRl);
    }

    public void resetDriverStateOffBreak() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            draggableButton.setBackgroundTintList(getResources().getColorStateList(R.color.green_color));
            //draggableimg.setImageDrawable(getResources().getDrawable(R.drawable.whiteuser));
            draggabletxt.setText(masterPojo.getAvailable());
            draggableButton.setX(endL1);
            animate(draggableButton, (int) draggableButton.getX());
        }
    }


    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    @Override
    public void onMainSignatureSucessClicked(String bytes) {
        Toast.makeText(getApplicationContext(), bytes, Toast.LENGTH_LONG).show();
    }

    public void ChangeDayNightMode() {
        switch (getResources().getConfiguration().uiMode & Configuration.UI_MODE_NIGHT_MASK) {
            case Configuration.UI_MODE_NIGHT_YES:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                //    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case Configuration.UI_MODE_NIGHT_NO:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                //  overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
        }
    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    public class DialogGoOnBreak {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.layout_go_break);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


            Button btnYes = (Button) dialog.findViewById(R.id.btnYes);
            Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
            TextView textNote = (TextView) dialog.findViewById(R.id.textNote);


            if (Stash.getBoolean(Constants.JOB_IS_RUNNING)) {
                btnYes.setVisibility(View.GONE);
                btnNo.setText("Ok");
                textNote.setText("You can't go on break while the Job is running");
            } else {
                btnYes.setVisibility(View.VISIBLE);
                textNote.setText("Are you sure you would like to go on break?");
            }

            btnYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callChangeStatus(driverObj.getId() + "", 14, false);
                    statusText.setText(masterPojo.getBreaks());
                    dialog.dismiss();
                }
            });


            btnNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    revertBackToOldPos(endL1, 1);
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }
}


