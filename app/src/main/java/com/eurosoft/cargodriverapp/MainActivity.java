package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bcgdv.asia.lib.ticktock.TickTockView;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.RequestLogin;
import com.eurosoft.cargodriverapp.Pojo.RequestLoginBody;
import com.eurosoft.cargodriverapp.Services.GoogleService;
import com.eurosoft.cargodriverapp.Services.SignalRService;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.LocationEnable;
import com.eurosoft.cargodriverapp.Utils.ServiceTools;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private MainActivity mContext;
    Geocoder geocoder;
    private static final int REQUEST_PERMISSIONS = 100;
    boolean boolean_permission;
    private static final int REQUEST_LOCATION = 99;
    public static String str_receiver = "servicetutorial.service.receiver";

    private boolean end = false;
    private Button btnConnect, btnLogout;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout mainRl;
    private Button btnSendMsg;
    private LoginResponse driverObj;
    private TickTockView mCountDown;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Stash.init(getApplicationContext());

        mCountDown = (TickTockView) findViewById(R.id.view_ticktock_countdown);
        if (mCountDown != null) {
            mCountDown.setOnTickListener(new TickTockView.OnTickListener() {
                @Override
                public String getText(long timeRemaining) {
                    int seconds = (int) (timeRemaining / 1000) % 60;

                    return "seconds"+"";
                }
            });
        }

        // getCurrentLat();
    }

    @Override
    protected void onStart() {
        super.onStart();


        Calendar end = Calendar.getInstance();
        end.add(Calendar.SECOND, 12);

        Calendar start = Calendar.getInstance();
        start.add(Calendar.SECOND, -1);
        if (mCountDown != null) {
            mCountDown.start(start, end);
        }
    }

    private void initViews() {
        btnLogout = findViewById(R.id.btnLogout);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        btnSendMsg = findViewById(R.id.btnSendMsg);


        btnSendMsg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MessageActivity.class));
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (driverObj == null) {
                    return;
                }


                if (Stash.getBoolean(Constants.JOB_IS_RUNNING) == true) {
                    Toast.makeText(getApplicationContext(), "Can't Logout when a job is running", Toast.LENGTH_SHORT).show();
                    return;
                }
                Log.e("driverObj", driverObj.getId() + "");
                Log.e("driverObj", driverObj.getDeviceId());
                callApiLogout(driverObj.getId(), driverObj.getDeviceId());


            }
        });
    }


    private void getCurrentLat() {
        try {
            mContext = this;
            geocoder = new Geocoder(this, Locale.getDefault());
            fn_permission();
            Intent intent = new Intent(getApplicationContext(), GoogleService.class);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
            ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);


            ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    REQUEST_PERMISSIONS);

        } else {
            boolean_permission = true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (requestCode == REQUEST_LOCATION) {
                    if (grantResults.length >= 1
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // We can now safely use the API we requested access to
                        boolean_permission = true;
                        getCurrentLat();
                        fn_permission();
                        // startLocationUpdates();
                    } else {
                    }
                    break;
                }
        }
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {


            double latitude = Double.valueOf(intent.getStringExtra("latutide"));
            double longitude = Double.valueOf(intent.getStringExtra("longitude"));

            List<Address> addresses = null;

            Log.e("latitude", latitude + "");
            Log.e("longitude", longitude + "");
            //Toast.makeText(getApplicationContext(), latitude + " " + longitude, Toast.LENGTH_SHORT).show();

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(str_receiver));
    }


    private void callApiLogout(int id, String deviceId) {
        progressVisiblityVisible();
        Log.e("id", id + "");
        Log.e("deviceId", deviceId);

        RequestLoginBody requestLoginBody = new RequestLoginBody(deviceId, id);


        RequestLogin requestLogin = new RequestLogin();
        requestLogin.setRequest(requestLoginBody);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<LoginResponse>> call = apiService.logout(requestLogin);


        call.enqueue(new Callback<WebResponse<LoginResponse>>() {
            @Override
            public void onResponse(Call<WebResponse<LoginResponse>> call, Response<WebResponse<LoginResponse>> response) {

                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();

                    try {

                        Boolean isServiceRunning = ServiceTools.isServiceRunning(
                                MainActivity.this,
                                GoogleService.class);
                        Log.e("isServiceRunning", isServiceRunning + "");
                        if (isServiceRunning) {
                            Intent stopIntent = new Intent(MainActivity.this, GoogleService.class);
                            stopIntent.setAction(Constants.STOPFOREGROUND_ACTION);
                            startService(stopIntent);
                        } else {
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    Toast.makeText(MainActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    Stash.clearAll();
                    Intent i = new Intent(MainActivity.this, ActivityLogin.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);

                }
            }

            @Override
            public void onFailure(Call<WebResponse<LoginResponse>> call, Throwable t) {
                progressVisiblityGone();
                Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        disableEnableControls(true, mainRl);
    }


    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
        disableEnableControls(false, mainRl);
    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }
}