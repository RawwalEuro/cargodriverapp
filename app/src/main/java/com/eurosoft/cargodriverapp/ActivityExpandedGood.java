package com.eurosoft.cargodriverapp;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.cargodriverapp.Adapter.AdapterGoodsDetail;
import com.eurosoft.cargodriverapp.Adapter.AdapterHomeJobs;
import com.eurosoft.cargodriverapp.Pojo.GoodInfo;
import com.eurosoft.cargodriverapp.Pojo.GoodInfoModelGoodInfoModelInner;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;

import java.util.ArrayList;

public class ActivityExpandedGood extends AppCompatActivity {

    private ImageView bckIcon;
    private RecyclerView rvExpanded;
    private ArrayList<GoodInfo> lstGoods;
    private AdapterGoodsDetail adapterGoodsDetail;
    private LinearLayoutManager layoutManager;
    private TextView textHeader,measurement,desc,qty,volume,weight;
    private MasterPojo masterPojo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_expanded);
        Stash.init(getApplicationContext());
        lstGoods = new ArrayList<>();
        lstGoods = Stash.getArrayList(Constants.LIST_GOODS, GoodInfo.class);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO,MasterPojo.class);
        initViews();
    }

    private void initViews() {
        bckIcon = findViewById(R.id.bckIcon);
        rvExpanded = findViewById(R.id.rvExpanded);
        //////////////////////////////////////Language Changes ///////////////////////////////
        textHeader=findViewById(R.id.textHeader);
        measurement=findViewById(R.id.measurement);
        desc=findViewById(R.id.desc);
        qty=findViewById(R.id.qty);
        volume=findViewById(R.id.volume);
        weight=findViewById(R.id.weight);
        textHeader.setText(masterPojo.getGoodsDetails());
        measurement.setText(masterPojo.getMeasurement());
        qty.setText(masterPojo.getQty());
        volume.setText(masterPojo.getVolume());
        weight.setText(masterPojo.getWeight());
        /////////////////////////////////////////////////////////////////////////////////////


        rvExpanded.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ActivityExpandedGood.this, LinearLayoutManager.VERTICAL, true);
        rvExpanded.setLayoutManager(layoutManager);

        adapterGoodsDetail  = new AdapterGoodsDetail(ActivityExpandedGood.this, 1,lstGoods, new AdapterGoodsDetail.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.mainRl:

                        break;

                }
            }
        });

        rvExpanded.setAdapter(adapterGoodsDetail);
        adapterGoodsDetail.notifyDataSetChanged();


        bckIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
