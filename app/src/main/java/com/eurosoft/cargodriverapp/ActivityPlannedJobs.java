package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.cargodriverapp.Adapter.AdapterFutureJobs;
import com.eurosoft.cargodriverapp.Adapter.AdapterPlannedJobs;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

public class ActivityPlannedJobs extends AppCompatActivity {

    private ImageView bckIcon;
    private TextView textHeader, subHeading, distance, timeTaken;
    private RecyclerView rvPlannedJobs;
    private CircularProgressBar circularProgressBar;
    private LinearLayout mainRl;
    private RelativeLayout rlProgressBar;
    private LinearLayoutManager layoutManager;
    private AdapterPlannedJobs adapterPlannedJobs;
    private MasterPojo masterPojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planned_jobs);
        initViews();
    }

    private void initViews() {
        bckIcon = findViewById(R.id.finish);
        textHeader = findViewById(R.id.textHeader);
        subHeading = findViewById(R.id.subHeading);
        distance = findViewById(R.id.distance);
        timeTaken = findViewById(R.id.timeTaken);
        rvPlannedJobs = findViewById(R.id.rvPlannedJobs);
        masterPojo=new MasterPojo();
        ////////////////////////////////// Language Changes ///////////////////////////////////////////////
        textHeader.setText(masterPojo.getPlannedJobs());
        subHeading.setText("("+masterPojo.getPlanNumber()+"205"+")");
        ////////////////////////////////// Language Changes ///////////////////////////////////////////////

        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        rlProgressBar = findViewById(R.id.rlProgressBar);


        bckIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rvPlannedJobs.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ActivityPlannedJobs.this, LinearLayoutManager.VERTICAL, false);
        rvPlannedJobs.setLayoutManager(layoutManager);




    }
}