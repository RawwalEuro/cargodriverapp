package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.AdapterPlannedJobs;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobs;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCompletedPlanJobs extends AppCompatActivity {

    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private RecyclerView rvPlannedJobs;
    private ImageView bckBtn;
    private MasterPojo masterPojo;
    private LoginResponse driverObj;
    private AdapterPlannedJobs mPlannedAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_plan_jobs);
        Stash.init(this);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        initViews();
        callApiPlannedJobs(0);
    }

    private void initViews() {
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        rvPlannedJobs = findViewById(R.id.rvPlannedJobs);
        bckBtn = findViewById(R.id.bckBtn);
        rvPlannedJobs.setHasFixedSize(true);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        // disableEnableControls(true, mainRl);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        //   disableEnableControls(false, mainRl);
    }


    private void callApiPlannedJobs(Integer id) {

        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
            progressVisiblityGone();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        PlannedJobs plannedJobs = new PlannedJobs(driverObj.getId()+"", "" ,driverObj.getVehicleId());

        Call<WebResponseList<PlannedJobs>> call = apiService.getCompletedPlannedJobs(plannedJobs);

        call.enqueue(new Callback<WebResponseList<PlannedJobs>>() {
            @Override
            public void onResponse(Call<WebResponseList<PlannedJobs>> call, Response<WebResponseList<PlannedJobs>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();
                    mPlannedAdapter = new AdapterPlannedJobs(ActivityCompletedPlanJobs.this, response.body().getData(), new AdapterPlannedJobs.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position, PlannedJobs plannedJobs) {
                            switch (view.getId()) {

                                case R.id.cardView:

                                    Intent intent = new Intent(ActivityCompletedPlanJobs.this, ActivityCompletedNestedJobs.class);
                                    intent.putExtra(Constants.PLANNED_JOBS, plannedJobs);
                                    startActivity(intent);

                                    break;

                            }
                        }
                    }, masterPojo);

                    rvPlannedJobs.setAdapter(mPlannedAdapter);
                    mPlannedAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<PlannedJobs>> call, Throwable t) {
                progressVisiblityGone();
            }
        });
    }

}