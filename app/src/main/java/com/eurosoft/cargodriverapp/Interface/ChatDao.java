package com.eurosoft.cargodriverapp.Interface;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.eurosoft.cargodriverapp.Pojo.Message;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface ChatDao {

    @Query("SELECT * FROM message")
    List<Message> getAll();

    @Insert
    void insert(Message message);

    @Delete
    void delete(Message message);

    @Update
    void update(Message message);

    @Query("DELETE  FROM message")
    void deleteAllMessages();
}