package com.eurosoft.cargodriverapp.Interface;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.eurosoft.cargodriverapp.Utils.JobData;

import java.util.List;
@Dao
public interface NextJobsDao {

    @Query("SELECT * FROM jobdata")
    List<JobData> getAllNextJobs();

    @Insert
    void insert(JobData jobData);

    @Delete
    void delete(JobData jobData);

    @Update
    void update(JobData jobData);

    @Query("DELETE  FROM message")
    void deleteAllMessages();
}
