package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.AdapterHomeJobs;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.JobDetailData;
import com.eurosoft.cargodriverapp.Pojo.JobNotificationStatus;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.ResponseJobNotification;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNextJobs extends AppCompatActivity {

    private ImageView bckBtn;
    private RecyclerView nextJobsRv;
    private TextView headerNextJobs;
    private MasterPojo masterPojo;
    private ViewDialog alertDialoge;
    private LoginResponse driverObj;
    private JobNotificationStatus jobNotificationStatus;
    private AdapterHomeJobs mAdapter;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next_jobs);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO,MasterPojo.class);
        alertDialoge = new ViewDialog();
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);

        initViews();
        callApiGetPendingJobs(driverObj.getId());
        Stash.put(Constants.IS_FOJ_ACCEPTED,false);
    }

    private void initViews() {

        bckBtn = findViewById(R.id.bckBtn);
        nextJobsRv = findViewById(R.id.nextJobsRv);
        headerNextJobs = findViewById(R.id.headerNextJobs);

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        nextJobsRv.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ActivityNextJobs.this, LinearLayoutManager.VERTICAL, false);
        nextJobsRv.setLayoutManager(layoutManager);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private void callApiGetPendingJobs(Integer id) {

        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
            progressVisiblityGone();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseList<JobDetailData>> call = apiService.getPendingJobs(id + "", 15);

        call.enqueue(new Callback<WebResponseList<JobDetailData>>() {
            @Override
            public void onResponse(Call<WebResponseList<JobDetailData>> call, Response<WebResponseList<JobDetailData>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();
                    mAdapter = new AdapterHomeJobs(ActivityNextJobs.this, response.body().getData(), masterPojo,
                            new AdapterHomeJobs.OnItemClickListenerHome() {
                                @Override
                                public void onItemClick(View view, int position, int selectedOption, JobDetailData jobDetailData) {
                                    switch (view.getId()) {

                                        case R.id.fullCard:


                                            /*Intent i = new Intent(HomeActivity.this, ActivityJobStatus.class);
                                            i.putExtra("showSlider", showSlider);
                                            i.putExtra("object", jobDetailData);
                                            startActivity(i);*/

                                            break;


                                        case R.id.llshowDetails:
                                            boolean showSlider = false;
                                           // Stash.put(Constants.JOB_ID, jobDetailData.getId());
                                            Intent i = new Intent(ActivityNextJobs.this, ActivityJobStatus.class);
                                            i.putExtra("showSlider", showSlider);
                                            i.putExtra("object", jobDetailData);
                                            Log.e("ID", jobDetailData.getId() + "");
                                            startActivity(i);
                                            break;


                                        case R.id.llRecover:
                                            alertDialoge.showDialog(ActivityNextJobs.this, true, jobDetailData);
                                            break;


                                        case R.id.llStart:
                                            alertDialoge.showDialog(ActivityNextJobs.this, false, jobDetailData);
                                            break;
                                    }
                                }
                            });

                    nextJobsRv.setAdapter(mAdapter);
                    mAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<WebResponseList<JobDetailData>> call, Throwable t) {
                progressVisiblityGone();

            }
        });


    }

    public class ViewDialog {

        public void showDialog(Activity activity, boolean isRecover, JobDetailData jobDetailData) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_recover_restart);


            TextView areYouSure = (TextView) dialog.findViewById(R.id.areYouSure);

            Button btnRecover = (Button) dialog.findViewById(R.id.btnRecover);
            Button btnReject = (Button) dialog.findViewById(R.id.btnLeave);


            if (isRecover) {
                areYouSure.setText(masterPojo.getAreyousureyouwanttorecoverthisjob());
                btnRecover.setText(masterPojo.getRecover());
            } else {
                areYouSure.setText(masterPojo.getAreyousureyouwanttostartthisjob());
                btnRecover.setText(masterPojo.getStart());
            }


            btnRecover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isRecover) {
                        dialog.dismiss();
                        callApiSendStatusAccept(9, jobDetailData, false);
                    } else {
                        dialog.dismiss();
                        callApiSendStatusAccept(5, jobDetailData, true);
                    }
                }
            });

            btnReject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }
    }

    private void callApiSendStatusAccept(int i, JobDetailData jobDetailData, boolean isRecover) {
        progressVisiblityVisible();

        if (isRecover) {
            jobNotificationStatus = new JobNotificationStatus(jobDetailData.getId() + "", driverObj.getId() + "", i + "", "Start" + "", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID), "DriverApp,Android");


        } else {

            jobNotificationStatus = new JobNotificationStatus(jobDetailData.getId() + "", driverObj.getId() + "", i + "", "Recover" + "", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID), "DriverApp,Android");
        }
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<ResponseJobNotification>> call = apiService.sendStatus(jobNotificationStatus);

        call.enqueue(new Callback<WebResponse<ResponseJobNotification>>() {
            @Override
            public void onResponse(Call<WebResponse<ResponseJobNotification>> call, Response<WebResponse<ResponseJobNotification>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    if (isRecover == false) {
                        callApiGetPendingJobs(driverObj.getId());
                        return;
                    }

                    boolean showSlider = true;
                    Stash.put(Constants.JOB_ID, jobDetailData.getId());
                    Intent i = new Intent(ActivityNextJobs.this, ActivityJobStatus.class);
                    i.putExtra("showSlider", showSlider);
                    startActivity(i);
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    if (isRecover == false) {
                        callApiGetPendingJobs(driverObj.getId());
                        return;
                    }
                    boolean showSlider = true;
                    Stash.put(Constants.JOB_ID, jobDetailData.getId());
                    Intent i = new Intent(ActivityNextJobs.this, ActivityJobStatus.class);
                    i.putExtra("showSlider", showSlider);
                    startActivity(i);
                    progressVisiblityGone();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ResponseJobNotification>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong() + "", Toast.LENGTH_SHORT).show();
                progressVisiblityGone();
            }
        });
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        // disableEnableControls(true, mainRl);
    }


    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        //   disableEnableControls(false, mainRl);
    }
}