package com.eurosoft.cargodriverapp.Utils.Db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.eurosoft.cargodriverapp.Interface.ChatDao;
import com.eurosoft.cargodriverapp.Interface.NextJobsDao;
import com.eurosoft.cargodriverapp.Pojo.Message;
import com.eurosoft.cargodriverapp.Utils.JobData;

@Database(entities = {Message.class, JobData.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {

  public abstract ChatDao chatDao();
  public abstract NextJobsDao nextJobsDao();
}
