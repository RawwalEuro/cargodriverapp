package com.eurosoft.cargodriverapp.Utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebResponse<T> {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Data")
    @Expose
    private T data;

    @SerializedName("HasError")
    @Expose
    private Boolean success;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

}

