package com.eurosoft.cargodriverapp.Utils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebResponseString<T> {

    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("Data")
    @Expose
    private String string;

    @SerializedName("HasError")
    @Expose
    private Boolean success;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return string;
    }

    public void setData(String data) {
        this.string = data;
    }

}