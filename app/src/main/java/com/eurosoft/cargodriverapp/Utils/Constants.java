package com.eurosoft.cargodriverapp.Utils;

public class Constants {

    public static final String STARTFOREGROUND_ACTION = "STARTFOREGROUND_ACTION";
    public static final String STOPFOREGROUND_ACTION = "STOPFOREGROUND_ACTION";
    public static final String BASE_URL = "https://cabtreasureappapi.co.uk/CabTreasureWebApi/Home/";
    public static final String TEST_URL = "http://88.208.220.41/DTS_API/";
    public static final String DRIVER_OBJ = "DRIVER_OBJ";
    public static final String DRIVER_Dummy_Veh = "DRIVER_Dummy_Veh";
    public static final String isLoggenIn = "isLoggenIn";
    public static final String SERVER_URL = "http://88.208.220.41:9000";
    public static final String DEVICE_ID = "DEVICE_ID";
    public static final String SOCKET_ID = "SOCKET_ID";
    public static final String CURRENT_JOB_STATUS = "CURRENT_JOB_STATUS";
    public static final String JOB_IS_RUNNING = "JOB_IS_RUNNING";
    public static final String IS_AUTHORIZED = "IS_AUTHORIZED";
    public static final String JOB_ID = "JOB_ID";
    public static final String LIST_GOODS = "LIST_GOODS";
    public static final String DRIVER_VEHICLE = "DRIVER_VEHICLE";
    public static final String DRIVER_ID = "DRIVER_ID";
    public static final String CURRENT_STATUS = "CURRENT_STATUS";
    public static final String CHATACTIVITY_IS_RUNNING = "CHATACTIVITY_IS_RUNNING";
    public static final String JOB_DETAILS = "JOB_DETAILS";
    public static final String MASTER_POJO = "MASTER_POJO";
    public static final String IS_LANG_CHANGED = "IS_LANG_CHANGED";
    public static final String JOB_TYPE = "JOB_TYPE";
    public static final String JOB_ID_FOLLOW_ON = "JOB_ID_FOLLOW_ON";
    public static final String IS_FOJ_ACCEPTED = "IS_FOJ_ACCEPTED";
    public static final String PLANNED_JOBS = "PLANNED_JOBS";
    public static final String CLOSE_CHAT = "CLOSE_CHAT";
    public static final String PLANNED_ID = "PLANNED_ID";
    public static final String TAP_PLANNED = "TAP_PLANNED";
    public static final String CONFIRMED_VEHICLE = "CONFIRMED_VEHICLE";
    public static final String ADDITIONAL_MSG = "ADDITIONAL_MSG";
    public static final String IS_LAUNCH = "IS_LAUNCH";
    public static final String PASS_JOB_DETAIL_OBJ = "PASS_JOB_DETAIL_OBJ";
    public static final String DRIVER_STATUS = "DRIVER_STATUS";

    public static Double lat = 0.0;
    public static Double lng = 0.0;
    public static float speed = 0.0f;

    public static int AVAIALBLE_INDEX = 1;
    public static int PANIC_INDEX = 2;
    public static int BREAK_INDEX = 0;
}
