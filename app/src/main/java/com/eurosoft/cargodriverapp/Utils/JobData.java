package com.eurosoft.cargodriverapp.Utils;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
@Entity
public class JobData implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "JobDataId")
    @SerializedName("JobDataId")
    @Expose
    private Integer jobDataId;

    @ColumnInfo(name = "AccountAddressId")
    @SerializedName("AccountAddressId")
    @Expose
    private Integer accountAddressId;

    @ColumnInfo(name = "Account_Number")
    @SerializedName("Account_Number")
    @Expose
    private String accountNumber;

    @ColumnInfo(name = "ColorCode")
    @SerializedName("ColorCode")
    @Expose
    private String ColorCode;

    @ColumnInfo(name = "ServiceDesc")
    @SerializedName("ServiceDesc")
    @Expose
    private String ServiceDesc;

    @ColumnInfo(name = "Company_Name")
    @SerializedName("Company_Name")
    @Expose
    private String companyName;

    @ColumnInfo(name = "Country_Code")
    @SerializedName("Country_Code")
    @Expose
    private String countryCode;

    @ColumnInfo(name = "InvoiceCurrency")
    @SerializedName("InvoiceCurrency")
    @Expose
    private String invoiceCurrency;

    @ColumnInfo(name = "Address_Line_1")
    @SerializedName("Address_Line_1")
    @Expose
    private String addressLine1;

    @ColumnInfo(name = "Address_Line_2")
    @SerializedName("Address_Line_2")
    @Expose
    private String addressLine2;

    @ColumnInfo(name = "Address_Line_3")
    @SerializedName("Address_Line_3")
    @Expose
    private String addressLine3;

    @ColumnInfo(name = "Address_Line_4")
    @SerializedName("Address_Line_4")
    @Expose
    private String addressLine4;

    @ColumnInfo(name = "PostalCode")
    @SerializedName("PostalCode")
    @Expose
    private String postalCode;

    @ColumnInfo(name = "FirstName")
    @SerializedName("FirstName")
    @Expose
    private String firstName;

    @ColumnInfo(name = "Phone")
    @SerializedName("Phone")
    @Expose
    private String phone;

    @ColumnInfo(name = "EmailAddress")
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;

    @ColumnInfo(name = "Shipper")
    @SerializedName("Shipper")
    @Expose
    private Integer shipper;

    @ColumnInfo(name = "Consignee")
    @SerializedName("Consignee")
    @Expose
    private Integer consignee;

    @ColumnInfo(name = "CollectionDate")
    @SerializedName("CollectionDate")
    @Expose
    private String collectionDate;

    @ColumnInfo(name = "CollectionFromTime")
    @SerializedName("CollectionFromTime")
    @Expose
    private String collectionFromTime;

    @ColumnInfo(name = "CollectionTillTime")
    @SerializedName("CollectionTillTime")
    @Expose
    private String collectionTillTime;

    @ColumnInfo(name = "DeliveryDate")
    @SerializedName("DeliveryDate")
    @Expose
    private String deliveryDate;

    @ColumnInfo(name = "DeliveryFromTime")
    @SerializedName("DeliveryFromTime")
    @Expose
    private String deliveryFromTime;

    @ColumnInfo(name = "DeliveryTillTime")
    @SerializedName("DeliveryTillTime")
    @Expose
    private String deliveryTillTime;

    @ColumnInfo(name = "Createdby")
    @SerializedName("Createdby")
    @Expose
    private Integer createdby;

    @ColumnInfo(name = "CreateDate")
    @SerializedName("CreateDate")
    @Expose
    private String createDate;

    @ColumnInfo(name = "CollectionAddr")
    @SerializedName("CollectionAddr")
    @Expose
    private String collectionAddr;

    @ColumnInfo(name = "DeliveryAddr")
    @SerializedName("DeliveryAddr")
    @Expose
    private String deliveryAddr;

    @ColumnInfo(name = "Country")
    @SerializedName("Country")
    @Expose
    private String country;

    @ColumnInfo(name = "JobType")
    @SerializedName("JobType")
    @Expose
    private String jobType;

    @ColumnInfo(name = "Date")
    @SerializedName("Date")
    @Expose
    private String date;

    @ColumnInfo(name = "FromTime")
    @SerializedName("FromTime")
    @Expose
    private String fromTime;

    @ColumnInfo(name = "TillTime")
    @SerializedName("TillTime")
    @Expose
    private String tillTime;

    @ColumnInfo(name = "ShiftType")
    @SerializedName("ShiftType")
    @Expose
    private Integer shiftType;

    @ColumnInfo(name = "JobStatus")
    @SerializedName("JobStatus")
    @Expose
    private String jobStatus;

    @ColumnInfo(name = "JobStatusId")
    @SerializedName("JobStatusId")
    @Expose
    private Integer jobStatusId;

    @ColumnInfo(name = "DriverId")
    @SerializedName("DriverId")
    @Expose
    private Integer driverId;

    @ColumnInfo(name = "JobAddrId")
    @SerializedName("JobAddrId")
    @Expose
    private String JobAddrId;


    public JobData(String jobAddrId) {
        JobAddrId = jobAddrId;
    }

    public Integer getJobDataId() {
        return jobDataId;
    }

    public void setJobDataId(Integer jobDataId) {
        this.jobDataId = jobDataId;
    }

    public Integer getAccountAddressId() {
        return accountAddressId;
    }

    public void setAccountAddressId(Integer accountAddressId) {
        this.accountAddressId = accountAddressId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getInvoiceCurrency() {
        return invoiceCurrency;
    }

    public void setInvoiceCurrency(String invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Integer getShipper() {
        return shipper;
    }

    public void setShipper(Integer shipper) {
        this.shipper = shipper;
    }

    public Integer getConsignee() {
        return consignee;
    }

    public void setConsignee(Integer consignee) {
        this.consignee = consignee;
    }

    public String getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    public String getCollectionFromTime() {
        return collectionFromTime;
    }

    public void setCollectionFromTime(String collectionFromTime) {
        this.collectionFromTime = collectionFromTime;
    }

    public String getCollectionTillTime() {
        return collectionTillTime;
    }

    public void setCollectionTillTime(String collectionTillTime) {
        this.collectionTillTime = collectionTillTime;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryFromTime() {
        return deliveryFromTime;
    }

    public void setDeliveryFromTime(String deliveryFromTime) {
        this.deliveryFromTime = deliveryFromTime;
    }

    public String getDeliveryTillTime() {
        return deliveryTillTime;
    }

    public void setDeliveryTillTime(String deliveryTillTime) {
        this.deliveryTillTime = deliveryTillTime;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCollectionAddr() {
        return collectionAddr;
    }

    public void setCollectionAddr(String collectionAddr) {
        this.collectionAddr = collectionAddr;
    }

    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getDate() {
        if(date == null){
            date = "--";
        }
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        if(fromTime == null){
            fromTime = "--";
        }
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getTillTime() {

        if(tillTime == null){
            tillTime = "--";
        }
        return tillTime;
    }

    public void setTillTime(String tillTime) {
        this.tillTime = tillTime;
    }

    public Integer getShiftType() {
        return shiftType;
    }

    public void setShiftType(Integer shiftType) {
        this.shiftType = shiftType;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Integer getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(Integer jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getJobAddrId() {
        return JobAddrId;
    }

    public void setJobAddrId(String jobAddrId) {
        JobAddrId = jobAddrId;
    }

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getServiceDesc() {
        return ServiceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        ServiceDesc = serviceDesc;
    }

    public JobData(int id, Integer jobDataId, Integer accountAddressId, String accountNumber, String companyName, String countryCode, String invoiceCurrency, String addressLine1, String addressLine2, String addressLine3, String addressLine4, String postalCode, String firstName, String phone, String emailAddress, Integer shipper, Integer consignee, String collectionDate, String collectionFromTime, String collectionTillTime, String deliveryDate, String deliveryFromTime, String deliveryTillTime, Integer createdby, String createDate, String collectionAddr, String deliveryAddr, String country, String jobType, String date, String fromTime, String tillTime, Integer shiftType, String jobStatus, Integer jobStatusId, Integer driverId, String jobAddrId) {
        this.id = id;
        this.jobDataId = jobDataId;
        this.accountAddressId = accountAddressId;
        this.accountNumber = accountNumber;
        this.companyName = companyName;
        this.countryCode = countryCode;
        this.invoiceCurrency = invoiceCurrency;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
        this.addressLine4 = addressLine4;
        this.postalCode = postalCode;
        this.firstName = firstName;
        this.phone = phone;
        this.emailAddress = emailAddress;
        this.shipper = shipper;
        this.consignee = consignee;
        this.collectionDate = collectionDate;
        this.collectionFromTime = collectionFromTime;
        this.collectionTillTime = collectionTillTime;
        this.deliveryDate = deliveryDate;
        this.deliveryFromTime = deliveryFromTime;
        this.deliveryTillTime = deliveryTillTime;
        this.createdby = createdby;
        this.createDate = createDate;
        this.collectionAddr = collectionAddr;
        this.deliveryAddr = deliveryAddr;
        this.country = country;
        this.jobType = jobType;
        this.date = date;
        this.fromTime = fromTime;
        this.tillTime = tillTime;
        this.shiftType = shiftType;
        this.jobStatus = jobStatus;
        this.jobStatusId = jobStatusId;
        this.driverId = driverId;
        JobAddrId = jobAddrId;
    }

    public JobData() {

    }

}
