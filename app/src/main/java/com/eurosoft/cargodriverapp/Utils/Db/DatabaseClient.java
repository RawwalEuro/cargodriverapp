package com.eurosoft.cargodriverapp.Utils.Db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

public class DatabaseClient {
    private Context mCtx;
    private static DatabaseClient mInstance;

    private AppDataBase appDatabase;

    private DatabaseClient(Context mCtx) {
        this.mCtx = mCtx;
        appDatabase = Room.databaseBuilder(mCtx, AppDataBase.class, "TreasureCargo").
                allowMainThreadQueries().addMigrations(migration).
                build();

    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance;
    }

    public AppDataBase getAppDatabase() {
        return appDatabase;
    }

    Migration migration = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
          //  database.execSQL("ALTER TABLE user ADD COLUMN isActive INTEGER NOT NULL DEFAULT(1)");
        }
    };
}
