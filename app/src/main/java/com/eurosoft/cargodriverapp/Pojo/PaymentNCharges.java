package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentNCharges implements Serializable {

    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;


    @SerializedName("total")
    @Expose
    private String total;


    @SerializedName("price")
    @Expose
    private String price;


    @SerializedName("service")
    @Expose
    private String service;


    @SerializedName("packaging")
    @Expose
    private String packaging;


    @SerializedName("extra")
    @Expose
    private String extra;


    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
