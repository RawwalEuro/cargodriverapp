package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CosigneeName implements Serializable {


    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Account_Number")
    @Expose
    private String accountNumber;
    @SerializedName("Company_Name")
    @Expose
    private String companyName;
    @SerializedName("Country_Code")
    @Expose
    private String countryCode;
    @SerializedName("InvoiceCurrency")
    @Expose
    private String invoiceCurrency;
    @SerializedName("Address_Line_1")
    @Expose
    private String addressLine1;
    @SerializedName("Address_Line_2")
    @Expose
    private String addressLine2;
    @SerializedName("Address_Line_3")
    @Expose
    private String addressLine3;
    @SerializedName("Address_Line_4")
    @Expose
    private String addressLine4;
    @SerializedName("Postal_Code")
    @Expose
    private String postalCode;
    @SerializedName("Contact_Name")
    @Expose
    private String contactName;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("CollectionAddress")
    @Expose
    private String collectionAddress;
    @SerializedName("AccountAddressId")
    @Expose
    private Object accountAddressId;
    @SerializedName("AccountInformationId")
    @Expose
    private String accountInformationId;
    @SerializedName("ACCNo")
    @Expose
    private Object aCCNo;


    @SerializedName("Search")
    @Expose
    private String Search;


    public CosigneeName() {

    }


    public CosigneeName(String accountInformationId, String search) {
        this.accountInformationId = accountInformationId;
        Search = search;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCountryCode() {
        return  countryCode == null || countryCode.isEmpty() ? "" : countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getInvoiceCurrency() {
        return invoiceCurrency == null || invoiceCurrency.isEmpty() ? "" : invoiceCurrency;
    }

    public void setInvoiceCurrency(String invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    public String getAddressLine1() {
        return  addressLine1 == null || addressLine1.isEmpty() ? "" : addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return  addressLine2 == null || addressLine2.isEmpty() ? "" : addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3 == null || addressLine3.isEmpty() ? "" : addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4 == null || addressLine4.isEmpty() ? "" : addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhone() {
        return  phone == null || phone.isEmpty() ? "" : phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCollectionAddress() {
        return collectionAddress;
    }

    public void setCollectionAddress(String collectionAddress) {
        this.collectionAddress = collectionAddress;
    }

    public Object getAccountAddressId() {
        return accountAddressId;
    }

    public void setAccountAddressId(Object accountAddressId) {
        this.accountAddressId = accountAddressId;
    }

    public String getAccountInformationId() {
        return accountInformationId;
    }

    public void setAccountInformationId(String accountInformationId) {
        this.accountInformationId = accountInformationId;
    }

    public Object getACCNo() {
        return aCCNo;
    }

    public void setACCNo(Object aCCNo) {
        this.aCCNo = aCCNo;
    }
}
