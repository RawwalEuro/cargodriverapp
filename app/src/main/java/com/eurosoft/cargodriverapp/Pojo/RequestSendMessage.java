package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestSendMessage {

    @SerializedName("LoginId")
    @Expose
    private String LoginId;

    @SerializedName("ToId")
    @Expose
    private String ToId;

    @SerializedName("request")
    @Expose
    private RequestMessage request;


    public void setLoginId(String loginId) {
        LoginId = loginId;
    }

    public void setRequest(RequestMessage request) {
        this.request = request;
    }

    public String getToId() {
        return ToId;
    }

    public void setToId(String toId) {
        ToId = toId;
    }

    public String getLoginId() {
        return LoginId;
    }

    public RequestMessage getRequest() {
        return request;
    }
}
