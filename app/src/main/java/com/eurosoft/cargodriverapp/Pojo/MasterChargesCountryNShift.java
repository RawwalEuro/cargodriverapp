package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class MasterChargesCountryNShift implements Serializable {


    @SerializedName("charges")
    @Expose
    private List<PaymentNChargesField> charges = null;
    @SerializedName("shiftSettings")
    @Expose
    private List<ShiftSettings> shiftSettings = null;
    @SerializedName("countries")
    @Expose
    private List<Country> countries = null;


    @SerializedName("MeasurementTypes")
    @Expose
    private List<MeasurementTypes> measurementTypesList = null;


    public List<MeasurementTypes> getMeasurementTypesList() {
        return measurementTypesList;
    }

    public void setMeasurementTypesList(List<MeasurementTypes> measurementTypesList) {
        this.measurementTypesList = measurementTypesList;
    }

    public List<PaymentNChargesField> getCharges() {
        return charges;
    }

    public void setCharges(List<PaymentNChargesField> charges) {
        this.charges = charges;
    }

    public List<ShiftSettings> getShiftSettings() {
        return shiftSettings;
    }

    public void setShiftSettings(List<ShiftSettings> shiftSettings) {
        this.shiftSettings = shiftSettings;
    }

    public List<Country> getCountries() {
        return countries;
    }

    public void setCountries(List<Country> countries) {
        this.countries = countries;
    }

}
