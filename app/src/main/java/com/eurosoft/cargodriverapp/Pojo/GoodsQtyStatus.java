package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GoodsQtyStatus implements Serializable {

    @SerializedName("GoodsQty")
    @Expose
    private String goodsQty;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Volume")
    @Expose
    private String volume;
    @SerializedName("Weight")
    @Expose
    private String weight;

    public String getGoodsQty() {
        return goodsQty;
    }

    public void setGoodsQty(String goodsQty) {
        this.goodsQty = goodsQty;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

}
