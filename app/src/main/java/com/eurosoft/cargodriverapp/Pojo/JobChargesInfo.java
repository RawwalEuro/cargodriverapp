package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class JobChargesInfo implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("JobDataId")
    @Expose
    private int jobDataId;
    @SerializedName("ChargeId")
    @Expose
    private Integer chargeId;
    @SerializedName("Amount")
    @Expose
    private String amount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getJobDataId() {
        return jobDataId;
    }

    public void setJobDataId(int jobDataId) {
        this.jobDataId = jobDataId;
    }

    public Integer getChargeId() {
        return chargeId;
    }

    public void setChargeId(Integer chargeId) {
        this.chargeId = chargeId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
