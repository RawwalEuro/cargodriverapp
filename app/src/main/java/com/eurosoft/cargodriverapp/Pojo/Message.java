package com.eurosoft.cargodriverapp.Pojo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity
public class Message implements Serializable {

    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_LOG = 1;
    public static final int TYPE_ACTION = 2;


    public Message( int mType, String mMessage, String mUsername, String msgDate) {
        this.mType = mType;
        this.mMessage = mMessage;
        this.mUsername = mUsername;
        this.msgDate = msgDate;
    }



    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "mType")
    private int mType;

    @ColumnInfo(name = "mMessage")
    private String mMessage;

    @ColumnInfo(name = "mUsername")
    private String mUsername;


    @ColumnInfo(name = "msgDate")
    private String msgDate;

    private Message() {}


    public int getmType() {
        return mType;
    }

    public String getmMessage() {
        return mMessage;
    }

    public String getmUsername() {
        return mUsername;
    }

    public int getType() {
        return mType;
    };

    public String getMessage() {
        return mMessage;
    };

    public String getUsername() {
        return mUsername;
    };

    public String getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        this.msgDate = msgDate;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public static class Builder {
        private final int tType;
        private String userNamee;
        private String mMessageee;
        private String datee;

        public Builder(int type) {
            tType = type;
        }

        public Builder username(String username) {
            userNamee = username;
            return this;
        }
        public Builder mDate(String mDate) {
            this.datee = mDate;
            return this;
        }

        public Builder message(String message) {
            mMessageee = message;
            return this;
        }

        public Message build() {
            Message message = new Message();
            message.mType = tType;
            message.mUsername = userNamee;
            message.mMessage = mMessageee;
            message.msgDate = datee;
            return message;
        }
    }
}
