package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SmartChecklistPojo {
        private  int Id;
    private String heading;


    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public List<ChecklistQuestionPojo> getQuestionList() {
        return questionList;
    }

    public void setQuestionList(List<ChecklistQuestionPojo> questionList) {
        this.questionList = questionList;
    }

    @SerializedName("Detail")
    private List<ChecklistQuestionPojo> questionList;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }
}
