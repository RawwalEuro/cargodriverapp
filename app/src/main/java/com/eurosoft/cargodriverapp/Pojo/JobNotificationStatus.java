package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobNotificationStatus {


    @SerializedName("JobAddrId")
    @Expose
    private String jobAddrId;
    @SerializedName("DriverId")
    @Expose
    private String driverId;
    @SerializedName("JobStatusId")
    @Expose
    private String jobStatusId;
    @SerializedName("JobStatus")
    @Expose
    private String jobStatus;
    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("fromname")
    @Expose
    private String fromname;
    @SerializedName("transId")
    @Expose
    private String transId;

    public JobNotificationStatus(String jobAddrId, String driverId, String jobStatusId, String jobStatus, String deviceId, String fromname, String transId, int fillLater) {
        this.jobAddrId = jobAddrId;
        this.driverId = driverId;
        this.jobStatusId = jobStatusId;
        this.jobStatus = jobStatus;
        this.deviceId = deviceId;
        this.fromname = fromname;
        this.transId = transId;
        this.fillLater = fillLater;
    }

    @SerializedName("FillLater")
    @Expose
    private int fillLater;


    public JobNotificationStatus(String jobAddrId, String driverId, String jobStatusId, String jobStatus, String deviceId, String fromname) {
        this.jobAddrId = jobAddrId;
        this.driverId = driverId;
        this.jobStatusId = jobStatusId;
        this.jobStatus = jobStatus;
        this.deviceId = deviceId;
        this.fromname = fromname;
    }

    public String getJobAddrId() {
        return jobAddrId;
    }

    public void setJobAddrId(String jobAddrId) {
        this.jobAddrId = jobAddrId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(String jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFromname() {
        return fromname;
    }

    public void setFromname(String fromname) {
        this.fromname = fromname;
    }


    public String getTransId() {
        return transId;
    }

    public int getFillLater() {
        return fillLater;
    }
}
