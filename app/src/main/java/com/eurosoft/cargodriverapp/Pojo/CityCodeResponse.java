package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CityCodeResponse implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("CountryId")
    @Expose
    private Integer countryId;
    @SerializedName("CountryCode")
    @Expose
    private String countryCode;
    @SerializedName("Code")
    @Expose
    private String code;
    @SerializedName("CityDesc")
    @Expose
    private String cityDesc;


    @SerializedName("CityCode")
    @Expose
    private String CityCode;


    public CityCodeResponse(String cityCode) {
        CityCode = cityCode;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCityDesc() {
        return cityDesc;
    }

    public void setCityDesc(String cityDesc) {
        this.cityDesc = cityDesc;
    }
}
