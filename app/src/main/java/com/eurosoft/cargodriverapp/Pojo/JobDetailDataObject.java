package com.eurosoft.cargodriverapp.Pojo;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class JobDetailDataObject implements Serializable {

    @SerializedName("status")
    @Expose
    private Status status;

    @SerializedName("GoodsQtyStatus")
    @Expose
    private GoodsQtyStatus goodsQtyStatus;

    @SerializedName("Shipper")
    @Expose
    private Shipper shipper;
    @SerializedName("consignee")
    @Expose
    private Cosignee consignee;
    @SerializedName("goodinfo")
    @Expose
    private List<GoodInfo> goodinfo = null;


    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Shipper getShipper() {
        return shipper;
    }

    public void setShipper(Shipper shipper) {
        this.shipper = shipper;
    }

    public Cosignee getConsignee() {
        return consignee;
    }

    public GoodsQtyStatus getGoodsQtyStatus() {
        return goodsQtyStatus;
    }

    public void setGoodsQtyStatus(GoodsQtyStatus goodsQtyStatus) {
        this.goodsQtyStatus = goodsQtyStatus;
    }

    public void setConsignee(Cosignee consignee) {
        this.consignee = consignee;
    }

    public List<GoodInfo> getGoodinfo() {
        return goodinfo;
    }

    public void setGoodinfo(List<GoodInfo> goodinfo) {
        this.goodinfo = goodinfo;
    }
}
