package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Shipper implements Serializable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("JobDataId")
    @Expose
    private Integer jobDataId;
    @SerializedName("Account_Number")
    @Expose
    private String accountNumber;
    @SerializedName("Company_Name")
    @Expose
    private String companyName;
    @SerializedName("Country_Code")
    @Expose
    private String countryCode;
    @SerializedName("Address_Line_1")
    @Expose
    private String addressLine1;
    @SerializedName("Address_Line_2")
    @Expose
    private String addressLine2;
    @SerializedName("Address_Line_3")
    @Expose
    private String addressLine3;
    @SerializedName("Address_Line_4")
    @Expose
    private String addressLine4;
    @SerializedName("PostalCode")
    @Expose
    private String postalCode;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("CollectionAddr")
    @Expose
    private String collectionAddr;
    @SerializedName("DeliveryAddr")
    @Expose
    private String deliveryAddr;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("FromTime")
    @Expose
    private String fromTime;
    @SerializedName("TillTime")
    @Expose
    private String tillTime;
    @SerializedName("JobStatus")
    @Expose
    private String jobStatus;
    @SerializedName("JobStatusId")
    @Expose
    private Integer jobStatusId;
    @SerializedName("DriverId")
    @Expose
    private Integer driverId;
    @SerializedName("IsSigned")
    @Expose
    private Boolean isSigned;
    @SerializedName("SignatureDatetime")
    @Expose
    private String signatureDatetime;
    @SerializedName("CollectionLatLng")
    @Expose
    private String collectionLatLng;
    @SerializedName("DeliveryLatLng")
    @Expose
    private String deliveryLatLng;
    @SerializedName("JobRefNo")
    @Expose
    private String jobRefNo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobDataId() {
        return jobDataId;
    }

    public void setJobDataId(Integer jobDataId) {
        this.jobDataId = jobDataId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCollectionAddr() {
        return collectionAddr;
    }

    public void setCollectionAddr(String collectionAddr) {
        this.collectionAddr = collectionAddr;
    }

    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getTillTime() {
        return tillTime;
    }

    public void setTillTime(String tillTime) {
        this.tillTime = tillTime;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Integer getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(Integer jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Boolean getIsSigned() {
        return isSigned;
    }

    public void setIsSigned(Boolean isSigned) {
        this.isSigned = isSigned;
    }

    public String getSignatureDatetime() {
        return signatureDatetime;
    }

    public void setSignatureDatetime(String signatureDatetime) {
        this.signatureDatetime = signatureDatetime;
    }

    public String getCollectionLatLng() {
        return collectionLatLng;
    }

    public void setCollectionLatLng(String collectionLatLng) {
        this.collectionLatLng = collectionLatLng;
    }

    public String getDeliveryLatLng() {
        return deliveryLatLng;
    }

    public void setDeliveryLatLng(String deliveryLatLng) {
        this.deliveryLatLng = deliveryLatLng;
    }

    public String getJobRefNo() {
        return jobRefNo;
    }

    public void setJobRefNo(String jobRefNo) {
        this.jobRefNo = jobRefNo;
    }
}
