package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ChangeStatus implements Serializable {

    @SerializedName("DriverId")
    @Expose
    private String driverId;
    @SerializedName("StatusId")
    @Expose
    private Integer statusId;


    @SerializedName("IsPanic")
    @Expose
    private Boolean IsPanic;


    public ChangeStatus(String driverId, Integer statusId,boolean isPanic) {
        this.driverId = driverId;
        this.statusId = statusId;
        this.IsPanic = isPanic;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }


    public Boolean getPanic() {
        return IsPanic;
    }

    public void setPanic(Boolean panic) {
        IsPanic = panic;
    }
}
