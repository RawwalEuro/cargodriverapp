package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendSignature {


    @SerializedName("JobAddrId")
    @Expose
    private String JobAddrId;


    @SerializedName("ImgBase64")
    @Expose
    private String ImgBase64;


    public String getJobAddrId() {
        return JobAddrId;
    }

    public void setJobAddrId(String jobAddrId) {
        JobAddrId = jobAddrId;
    }

    public String getImgBase64() {
        return ImgBase64;
    }

    public void setImgBase64(String imgBase64) {
        ImgBase64 = imgBase64;
    }
}
