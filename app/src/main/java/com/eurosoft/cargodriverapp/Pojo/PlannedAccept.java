package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PlannedAccept implements Serializable {

    @SerializedName("PlannedId")
    @Expose
    private String plannedId;
    @SerializedName("DriverInfoId")
    @Expose
    private String driverInfoId;
    @SerializedName("JobStatusId")
    @Expose
    private String jobStatusId;
    @SerializedName("JobStatus")
    @Expose
    private String jobStatus;
    @SerializedName("PlanStatusId")
    @Expose
    private String planStatusId;
    @SerializedName("PlanStatus")
    @Expose
    private String planStatus;
    @SerializedName("fromname")
    @Expose
    private String fromname;
    @SerializedName("DeviceId")
    @Expose
    private String deviceId;


    public PlannedAccept(String plannedId, String driverInfoId, String jobStatusId, String jobStatus, String planStatusId, String planStatus, String fromname, String deviceId) {
        this.plannedId = plannedId;
        this.driverInfoId = driverInfoId;
        this.jobStatusId = jobStatusId;
        this.jobStatus = jobStatus;
        this.planStatusId = planStatusId;
        this.planStatus = planStatus;
        this.fromname = fromname;
        this.deviceId = deviceId;
    }

    public String getPlannedId() {
        return plannedId;
    }

    public void setPlannedId(String plannedId) {
        this.plannedId = plannedId;
    }

    public String getDriverInfoId() {
        return driverInfoId;
    }

    public void setDriverInfoId(String driverInfoId) {
        this.driverInfoId = driverInfoId;
    }

    public String getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(String jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getPlanStatusId() {
        return planStatusId;
    }

    public void setPlanStatusId(String planStatusId) {
        this.planStatusId = planStatusId;
    }

    public String getPlanStatus() {
        return planStatus;
    }

    public void setPlanStatus(String planStatus) {
        this.planStatus = planStatus;
    }

    public String getFromname() {
        return fromname;
    }

    public void setFromname(String fromname) {
        this.fromname = fromname;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
