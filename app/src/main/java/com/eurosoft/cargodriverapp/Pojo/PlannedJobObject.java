package com.eurosoft.cargodriverapp.Pojo;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PlannedJobObject implements Serializable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ColorCode")
    @Expose
    private String ColorCode;

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getServiceDesc() {
        return ServiceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        ServiceDesc = serviceDesc;
    }

    @ColumnInfo(name = "ServiceDesc")
    @SerializedName("ServiceDesc")
    @Expose
    private String ServiceDesc;
    @SerializedName("JobDataId")
    @Expose
    private Integer jobDataId;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("CollectionAddr")
    @Expose
    private String collectionAddr;
    @SerializedName("DeliveryAddr")
    @Expose
    private String deliveryAddr;

    @SerializedName("JobType")
    @Expose
    private String JobType;


    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("FromTime")
    @Expose
    private String fromTime;
    @SerializedName("TillTime")
    @Expose
    private String tillTime;
    @SerializedName("JobStatusId")
    @Expose
    private Integer jobStatusId;
    @SerializedName("JobRefNo")
    @Expose
    private String jobRefNo;
    @SerializedName("JobPlannerMasterId")
    @Expose
    private Integer jobPlannerMasterId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobDataId() {
        return jobDataId;
    }

    public void setJobDataId(Integer jobDataId) {
        this.jobDataId = jobDataId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCollectionAddr() {
        return collectionAddr;
    }

    public void setCollectionAddr(String collectionAddr) {
        this.collectionAddr = collectionAddr;
    }

    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getTillTime() {
        return tillTime;
    }

    public void setTillTime(String tillTime) {
        this.tillTime = tillTime;
    }

    public Integer getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(Integer jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public String getJobRefNo() {
        return jobRefNo;
    }

    public void setJobRefNo(String jobRefNo) {
        this.jobRefNo = jobRefNo;
    }

    public Integer getJobPlannerMasterId() {
        return jobPlannerMasterId;
    }

    public void setJobPlannerMasterId(Integer jobPlannerMasterId) {
        this.jobPlannerMasterId = jobPlannerMasterId;
    }


    public String getJobType() {
        return JobType;
    }

    public void setJobType(String jobType) {
        JobType = jobType;
    }
}
