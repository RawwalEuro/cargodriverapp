package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestLoginBody {

    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("Password")
    @Expose
    private String password;
    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("AppVersion")
    @Expose
    private String appVersion;

    @SerializedName("VehicleNumber")
    @Expose
    private String VehicleNumber;

    @SerializedName("DriverId")
    @Expose
    private Integer DriverId;

    public Integer getDriverId() {
        return DriverId;
    }


    public RequestLoginBody() {
    }

    public RequestLoginBody(String deviceId, Integer driverId) {
        this.deviceId = deviceId;
        DriverId = driverId;
    }


    public String getVehicleNumber() {
        return VehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        VehicleNumber = vehicleNumber;
    }

    public void setDriverId(Integer driverId) {
        DriverId = driverId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }
}
