package com.eurosoft.cargodriverapp.Pojo;

import android.content.Context;

import java.util.ArrayList;

public class ImageSheetModel {

    private Context context;
    private ArrayList<String> imageList;

    public ImageSheetModel(Context context, ArrayList<String> imageList) {
        this.context = context;
        this.imageList = imageList;
    }

    public Context getContext() {
        return context;
    }

    public ArrayList<String> getImageList() {
        return imageList;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setImageList(ArrayList<String> imageList) {
        this.imageList = imageList;
    }
}
