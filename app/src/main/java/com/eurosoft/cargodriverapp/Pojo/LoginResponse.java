package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("DriverNumber")
    @Expose
    private String driverNumber;
    @SerializedName("F_Name")
    @Expose
    private String fName;
    @SerializedName("SurName")
    @Expose
    private String surName;
    @SerializedName("showChecklist")
    @Expose
    private boolean showChecklist;
    @SerializedName("PhoneNo")
    @Expose
    private String phoneNo;
    @SerializedName("DriverPassword")
    @Expose
    private String driverPassword;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("DriverAddress")
    @Expose
    private String driverAddress;
    @SerializedName("ImageBaseUrl")
    @Expose
    private String imageBaseUrl;
    @SerializedName("ImagePath")
    @Expose
    private String imagePath;
    @SerializedName("CreatedBy")
    @Expose
    private Integer createdBy;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("DriverStatusId")
    @Expose
    private Integer driverStatusId;
    @SerializedName("DriverDocuments")
    @Expose
    private Object driverDocuments;
    @SerializedName("DriverVehicle")
    @Expose
    private String driverVehicle;

    @SerializedName("DriverAppMapNavigationTypeId")
    @Expose
    private String DriverAppMapNavigationTypeId;


    @SerializedName("VehicleId")
    @Expose
    private String VehicleId;


    public String getVehicleId() {
        return VehicleId;
    }

    public void setVehicleId(String vehicleId) {
        VehicleId = vehicleId;
    }

    public String getDriverAppMapNavigationTypeId() {
        return DriverAppMapNavigationTypeId;
    }

    public void setDriverAppMapNavigationTypeId(String driverAppMapNavigationTypeId) {
        DriverAppMapNavigationTypeId = driverAppMapNavigationTypeId;
    }

    public LoginResponse(Integer id, String deviceId) {
        this.id = id;
        this.deviceId = deviceId;
    }

    public Integer getId() {
        if (id == null) {
            return 0;
        } else {
            return id;
        }
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDriverNumber() {
        return driverNumber;
    }

    public void setDriverNumber(String driverNumber) {
        this.driverNumber = driverNumber;
    }

    public String getFName() {
        if(fName == null){
            fName = "";
        }
        return fName;
    }

    public void setFName(String fName) {
        this.fName = fName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getDriverPassword() {
        return driverPassword;
    }

    public void setDriverPassword(String driverPassword) {
        this.driverPassword = driverPassword;
    }

    public String getEmailAddress() {

        if(emailAddress == null){
            return "--";
        }
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getDriverAddress() {
        return driverAddress;
    }

    public void setDriverAddress(String driverAddress) {
        this.driverAddress = driverAddress;
    }

    public String getImageBaseUrl() {
        return imageBaseUrl;
    }

    public void setImageBaseUrl(String imageBaseUrl) {
        this.imageBaseUrl = imageBaseUrl;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getDriverStatusId() {
        return driverStatusId;
    }

    public void setDriverStatusId(Integer driverStatusId) {
        this.driverStatusId = driverStatusId;
    }

    public Object getDriverDocuments() {
        return driverDocuments;
    }

    public void setDriverDocuments(Object driverDocuments) {
        this.driverDocuments = driverDocuments;
    }

    public String getDriverVehicle() {
        if(driverVehicle == null){
            return "--";
        }
        return driverVehicle;
    }

    public void setDriverVehicle(String driverVehicle) {
        this.driverVehicle = driverVehicle;
    }

    public boolean isShowChecklist() {
        return showChecklist;
    }

    public void setShowChecklist(boolean showChecklist) {
        this.showChecklist = showChecklist;
    }
}
