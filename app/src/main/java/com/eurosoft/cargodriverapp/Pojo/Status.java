package com.eurosoft.cargodriverapp.Pojo;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Status implements Serializable {
    @SerializedName("JobType")
    @Expose
    private String jobType;
    @SerializedName("TotalAmount")
    @Expose
    private Double TotalAmount;
    @SerializedName("JobRefNo")
    @Expose
    private String jobRefNo;


    @SerializedName("ColorCode")
    @Expose
    private String ColorCode;

    public String getColorCode() {
        return ColorCode;
    }

    public void setColorCode(String colorCode) {
        ColorCode = colorCode;
    }

    public String getServiceDesc() {
        return ServiceDesc;
    }

    public void setServiceDesc(String serviceDesc) {
        ServiceDesc = serviceDesc;
    }

    @ColumnInfo(name = "ServiceDesc")
    @SerializedName("ServiceDesc")
    @Expose
    private String ServiceDesc;

    @SerializedName("PaymentMode")
    @Expose
    private String PaymentMode;

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("Paid")
    @Expose
    private int Paid;

    @SerializedName("JobDataId")
    @Expose
    private int JobDataId;


    @SerializedName("JobStatus")
    @Expose
    private String JobStatus;


    @SerializedName("JobStatusId")
    @Expose
    private int JobStatusId;


    @SerializedName("DriverId")
    @Expose
    private int DriverId;

    @SerializedName("AccountInformationId")
    @Expose
    private int AccountInformationId;

    @SerializedName("JobTypeId")
    @Expose
    private int JobTypeId;


    @SerializedName("FillLater")
    @Expose
    private Boolean IsFillLater;


    @SerializedName("consigneeCollectionAddress")
    @Expose
    private String consigneeCollectionAddress;


    public String getConsigneeCollectionAddress() {
        return consigneeCollectionAddress;
    }

    public void setConsigneeCollectionAddress(String consigneeCollectionAddress) {
        this.consigneeCollectionAddress = consigneeCollectionAddress;
    }

    public Boolean getFillLater() {
        return IsFillLater;
    }

    public void setFillLater(Boolean fillLater) {
        IsFillLater = fillLater;
    }

    public int getJobTypeId() {
        return JobTypeId;
    }

    public void setJobTypeId(int jobTypeId) {
        JobTypeId = jobTypeId;
    }

    public int getAccountInformationId() {


        return AccountInformationId;
    }

    public void setAccountInformationId(int accountInformationId) {
        AccountInformationId = accountInformationId;
    }

    public String getPaymentMode() {
        return PaymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        PaymentMode = paymentMode;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getJobRefNo() {
        if(jobRefNo == null){
            return "--";
        }
        return jobRefNo;
    }

    public void setJobRefNo(String jobRefNo) {
        this.jobRefNo = jobRefNo;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getJobDataId() {
        return JobDataId;
    }

    public void setJobDataId(int jobDataId) {
        JobDataId = jobDataId;
    }

    public String getJobStatus() {
        return JobStatus;
    }

    public void setJobStatus(String jobStatus) {
        JobStatus = jobStatus;
    }

    public int getJobStatusId() {
        return JobStatusId;
    }

    public void setJobStatusId(int jobStatusId) {
        JobStatusId = jobStatusId;
    }

    public int getDriverId() {
        return DriverId;
    }

    public void setDriverId(int driverId) {
        DriverId = driverId;
    }

    public int getPaid() {
        return Paid;
    }

    public void setPaid(int paid) {
        Paid = paid;
    }

    public Double getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        TotalAmount = totalAmount;
    }
}
