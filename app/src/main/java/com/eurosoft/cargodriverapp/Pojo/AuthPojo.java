package com.eurosoft.cargodriverapp.Pojo;

public class AuthPojo {

    private String Company;
    private String Driver;
    private String SocketIOUrl;

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getDriver() {
        return Driver;
    }

    public void setDriver(String driver) {
        Driver = driver;
    }

    public String getSocketIOUrl() {
        return SocketIOUrl;
    }

    public void setSocketIOUrl(String socketIOUrl) {
        SocketIOUrl = socketIOUrl;
    }

    public String getSocketIOPath() {
        return SocketIOPath;
    }

    public void setSocketIOPath(String socketIOPath) {
        SocketIOPath = socketIOPath;
    }

    private String SocketIOPath;
}
