package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentNChargesField implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Header")
    @Expose
    private String header;
    @SerializedName("Desc")
    @Expose
    private String desc;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("IndexNumber")
    @Expose
    private Integer indexNumber;
    @SerializedName("Type")
    @Expose
    private String type;

    @SerializedName("valuesInit")
    @Expose
    private Double valuesInit;


    @SerializedName("isEmpty")
    @Expose
    private boolean isEmpty;


    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public Double getValuesInit() {

        if(valuesInit == null){
            return 0.0;
        }
        return valuesInit;
    }

    public void setValuesInit(Double valuesInit) {
        this.valuesInit = valuesInit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getIndexNumber() {
        return indexNumber;
    }

    public void setIndexNumber(Integer indexNumber) {
        this.indexNumber = indexNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
