package com.eurosoft.cargodriverapp.Pojo;

public class SubmitChecklistPojo {

private int CheckListHeaderID;
private int CheckListDetailID;
private int DriverId;
private int isSuccess;
private int VehicleId;
private String imgUrl;

    public SubmitChecklistPojo(int checkListHeaderID, int checkListDetailID, int driverId, int isSuccess, int vehicleId, String imgUrl, String comment) {
        CheckListHeaderID = checkListHeaderID;
        CheckListDetailID = checkListDetailID;
        DriverId = driverId;
        this.isSuccess = isSuccess;
        VehicleId = vehicleId;
        this.imgUrl = imgUrl;
        this.comment = comment;
    }

    public int getCheckListHeaderID() {
        return CheckListHeaderID;
    }

    public void setCheckListHeaderID(int checkListHeaderID) {
        CheckListHeaderID = checkListHeaderID;
    }

    public int getCheckListDetailID() {
        return CheckListDetailID;
    }

    public void setCheckListDetailID(int checkListDetailID) {
        CheckListDetailID = checkListDetailID;
    }

    public int getDriverId() {
        return DriverId;
    }

    public void setDriverId(int driverId) {
        DriverId = driverId;
    }

    public int getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(int isSuccess) {
        this.isSuccess = isSuccess;
    }

    public int getVehicleId() {
        return VehicleId;
    }

    public void setVehicleId(int vehicleId) {
        VehicleId = vehicleId;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    private String comment;
}
