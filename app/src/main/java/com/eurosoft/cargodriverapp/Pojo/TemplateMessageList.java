package com.eurosoft.cargodriverapp.Pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TemplateMessageList {

        @SerializedName("Id")
        @Expose
        private Integer id;
        @SerializedName("MsgDesc")
        @Expose
        private String msgDesc;
        @SerializedName("IsActive")
        @Expose
        private Boolean isActive;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMsgDesc() {
            return msgDesc;
        }

        public void setMsgDesc(String msgDesc) {
            this.msgDesc = msgDesc;
        }

        public Boolean getIsActive() {
            return isActive;
        }

        public void setIsActive(Boolean isActive) {
            this.isActive = isActive;
        }

}
