package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfimedPlan {

    @SerializedName("DriverInfoId")
    @Expose
    private String driverInfoId;
    @SerializedName("VehicleInfoID")
    @Expose
    private String vehicleInfoID;


    public ConfimedPlan(String driverInfoId, String vehicleInfoID) {
        this.driverInfoId = driverInfoId;
        this.vehicleInfoID = vehicleInfoID;
    }

    public String getDriverInfoId() {
        return driverInfoId;
    }

    public void setDriverInfoId(String driverInfoId) {
        this.driverInfoId = driverInfoId;
    }

    public String getVehicleInfoID() {
        return vehicleInfoID;
    }

    public void setVehicleInfoID(String vehicleInfoID) {
        this.vehicleInfoID = vehicleInfoID;
    }
}
