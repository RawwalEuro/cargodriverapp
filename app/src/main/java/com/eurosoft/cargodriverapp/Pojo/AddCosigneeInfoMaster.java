package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AddCosigneeInfoMaster implements Serializable {

    @SerializedName("CosigneeInfo")
    @Expose
    private CosigneeInfo cosigneeInfo;


    @SerializedName("JobChargesInfo")
    @Expose
    private ArrayList<JobChargesInfo> jobChargesInfos;


    @SerializedName("goodInformations")
    @Expose
    private ArrayList<GoodsInfoAddObject> goodsInfoList;

    public AddCosigneeInfoMaster(CosigneeInfo cosigneeInfo, ArrayList<JobChargesInfo> jobChargesInfos, ArrayList<GoodsInfoAddObject> goodsInfoList) {
        this.cosigneeInfo = cosigneeInfo;
        this.jobChargesInfos = jobChargesInfos;
        this.goodsInfoList = goodsInfoList;
    }

    public CosigneeInfo getCosigneeInfo() {
        return cosigneeInfo;
    }

    public void setCosigneeInfo(CosigneeInfo cosigneeInfo) {
        this.cosigneeInfo = cosigneeInfo;
    }

    public ArrayList<JobChargesInfo> getJobChargesInfos() {
        return jobChargesInfos;
    }

    public void setJobChargesInfos(ArrayList<JobChargesInfo> paymentNCharges) {
        this.jobChargesInfos = jobChargesInfos;
    }

    public ArrayList<GoodsInfoAddObject> getGoodsInfoList() {
        return goodsInfoList;
    }

    public void setGoodsInfoList(ArrayList<GoodsInfoAddObject> goodsInfoList) {
        this.goodsInfoList = goodsInfoList;
    }
}
