package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SocketEmitResponse {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("DriverId")
    @Expose
    private Integer driverId;
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("WorkStatusId")
    @Expose
    private Integer workStatusId;
    @SerializedName("Latitude")
    @Expose
    private Double latitude;
    @SerializedName("Longitude")
    @Expose
    private Double longitude;
    @SerializedName("Heading")
    @Expose
    private Object heading;
    @SerializedName("Speed")
    @Expose
    private float speed;
    @SerializedName("ZoneId")
    @Expose
    private Object zoneId;
    @SerializedName("OldZoneId")
    @Expose
    private Object oldZoneId;
    @SerializedName("CurrentJobId")
    @Expose
    private Integer currentJobId;
    @SerializedName("PlotEnteringDateTime")
    @Expose
    private Object plotEnteringDateTime;
    @SerializedName("LoginDateTime")
    @Expose
    private Object loginDateTime;
    @SerializedName("LogoutDateTime")
    @Expose
    private Object logoutDateTime;


    @SerializedName("DeviceId")
    @Expose
    private String DeviceId;

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String deviceId) {
        DeviceId = deviceId;
    }

    public SocketEmitResponse(Integer id, Integer driverId, Integer status, Integer workStatusId, Double latitude, Double longitude, Object heading, float speed, Object zoneId, Object oldZoneId, Integer currentJobId, Object plotEnteringDateTime, Object loginDateTime, Object logoutDateTime, String deviceId) {
        this.id = id;
        this.driverId = driverId;
        this.status = status;
        this.workStatusId = workStatusId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.heading = heading;
        this.speed = speed;
        this.zoneId = zoneId;
        this.oldZoneId = oldZoneId;
        this.currentJobId = currentJobId;
        this.plotEnteringDateTime = plotEnteringDateTime;
        this.loginDateTime = loginDateTime;
        this.logoutDateTime = logoutDateTime;
        DeviceId = deviceId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getWorkStatusId() {
        return workStatusId;
    }

    public void setWorkStatusId(Integer workStatusId) {
        this.workStatusId = workStatusId;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Object getHeading() {
        return heading;
    }

    public void setHeading(Object heading) {
        this.heading = heading;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public Object getZoneId() {
        return zoneId;
    }

    public void setZoneId(Object zoneId) {
        this.zoneId = zoneId;
    }

    public Object getOldZoneId() {
        return oldZoneId;
    }

    public void setOldZoneId(Object oldZoneId) {
        this.oldZoneId = oldZoneId;
    }

    public Integer getCurrentJobId() {
        return currentJobId;
    }

    public void setCurrentJobId(Integer currentJobId) {
        this.currentJobId = currentJobId;
    }

    public Object getPlotEnteringDateTime() {
        return plotEnteringDateTime;
    }

    public void setPlotEnteringDateTime(Object plotEnteringDateTime) {
        this.plotEnteringDateTime = plotEnteringDateTime;
    }

    public Object getLoginDateTime() {
        return loginDateTime;
    }

    public void setLoginDateTime(Object loginDateTime) {
        this.loginDateTime = loginDateTime;
    }

    public Object getLogoutDateTime() {
        return logoutDateTime;
    }

    public void setLogoutDateTime(Object logoutDateTime) {
        this.logoutDateTime = logoutDateTime;
    }

}
