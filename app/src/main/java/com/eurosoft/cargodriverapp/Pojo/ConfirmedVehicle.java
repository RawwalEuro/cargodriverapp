package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfirmedVehicle {

    @SerializedName("ConfirmedVehicleId")
    @Expose
    private String confirmedVehicleId;
    @SerializedName("AdditionalMsg")
    @Expose
    private String additionalMsg;

    public String getConfirmedVehicleId() {
        return confirmedVehicleId;
    }

    public void setConfirmedVehicleId(String confirmedVehicleId) {
        this.confirmedVehicleId = confirmedVehicleId;
    }

    public String getAdditionalMsg() {
        return additionalMsg;
    }

    public void setAdditionalMsg(String additionalMsg) {
        this.additionalMsg = additionalMsg;
    }

}
