package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleDetails {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("DriverId")
    @Expose
    private Integer driverId;
    @SerializedName("VehicleId")
    @Expose
    private Integer vehicleId;
    @SerializedName("VehicleNumber")
    @Expose
    private String vehicleNumber;


    @SerializedName("fromname")
    @Expose
    private String fromname;

    public VehicleDetails(Integer driverId) {
        this.driverId = driverId;
    }


    public VehicleDetails(Integer driverId,Integer vehicleId,String fromName) {
        this.driverId = driverId;
        this.vehicleId = vehicleId;
        this.fromname = fromName;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(Integer vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

}
