package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GoodsInfoAddObject implements Serializable {

    @SerializedName("GoodsQty")
    @Expose
    private String goodsQty;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Volume")
    @Expose
    private String volume;
    @SerializedName("Weight")
    @Expose
    private String weight;
    @SerializedName("GoodDesc")
    @Expose
    private String goodsDesc;

    @SerializedName("MeasurementTypeId")
    @Expose
    private int measurement;


    @SerializedName("JobDataId")
    @Expose
    private int JobDataId;


    @SerializedName("MeasurementTypeDesc")
    @Expose
    private String MeasurementTypeDesc;

    @SerializedName("Id")
    @Expose
    private int Id;


    public int getJobDataId() {
        return JobDataId;
    }

    public void setJobDataId(int jobDataId) {
        JobDataId = jobDataId;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public GoodsInfoAddObject() {

    }

    public GoodsInfoAddObject(String goodsQty, String quantity, String volume, String weight, String goodsDesc, int measurement,int jobDataId,String measureTpeDesc) {
        this.goodsQty = goodsQty;
        this.quantity = quantity;
        this.volume = volume;
        this.weight = weight;
        this.goodsDesc = goodsDesc;
        this.measurement = measurement;
        this.MeasurementTypeDesc = measureTpeDesc;
        this.JobDataId = jobDataId;
    }

    public String getMeasurementTypeDesc() {
        return MeasurementTypeDesc;
    }

    public void setMeasurementTypeDesc(String measurementTypeDesc) {
        MeasurementTypeDesc = measurementTypeDesc;
    }

    public String getGoodsQty() {
        return goodsQty;
    }

    public void setGoodsQty(String goodsQty) {
        this.goodsQty = goodsQty;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public int getMeasurement() {
        return measurement;
    }

    public void setMeasurement(int measurement) {
        this.measurement = measurement;
    }
}
