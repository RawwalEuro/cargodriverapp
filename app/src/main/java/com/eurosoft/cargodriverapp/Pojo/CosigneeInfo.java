package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CosigneeInfo implements Serializable {


    @SerializedName("JobDataId")
    @Expose
    private int JobDataId;

    @SerializedName("Date")
    @Expose
    private String deliveryDate;


    @SerializedName("ShiftType")
    @Expose
    private int timeShift;

    @SerializedName("FromTime")
    @Expose
    private String fromTime;


    @SerializedName("TillTime")
    @Expose
    private String tillTime;


    @SerializedName("FirstName")
    @Expose
    private String cosigneeName;


    @SerializedName("EmailAddress")
    @Expose
    private String email;


    @SerializedName("Phone")
    @Expose
    private String number;


    @SerializedName("DeliveryAddr")
    @Expose
    private String deliveryAddress;


    @SerializedName("Country")
    @Expose
    private String country;


    @SerializedName("Address_Line_1")
    @Expose
    private String addressLine1;
    @SerializedName("Address_Line_2")
    @Expose
    private String addressLine2;
    @SerializedName("Address_Line_3")
    @Expose
    private String addressLine3;
    @SerializedName("Address_Line_4")
    @Expose
    private String addressLine4;

    @SerializedName("InvoiceCurrency")
    @Expose
    private String invoiceCurrency;


    @SerializedName("Postal_Code")
    @Expose
    private String Postal_Code;

    @SerializedName("Shipper")
    @Expose
    private String Shipper;


    @SerializedName("Consignee")
    @Expose
    private String consignee;


    @SerializedName("Createdby")
    @Expose
    private int Createdby;


    @SerializedName("CreateDate")
    @Expose
    private String CreateDate;


    @SerializedName("Country_Code")
    @Expose
    private String countryCode;



    @SerializedName("JobStatus")
    @Expose
    private String JobStatus;


    @SerializedName("JobStatusId")
    @Expose
    private int JobStatusId;


    @SerializedName("DriverId")
    @Expose
    private int DriverId;


       // sdfjkklsd
    @SerializedName("CollectionAddr")
    @Expose
    private String CollectionAddr;


    @SerializedName("JobType")
    @Expose
    private String JobType;


    @SerializedName("JobTypeId")
    @Expose
    private int JobTypeId;


    @SerializedName("Cosignee")
    @Expose
    private String Cosignee;


    @SerializedName("Account_Number")
    @Expose
    private String accNumber;


    @SerializedName("Company_Name")
    @Expose
    private String companyName;


    @SerializedName("PostalCode")
    @Expose
    private String PostalCode;


    @SerializedName("AccountAddressId")
    @Expose
    private int AccountAddressId;


    @SerializedName("AccountInformationId")
    @Expose
    private int AccountInformationId;


    @SerializedName("JobAddrId")
    @Expose
    private int JobAddrId;



    @SerializedName("DestinationId")
    @Expose
    private int DestinationId;

    public int getDestinationId() {
        return DestinationId;
    }

    public void setDestinationId(int destinationId) {
        DestinationId = destinationId;
    }

    public int getJobAddrId() {
        return JobAddrId;
    }

    public void setJobAddrId(int jobAddrId) {
        JobAddrId = jobAddrId;
    }

    public int getAccountInformationId() {
        return AccountInformationId;
    }

    public void setAccountInformationId(int accountInformationId) {
        AccountInformationId = accountInformationId;
    }

    public int getAccountAddressId() {
        return AccountAddressId;
    }

    public void setAccountAddressId(int accountAddressId) {
        AccountAddressId = accountAddressId;
    }

    public String getPostalCode() {
        return PostalCode;
    }

    public void setPostalCode(String postalCode) {
        PostalCode = postalCode;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCosignee() {
        return Cosignee;
    }

    public void setCosignee(String cosignee) {
        Cosignee = cosignee;
    }

    public String getAccNumber() {
        return accNumber;
    }

    public void setAccNumber(String accNumber) {
        this.accNumber = accNumber;
    }

    public int getJobTypeId() {
        return JobTypeId;
    }

    public void setJobTypeId(int jobTypeId) {
        JobTypeId = jobTypeId;
    }

    public String getJobType() {
        return JobType;
    }

    public void setJobType(String jobType) {
        JobType = jobType;
    }

    public String getCollectionAddr() {
        return CollectionAddr;
    }

    public void setCollectionAddr(String collectionAddr) {
        CollectionAddr = collectionAddr;
    }

    public int getDriverId() {
        return DriverId;
    }

    public void setDriverId(int driverId) {
        DriverId = driverId;
    }

    public int getJobStatusId() {
        return JobStatusId;
    }

    public void setJobStatusId(int jobStatusId) {
        JobStatusId = jobStatusId;
    }

    public String getJobStatus() {
        return JobStatus == null || JobStatus.isEmpty() ? "" : JobStatus;
    }

    public void setJobStatus(String jobStatus) {
        JobStatus = jobStatus;
    }

    public String getCountryCode() {
        return countryCode == null || countryCode.isEmpty() ? "" : countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public int getCreatedby() {
        return Createdby;
    }

    public void setCreatedby(int createdby) {
        Createdby = createdby;
    }

    public String getConsignee() {
        return consignee == null || consignee.isEmpty() ? "-1" : consignee;
    }

    public void setConsignee(String consigne) {
        consignee = consigne;
    }

    public String getShipper() {
        return Shipper == null || Shipper.isEmpty() ? "0" : Shipper;
    }

    public void setShipper(String shipper) {
        Shipper = shipper;
    }

    public String getPostal_Code() {
        return Postal_Code == null || Postal_Code.isEmpty() ? "" : Postal_Code;
    }

    public void setPostal_Code(String postal_Code) {
        Postal_Code = postal_Code;
    }

    public String getInvoiceCurrency() {
        return invoiceCurrency == null || invoiceCurrency.isEmpty() ? "" : invoiceCurrency;
    }

    public void setInvoiceCurrency(String invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    public String getAddressLine1() {
        return  addressLine1 == null || addressLine1.isEmpty() ? "" : addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return  addressLine2 == null || addressLine2.isEmpty() ? "" : addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3 == null || addressLine3.isEmpty() ? "" : addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4 == null || addressLine4.isEmpty() ? "" : addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public int getTimeShift() {
        return timeShift;
    }

    public void setTimeShift(int timeShift) {
        this.timeShift = timeShift;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getTillTime() {
        return tillTime;
    }

    public void setTillTime(String tillTime) {
        this.tillTime = tillTime;
    }

    public String getCosigneeName() {
        return cosigneeName;
    }

    public void setCosigneeName(String cosigneeName) {
        this.cosigneeName = cosigneeName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public int getJobDataId() {
        return JobDataId;
    }

    public void setJobDataId(int jobDataId) {
        JobDataId = jobDataId;
    }
}
