package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestLogin {
    @SerializedName("request")
    @Expose
    private RequestLoginBody request;

    public RequestLoginBody getRequest() {

        return request;
    }

    public void setRequest(RequestLoginBody request) {
        this.request = request;
    }
}
