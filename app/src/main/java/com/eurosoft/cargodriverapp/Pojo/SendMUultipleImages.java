package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SendMUultipleImages {


    @SerializedName("JobAddrId")
    @Expose
    private String JobAddrId;


    @SerializedName("images")
    @Expose
    private ArrayList<String> ImgBase64;


    public String getJobAddrId() {
        return JobAddrId;
    }

    public void setJobAddrId(String jobAddrId) {
        JobAddrId = jobAddrId;
    }

    public ArrayList<String> getImgBase64() {
        return ImgBase64;
    }

    public void setImgBase64(ArrayList<String> imgBase64) {
        ImgBase64 = imgBase64;
    }
}
