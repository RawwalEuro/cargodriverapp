package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GoodInfo {

    @SerializedName("Id")
    @Expose
    private Integer goodInformationId;
    @SerializedName("JobDataId")
    @Expose
    private Integer jobDataId;

    @SerializedName("MeasurementTypeId")
    @Expose
    private String measurementTypeId;
    @SerializedName("GoodDesc")
    @Expose
    private String goodDesc;
    @SerializedName("Quantity")
    @Expose
    private String quantity;
    @SerializedName("Volume")
    @Expose
    private String volume;
    @SerializedName("Weight")
    @Expose
    private String weight;
    @SerializedName("MeasurementTypeDesc")
    @Expose
    private String measurementTypeDesc;

    public Integer getGoodInformationId() {
        return goodInformationId;
    }

    public void setGoodInformationId(Integer goodInformationId) {
        this.goodInformationId = goodInformationId;
    }

    public Integer getJobDataId() {
        return jobDataId;
    }

    public void setJobDataId(Integer jobDataId) {
        this.jobDataId = jobDataId;
    }


    public String getMeasurementTypeId() {
        return measurementTypeId;
    }

    public void setMeasurementTypeId(String measurementTypeId) {
        this.measurementTypeId = measurementTypeId;
    }

    public String getGoodDesc() {
        return goodDesc;
    }

    public void setGoodDesc(String goodDesc) {
        this.goodDesc = goodDesc;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getMeasurementTypeDesc() {
        return measurementTypeDesc;
    }

    public void setMeasurementTypeDesc(String measurementTypeDesc) {
        this.measurementTypeDesc = measurementTypeDesc;
    }
}
