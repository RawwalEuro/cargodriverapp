package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class MessageHistory implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("LoginId")
    @Expose
    private Integer loginId;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("MessageFrom")
    @Expose
    private String messageFrom;
    @SerializedName("MessageDate")
    @Expose
    private String messageDate;



    @SerializedName("FromRange")
    @Expose
    private int FromRange;


    @SerializedName("ToRange")
    @Expose
    private int ToRange;


    public MessageHistory() {
    }


    public MessageHistory(int loginid, int fromRange, int toRange) {
        loginId = loginid;
        FromRange = fromRange;
        ToRange = toRange;
    }

    public void setLoginId(String loginId) {
        loginId = loginId;
    }

    public int getFromRange() {
        return FromRange;
    }

    public void setFromRange(int fromRange) {
        FromRange = fromRange;
    }

    public int getToRange() {
        return ToRange;
    }

    public void setToRange(int toRange) {
        ToRange = toRange;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLoginId() {
        return loginId;
    }

    public void setLoginId(Integer loginId) {
        this.loginId = loginId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }


}
