package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RequestMessage {


    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("fromname")
    @Expose
    private String fromname;
    @SerializedName("fromsocketid")
    @Expose
    private String fromsocketid;
    @SerializedName("ToDeviceId")
    @Expose
    private Integer toDeviceId;

    @SerializedName("Purpose")
    @Expose
    private String Purpose;


    @SerializedName("driverName")
    @Expose
    private String driverName;


    @SerializedName("DriverNumber")
    @Expose
    private String DriverNumber;

    @SerializedName("driverId")
    @Expose
    private String driverId;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverNumber() {
        return DriverNumber;
    }

    public void setDriverNumber(String driverNumber) {
        DriverNumber = driverNumber;
    }

    public String getPurpose() {
        return Purpose;
    }

    public void setPurpose(String purpose) {
        Purpose = purpose;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromname() {
        return fromname;
    }

    public void setFromname(String fromname) {
        this.fromname = fromname;
    }

    public String getFromsocketid() {
        return fromsocketid;
    }

    public void setFromsocketid(String fromsocketid) {
        this.fromsocketid = fromsocketid;
    }

    public Integer getToDeviceId() {
        return toDeviceId;
    }

    public void setToDeviceId(Integer toDeviceId) {
        this.toDeviceId = toDeviceId;
    }
}
