package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasterPojo {


    @SerializedName("SpecialKeyDirection")
    @Expose
    private String specialKeyDirection;
    @SerializedName("driverLogin")
    @Expose
    private String driverLogin;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("pleaseAllowPermission")
    @Expose
    private String pleaseAllowPermission;
    @SerializedName("emailEmpty")
    @Expose
    private String emailEmpty;
    @SerializedName("passwordEmpty")
    @Expose
    private String passwordEmpty;
    @SerializedName("checkInternet")
    @Expose
    private String checkInternet;
    @SerializedName("somethingWentWrong")
    @Expose
    private String somethingWentWrong;
    @SerializedName("authorizeCode")
    @Expose
    private String authorizeCode;
    @SerializedName("verify")
    @Expose
    private String verify;
    @SerializedName("otpEmpty")
    @Expose
    private String otpEmpty;
    @SerializedName("enableLocationServices")
    @Expose
    private String enableLocationServices;
    @SerializedName("turnOnLocationSerivices")
    @Expose
    private String turnOnLocationSerivices;
    @SerializedName("home")
    @Expose
    private String home;
    @SerializedName("rank")
    @Expose
    private String rank;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("driver")
    @Expose
    private String driver;
    @SerializedName("vehicle")
    @Expose
    private String vehicle;
    @SerializedName("pendingJobs")
    @Expose
    private String pendingJobs;
    @SerializedName("breaks")
    @Expose
    private String breaks;
    @SerializedName("available")
    @Expose
    private String available;
    @SerializedName("panic")
    @Expose
    private String panic;
    @SerializedName("presstodeliver")
    @Expose
    private String presstodeliver;
    @SerializedName("presstoWC")
    @Expose
    private String presstoWC;
    @SerializedName("pendingAccept")
    @Expose
    private String pendingAccept;
    @SerializedName("onroute")
    @Expose
    private String onroute;
    @SerializedName("completed")
    @Expose
    private String completed;
    @SerializedName("rejected")
    @Expose
    private String rejected;
    @SerializedName("arrive")
    @Expose
    private String arrive="Arrive";
    @SerializedName("withCustomer")
    @Expose
    private String withCustomer;
    @SerializedName("currentJob")
    @Expose
    private String currentJob;
    @SerializedName("goodsInformation")
    @Expose
    private String goodsInformation;
    @SerializedName("measurement")
    @Expose
    private String measurement;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("volume")
    @Expose
    private String volume;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("totalWeight")
    @Expose
    private String totalWeight;
    @SerializedName("totalQty")
    @Expose
    private String totalQty;
    @SerializedName("totalVolume")
    @Expose
    private String totalVolume;
    @SerializedName("swipeToAccept")
    @Expose
    private String swipeToAccept;
    @SerializedName("swipeToArrive")
    @Expose
    private String swipeToArrive="Swipe To Arrive";
    @SerializedName("Pleasecheckyourinternet")
    @Expose
    private String pleasecheckyourinternet;
    @SerializedName("Decline")
    @Expose
    private String decline;
    @SerializedName("goodsDetails")
    @Expose
    private String goodsDetails;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("cosigneeInfo")
    @Expose
    private String cosigneeInfo;
    @SerializedName("shipperInfo")
    @Expose
    private String shipperInfo;
    @SerializedName("JobHistoryTitle")
    @Expose
    private String jobHistoryTitle;
    @SerializedName("noRecordFound")
    @Expose
    private String noRecordFound;
    @SerializedName("jobHistory")
    @Expose
    private String jobHistory;
    @SerializedName("ok")
    @Expose
    private String ok;
    @SerializedName("cancel")
    @Expose
    private String cancel;
    @SerializedName("fromDate")
    @Expose
    private String fromDate;
    @SerializedName("tillDate")
    @Expose
    private String tillDate;
    @SerializedName("futureJobsTitle")
    @Expose
    private String futureJobsTitle;
    @SerializedName("singleJob")
    @Expose
    private String singleJob;
    @SerializedName("plannedJobs")
    @Expose
    private String plannedJobs;
    @SerializedName("planNumber")
    @Expose
    private String planNumber;
    @SerializedName("jobNumber")
    @Expose
    private String jobNumber;
    @SerializedName("cantSendEmptyText")
    @Expose
    private String cantSendEmptyText;
    @SerializedName("pleasegrantthelocationpermission")
    @Expose
    private String pleasegrantthelocationpermission;
    @SerializedName("pleaseentervehiclenumber")
    @Expose
    private String pleaseentervehiclenumber;
    @SerializedName("cantLogoutwhenajobisrunning")
    @Expose
    private String cantLogoutwhenajobisrunning;
    @SerializedName("areyousureyouwanttorecoverthisjob")
    @Expose
    private String areyousureyouwanttorecoverthisjob;
    @SerializedName("recover")
    @Expose
    private String recover;
    @SerializedName("areyousureyouwanttostartthisjob")
    @Expose
    private String areyousureyouwanttostartthisjob;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("treasureCargo")
    @Expose
    private String treasureCargo;
    @SerializedName("todayJob")
    @Expose
    private String todayJob;
    @SerializedName("serverConnected")
    @Expose
    private String serverConnected;
    @SerializedName("serverNotConnected")
    @Expose
    private String serverNotConnected;
    @SerializedName("pleaseDrawYourSignature")
    @Expose
    private String pleaseDrawYourSignature;
    @SerializedName("clear")
    @Expose
    private String clear;
    @SerializedName("done")
    @Expose
    private String done;
    @SerializedName("cantrecoverjobwhenthejobisrunning")
    @Expose
    private String cantrecoverjobwhenthejobisrunning;
    @SerializedName("cantstartorrecoverajobwhenthedriverisonbreak")
    @Expose
    private String cantstartorrecoverajobwhenthedriverisonbreak;
    @SerializedName("pickup")
    @Expose
    private String pickup;
    @SerializedName("dropoff")
    @Expose
    private String dropoff;
    @SerializedName("paymentType")
    @Expose
    private String paymentType;
    @SerializedName("selectLanguage")
    @Expose
    private String selectLanguage;

    @SerializedName("messaging")
    @Expose
    private String messaging;


    @SerializedName("jobHistoryTv")
    @Expose
    private String jobHistoryTv;

    @SerializedName("enterMessageHere")
    @Expose
    private String enterMessageHere;


    @SerializedName("logout")
    @Expose
    private String logout;


    @SerializedName("appVersion")
    @Expose
    private String appVersion;


    @SerializedName("today")
    @Expose
    private String today;


    @SerializedName("yesterday")
    @Expose
    private String yesterday;


    @SerializedName("dateRange")
    @Expose
    private String dateRange;

    @SerializedName("cantLogout")
    @Expose
    private String cantLogout;



    @SerializedName("goToCurrentJob")
    @Expose
    private String goToCurrentJob;

    @SerializedName("navigate")
    @Expose
    private String navigate;


    @SerializedName("googleMaps")
    @Expose
    private String googleMaps;


    @SerializedName("waze")
    @Expose
    private String waze;


    @SerializedName("hereWeGo")
    @Expose
    private String hereWeGo;

    @SerializedName("navigateWith")
    @Expose
    private String navigateWith;


    @SerializedName("dismiss")
    @Expose
    private String dismiss;

    @SerializedName("pleaseWait")
    @Expose
    private String pleaseWait;


    @SerializedName("showDetails")
    @Expose
    private String showDetails;


    @SerializedName("languageNotSelected")
    @Expose
    private String languageNotSelected;

    public String getLanguageNotSelected() {
        return languageNotSelected;
    }

    public String getShowDetails() {
        return showDetails;
    }

    public String getPleaseWait() {
        return pleaseWait;
    }

    public String getDismiss() {
        return dismiss;
    }

    public String getGoogleMaps() {
        return googleMaps;
    }

    public String getWaze() {
        return waze;
    }

    public String getHereWeGo() {
        return hereWeGo;
    }

    public String getNavigateWith() {
        return navigateWith;
    }

    public String getNavigate() {
        return navigate;
    }

    public String getCantLogout() {
        return cantLogout;
    }

    public String getGoToCurrentJob() {
        return goToCurrentJob;
    }

    public String getYesterday() {

        if(yesterday == null || yesterday.equalsIgnoreCase("")){
            return "";
        }
        return yesterday;
    }

    public String getDateRange() {

        if(dateRange == null || dateRange.equalsIgnoreCase("")){
            return "";
        }
        return dateRange;
    }

    public String getToday() {
        if(today == null || today.equalsIgnoreCase("")){
            return "";
        }
        return today;
    }

    public void setMessaging(String messaging) {
        this.messaging = messaging;
    }

    public void setJobHistoryTv(String jobHistoryTv) {
        this.jobHistoryTv = jobHistoryTv;
    }

    public String getLogout() {
        return logout;
    }

    public void setLogout(String logout) {
        this.logout = logout;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getMessaging() {
        return messaging;
    }

    public String getJobHistoryTv() {
        return jobHistoryTv;
    }

    public String getStrShortName() {
        return strShortName;
    }

    public String getEnterMessageHere() {
        if( enterMessageHere == null || enterMessageHere.equalsIgnoreCase("")){
            return "Enter your message here ..";
        }
        return enterMessageHere;
    }

    public void setEnterMessageHere(String enterMessageHere) {
        this.enterMessageHere = enterMessageHere;
    }

    @SerializedName("strShortName")
    @Expose
    private String strShortName;


    public MasterPojo() {
    }

    public MasterPojo(String strShortName) {
        this.strShortName = strShortName;
    }

    public String getSpecialKeyDirection() {
        return specialKeyDirection;
    }

    public void setSpecialKeyDirection(String specialKeyDirection) {
        this.specialKeyDirection = specialKeyDirection;
    }

    public String getDriverLogin() {
        return driverLogin;
    }

    public void setDriverLogin(String driverLogin) {
        this.driverLogin = driverLogin;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPleaseAllowPermission() {
        return pleaseAllowPermission;
    }

    public void setPleaseAllowPermission(String pleaseAllowPermission) {
        this.pleaseAllowPermission = pleaseAllowPermission;
    }

    public String getEmailEmpty() {
        return emailEmpty;
    }

    public void setEmailEmpty(String emailEmpty) {
        this.emailEmpty = emailEmpty;
    }

    public String getPasswordEmpty() {
        return passwordEmpty;
    }

    public void setPasswordEmpty(String passwordEmpty) {
        this.passwordEmpty = passwordEmpty;
    }

    public String getCheckInternet() {
        return checkInternet;
    }

    public void setCheckInternet(String checkInternet) {
        this.checkInternet = checkInternet;
    }

    public String getSomethingWentWrong() {
        return somethingWentWrong;
    }

    public void setSomethingWentWrong(String somethingWentWrong) {
        this.somethingWentWrong = somethingWentWrong;
    }

    public String getAuthorizeCode() {
        return authorizeCode;
    }

    public void setAuthorizeCode(String authorizeCode) {
        this.authorizeCode = authorizeCode;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public String getOtpEmpty() {
        return otpEmpty;
    }

    public void setOtpEmpty(String otpEmpty) {
        this.otpEmpty = otpEmpty;
    }

    public String getEnableLocationServices() {
        return enableLocationServices;
    }

    public void setEnableLocationServices(String enableLocationServices) {
        this.enableLocationServices = enableLocationServices;
    }

    public String getTurnOnLocationSerivices() {
        return turnOnLocationSerivices;
    }

    public void setTurnOnLocationSerivices(String turnOnLocationSerivices) {
        this.turnOnLocationSerivices = turnOnLocationSerivices;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getPendingJobs() {
        return pendingJobs;
    }

    public void setPendingJobs(String pendingJobs) {
        this.pendingJobs = pendingJobs;
    }

    public String getBreaks() {
        return breaks;
    }

    public void setBreaks(String breaks) {
        this.breaks = breaks;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getPanic() {
        return panic;
    }

    public void setPanic(String panic) {
        this.panic = panic;
    }

    public String getPresstodeliver() {
        return presstodeliver;
    }

    public void setPresstodeliver(String presstodeliver) {
        this.presstodeliver = presstodeliver;
    }

    public String getPresstoWC() {
        return presstoWC;
    }

    public void setPresstoWC(String presstoWC) {
        this.presstoWC = presstoWC;
    }

    public String getPendingAccept() {
        return pendingAccept;
    }

    public void setPendingAccept(String pendingAccept) {
        this.pendingAccept = pendingAccept;
    }

    public String getOnroute() {
        return onroute;
    }

    public void setOnroute(String onroute) {
        this.onroute = onroute;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getRejected() {
        return rejected;
    }

    public void setRejected(String rejected) {
        this.rejected = rejected;
    }

    public String getWithCustomer() {
        return withCustomer;
    }

    public void setWithCustomer(String withCustomer) {
        this.withCustomer = withCustomer;
    }

    public String getCurrentJob() {
        return currentJob;
    }

    public void setCurrentJob(String currentJob) {
        this.currentJob = currentJob;
    }

    public String getGoodsInformation() {
        return goodsInformation;
    }

    public void setGoodsInformation(String goodsInformation) {
        this.goodsInformation = goodsInformation;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getSwipeToAccept() {
        return swipeToAccept;
    }

    public void setSwipeToAccept(String swipeToAccept) {
        this.swipeToAccept = swipeToAccept;
    }

    public String getPleasecheckyourinternet() {
        return pleasecheckyourinternet;
    }

    public void setPleasecheckyourinternet(String pleasecheckyourinternet) {
        this.pleasecheckyourinternet = pleasecheckyourinternet;
    }

    public String getDecline() {
        return decline;
    }

    public void setDecline(String decline) {
        this.decline = decline;
    }

    public String getGoodsDetails() {
        return goodsDetails;
    }

    public void setGoodsDetails(String goodsDetails) {
        this.goodsDetails = goodsDetails;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCosigneeInfo() {
        return cosigneeInfo;
    }

    public void setCosigneeInfo(String cosigneeInfo) {
        this.cosigneeInfo = cosigneeInfo;
    }

    public String getShipperInfo() {
        return shipperInfo;
    }

    public void setShipperInfo(String shipperInfo) {
        this.shipperInfo = shipperInfo;
    }

    public String getJobHistoryTitle() {
        return jobHistoryTitle;
    }

    public void setJobHistoryTitle(String jobHistoryTitle) {
        this.jobHistoryTitle = jobHistoryTitle;
    }

    public String getNoRecordFound() {
        return noRecordFound;
    }

    public void setNoRecordFound(String noRecordFound) {
        this.noRecordFound = noRecordFound;
    }

    public String getJobHistory() {
        return jobHistory;
    }

    public void setJobHistory(String jobHistory) {
        this.jobHistory = jobHistory;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getTillDate() {
        return tillDate;
    }

    public void setTillDate(String tillDate) {
        this.tillDate = tillDate;
    }

    public String getFutureJobsTitle() {
        return futureJobsTitle;
    }

    public void setFutureJobsTitle(String futureJobsTitle) {
        this.futureJobsTitle = futureJobsTitle;
    }

    public String getSingleJob() {
        return singleJob;
    }

    public void setSingleJob(String singleJob) {
        this.singleJob = singleJob;
    }

    public String getPlannedJobs() {
        return plannedJobs;
    }

    public void setPlannedJobs(String plannedJobs) {
        this.plannedJobs = plannedJobs;
    }

    public String getPlanNumber() {
        return planNumber;
    }

    public void setPlanNumber(String planNumber) {
        this.planNumber = planNumber;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getCantSendEmptyText() {
        return cantSendEmptyText;
    }

    public void setCantSendEmptyText(String cantSendEmptyText) {
        this.cantSendEmptyText = cantSendEmptyText;
    }

    public String getPleasegrantthelocationpermission() {
        return pleasegrantthelocationpermission;
    }

    public void setPleasegrantthelocationpermission(String pleasegrantthelocationpermission) {
        this.pleasegrantthelocationpermission = pleasegrantthelocationpermission;
    }

    public String getPleaseentervehiclenumber() {
        return pleaseentervehiclenumber;
    }

    public void setPleaseentervehiclenumber(String pleaseentervehiclenumber) {
        this.pleaseentervehiclenumber = pleaseentervehiclenumber;
    }

    public String getCantLogoutwhenajobisrunning() {
        return cantLogoutwhenajobisrunning;
    }

    public void setCantLogoutwhenajobisrunning(String cantLogoutwhenajobisrunning) {
        this.cantLogoutwhenajobisrunning = cantLogoutwhenajobisrunning;
    }

    public String getAreyousureyouwanttorecoverthisjob() {
        return areyousureyouwanttorecoverthisjob;
    }

    public void setAreyousureyouwanttorecoverthisjob(String areyousureyouwanttorecoverthisjob) {
        this.areyousureyouwanttorecoverthisjob = areyousureyouwanttorecoverthisjob;
    }

    public String getRecover() {
        return recover;
    }

    public void setRecover(String recover) {
        this.recover = recover;
    }

    public String getAreyousureyouwanttostartthisjob() {
        return areyousureyouwanttostartthisjob;
    }

    public void setAreyousureyouwanttostartthisjob(String areyousureyouwanttostartthisjob) {
        this.areyousureyouwanttostartthisjob = areyousureyouwanttostartthisjob;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getTreasureCargo() {
        return treasureCargo;
    }

    public void setTreasureCargo(String treasureCargo) {
        this.treasureCargo = treasureCargo;
    }

    public String getTodayJob() {
        return todayJob;
    }

    public void setTodayJob(String todayJob) {
        this.todayJob = todayJob;
    }

    public String getServerConnected() {
        return serverConnected;
    }

    public void setServerConnected(String serverConnected) {
        this.serverConnected = serverConnected;
    }

    public String getServerNotConnected() {
        return serverNotConnected;
    }

    public void setServerNotConnected(String serverNotConnected) {
        this.serverNotConnected = serverNotConnected;
    }

    public String getPleaseDrawYourSignature() {
        return pleaseDrawYourSignature;
    }

    public void setPleaseDrawYourSignature(String pleaseDrawYourSignature) {
        this.pleaseDrawYourSignature = pleaseDrawYourSignature;
    }

    public String getClear() {
        return clear;
    }

    public void setClear(String clear) {
        this.clear = clear;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    public String getCantrecoverjobwhenthejobisrunning() {
        return cantrecoverjobwhenthejobisrunning;
    }

    public void setCantrecoverjobwhenthejobisrunning(String cantrecoverjobwhenthejobisrunning) {
        this.cantrecoverjobwhenthejobisrunning = cantrecoverjobwhenthejobisrunning;
    }

    public String getCantstartorrecoverajobwhenthedriverisonbreak() {
        return cantstartorrecoverajobwhenthedriverisonbreak;
    }

    public void setCantstartorrecoverajobwhenthedriverisonbreak(String cantstartorrecoverajobwhenthedriverisonbreak) {
        this.cantstartorrecoverajobwhenthedriverisonbreak = cantstartorrecoverajobwhenthedriverisonbreak;
    }

    public String getPickup() {
        return pickup;
    }

    public void setPickup(String pickup) {
        this.pickup = pickup;
    }

    public String getDropoff() {
        return dropoff;
    }

    public void setDropoff(String dropoff) {
        this.dropoff = dropoff;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSelectLanguage() {
        return "Settings";
    }

    public void setSelectLanguage(String selectLanguage) {
        this.selectLanguage = selectLanguage;
    }

    public String getArrive() {
        return arrive;
    }

    public void setArrive(String arrive) {
        this.arrive = arrive;
    }

    public String getSwipeToArrive() {
        return swipeToArrive;
    }

    public void setSwipeToArrive(String swipeToArrive) {
        this.swipeToArrive = swipeToArrive;
    }

   /* private String driverLogin = "Driver Login";
    private String userName = "Driver No";
    private String password = "Password";
    private String login = "Login";
    private String pleaseAllowPermission = "Please allow permission to login";
    private String emailEmpty = "Driver No is empty!";
    private String passwordEmpty = "Password is empty!";
    private String checkInternet = "Check your internet";
    private String somethingWentWrong = "Something went wrong";
    private String authorizeCode = "Authorize Code";
    private String verify = "Verify";
    private String otpEmpty = "Otp is empty";
    private String enableLocationServices = "Enable location services for a more accurate experience";
    private String turnOnLocationSerivices = "Turn on location services";
    private String home = "Home";
    private String rank = "Rank";
    private String status = "Status";
    private String driver = "Driver";
    private String vehicle = "Vehicle";

    public String getVehicle() {
        return vehicle;
    }

    private String pendingJobs = "Today's Pending Job";
    private String breaks = "Break";
    private String available = "Available";
    private String panic = "Panic";
    /////////////////////////////////////////// ActivityJobStatus //////////////////////////////////////////////////////////////
    private String presstodeliver = "Deliver";
    private String presstoWC = "With Customer";
    private String pendingAccept = "Pending Accept";
    private String onroute = "ONROUTE";
    private String completed = "COMPLETED";
    private String rejected = "Rejected";
    private String withCustomer = "With Customer";
    private String currentJob = "Current Job";
    private String goodsInformation = "Goods Information";
    private String measurement = "Unit";
    private String desc = "Desc";
    private String qty = "Qty";
    private String volume = "Volume";
    private String weight = "Weight";
    private String totalWeight = "Total Weight";
    private String totalQty = "Total Qty";
    private String totalVolume = "Total Volume";
    private String swipeToAccept = "Accept";
    /////////////////////////////////////////// JobDetailsActivity    //////////////////////////////////////////////////////////////
    private String Pleasecheckyourinternet = "Please check your internet";
    private String Decline = "Decline";
    /////////////////////////////////////////// ActivityExpandedGood //////////////////////////////////////////////////////////////
    private String goodsDetails = "Goods Details";
    private String reference = "Ref No #";
    private String cosigneeInfo = "Consignee Info";
    private String shipperInfo = "Shipper Info";
    /////////////////////////////////////////// ActivityJobHistoryDetail //////////////////////////////////////////////////////////////
    private String JobHistoryTitle = "Job History Detail";
    /////////////////////////////////////////// ActivityJobHistory //////////////////////////////////////////////////////////////
    private String noRecordFound = "No Record Found";
    private String jobHistory = "Job History";
    private String ok = "OK";
    private String cancel = "Cancel";
    private String fromDate = "From Date";
    private String tillDate = "Till Date";
    /////////////////////////////////////////// ActivityFutureJobs //////////////////////////////////////////////////////////////
    private String futureJobsTitle = "Future Jobs";
    private String singleJob = "Single Job";
    private String plannedJobs = "Planned Jobs";
    private String planNumber = "Plan Number #";
    private String jobNumber = "Job Number #";
    private String pickup = "Pickup";
    private String dropoff = "Dropoff";
    private String paymentType = "Payment Type";


    public String getPaymentType() {
        return paymentType;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    /////////////////////////////////////////// MessageActivity //////////////////////////////////////////////////////////////
    private String cantSendEmptyText = "Can't Send Empty Text";
    /////////////////////////////////////////// ActivityLogin //////////////////////////////////////////////////////////////
    private String pleasegrantthelocationpermission = "Please grant the location permission";
    private String pleaseentervehiclenumber = "Please enter vehicle id";
    /////////////////////////////////////////// HomeActivity //////////////////////////////////////////////////////////////
    private String cantLogoutwhenajobisrunning = "Can't Logout when a job is running";
    private String areyousureyouwanttorecoverthisjob = "Are you sure you want to recover this job?";
    private String recover = "Recover";
    private String areyousureyouwanttostartthisjob = "Are you sure you want to start this job?";
    private String start = "Start";
    private String treasureCargo = "Treasure Cargo";
    private String todayJob = "Today's Job";
    /////////////////////////////////////////// SignalRService //////////////////////////////////////////////////////////////
    private String serverConnected = "Server Connected";
    private String serverNotConnected = "Server Not Connected";
    /////////////////////////////////////////// BottomSheetSignature //////////////////////////////////////////////////////////////
    private String pleaseDrawYourSignature = "Please Draw Your Signature";
    private String clear = "Clear";
    private String done = "Done";
    private String selectLanguage = "Select Language";
    /////////////////////////////////////////// AdapterHomeJobs //////////////////////////////////////////////////////////////
    private String cantrecoverjobwhenthejobisrunning = "Can't recover job when the job is running";
    private String cantstartorrecoverajobwhenthedriverisonbreak = "Can't start or recover a job when the driver is on break";

    public String getCantrecoverjobwhenthejobisrunning() {
        return cantrecoverjobwhenthejobisrunning;
    }

    public String getCantstartorrecoverajobwhenthedriverisonbreak() {
        return cantstartorrecoverajobwhenthedriverisonbreak;
    }


    public String getSelectLanguage() {
        return selectLanguage;
    }

    public String getPickup() {
        return pickup;
    }

    public String getDropoff() {
        return dropoff;
    }

    public String getClear() {
        return clear;
    }

    public String getDone() {
        return done;
    }

    public String getPleaseDrawYourSignature() {
        return pleaseDrawYourSignature;
    }

    public String getServerConnected() {
        return serverConnected;
    }

    public String getServerNotConnected() {
        return serverNotConnected;
    }

    public String getTodayJob() {
        return todayJob;
    }

    public String getTreasureCargo() {
        return treasureCargo;
    }

    public String getAreyousureyouwanttorecoverthisjob() {
        return areyousureyouwanttorecoverthisjob;
    }

    public String getRecover() {
        return recover;
    }

    public String getAreyousureyouwanttostartthisjob() {
        return areyousureyouwanttostartthisjob;
    }

    public String getStart() {
        return start;
    }

    public String getCantLogoutwhenajobisrunning() {
        return cantLogoutwhenajobisrunning;
    }

    public String getPleaseentervehiclenumber() {
        return pleaseentervehiclenumber;
    }

    public String getPleasegrantthelocationpermission() {
        return pleasegrantthelocationpermission;
    }

    public String getPlanNumber() {
        return planNumber;
    }

    public String getSingleJob() {
        return singleJob;
    }

    public String getPlannedJobs() {
        return plannedJobs;
    }

    public String getFutureJobsTitle() {
        return futureJobsTitle;
    }

    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getTillDate() {
        return tillDate;
    }

    public void setTillDate(String tillDate) {
        this.tillDate = tillDate;
    }

    public String getJobHistory() {
        return jobHistory;
    }

    public void setJobHistory(String jobHistory) {
        this.jobHistory = jobHistory;
    }

    public String getCantSendEmptyText() {
        return cantSendEmptyText;
    }

    public void setCantSendEmptyText(String cantSendEmptyText) {
        this.cantSendEmptyText = cantSendEmptyText;
    }

    public String getNoRecordFound() {
        return noRecordFound;
    }

    public void setNoRecordFound(String noRecordFound) {
        this.noRecordFound = noRecordFound;
    }

    public String getJobHistoryTitle() {
        return JobHistoryTitle;
    }

    public void setJobHistoryTitle(String jobHistoryTitle) {
        JobHistoryTitle = jobHistoryTitle;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCosigneeInfo() {
        return cosigneeInfo;
    }

    public void setCosigneeInfo(String cosigneeInfo) {
        this.cosigneeInfo = cosigneeInfo;
    }

    public String getShipperInfo() {
        return shipperInfo;
    }

    public void setShipperInfo(String shipperInfo) {
        this.shipperInfo = shipperInfo;
    }

    public String getReference() {
        return reference;
    }

    public String getGoodsDetails() {
        return goodsDetails;
    }

    public void setGoodsDetails(String goodsDetails) {
        goodsDetails = goodsDetails;
    }

    public String getDecline() {
        return Decline;
    }

    public void setDecline(String decline) {
        Decline = decline;
    }

    public String getPleasecheckyourinternet() {
        return Pleasecheckyourinternet;
    }

    public void setPleasecheckyourinternet(String pleasecheckyourinternet) {
        Pleasecheckyourinternet = pleasecheckyourinternet;
    }

    public String getPresstodeliver() {
        return presstodeliver;
    }

    public void setPresstodeliver(String presstodeliver) {
        this.presstodeliver = presstodeliver;
    }

    public String getPresstoWC() {
        return presstoWC;
    }

    public void setPresstoWC(String presstoWC) {
        this.presstoWC = presstoWC;
    }

    public String getPendingAccept() {
        return pendingAccept;
    }

    public void setPendingAccept(String pendingAccept) {
        this.pendingAccept = pendingAccept;
    }

    public String getOnroute() {
        return onroute;
    }

    public void setOnroute(String onroute) {
        this.onroute = onroute;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getRejected() {
        return rejected;
    }

    public void setRejected(String rejected) {
        this.rejected = rejected;
    }

    public String getWithCustomer() {
        return withCustomer;
    }

    public void setWithCustomer(String withCustomer) {
        this.withCustomer = withCustomer;
    }

    public String getCurrentJob() {
        return currentJob;
    }

    public void setCurrentJob(String currentJob) {
        this.currentJob = currentJob;
    }

    public String getGoodsInformation() {
        return goodsInformation;
    }

    public void setGoodsInformation(String goodsInformation) {
        this.goodsInformation = goodsInformation;
    }

    public String getMeasurement() {
        return measurement;
    }

    public void setMeasurement(String measurement) {
        this.measurement = measurement;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(String totalQty) {
        this.totalQty = totalQty;
    }

    public String getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(String totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getSwipeToAccept() {
        return swipeToAccept;
    }

    public void setSwipeToAccept(String swipeToAccept) {
        this.swipeToAccept = swipeToAccept;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPleaseAllowPermission() {
        return pleaseAllowPermission;
    }

    public void setPleaseAllowPermission(String pleaseAllowPermission) {
        this.pleaseAllowPermission = pleaseAllowPermission;
    }

    public String getEmailEmpty() {
        return emailEmpty;
    }

    public void setEmailEmpty(String emailEmpty) {
        this.emailEmpty = emailEmpty;
    }

    public String getPasswordEmpty() {
        return passwordEmpty;
    }

    public void setPasswordEmpty(String passwordEmpty) {
        this.passwordEmpty = passwordEmpty;
    }

    public String getCheckInternet() {
        return checkInternet;
    }

    public void setCheckInternet(String checkInternet) {
        this.checkInternet = checkInternet;
    }

    public String getSomethingWentWrong() {
        return somethingWentWrong;
    }

    public void setSomethingWentWrong(String somethingWentWrong) {
        this.somethingWentWrong = somethingWentWrong;
    }

    public String getAuthorizeCode() {
        return authorizeCode;
    }

    public void setAuthorizeCode(String authorizeCode) {
        this.authorizeCode = authorizeCode;
    }

    public String getVerify() {
        return verify;
    }

    public void setVerify(String verify) {
        this.verify = verify;
    }

    public String getOtpEmpty() {
        return otpEmpty;
    }

    public void setOtpEmpty(String otpEmpty) {
        this.otpEmpty = otpEmpty;
    }

    public String getEnableLocationServices() {
        return enableLocationServices;
    }

    public void setEnableLocationServices(String enableLocationServices) {
        this.enableLocationServices = enableLocationServices;
    }

    public String getTurnOnLocationSerivices() {
        return turnOnLocationSerivices;
    }

    public void setTurnOnLocationSerivices(String turnOnLocationSerivices) {
        this.turnOnLocationSerivices = turnOnLocationSerivices;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getPendingJobs() {
        return pendingJobs;
    }

    public void setPendingJobs(String pendingJobs) {
        this.pendingJobs = pendingJobs;
    }

    public String getBreaks() {
        return breaks;
    }

    public void setBreaks(String breaks) {
        this.breaks = breaks;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getPanic() {
        return panic;
    }

    public void setPanic(String panic) {
        this.panic = panic;
    }

    public String getDriverLogin() {
        return driverLogin;
    }

    public void setDriverLogin(String driverLogin) {
        this.driverLogin = driverLogin;
    }*/
}
