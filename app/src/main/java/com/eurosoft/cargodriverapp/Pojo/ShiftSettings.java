package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ShiftSettings implements Serializable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ShiftDesc")
    @Expose
    private String shiftDesc;
    @SerializedName("PickupFromTime")
    @Expose
    private String pickupFromTime;
    @SerializedName("PickupToTime")
    @Expose
    private String pickupToTime;
    @SerializedName("DeliveryFromTime")
    @Expose
    private String deliveryFromTime;
    @SerializedName("DeliveryToTime")
    @Expose
    private String deliveryToTime;
    @SerializedName("ShiftNamesId")
    @Expose
    private Integer shiftNamesId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getShiftDesc() {
        return shiftDesc;
    }

    public void setShiftDesc(String shiftDesc) {
        this.shiftDesc = shiftDesc;
    }

    public String getPickupFromTime() {
        return pickupFromTime;
    }

    public void setPickupFromTime(String pickupFromTime) {
        this.pickupFromTime = pickupFromTime;
    }

    public String getPickupToTime() {
        return pickupToTime;
    }

    public void setPickupToTime(String pickupToTime) {
        this.pickupToTime = pickupToTime;
    }

    public String getDeliveryFromTime() {
        return deliveryFromTime;
    }

    public void setDeliveryFromTime(String deliveryFromTime) {
        this.deliveryFromTime = deliveryFromTime;
    }

    public String getDeliveryToTime() {
        return deliveryToTime;
    }

    public void setDeliveryToTime(String deliveryToTime) {
        this.deliveryToTime = deliveryToTime;
    }

    public Integer getShiftNamesId() {
        return shiftNamesId;
    }

    public void setShiftNamesId(Integer shiftNamesId) {
        this.shiftNamesId = shiftNamesId;
    }

}
