package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PlannedJobs implements Serializable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("VehicleInfoID")
    @Expose
    private Integer vehicleInfoID;
    @SerializedName("DriverInfoId")
    @Expose
    private String driverInfoId;
    @SerializedName("StatusId")
    @Expose
    private Integer statusId;
    @SerializedName("PlanNo")
    @Expose
    private String planNo;
    @SerializedName("PlannedDate")
    @Expose
    private String plannedDate;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;

    @SerializedName("VehicleId")
    @Expose
    private String VehicleId;

    @SerializedName("CreatedBy")
    @Expose
    private Integer createdBy;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;


    @SerializedName("IsConfirmed")
    @Expose
    private Boolean IsConfirmed;


    @SerializedName("JobAddrJsons")
    @Expose
    private List<PlannedJobObject> jobAddrJsons = null;



    @SerializedName("JobPlannerMasterId")
    @Expose
    private String JobPlannerMasterId;


    public PlannedJobs(String driverInfoIds, String jobPlannerMasterId, String Vehicleid) {
        driverInfoId = driverInfoIds;
        JobPlannerMasterId = jobPlannerMasterId;
        VehicleId = Vehicleid;
    }


    public Boolean getConfirmed() {
        if(IsConfirmed == null){
            return false;
        }
        return IsConfirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        IsConfirmed = confirmed;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVehicleInfoID() {
        return vehicleInfoID;
    }

    public void setVehicleInfoID(Integer vehicleInfoID) {
        this.vehicleInfoID = vehicleInfoID;
    }

    public String getDriverInfoId() {
        return driverInfoId;
    }

    public void setDriverInfoId(String driverInfoId) {
        this.driverInfoId = driverInfoId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getPlanNo() {
        return planNo;
    }

    public void setPlanNo(String planNo) {
        this.planNo = planNo;
    }

    public String getPlannedDate() {
        return plannedDate;
    }

    public void setPlannedDate(String plannedDate) {
        this.plannedDate = plannedDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public List<PlannedJobObject> getJobAddrJsons() {
        return jobAddrJsons;
    }

    public void setJobAddrJsons(List<PlannedJobObject> jobAddrJsons) {
        this.jobAddrJsons = jobAddrJsons;
    }
}
