package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseJobNotification {

    @SerializedName("JobDataId")
    @Expose
    private Integer jobDataId;
    @SerializedName("AccountAddressId")
    @Expose
    private Integer accountAddressId;
    @SerializedName("Account_Number")
    @Expose
    private String accountNumber;
    @SerializedName("Company_Name")
    @Expose
    private String companyName;
    @SerializedName("Country_Code")
    @Expose
    private String countryCode;
    @SerializedName("InvoiceCurrency")
    @Expose
    private String invoiceCurrency;
    @SerializedName("Address_Line_1")
    @Expose
    private String addressLine1;
    @SerializedName("Address_Line_2")
    @Expose
    private String addressLine2;
    @SerializedName("Address_Line_3")
    @Expose
    private String addressLine3;
    @SerializedName("Address_Line_4")
    @Expose
    private String addressLine4;
    @SerializedName("PostalCode")
    @Expose
    private String postalCode;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("Shipper")
    @Expose
    private Integer shipper;
    @SerializedName("Consignee")
    @Expose
    private Integer consignee;
    @SerializedName("CollectionDate")
    @Expose
    private String collectionDate;
    @SerializedName("CollectionFromTime")
    @Expose
    private String collectionFromTime;
    @SerializedName("CollectionTillTime")
    @Expose
    private String collectionTillTime;
    @SerializedName("DeliveryDate")
    @Expose
    private String deliveryDate;
    @SerializedName("DeliveryFromTime")
    @Expose
    private String deliveryFromTime;
    @SerializedName("DeliveryTillTime")
    @Expose
    private String deliveryTillTime;
    @SerializedName("Createdby")
    @Expose
    private Integer createdby;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("CollectionAddr")
    @Expose
    private String collectionAddr;
    @SerializedName("DeliveryAddr")
    @Expose
    private String deliveryAddr;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("JobType")
    @Expose
    private String jobType;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("FromTime")
    @Expose
    private String fromTime;
    @SerializedName("TillTime")
    @Expose
    private String tillTime;
    @SerializedName("ShiftType")
    @Expose
    private Integer shiftType;
    @SerializedName("JobStatus")
    @Expose
    private String jobStatus;
    @SerializedName("JobStatusId")
    @Expose
    private Integer jobStatusId;
    @SerializedName("DriverId")
    @Expose
    private Integer driverId;

    public Integer getJobDataId() {
        return jobDataId;
    }

    public void setJobDataId(Integer jobDataId) {
        this.jobDataId = jobDataId;
    }

    public Integer getAccountAddressId() {
        return accountAddressId;
    }

    public void setAccountAddressId(Integer accountAddressId) {
        this.accountAddressId = accountAddressId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getInvoiceCurrency() {
        return invoiceCurrency;
    }

    public void setInvoiceCurrency(String invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    public String getAddressLine4() {
        return addressLine4;
    }

    public void setAddressLine4(String addressLine4) {
        this.addressLine4 = addressLine4;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Integer getShipper() {
        return shipper;
    }

    public void setShipper(Integer shipper) {
        this.shipper = shipper;
    }

    public Integer getConsignee() {
        return consignee;
    }

    public void setConsignee(Integer consignee) {
        this.consignee = consignee;
    }

    public String getCollectionDate() {
        return collectionDate;
    }

    public void setCollectionDate(String collectionDate) {
        this.collectionDate = collectionDate;
    }

    public String getCollectionFromTime() {
        return collectionFromTime;
    }

    public void setCollectionFromTime(String collectionFromTime) {
        this.collectionFromTime = collectionFromTime;
    }

    public String getCollectionTillTime() {
        return collectionTillTime;
    }

    public void setCollectionTillTime(String collectionTillTime) {
        this.collectionTillTime = collectionTillTime;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryFromTime() {
        return deliveryFromTime;
    }

    public void setDeliveryFromTime(String deliveryFromTime) {
        this.deliveryFromTime = deliveryFromTime;
    }

    public String getDeliveryTillTime() {
        return deliveryTillTime;
    }

    public void setDeliveryTillTime(String deliveryTillTime) {
        this.deliveryTillTime = deliveryTillTime;
    }

    public Integer getCreatedby() {
        return createdby;
    }

    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCollectionAddr() {
        return collectionAddr;
    }

    public void setCollectionAddr(String collectionAddr) {
        this.collectionAddr = collectionAddr;
    }

    public String getDeliveryAddr() {
        return deliveryAddr;
    }

    public void setDeliveryAddr(String deliveryAddr) {
        this.deliveryAddr = deliveryAddr;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getTillTime() {
        return tillTime;
    }

    public void setTillTime(String tillTime) {
        this.tillTime = tillTime;
    }

    public Integer getShiftType() {
        return shiftType;
    }

    public void setShiftType(Integer shiftType) {
        this.shiftType = shiftType;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public Integer getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(Integer jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }
}
