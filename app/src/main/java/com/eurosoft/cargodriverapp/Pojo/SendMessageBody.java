package com.eurosoft.cargodriverapp.Pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendMessageBody {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("fromname")
    @Expose
    private String fromname;
    @SerializedName("fromsocketid")
    @Expose
    private String fromsocketid;
    @SerializedName("ToDeviceId")
    @Expose
    private String toDeviceId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFromname() {
        return fromname;
    }

    public void setFromname(String fromname) {
        this.fromname = fromname;
    }

    public String getFromsocketid() {
        return fromsocketid;
    }

    public void setFromsocketid(String fromsocketid) {
        this.fromsocketid = fromsocketid;
    }

    public String getToDeviceId() {
        return toDeviceId;
    }

    public void setToDeviceId(String toDeviceId) {
        this.toDeviceId = toDeviceId;
    }
}
