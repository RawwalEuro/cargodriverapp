package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.fxn.stash.Stash;

public class ActivityChatDialog extends Activity {
    TextView ServerMessage,done,replY;
    MasterPojo masterPojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_dialog);
        Stash.init(getApplicationContext());
        ServerMessage = findViewById(R.id.message);
        masterPojo= (MasterPojo) Stash.getObject(Constants.MASTER_POJO,MasterPojo.class);
        done = findViewById(R.id.btnOK);
        replY = findViewById(R.id.replY);
        Intent intent = getIntent();
        String message = intent.getStringExtra("IncomingMessageFromServer");
        ServerMessage.setText(message);
        done.setText(masterPojo.getOk());
        replY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(ActivityChatDialog.this,MessageActivity.class);
                intent1.putExtra(Constants.CLOSE_CHAT,true);
                startActivity(intent1);
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}