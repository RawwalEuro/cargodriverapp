package com.eurosoft.cargodriverapp;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.AuthPojo;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.ResponseJobNotification;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.fxn.stash.Stash;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//import com.paypal.paypalretailsdk.AppInfo;
//import com.paypal.paypalretailsdk.Invoice;
//import com.paypal.paypalretailsdk.RetailSDK;
//import com.paypal.paypalretailsdk.RetailSDKException;
//import com.paypal.paypalretailsdk.TransactionContext;

public class Authorize extends AppCompatActivity {
    public static final String KEY_AUTHORIZED = "keyAuthorizedApp";

    EditText editText1;
    String response;
    RelativeLayout save;





    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authorize);
        editText1 = (EditText) findViewById(R.id.editText1);

        save = (RelativeLayout) findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                authorize();
            }
        });
    }

    public static void setNighmode(int nighmode) {
        AppCompatDelegate.setDefaultNightMode(nighmode);
    }

    String deviceid, deviceinfo;
    String code;

    public void Error(String response) {
//        AlertDialogModel alertDialogModel = new AlertDialogModel(true, true, false, false,
//                response.split(">>")[0], "", "", "", authorizeLangModel.getOK(), "", getResources().getDrawable(R.drawable.error)
//        );
//        AlertBottomSheet.getInstance(this, alertDialogModel, new CustomSheetListner() {
//            @Override
//            public void doneClick(BottomSheetDialog dialog, String text) {
//                dialog.dismiss();
//            }
//
//            @Override
//            public void cancelClick(BottomSheetDialog dialog) {
//
//            }
//        }).show();
        Toast.makeText(this, response.split(">>")[0],Toast.LENGTH_LONG).show();

    }

    private void callApiForLanguage() {
        MasterPojo masterPojo = new MasterPojo("en");
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<MasterPojo>> call = apiService.getSelectedLanguage(masterPojo);

        call.enqueue(new Callback<WebResponse<MasterPojo>>() {
            @Override
            public void onResponse(Call<WebResponse<MasterPojo>> call, Response<WebResponse<MasterPojo>> response) {
                save.setEnabled(true);
                findViewById(R.id.savebtn).setVisibility(View.VISIBLE);
                findViewById(R.id.searchProgress).setVisibility(View.GONE);
                if (response.code() != 200 || response.body() == null) {
                    Error("Check your internet connection and try again");
                    return;
                }
                if (!response.body().getSuccess() && response.body().getData() != null) {
                    Toast.makeText(Authorize.this,"Successfully Authorized",Toast.LENGTH_SHORT).show();
                    Stash.put(Constants.MASTER_POJO, response.body().getData());
                    Intent intent = new Intent(Authorize.this,ActivityLogin.class);
                    startActivity(intent);
                    finish();

                }
            }

            @Override
            public void onFailure(Call<WebResponse<MasterPojo>> call, Throwable t) {
                save.setEnabled(true);
                findViewById(R.id.savebtn).setVisibility(View.VISIBLE);
                findViewById(R.id.searchProgress).setVisibility(View.GONE);
                Error("Check your internet connection and try again");
            }
        });
    }
    public void authorize() {
        try {
            save.setEnabled(false);
            findViewById(R.id.savebtn).setVisibility(View.GONE);
            findViewById(R.id.searchProgress).setVisibility(View.VISIBLE);
            code = editText1.getText().toString();
            deviceid = Secure.getString(getApplicationContext()
                    .getContentResolver(), Secure.ANDROID_ID);
            deviceinfo = "Android - " + android.os.Build.VERSION.RELEASE;
            String stringToSend = "accountCode=" + code + "&deviceInfo=" + deviceinfo + "&deviceId=" + deviceid + "&hashKey=" + code + deviceinfo + deviceid + "4321orue";
            ApiInterface apiService = APIClient.getAuthClient().create(ApiInterface.class);
            Call<AuthPojo> call = apiService.getAuthDetails(code,deviceinfo,deviceid,code + deviceinfo + deviceid + "4321orue");
            call.enqueue(new Callback<AuthPojo>() {
                @Override
                public void onResponse(Call<AuthPojo> call, Response<AuthPojo> response) {

                    if(response.code() != 200){
                        Toast.makeText(Authorize.this,"Check your internet connection and try again",Toast.LENGTH_SHORT).show();
                        save.setEnabled(true);
                        findViewById(R.id.savebtn).setVisibility(View.VISIBLE);
                        findViewById(R.id.searchProgress).setVisibility(View.GONE);
                        return;
                    }

                    handleResponse(response.body());
                }

                @Override
                public void onFailure(Call<AuthPojo> call, Throwable t) {
                    Toast.makeText(Authorize.this,t.getMessage(),Toast.LENGTH_SHORT).show();
                    save.setEnabled(true);
                    findViewById(R.id.savebtn).setVisibility(View.VISIBLE);
                    findViewById(R.id.searchProgress).setVisibility(View.GONE);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            save.setEnabled(true);
            findViewById(R.id.savebtn).setVisibility(View.VISIBLE);
            findViewById(R.id.searchProgress).setVisibility(View.GONE);

        }

    }

    private void handleResponse(AuthPojo response1) {



        try {
            if (response1 != null) {
                String response = response1.getCompany();
//                Verified>>democ>>http://88.198.16.71:9001>>http://88.198.16.71/Dev_DTSAPI>>DEMO CARGO
                if (response.startsWith("Verified")) {
                    // Verified>>local123>>14.192.148.19
                    // First use and company details
                    List<String> listmessang = new ArrayList<String>(
                            Arrays.asList(response.split(">>")));
                    // listmessang.set(2, "110.93.200.162");
                    try {
                        if (listmessang.size() > 3) {
                            try {
                                Stash.put(APIClient.BASE_URL_KEY,listmessang.get(3));
                                Stash.put(APIClient.SOCKET_URL_KEY,listmessang.get(2));
                                Stash.put(APIClient.COMPANY_NAME,listmessang.get(4));

                                Stash.put(Constants.IS_AUTHORIZED,true);
                                callApiForLanguage();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

//


                        } else {
                            save.setEnabled(true);
                            findViewById(R.id.savebtn).setVisibility(View.VISIBLE);
                            findViewById(R.id.searchProgress).setVisibility(View.GONE);
                            Error("Check your internet connection and try again");
                        }
                    } catch (Exception e) {
                        save.setEnabled(true);
                        findViewById(R.id.savebtn).setVisibility(View.VISIBLE);
                        findViewById(R.id.searchProgress).setVisibility(View.GONE);
                        Error("Check your internet connection and try again");
                    }


                } else {
                    save.setEnabled(true);
                    findViewById(R.id.savebtn).setVisibility(View.VISIBLE);
                    findViewById(R.id.searchProgress).setVisibility(View.GONE);
                    Error(response);
                }
            } else {
                Toast.makeText(getApplicationContext(),
                        "Check your internet connection and try again", Toast.LENGTH_LONG)
                        .show();
            }
        } catch (Exception e) {

        }
    }


}
