package com.eurosoft.cargodriverapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Adapter.AdapterNestedJobs;
import com.eurosoft.cargodriverapp.Adapter.AdapterPlannedJobs;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.JobNotificationStatus;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobObject;
import com.eurosoft.cargodriverapp.Pojo.PlannedJobs;
import com.eurosoft.cargodriverapp.Pojo.ResponseJobNotification;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseList;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//ACtivityPlannedJobs
public class ActivityNestedJobs extends AppCompatActivity {

    private RecyclerView rvNestedJobs;
    private ImageView bckBtn;
    private AdapterNestedJobs adapterNestedJobs;
    private MasterPojo masterPojo;
    private PlannedJobs plannedJob;
    private LoginResponse driverObj;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout mainRl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nested_jobs);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        driverObj = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
        initViews();
        getBundle();

    }

    private void getBundle() {
        try {
            plannedJob = (PlannedJobs) getIntent().getExtras().getSerializable(Constants.PLANNED_JOBS);
            if (plannedJob == null) {
                finish();
                return;
            } else {
                callApiPlannedJobs(driverObj.getId(), false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if (plannedJob == null) {
            Log.e("Returned", "Returned");
            return;
        } else {
            Log.e("API Called", "API Called");
            try {
                callApiPlannedJobs(driverObj.getId(), false);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private void callApiPlannedJobs(Integer id, boolean showLoader) {
        if (!AppStatus.getInstance(getApplicationContext()).isOnline()) {
            Toast.makeText(getApplicationContext(), masterPojo.getPleasecheckyourinternet(), Toast.LENGTH_SHORT).show();
            progressVisiblityGone();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        PlannedJobs plannedJobs = new PlannedJobs(driverObj.getId() + "", plannedJob.getId() + "", driverObj.getVehicleId());

        Call<WebResponseList<PlannedJobs>> call = apiService.getPlannedJobs(plannedJobs);

        call.enqueue(new Callback<WebResponseList<PlannedJobs>>() {
            @Override
            public void onResponse(Call<WebResponseList<PlannedJobs>> call, Response<WebResponseList<PlannedJobs>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();
                    if (!showLoader) {
                        progressVisiblityGone();
                    }


                    if (response.body().getData() == null || response.body().getData().isEmpty()) {
                        setupAdapter(new ArrayList<>());
                        progressVisiblityGone();
                        return;
                    }


                    setupAdapter(response.body().getData().get(0).getJobAddrJsons());
                }
            }

            @Override
            public void onFailure(Call<WebResponseList<PlannedJobs>> call, Throwable t) {
                progressVisiblityGone();

            }
        });


    }

    private void setupAdapter(List<PlannedJobObject> jobAddrJsons) {
        adapterNestedJobs = new AdapterNestedJobs(ActivityNestedJobs.this, jobAddrJsons, new AdapterNestedJobs.interfaceCallApi() {
            @Override
            public void interfaceCall(int jobId) {
                progressVisiblityVisible();
                callApiSendStatus(jobId);
            }
        }, new AdapterNestedJobs.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position, PlannedJobObject plannedJobObject) {
                switch (view.getId()) {

                    case R.id.start:


                        if (plannedJobObject.getJobStatusId() <= 4) {
                            Log.e("JobStatus", plannedJobObject.getJobStatusId() + "");

                            Toast.makeText(getApplicationContext(), "Please change status to On Route to Start the job", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (plannedJobObject.getJobStatusId() == 7) {
                            Log.e("JobStatus", plannedJobObject.getJobStatusId() + "");
                            Toast.makeText(getApplicationContext(), "You have completed this job", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if (jobAddrJsons.size() == 1) {
                            Intent intent = new Intent(ActivityNestedJobs.this, ActivtityPlanJobStatus.class);
                            intent.putExtra("object", plannedJobObject);
                            intent.putExtra("showSlider", true);
                            startActivity(intent);
                            finish();
                        } else {
                            Intent intent = new Intent(ActivityNestedJobs.this, ActivtityPlanJobStatus.class);
                            intent.putExtra("object", plannedJobObject);
                            intent.putExtra("showSlider", true);
                            startActivity(intent);
                        }


                        break;


                    case R.id.showDetails:

                        Intent intentI = new Intent(ActivityNestedJobs.this, ActivtityPlanJobStatus.class);
                        intentI.putExtra("object", plannedJobObject);
                        intentI.putExtra("showSlider", false);
                        startActivity(intentI);

                        break;

                }
            }
        }, masterPojo);

        rvNestedJobs.setAdapter(adapterNestedJobs);
        adapterNestedJobs.notifyDataSetChanged();
    }

    private void initViews() {
        rvNestedJobs = findViewById(R.id.rv_nested_job);
        bckBtn = findViewById(R.id.bckPress);

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        rvNestedJobs.setHasFixedSize(true);

        bckBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        // disableEnableControls(true, mainRl);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
        //   disableEnableControls(false, mainRl);
    }


    private void callApiSendStatus(int i) {
        JobNotificationStatus jobNotificationStatus = new JobNotificationStatus(i + "", driverObj.getId() + "", 5 + "", "On Route", Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID), "DriverApp,Android");
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<ResponseJobNotification>> call = apiService.sendStatus(jobNotificationStatus);

        call.enqueue(new Callback<WebResponse<ResponseJobNotification>>() {
            @Override
            public void onResponse(Call<WebResponse<ResponseJobNotification>> call, Response<WebResponse<ResponseJobNotification>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }

                if (!response.body().getSuccess() && response.code() == 200) {
                    callApiPlannedJobs(driverObj.getId(), true);
                }
            }

            @Override
            public void onFailure(Call<WebResponse<ResponseJobNotification>> call, Throwable t) {
                progressVisiblityGone();
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong() + "", Toast.LENGTH_SHORT).show();
            }
        });
    }
}