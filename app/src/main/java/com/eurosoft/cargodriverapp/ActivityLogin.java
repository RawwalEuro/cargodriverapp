package com.eurosoft.cargodriverapp;

import static com.eurosoft.cargodriverapp.ActivityJobStatus.CAMERA_REQUEST;
import static com.eurosoft.cargodriverapp.ActivityJobStatus.MY_CAMERA_PERMISSION_CODE;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.cargodriverapp.Fragment.BottomSheetImageList;
import com.eurosoft.cargodriverapp.Fragment.BottomSheetImageLogin;
import com.eurosoft.cargodriverapp.NetworkUtils.APIClient;
import com.eurosoft.cargodriverapp.NetworkUtils.ApiInterface;
import com.eurosoft.cargodriverapp.Pojo.ChecklistQuestionPojo;
import com.eurosoft.cargodriverapp.Pojo.ImageSheetModel;
import com.eurosoft.cargodriverapp.Pojo.LoginResponse;
import com.eurosoft.cargodriverapp.Pojo.MasterPojo;
import com.eurosoft.cargodriverapp.Pojo.RequestLogin;
import com.eurosoft.cargodriverapp.Pojo.RequestLoginBody;
import com.eurosoft.cargodriverapp.Pojo.SmartChecklistPojo;
import com.eurosoft.cargodriverapp.Pojo.SubmitChecklistPojo;
import com.eurosoft.cargodriverapp.Utils.AppStatus;
import com.eurosoft.cargodriverapp.Utils.Constants;
import com.eurosoft.cargodriverapp.Utils.ImageUtil;
import com.eurosoft.cargodriverapp.Utils.InputValidatorHelper;
import com.eurosoft.cargodriverapp.Utils.LocationEnable;
import com.eurosoft.cargodriverapp.Utils.WebResponse;
import com.eurosoft.cargodriverapp.Utils.WebResponseLogin;
import com.eurosoft.cargodriverapp.Utils.WebResponseString;
import com.fxn.stash.Stash;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;
    private EditText edtEmail;
    private EditText edtPassword;

    private TextInputLayout passwordTextInput, emailTextInput;
    private InputValidatorHelper inputValidatorHelper;
    private Button btnLogin;

    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private RelativeLayout mainRl;
    private final int REQUEST_LOCATION_PERMISSION = 1;
    private MasterPojo masterPojo;
    private TextView textHeader;
    private EditText vehicleId;
    private Handler mHandler;
    private ImageView viewPassToggle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Stash.init(this);
        inputValidatorHelper = new InputValidatorHelper();
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        initViews();
        //  checkDrawOverAppPermission();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(ActivityLogin.this)) {
                checkDrawOverAppPermission();
            }
        }
        mHandler = new Handler();
        mHandler.post(
                new Runnable() {
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                        inputMethodManager.toggleSoftInputFromWindow(edtEmail.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                        edtEmail.requestFocus();
                        if (edtEmail.getText().toString() != null || !edtEmail.getText().toString().equalsIgnoreCase("")) {
                            edtEmail.setSelection(edtEmail.getText().length());
                        }
                    }
                });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
//        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
        boolean isGranted = false;
        if (requestCode == 123) {
            for (int i = 0; i < permissions.length; i++) {
//                Log.i("[Permission] " + permissions[i] + " is " + (grantResults[i] == PackageManager.PERMISSION_GRANTED ? "granted" : "denied"));

                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    isGranted = true;
                } else {
                    isGranted = false;
                }


            }

            if (isGranted) {

                validation();

            } else {

                requestPermissions(true);
            }


        }
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "camera permission granted--", Toast.LENGTH_LONG).show();
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            } else {
                ActivityCompat.requestPermissions(ActivityLogin.this, new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
            }
        }
    }


    @AfterPermissionGranted(REQUEST_LOCATION_PERMISSION)
    public void requestLocationPermission() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if (EasyPermissions.hasPermissions(this, perms)) {
            // Toast.makeText(this, "Permission already granted", Toast.LENGTH_SHORT).show();
        } else {
            EasyPermissions.requestPermissions(this, masterPojo.getPleasegrantthelocationpermission(), REQUEST_LOCATION_PERMISSION, perms);
        }
    }
    public static boolean shouldAskPermission() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M);
    }
    public void requestPermissions(boolean isReject) {
        if (shouldAskPermission()) {
            ArrayList<String> permissionsList = new ArrayList<>();

            int locationPermission = getPackageManager().checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());

            int locationPermission2 = getPackageManager().checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, getPackageName());
            if (locationPermission != PackageManager.PERMISSION_GRANTED) {

                permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION);

            }
            if (locationPermission2 != PackageManager.PERMISSION_GRANTED) {

                permissionsList.add(Manifest.permission.ACCESS_COARSE_LOCATION);

            }
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q&&android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.R) {
                int locationPermission3 = getPackageManager().checkPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION, getPackageName());
                if (locationPermission3 != PackageManager.PERMISSION_GRANTED) {

                    permissionsList.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION);

                }
            }
//			int readPermission = getPackageManager().checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, getPackageName());
//			int writePermission2 = getPackageManager().checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, getPackageName());

//			if (readPermission != PackageManager.PERMISSION_GRANTED) {
//
//				permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
//
//			}
//			if (writePermission2 != PackageManager.PERMISSION_GRANTED) {
//
//				permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
//
//			}


            if (permissionsList.size() > 0) {
                if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R){
//                    MediaPlayer mPlay2 = MediaPlayer.create(DriverLogin.this, R.raw.alert);
//                    mPlay2.start();
                    new AlertDialog.Builder(ActivityLogin.this).setTitle("Location Permission")
                            .setMessage("Need Location Permission to track your location even when the app is closed or not in use until you logged out.")
                            .setPositiveButton("Allow", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try{
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                                        intent.setData(uri);
                                        startActivity(intent);
                                    }catch (Exception e){

                                    }
                                }
                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();

                }else {
                    showLocationRequest(isReject,permissionsList);
                }
            } else {

                validation();
            }
        } else {
            validation();
        }

    }
    public  void showLocationRequest(boolean isRejected, final ArrayList<String> permissionsList){
        String text="DTS Cargo app collects location data to track your location even when the app is closed or not in use until you logged out.";
        if(isRejected){
            text="Need Location Permission to track your location even when the app is closed or not in use until you logged out.";
        }

        AlertDialog.Builder builder =  new  AlertDialog.Builder(ActivityLogin.this);
        builder.setMessage(text)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        arg0.dismiss();

                        String[] permissions = new String[permissionsList.size()];
                        permissions = permissionsList.toArray(permissions);
                        ActivityCompat.requestPermissions(ActivityLogin.this, permissions, 123);
                    }
                })

                .create()
                .show();



    }
    private void navigateQuestions(){

        int questListSize = checklistQuestionPojos.get(headerIndex).getQuestionList().size();
        int parentListSize = checklistQuestionPojos.size();
        if(questionIndex>=questListSize-1){
            if(headerIndex<parentListSize-1) {
                headerIndex++;
                questionIndex = 0;
            }else{
                questionIndex = 0;
                headerIndex = 0;
                Toast.makeText(ActivityLogin.this,"Smart Checklist successfully Submitted!",Toast.LENGTH_SHORT).show();
                toggleCheckListView(false);
                return;
            }
        }else{
            questionIndex++;
        }


        setupQuestionData(headerIndex,questionIndex);
    }
    private void requestMultiplePermissions() {
        Dexter.withActivity(this)
                .withPermissions(
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {  // check if all permissions are granted
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            startActivityForResult(cameraIntent, CAMERA_REQUEST);
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) { // check for permanent denial of any permission
                            // show alert dialog navigating to Settings
                            //openSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getApplicationContext(), "Some Error! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }
    public void AskPermission(){
        if (getPackageManager().checkPermission(Manifest.permission.CAMERA,getPackageName()) != PackageManager.PERMISSION_GRANTED) {
            // requestPermissions(new String[]{Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);

            //ActivityCompat.requestPermissions(ActivtityPlanJobStatus.this, new String[] {Manifest.permission.CAMERA}, MY_CAMERA_PERMISSION_CODE);
            requestMultiplePermissions();
        } else {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, CAMERA_REQUEST);

        }
    }
    private void gotoCamera(){
        new  AlertDialog.Builder(this).setTitle("Please Upload a picture")
                .setPositiveButton("Take a picture", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    AskPermission();
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }
    int status=1;
    private void initViews() {
        edtEmail = findViewById(R.id.edtEmail);
        edtPassword = findViewById(R.id.edtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        textHeader = findViewById(R.id.textHeader);
        viewPassToggle = findViewById(R.id.viewPassToggle);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        vehicleId = findViewById(R.id.vehicleId);
        findViewById(R.id.checkFab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = 1;
              submitChecklistData("","");


            }
        });    findViewById(R.id.crossFab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = 2;
gotoCamera();

            }
        });    findViewById(R.id.warningFab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = 3;
                gotoCamera();
            }
        });
        findViewById(R.id.back_checklist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(questionIndex>0){
                    questionIndex--;
                }else {
                    if(headerIndex>0) {
                        headerIndex--;
                        questionIndex =  checklistQuestionPojos.get(headerIndex).getQuestionList().size()-1;
                    }
                }
                findViewById(R.id.forward_checklist).setVisibility(View.VISIBLE);
                setupQuestionData(headerIndex,questionIndex);
            }
        });
        findViewById(R.id.backToLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                headerIndex = 0;
                questionIndex = 0;
               toggleCheckListView(false);
            }
        });
        findViewById(R.id.forward_checklist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
navigateQuestions();
            }
        });
        edtEmail.setHint(masterPojo.getUserName());
        edtPassword.setHint(masterPojo.getPassword());
        btnLogin.setText(masterPojo.getLogin());
        textHeader.setText(masterPojo.getDriverLogin());


        if (Stash.getString(Constants.DRIVER_ID) == null || Stash.getString(Constants.DRIVER_ID).equalsIgnoreCase("")) {
            edtEmail.setHint(masterPojo.getUserName());
        } else {
            edtEmail.setText(Stash.getString(Constants.DRIVER_ID));
            mHandler = new Handler();
            mHandler.post(
                    new Runnable() {
                        public void run() {
                            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            inputMethodManager.toggleSoftInputFromWindow(vehicleId.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                            vehicleId.requestFocus();
                        }
                    });
        }


        viewPassToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPassToggleState(view);
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validation();
            }
        });
    }


    private void validation() {
//        requestLocationPermission();

        //btnLogin.setClickable(false);
        if (Build.VERSION.SDK_INT >= 23 && Build.VERSION.SDK_INT < Build.VERSION_CODES.Q&&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            requestPermissions(false);
            Toast.makeText(getApplicationContext(), masterPojo.getPleaseAllowPermission(), Toast.LENGTH_SHORT).show();
            return;
        } else   if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q &&
                ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_BACKGROUND_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(false);
            Toast.makeText(getApplicationContext(), masterPojo.getPleaseAllowPermission(), Toast.LENGTH_SHORT).show();
            return;
        }

       /* if (!Settings.canDrawOverlays(this)) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, 0);
            return;
        }*/

        String email = edtEmail.getText().toString().replace(" ", "");
        String password = edtPassword.getText().toString().trim();
        String vehicleNo = vehicleId.getText().toString().trim();

        if (inputValidatorHelper.isNullOrEmpty(email)) {
            Toast.makeText(this, masterPojo.getEmailEmpty(), Toast.LENGTH_SHORT).show();

            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(email)) {
            Toast.makeText(this, masterPojo.getEmailEmpty(), Toast.LENGTH_SHORT).show();

            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(vehicleNo)) {
            Toast.makeText(this, masterPojo.getPleaseentervehiclenumber(), Toast.LENGTH_SHORT).show();
            return;
        }

       /* if (inputValidatorHelper.isValidEmail(email) == false) {
            Toast.makeText(this, "Email is not valid !", Toast.LENGTH_SHORT).show();
            return;
        }*/


        if (inputValidatorHelper.isNullOrEmpty(password)) {
            Toast.makeText(this, masterPojo.getPasswordEmpty(), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!AppStatus.getInstance(this).isOnline()) {
            Toast.makeText(this, masterPojo.getCheckInternet(), Toast.LENGTH_SHORT).show();
            return;
        }
        Stash.put(Constants.DRIVER_VEHICLE, vehicleNo);
        callApiLogin(email, password, vehicleNo);
        //callApiLogin("DN0347", "123", "VN1523");
        //callApiLogin("DN0347", "123", "vn0369");
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void viewPassToggleState(View view) {

        if (edtPassword.getText().toString().equalsIgnoreCase("")) {
            return;
        }

        if (edtPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
            ((ImageView) (view)).setImageResource(R.drawable.password_toggle);
            //Show Password
            edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ((ImageView) (view)).setImageResource(R.drawable.ic_pass_eye);
            //Hide Password
            edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    public static void showKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static void hideSoftKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.clearFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtSearch.getWindowToken(), 0);


    }
    private void toggleCheckListView(boolean isCheckList){
        mainRl.setVisibility(isCheckList?View.GONE:View.VISIBLE);
        findViewById(R.id.checklistLyt).setVisibility(isCheckList?View.VISIBLE:View.GONE);
        if(isCheckList) {
            setupQuestionData(headerIndex, questionIndex);
        }
    }
    private void setupQuestionData(int headerIndex,int index){
        try {
            if(headerIndex<=0&&index<=0){
                findViewById(R.id.back_checklist).setVisibility(View.INVISIBLE);
            }else{
                findViewById(R.id.back_checklist).setVisibility(View.VISIBLE);
            }
            SmartChecklistPojo questionPojo = checklistQuestionPojos.get(headerIndex);
            if(checklistQuestionPojos.get(headerIndex).getQuestionList().size()>0) {
                ChecklistQuestionPojo questPojo = checklistQuestionPojos.get(headerIndex).getQuestionList().get(index);
                if(!questPojo.isSubmitted()){
                    findViewById(R.id.forward_checklist).setVisibility(View.INVISIBLE);
                }
                ((TextView) findViewById(R.id.headingTv)).setText(questionPojo.getHeading());
                ((TextView) findViewById(R.id.textTv)).setText(questPojo.getQuestion());
            }else{
                navigateQuestions();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    int questionIndex = 0;
    int headerIndex = 0;
    private void callApiLogin(String email, String password, String vehicleId) {
        progressVisiblityVisible();
        Log.e("email", email);
        Log.e("password", password);
        Log.e("vehicleNumber", vehicleId);

        RequestLoginBody requestLoginBody = new RequestLoginBody();
        requestLoginBody.setUserName(email);
        requestLoginBody.setPassword(password);
        requestLoginBody.setAppVersion("1.0.0");
        requestLoginBody.setVehicleNumber(vehicleId);
        requestLoginBody.setDeviceId(Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID));


        RequestLogin requestLogin = new RequestLogin();
        requestLogin.setRequest(requestLoginBody);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseLogin<LoginResponse>> call = apiService.loginUser(requestLogin);
        call.enqueue(new Callback<WebResponseLogin<LoginResponse>>() {
            @Override
            public void onResponse(Call<WebResponseLogin<LoginResponse>> call, Response<WebResponseLogin<LoginResponse>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess() && response.code() == 200) {
                    progressVisiblityGone();
                    if(response.body().getData().isShowChecklist()){
                        Stash.put(Constants.DRIVER_Dummy_Veh, response.body().getData().getVehicleId());
                        callApiGetCheckList();
                        return;
                    }
                    Stash.put(Constants.DRIVER_OBJ, response.body().getData());
                    Stash.put(Constants.isLoggenIn, true);
                    Stash.put(Constants.DRIVER_ID, email);

                    if (response.body().getConfirmedVehicle().getConfirmedVehicleId() == null
                            && response.body().getConfirmedVehicle().getAdditionalMsg() == null) {

                        Stash.put(Constants.CONFIRMED_VEHICLE, "");
                        Stash.put(Constants.ADDITIONAL_MSG, "");

                    } else {
                        Stash.put(Constants.CONFIRMED_VEHICLE, response.body().getConfirmedVehicle().getConfirmedVehicleId());
                        Stash.put(Constants.ADDITIONAL_MSG, response.body().getConfirmedVehicle().getAdditionalMsg());

                    }

                    LoginResponse loginResponse = (LoginResponse) Stash.getObject(Constants.DRIVER_OBJ, LoginResponse.class);
                    Stash.put(Constants.IS_LAUNCH, true);
                    if (LocationEnable.isLocationEnabled(getApplicationContext())) {
                        Stash.put(Constants.DRIVER_STATUS, 1);
                        startActivity(new Intent(ActivityLogin.this, HomeActivity.class));
                        finish();
                        return;
                    } else {
                        startActivity(new Intent(ActivityLogin.this, ActivityEnableLocation.class));
                        finish();
                        //  callApiLogin(email, password);
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponseLogin<LoginResponse>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();
                progressVisiblityGone();
            }
        });
    }

    private void callApiCheckList(SubmitChecklistPojo submitChecklistPojo) {
        progressVisiblityVisible();

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponseString<String>> call = apiService.AddCheckList(submitChecklistPojo);
        call.enqueue(new Callback<WebResponseString<String>>() {

            @Override
            public void onResponse(Call<WebResponseString<String>> call, Response<WebResponseString<String>> response) {
                progressVisiblityGone();
                if (response.code() != 200 || response.body() == null) {

                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!response.body().getSuccess() && response.code() == 200) {
                    Toast.makeText(getApplicationContext(), "Data submitted successfully", Toast.LENGTH_SHORT).show();
                    if(bottomSheetImageList!=null&&bottomSheetImageList.isVisible()){
                        bottomSheetImageList.dismiss();
                    }
                    checklistQuestionPojos.get(headerIndex).getQuestionList().get(questionIndex).setSubmitted(true);
                   navigateQuestions();

                }
            }

            @Override
            public void onFailure(Call<WebResponseString<String>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();
                progressVisiblityGone();
            }
        });
    }
    private void callApiGetCheckList() {
        progressVisiblityVisible();

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<List<SmartChecklistPojo>>> call = apiService.getCheckList();
        call.enqueue(new Callback<WebResponse<List<SmartChecklistPojo>>>() {
            @Override
            public void onResponse(Call<WebResponse<List<SmartChecklistPojo>>> call, Response<WebResponse<List<SmartChecklistPojo>>> response) {
                progressVisiblityGone();
                if (response.code() != 200 || response.body() == null) {
                    return;
                }
                if (response.body().getSuccess()) {
                    Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!response.body().getSuccess() && response.code() == 200) {
//                    Toast.makeText(getApplicationContext(), "Submitted Successfully", Toast.LENGTH_SHORT).show();
                    checklistQuestionPojos = response.body().getData();
                   toggleCheckListView(true);

                }
            }

            @Override
            public void onFailure(Call<WebResponse<List<SmartChecklistPojo>>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), masterPojo.getSomethingWentWrong(), Toast.LENGTH_SHORT).show();
                progressVisiblityGone();
            }
        });
    }
        List<SmartChecklistPojo>  checklistQuestionPojos = new ArrayList<>();
    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        //  disableEnableControls(false, mainRl);
    }


    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        //mainRl.setEnabled(true);
        // disableEnableControls(true,mainRl);
        // btnLogin.setClickable(true);

    }


    public void checkDrawOverAppPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Log.v("App", "Build Version Greater than or equal to M: " + Build.VERSION_CODES.M);
            Toast.makeText(getApplicationContext(), "We need draw over app permission to show Job in background", Toast.LENGTH_SHORT).show();
            //   Toast.makeText(LoginActivity.this, "Draw Over App Permission must Required", Toast.LENGTH_SHORT).show();
            checkDrawOverlayPermission();
        } else {
            Log.v("App", "OS Version Less than M");
            //No need for Permission as less then M OS.
        }
    }

    public void checkDrawOverlayPermission() {
        Log.v("App", "Package Name: " + getApplicationContext().getPackageName());
        // check if we already  have permission to draw over other apps
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(ActivityLogin.this)) {
                Log.e("App", "Requesting Permission" + Settings.canDrawOverlays(ActivityLogin.this));
                // if not construct intent to request permission
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getApplicationContext().getPackageName()));
                startActivityForResult(intent, REQUEST_CODE);
            } else {
                Log.e("App", "We already have permission for it.");

            }
        }
    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }
    public void submitChecklistData(String url,String comment){
    SubmitChecklistPojo submitChecklistPojo =  new SubmitChecklistPojo(checklistQuestionPojos.get(headerIndex).getId(),checklistQuestionPojos.get(headerIndex).getQuestionList().get(questionIndex).getId(),0,status,Integer.parseInt(Stash.getString(Constants.DRIVER_Dummy_Veh)),url,comment);
        callApiCheckList(submitChecklistPojo);
    }
BottomSheetImageLogin bottomSheetImageList;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            try {


                Bitmap photo = (Bitmap) data.getExtras().get("data");
                //camera.setImageBitmap(photo);
                String base64String = ImageUtil.convert(photo);
                ArrayList<String> imagesList =  new ArrayList<>();
//                if(bottomSheetImageList == null){
                    imagesList.add(base64String);
                    bottomSheetImageList = new BottomSheetImageLogin(new ImageSheetModel(ActivityLogin.this,imagesList));
                    bottomSheetImageList.show(((AppCompatActivity) ActivityLogin.this).getSupportFragmentManager(),"");
//                }else{
//                    imagesList = bottomSheetImageList.getImageList();
//                    imagesList.add(base64String);
//                    bottomSheetImageList.RefreshImageList(imagesList);
//                }
//                adapterImagePicker = new AdapterImagePicker(ActivityJobStatus.this, imagesList, new AdapterImagePicker.OnItemClickListener() {
//                    @Override
//                    public void onItemClick(View view, int position) {
//                        imagesList.remove(position);
//                        adapterImagePicker.notifyDataSetChanged();
//                    }
//                });
//                imageRecycler.setHasFixedSize(true);
//                imageRecycler.setAdapter(adapterImagePicker);

                //TODO
                //callApiSendCapterImage(jobIdToSend, base64String);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}